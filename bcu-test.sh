#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::bcu-test.sh][bcu-test.sh]]
export _BCU_SH_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")
#export PATH="${_BCU_SH_DIR}${PATH:+:}${PATH}"
unset Lines
mapfile -t Lines < <(
"${_BCU_SH_DIR}"/src/web/web-test.sh
"${_BCU_SH_DIR}"/src/ip/ip-test.sh
"${_BCU_SH_DIR}"/src/string/string-test.sh
"${_BCU_SH_DIR}"/src/array/array-test.sh
"${_BCU_SH_DIR}"/src/json/json-test.sh
"${_BCU_SH_DIR}"/src/xml/xml-test.sh
"${_BCU_SH_DIR}"/src/csv/csv-test.sh
"${_BCU_SH_DIR}"/src/misc/misc-test.sh)
check_results(){
    for line in "${Lines[@]}"; do
	[[ "$line" =~ (PASS|DISABLED)$ ]] || {
	    printf '%s\n' "bcu-test.sh FAILED - results:" \
		   "${Lines[@]}" \
		   "bcu-test.sh FAILED - end results."
	    exit 1
	}
    done
    printf '%s\n' "bcu-test.sh SUCCEDED - results:" \
    "${Lines[@]}" \
    "bcu-test.sh SUCCEDED - end results."
    exit 0
}
check_results
# bcu-test.sh ends here
