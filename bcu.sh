#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::bcu.sh][bcu.sh]]
export _BCU_SH_LOADED=YES
export _BCU_SH_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")
command -v bcu.sh &>/dev/null || export PATH="${_BCU_SH_DIR}${PATH:+:}$PATH"
# from the guile-bash-parallel submodule
export GUILE_AUTO_COMPILE=0
source "${_BCU_SH_DIR}/submodules/guile-bash-parallel/mapfork.sh" --source-only

source "${_BCU_SH_DIR}/src/ip/ip.sh" --source-only
source "${_BCU_SH_DIR}/src/xml/xml.sh" --source-only
source "${_BCU_SH_DIR}/src/array/array.sh" --source-only
source "${_BCU_SH_DIR}/src/json/json.sh" --source-only
source "${_BCU_SH_DIR}/src/misc/misc.sh" --source-only
source "${_BCU_SH_DIR}/src/web/web.sh" --source-only
source "${_BCU_SH_DIR}/src/string/string.sh" --source-only
source "${_BCU_SH_DIR}/src/csv/csv.sh" --source-only

# and from the pydaemon submodule, pypipe bcu pass initializses a unix socket for bcu in /tmp/pydaemon/
source "${_BCU_SH_DIR}/submodules/pydaemon/pydaemon.sh" --source-only \
    && export _BCU_PYDAEMON_CHANNEL="bcu.$$" \
    && pypipe "$_BCU_PYDAEMON_CHANNEL" pass
export PATH="$_BCU_SH_DIR"/submodules/pydaemon"${PATH:+:}$PATH"

if ! command -v scm &>/dev/null; then
    enable -f "$GUIX_PROFILE"/lib/bash/libguile-bash.so scm &>/dev/null || { printf '%s' "Enabling guile-bash failed: " && bcu__stack "$@" && return 1; }; fi

# Fix tab-completion
export COMP_WORDBREAKS="${COMP_WORDBREAKS/:}"
# Create configuration file direcory
mkdir -p ~/.config/bash-coding-utils
# bcu.sh ends here
