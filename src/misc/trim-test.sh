#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::trim-test-main][trim-test-main]]
org_args='()'
. bcu.sh
bcu__trim(){
    local _STACK z Options=() Input=() leading trailing newlines padding text var
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE      
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Trim whitespace etc.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    printf '%b\u0000' 'apa \n ' ' \nbepa\nbepa\n' | bcu__trim -l -t -n -z
    printf '%s\n' 'apa  ' '  bepa' | bcu__trim -l -t -p

Results:

    apa 
     
    bepa
    bepa
    apa  
      bepa

EOF
		)
    }
    local null=(z null "" "Read null-separated operands from stdin. Print results null-separated as well" 0)
    local Leading=(l leading "" "Preserve leading whitespace" 0)
    local Trailing=(t trailing "" "Preserve trailing whitespace" 0)
    local Newlines=(n newlines "" "Don't replace newlines with a blankspace char" 0)
    local Padding=(p padding "" "Add a newline as padding. Default if given more than one input operand unless the null option is used. Can't be used with the null option" 0)
    Options+=("(${null[*]@Q})" "(${Leading[*]@Q})" "(${Trailing[*]@Q})" "(${Newlines[*]@Q})" "(${Padding[*]@Q})")
    # shellcheck disable=SC2034      
    local -a Operands=(0:t:text)
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    #declare -p Leading Trailing Newlines leading trailing newlines padding && return 0
    [[ "${#text[@]}" -gt 1 ]] && padding=1
    for var in "${text[@]}"; do
	# remove leading whitespace characters
	[[ -n "$leading" ]] || var="${var#"${var%%[![:space:]]*}"}"
	# remove trailing whitespace characters
	[[ -n "$trailing" ]] || var="${var%"${var##*[![:space:]]}"}"
	[[ -n "$newlines" ]] || var="${var//$'\n'/ }"
	if [[ "$padding" == 1 ]]; then
	    if [[ -n "$null" ]]; then
		printf '%s\u0000' "${var}"
	    else
		printf '%s\n' "${var}";fi
	else
	    if [[ -n "$null" ]]; then
		printf '%s\u0000' "${var}"
	    else
		printf '%s' "${var}"; fi; fi; done
}
trim_test_f(){
    local -a a b
    mapfile -t -d '' a < <(printf '%b\u0000' 'apa \n ' ' \nbepa\nbepa\n' | bcu__trim -z)
    mapfile -t b < <(printf '%s\n' 'apa  ' '  bepa' | bcu__trim -n -p)
    declare -p a b
}
trim_test_1()(
    . <(trim_test_f)
    [[ "${a[0]}" == apa ]] || { echo FAIL && return 1; }
    [[ "${a[1]}" == "bepa bepa" ]] || { echo FAIL && return 1; }
    [[ "${b[0]}" == apa ]] || { echo FAIL && return 1; }
    [[ "${b[1]}" == bepa ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
trim_test_main(){
    printf trim_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )trim_test_1(\ |$) ]]; then trim_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	trim_test_main "${org_args[@]}"
    else
	trim_test_main "${@}"
    fi
fi
# trim-test-main ends here
