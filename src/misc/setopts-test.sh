#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::setopts-test-main][setopts-test-main]]
org_args='()'
bcu__setopts(){
    # shellcheck disable=SC2154
    #[[ -n "${Options[*]}" ]] || return 1

    # If we are invoked but not from a function, and are given the -h or
    # --help option flag, we consider the intent to be to get help on
    # using setopts itself.
    if { [[ "$1" == -h ]] || [[ "$1" == --help ]] ; } && [[ -z "$(caller 0)" ]]; then
	local _STACK z Options=() Input=() value_string
	[[ "$1" =~ ^(-h|--help)$ ]] && {
	    local _DESCRIPTION _EXAMPLE	
	    # shellcheck disable=SC2034
	    _DESCRIPTION=$(cat <<'EOF'
A more complete alternative to getopts. See the BCU manual and example
for usage info. setopts consumes the setopts_* namespace for functions
and variables and reads the Options and Operands array variables, so
be aware when using it.


EOF
			)			       
	    # shellcheck disable=SC2034	
	    _EXAMPLE=$(	
cat <<'EOF'
Example:

    . bcu.sh
    printMessages(){
        local debug message msg
        debug=(d debug 2 "Set the debug level" 1 f)
        local Operands=(0:t:message)
        local Options=("(${debug[*]@Q})") && bcu__setopts "$@"
        if [[ "$debug" -ge 2 ]]; then set -x ; else set +x; fi
        for msg in "${message[@]}"; do printf 'A message! %s\n' "$msg"; done
        echo Messages was printed with a debug level of $debug
    }
    printMessages -d 1 -- "Whazzaa" "Wazzzu" "Uzzaw" 2>&1
    printMessages --debug 2 -- "Whazzaa" "Wazzzu" "Uzzaw" 2>&1
    set +x

Results:

    A message! Whazzaa
    A message! Wazzzu
    A message! Uzzaw
    Messages was printed with a debug level of 1
    + for msg in "${message[@]}"
    s\n' Whazzaa
    A message! Whazzaa
    {message[@]}"
    s\n' Wazzzu
    A message! Wazzzu
    {message[@]}"
    s\n' Uzzaw
    A message! Uzzaw
    + echo Messages was printed with a debug level of 2
    Messages was printed with a debug level of 2
    + set +x
    

EOF
		    )
	}    
	Options=()
	# This code is mainly just a copy of the -h --help code from below. Sorry :/ TODO: fix!
	# print mandatory options in Usage: line.
	#printf 'Usage: %s [OPTION] [-- [OPERAND]]\n\n' "$(caller 0 | cut -f 2 -d ' ' --output-delimiter='')"
	# We test greater than 0 instead of gt 1 since we always have the help option.
	local setopts_options_string=$(if [[ ${#Options[@]} -gt 0 ]]; then printf '%s' '[OPTIONS...] '; elif [[ ${#Options[@]} -eq 1 ]]; then printf '%s' '[OPTION] '; fi )
	#printf "Usage: %s ${setopts_options_string}${setopts_operands_usage[*]}\n\n" "$(caller 0 | cut -f 2 -d ' ' --output-delimiter='')"

	printf "Usage: bcu__setopts ${setopts_options_string:-[OPTION] }%s\n\n" '"$@"'
	if [[ -v _DESCRIPTION ]]; then
	    # remove leading whitespace characters		  
	    _DESCRIPTION="${_DESCRIPTION#"${_DESCRIPTION%%[![:space:]]*}"}"
	    # remove trailing whitespace characters
	    _DESCRIPTION="${_DESCRIPTION%"${_DESCRIPTION##*[![:space:]]}"}"
	    printf '%s:\n%s\n\n' Description "$_DESCRIPTION"; fi
	printf '%s:\n\t%s | %s\t%s\n' "Options" "-h" "--help" "Display this help and exit"
	local setopts_opt
	for setopts_opt in "${Options[@]}"; do
	    local -a setopts_Option="${setopts_opt}"
	    local setopts_mandatory
	    setopts_mandatory=$(
		a=$(if [[ -n "${setopts_Option[5]}" ]]; then
			printf '%s' "${setopts_Option[5]}"; else
			printf '%s' f; fi)
		if [[ "$a" == t ]]; then
		    printf '%s' Required;
		else printf '%s' Optional; fi)
	    local setopts_args_num
	    setopts_args_num=$(a="${setopts_Option[4]}"; printf '%s' "${a:-1}")
	    # commented line is with displaying default value.
	    #printf '\t%s | %s\t[%s] [Args:%s] [Default:%s]\t%s\n' "-${setopts_Option[0]}" "--${setopts_Option[1]}" "$setopts_mandatory" "$setopts_args_num" "${setopts_Option[2]}" "${setopts_Option[3]}"; done
	    printf '\t%s | %s\t[%s] [Args:%s] %s\n' "-${setopts_Option[0]}" "--${setopts_Option[1]}" "$setopts_mandatory" "$setopts_args_num" "${setopts_Option[3]}"; done
	printf '%s\n' ""
	# Add examples to output if <function>_example is defined
	#local setopts_f_example
	#setopts_f_example=$(printf '%s_example' "$(caller 0 | cut -f 2 -d ' ' --output-delimiter='')")
	#if command -v "${setopts_f_example}" >/dev/null; then
	if [[ -v _EXAMPLE ]]; then
	    #printf '%s\n' "" && eval "${setopts_f_example}"
	    # remove leading whitespace characters		  
	    _EXAMPLE="${_EXAMPLE#"${_EXAMPLE%%[![:space:]]*}"}"
	    # remove trailing whitespace characters
	    _EXAMPLE="${_EXAMPLE%"${_EXAMPLE##*[![:space:]]}"}"
	    printf '%s\n' "$_EXAMPLE"
	    #trim -l -t -p -- "$_EXAMPLE" # && eval "${setopts_f_example}"
	    return 0
	else
	    return 0; fi; fi


    if [[ "$1" == -h ]] || [[ "$1" == --help ]]; then
	# Use <function>_usage if defined instead of the default auto-generated one.
	local setopts_f_usage;
	setopts_f_usage=$(printf '%s_usage' "$(caller 0 | cut -f 2 -d ' ' --output-delimiter='')")
	if command -v "${setopts_f_usage}" >/dev/null; then
	    eval "${setopts_f_usage}"
	    return 0
	else
	    #local setopts_operands_length="${#Operands[@]}"
	    local -a setopts_operands_usage=()
	    local -i setopts_i=0
	    for ((setopts_i=0; setopts_i<"${#Operands[@]}" ; setopts_i++ )); do
		setopts_op="${Operands[${setopts_i}]}"
		setopts_op_num="${setopts_op%%:*}"
		setopts_op_stat_and_var="${setopts_op##$setopts_op_num:}"
		setopts_op_stat="${setopts_op_stat_and_var%%:*}"
		setopts_op_var="${setopts_op_stat_and_var#$setopts_op_stat:}"
		if [[ "$setopts_op_stat" == t ]]; then
		    if [[ "$setopts_op_num" -eq 0 ]]; then
			setopts_operands_usage+=("<${setopts_op_var}> [<${setopts_op_var}>]...")
		    elif [[ "$setopts_op_num" -eq 1 ]]; then
			setopts_operands_usage+=("<${setopts_op_var}>")
		    elif [[ "$setopts_op_num" -gt 1 ]]; then
			for ((setopts_ii=0;$setopts_ii<$setopts_op_num;setopts_ii++)); do
			    setopts_operands_usage+=("<${setopts_op_var}-$(( setopts_ii + 1 ))>"); done; fi
		elif [[ "$setopts_op_stat" == f ]]; then
		    if [[ "$setopts_op_num" -eq 0 ]]; then
			setopts_operands_usage+=("[<${setopts_op_var}>]...")
		    elif [[ "$setopts_op_num" -eq 1 ]]; then
			setopts_operands_usage+=("[<${setopts_op_var}>]")
		    elif [[ "$setopts_op_num" -gt 1 ]]; then
			for ((setopts_ii=0;$setopts_ii<$setopts_op_num;setopts_ii++)); do
			    setopts_operands_usage+=("[<${setopts_op_var}-$(( setopts_ii + 1))>]");done; fi; fi; done

	    # print mandatory options in Usage: line.
	    #printf 'Usage: %s [OPTION] [-- [OPERAND]]\n\n' "$(caller 0 | cut -f 2 -d ' ' --output-delimiter='')"
	    # We test greater than 0 instead of gt 1 since we always have the help option.
	    local setopts_options_string=$(if [[ ${#Options[@]} -gt 0 ]]; then printf '%s' '[OPTIONS...] '; elif [[ ${#Options[@]} -eq 1 ]]; then printf '%s' '[OPTION] '; fi )
	    printf "Usage: %s ${setopts_options_string}${setopts_operands_usage[*]}\n\n" "$(caller 0 | cut -f 2 -d ' ' --output-delimiter='')"
	    if [[ -v _DESCRIPTION ]]; then
		# remove leading whitespace characters		  
		_DESCRIPTION="${_DESCRIPTION#"${_DESCRIPTION%%[![:space:]]*}"}"
		# remove trailing whitespace characters
		_DESCRIPTION="${_DESCRIPTION%"${_DESCRIPTION##*[![:space:]]}"}"
		printf '%s:\n%s\n\n' Description "$_DESCRIPTION"; fi
	    printf '%s:\n\t%s | %s\t%s\n' "Options" "-h" "--help" "Display this help and exit"
	    local setopts_opt
	    for setopts_opt in "${Options[@]}"; do
		local -a setopts_Option="${setopts_opt}"
		local setopts_mandatory
		setopts_mandatory=$(
		    a=$(if [[ -n "${setopts_Option[5]}" ]]; then
			    printf '%s' "${setopts_Option[5]}"; else
			    printf '%s' f; fi)
		    if [[ "$a" == t ]]; then
			printf '%s' Required;
		    else printf '%s' Optional; fi)
		local setopts_args_num
		setopts_args_num=$(a="${setopts_Option[4]}"; printf '%s' "${a:-1}")
		# commented line is with displaying default value.
		#printf '\t%s | %s\t[%s] [Args:%s] [Default:%s]\t%s\n' "-${setopts_Option[0]}" "--${setopts_Option[1]}" "$setopts_mandatory" "$setopts_args_num" "${setopts_Option[2]}" "${setopts_Option[3]}"; done
		printf '\t%s | %s\t[%s] [Args:%s] %s\n' "-${setopts_Option[0]}" "--${setopts_Option[1]}" "$setopts_mandatory" "$setopts_args_num" "${setopts_Option[3]}"; done
	    printf '%s\n' ""
	    # Add examples to output if <function>_example is defined
	    #local setopts_f_example
	    #setopts_f_example=$(printf '%s_example' "$(caller 0 | cut -f 2 -d ' ' --output-delimiter='')")
	    #if command -v "${setopts_f_example}" >/dev/null; then
	    if [[ -v _EXAMPLE ]]; then
		#printf '%s\n' "" && eval "${setopts_f_example}"
		# remove leading whitespace characters		  
		_EXAMPLE="${_EXAMPLE#"${_EXAMPLE%%[![:space:]]*}"}"
		# remove trailing whitespace characters
		_EXAMPLE="${_EXAMPLE%"${_EXAMPLE##*[![:space:]]}"}"
		printf '%s\n' "$_EXAMPLE"
		#trim -l -t -p -- "$_EXAMPLE" # && eval "${setopts_f_example}"
		return 0
	    else
		return 0; fi; fi; fi
    # end will hold end of option arguments, endless will be 1 if no '--' or options was provided.
    local -i end setopts_endless

    # p holds the number of processed args
    local -i setopts_p=0 

    # s is initially set to zero but shall be increased by 1 for each
    # option flag given as input.
    local -i setopts_s=0

    for opt in "${Options[@]}"; do
	local -a setopts_Option="${opt}"
	local setopts_short="${setopts_Option[0]}"
	local setopts_long="${setopts_Option[1]}"
	local setopts_default
	#setopts_default=$(printf '%s' "${setopts_Option[2]}")
	setopts_default="${setopts_Option[2]}"

	local description
	description="${setopts_Option[3]}"

	local -i setopts_args_num
	#setopts_args_num=$(
	#    if [[ -n "${setopts_Option[4]}" ]]; then
	#	  echo "${setopts_Option[4]}"
	#    else echo 1; fi)
	setopts_args_num="${setopts_Option[4]:-1}"

	local setopts_repeat
	# default is not repeatable
	setopts_repeat="${setopts_Option[6]:-f}" 
	#setopts_repeat="${setopts_repeat:-f}"

	local setopts_required
	# default is not required
	setopts_required="${setopts_Option[5]:-f}"
	#setopts_required="${setopts_required:-f}"

	# TODO: name of procedure to evaluate the option argument.
	local setopts_predicate
	setopts_predicate="${setopts_Option[6]}"

	# TODO: validate a,b,c,d.
	# Note: the eval here will also evaluate default values on the form '$(<somefunction or expression>)'
	if [[ ! $setopts_args_num -ge 2 ]] && [[ ! "$setopts_repeat" == t ]]; then
	    eval "unset ${setopts_Option[1]}"
	    # This is needed if for example the variable was previously declared as an int.
	    eval "unset ${setopts_long}; unset ${setopts_short}"
	    eval "${setopts_long}=\"${setopts_default}\"; ${setopts_short}=\"${setopts_default}\""
	else
	    local -n setopts_opt_array="${setopts_long}"
	    setopts_opt_array=()
	    local -a setopts_default="$setopts_default"
	    setopts_opt_array+=("${setopts_default[@]}"); fi


	local -i n=1

	# Will toggle setopts_set to 1 if an option flag is given (set) AND has been assigned a value. 
	local -i setopts_set=0

	#setopts_end=$(
	#    args=("$@")
	#    for ((ii=0; ii<=$#; ii++)); do
	#	[[ "${args[ii]}" == '--' ]] && printf '%s' "$ii"; done )

	# CHANGEHERE
	#[[ "$setopts_end" -eq 0 ]] && setopts_endless=1 && setopts_end=$#
	setopts_end=$#
	# start at the n where we left off perhaps?
	while [[ "$n" -le $setopts_end ]]; do
	    if [[ "${*:$n:1}" = "-${setopts_short}" ]] || [[ "${*:$n:1}" = "--${setopts_long}" ]]; then
		setopts_s+=1
		if [[ "$setopts_args_num" == 0 ]]; then eval "${setopts_short}=1; ${setopts_long}=1" && setopts_set=1 && continue 2; fi
		# TODO: error msg
		local setopts_args_n=$(($n+1))
		[[ -n "${*:$setopts_args_n}" ]] || return 1

		# if 1 args then create a string variable with the
		# long-option name, or add arg to array if it's a
		# repeatable option.
		if [[ $setopts_args_num -eq 1 ]]; then
		    if [[ "$setopts_repeat" == t ]]; then
			# empty array of default value if we got an option-argument
			if [[ "${setopts_opt_array[@]}" == "${setopts_default[@]}" ]]; then
			    setopts_opt_array=(); fi
			setopts_opt_array+=("${@:$setopts_args_n:$setopts_args_num}")
		    else
			eval "${setopts_long}=\"\${@:$setopts_args_n:$setopts_args_num}\""; fi
		    # "shift" the n-position the number of args we used for this option, but not counting the option-flag
		    n+=$setopts_args_num
		    setopts_set=1
		    setopts_p+=$((setopts_args_num))
		    # if 2 or more args create an array with the args
		elif [[ $setopts_args_num -ge 2 ]]; then
		    # empty array of default value if we got an option-argument
		    if [[ "${setopts_opt_array[@]}" == "${setopts_default[@]}" ]]; then
			setopts_opt_array=(); fi
		    # Add arguments starting from setopts_args_n
		    setopts_opt_array+=("${@:$setopts_args_n:$setopts_args_num}")
		    # "shift" the n-position the number of args we used for this option, but not counting the option-flag
		    n+=$setopts_args_num
		    setopts_set=1
		    setopts_p+=$((setopts_args_num))
		else
		    printf '%s\n' "Number of arguments was set to $setopts_args_num which is invalid" >&2 && return 1
		fi
	    fi
	    n+=1
	done
	if [[ "$setopts_required" == "t" ]] && [[ ! "$setopts_set" -ge 1 ]]; then
	    # TODO: error msg
	    printf '%s\n' "Required option $setopts_long is unset" >&2
	    return 1; fi
    done

    # TODO: Create a variable that holds the number of processed args
    # by counting in above while-loop.
    local -i setopts_op_start=0

    #if [[ "${@:$((p + 1)):1}" == '--' ]]; then
    #    setopts_endless=1; fi

    if [[ $setopts_endless -eq 1 ]]; then
	setopts_op_start=1
    else
	setopts_op_start=$(( setopts_end + 2)); fi

    #local op
    #for op in "${@:$setopts_op_start}"; do
    #Operands+=("$op"); done

    # CHANGEHERE
    if [[ "${@:$((setopts_p + setopts_s + 1)):1}" == -- ]] && [[ "$setopts_s" -ge 1 ]]; then
	setopts_op_start=$((setopts_p + setopts_s + 2))
    else
	setopts_op_start=$((setopts_p + setopts_s + 1)); fi

    # NEW STUFF BELOW
    [[ ! -n "$z" ]] && mapfile -t Input < <(printf '%s' "${Input[0]}")
    #declare -p Input >&2
    local -a setopts_Ops=()

    # First check whether we have specified operands for the function.
    if [[ -n "${Operands[@]}" ]]; then

	# If we have specified operands, check whether they come from
	# stdin (Input set by calling function) or via command-line
	# ($@)
	if [[ -n "${@:$setopts_op_start}" ]]; then
	    setopts_Ops=("${@:$setopts_op_start}")
	elif [[ -n "${Input[@]}" ]]; then
	    setopts_Ops=("${Input[@]}"); fi
	
	# We can't fail here because specified operands may be optional.
	# else     
	#    printf '%s' "Operands is specified as ${Operands[*]@Q} but no operands found in stdin or on the command-line: " && bcu__stack "$@" && return 1; fi

	# With given operands in Ops, and Operand-specifications in
	# Operands, parse the provided Ops.
	local -i setopts_i=0
	local setopts_op=""
	local -i setopts_op_num=0
	local setopts_op_stat_and_var=""
	local setopts_op_stat=""
	local setopts_op_var=""
	local -a setopts_OpsRange=()

	local -i setopts_ops_count=0
	local -i setopts_remaining=${#setopts_Ops[@]}

	for ((setopts_i=0; setopts_i<${#Operands[@]}; setopts_i++)); do
	    setopts_op="${Operands[$setopts_i]}"
	    setopts_op_num="${setopts_op%%:*}"
	    setopts_op_stat_and_var="${setopts_op##$setopts_op_num:}"
	    setopts_op_stat="${setopts_op_stat_and_var%%:*}"
	    setopts_op_var="${setopts_op_stat_and_var#$setopts_op_stat:}"

	    # Check that the number of operands field is a number.
	    [[ "${setopts_op_num}" =~ ^[0-9]+$ ]] || { printf '%s' "Number of operands - $setopts_op_num - is not a valid number: " && bcu__stack "$@" && return 1; }
	    # Ensure that the variable name field is a valid bash variable name
	    [[ "$setopts_op_var" =~ ^[_[:alpha:]][_[:alpha:][:digit:]]*$ ]] || { printf '%s' "$setopts_op_var is not a valid bash variable name: " && bcu__stack "$@" && return 1; }
	    #bcu__is_valid_var_name "$setopts_op_var" || { printf '%s' "$setopts_op_var is not a valid bash variable name: " && bcu__stack "$@" && return 1; }

	    # Ensure required or not required is specified correctly
	    { [[ "${setopts_op_stat}" == t ]] || [[ "${setopts_op_stat}" == f ]]; } || { printf '%s' "Invalid Operand specification - $op: " && bcu__stack "$@" && return 1; }

	    # If we have a required Option, there must be a sufficient
	    # number of remaining operands. This is checked depending
	    # on the setopts_op_num and remaning variables in below if-clauses
	    # that sets the var-names as well.

	    # number in array minus index
	    setopts_remaining=$(( ${#setopts_Ops[@]} - setopts_ops_count ))

	    # If endless number (i.e. 0) we create array
	    if [[ "${setopts_op_num}" == 0 ]]; then
		{ [[ ! "$setopts_remaining" -ge 1 ]] && [[ "$setopts_op_stat" == t ]] ;} && { printf '%s' "Missing operands. Unable to parse ${Operands[$i]}: " && bcu__stack "$@" && return 1; }
		setopts_OpsRange=("${setopts_Ops[@]:$setopts_ops_count}")
		eval "${setopts_op_var}=(${setopts_OpsRange[*]@Q})" && break
		# If just 1 we create a variable
	    elif [[ "${setopts_op_num}" == 1 ]]; then
		{ [[ ! "$setopts_remaining" -ge 1 ]] && [[ "$setopts_op_stat" == t ]] ; } && { printf '%s' "Missing operands. Unable to parse ${Operands[$i]}: " && bcu__stack "$@" && return 1; }
		eval "${setopts_op_var}=${setopts_Ops[$setopts_ops_count]@Q}"
		setopts_ops_count+=1
		#i+=1
		# Else it is some X number and we create an array and continue
	    else
	        { [[ ! "$setopts_remaining" -ge "$setopts_op_num" ]] && [[ "$setopts_op_stat" == t ]] ; } && { printf '%s' "Missing operands. Unable to parse ${Operands[$i]}: " && bcu__stack "$@" && return 1; }
		#[[ "$setopts_remaining" -ge "$setopts_op_num" ]] || { printf '%s' "Missing operands. Unable to parse ${Operands[$i]}: " && bcu__stack "$@" && return 1; }
		setopts_OpsRange=("${setopts_Ops[@]:$setopts_ops_count:$setopts_op_num}")
		eval "${setopts_op_var}=(${setopts_OpsRange[*]@Q})"
		setopts_ops_count+=$setopts_op_num; fi; done
	#i+=$((setopts_op_num - 1)); fi; done

	# If we haven't specified operands we set the Ops to be Input
	# which is from stdin (set by calling function), else to the
	# ramining command-line operands.
    else
	if [[ -n "${Input[@]}" ]]; then
	    setopts_Ops=("${Input[@]}")
	else
	    setopts_Ops=("${@:$setopts_op_start}"); fi; fi

    # Put all Ops in Operands, because either the Input will be
    # transferred there or the command-line operands.
    Operands=("${setopts_Ops[@]}")
    #declare -p Operands >&2
}
f(){
    local z; local -a Options
    local test=(t test "YES" "testrun, no commits" 1 t t)
    local debug=(d debug "1" "debug level, 1-3")
    local time=(w time '$(date)' "debug level, 1-3" 1)
    local messages=(m messages "(hulu)" "some 3 messages" 3 t)
    Options=("(${debug[*]@Q})" "(${test[*]@Q})" "(${messages[*]@Q})" "(${time[*]@Q})")
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")
    local -a Operands
    bcu__setopts "$@"
    local -a Input
    local z; local -a Options
    if [[ ! -t 0 ]]; then    	  
	if [[ -n "$z" ]]; then
	    mapfile -t -d '' Input
	else
	    mapfile -t Input; fi
    else
	Input=("${Operands[@]}"); fi
    declare -p test debug messages null Input z time Operands
}
f_example(){
    cat <<'EOF'
Example:

    f -t NO --debug 2 -m apa bepa 'cepa depa'
    f --test NOPE

Results:

    declare -a test=([0]="NO")
    declare -- debug="2"
    declare -a messages=([0]="apa" [1]="bepa" [2]="cepa depa")
    declare -- null=""
    declare -a Input=()
    declare -- z
    declare -- time="Sat 16 Oct 2021 07:39:10 PM CEST"
    declare -a Operands=()
    Required option messages is unset
    declare -a test=([0]="NOPE")
    declare -- debug="1"
    declare -a messages=([0]="hulu")
    declare -a null=([0]="z" [1]="null" [2]="" [3]="Read null-separated operands from stdin" [4]="0")
    declare -a Input=()
    declare -- z
    (date)" [3]="debug level, 1-3" [4]="1")
    declare -a Operands
    

EOF
}
g(){
    local z; local -a Options=()
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")
    local -a Operands
    local -a Input
    bcu__setopts "$@"
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    local -a Input
    if [[ ! -t 0 ]]; then    	  
	if [[ -n "$z" ]]; then
	    mapfile -t -d '' Input
	else
	    mapfile -t Input; fi
    else
	Input=("${Operands[@]}"); fi
    [[ ! -n "${Input[*]}" ]] && { bcu__stack "$@" && return 1 ; }	    
    declare -p Input Operands
}
h(){
    local _DESCRIPTION _EXAMPLE z
    local Options=()
    local z
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Newline=(n newline "" "Append a newline to the string you want to prepend" 0)
    local File=(f file "" "File to append to" 1)
    Options+=("(${null[*]@Q})" "(${Newline[*]@Q})" "(${File[*]@Q})")
    local -a Operands=(1:t:op1 8:t:MyOps)
    local -a Input
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: " && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    [[ ! -n "${Input[*]}" ]] && { bcu__stack "$@" && return 1 ; }
    declare -p Input Operands op1 MyOps
}
setopts_test_1()(
    . <(f -m apa bepa -t no)
    [[ "${test}" == no ]] || { echo FAIL && return 1 ; }
    echo PASS && return 0
)
setopts_test_2()(
    . <(f --test NOPE -m a b)
    [[ "$test" == NOPE ]] || { echo FAIL && return 1 ;}
    echo PASS && return 0
)
setopts_test_3()(
    . <(f -t NO --debug 2 -m apa bepa 'cepa depa')
    [[  "${#messages[@]}" == 3 ]] || { echo FAIL && return 1; }
    [[  "${messages[0]}" == apa ]] || { echo FAIL && return 1; }
    [[  "${messages[1]}" == bepa ]] || { echo FAIL && return 1; }
    [[  "${messages[2]}" == "cepa depa" ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
setopts_test_4()(
    . <(g apa bepa)
    [[ "${Input[0]}" == apa ]] || { echo FAIL && return 1; }
    . <(g -- apa bepa)
    { [[ "${Input[0]}" == -- ]] && [[ "${Input[1]}" == apa ]] ;} || { echo FAIL && return 1; }
    . <(g -z -- 0 -- apa bepa)
    [[ "${Input[0]}" == 0 ]] || { echo FAIL && return 1; }
    [[ "${Input[1]}" == -- ]] || { echo FAIL && return 1; }
    [[ "${Input[2]}" == apa ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
setopts_test_5()(
    . <(f -t yes -t no -m a b c -m d e f)
    [[ "${#messages[@]}" == 6 ]] || { echo FAIL && return 1; }
    [[ "${#test[@]}" == 2 ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
setopts_test_6()(
    local op1 a
    local -a MyOps 
    . <(printf '%b\n' 'apa\n bepa\n' cepa lala lolo a b c | h -n -- )
    [[ "${op1}" == apa ]] || { echo FAIL && return 1; }
    [[ "${#MyOps[@]}" == 8 ]] || { echo FAIL && return 1; }
    a=$(if printf '%b\n' 'apa\n bepa\n' cepa lala lolo a b | h -n -- >/dev/null; then echo true; else echo false; fi)
    [[ "$a" == false ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
setopts_test_main(){
    printf setopts_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )setopts_test_1(\ |$) ]]; then setopts_test_1; else echo DISABLED; fi
    printf setopts_test_2.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )setopts_test_2(\ |$) ]]; then setopts_test_2; else echo DISABLED; fi
    printf setopts_test_3.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )setopts_test_3(\ |$) ]]; then setopts_test_3; else echo DISABLED; fi
    printf setopts_test_4.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )setopts_test_4(\ |$) ]]; then setopts_test_4; else echo DISABLED; fi
    printf setopts_test_5.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )setopts_test_5(\ |$) ]]; then setopts_test_5; else echo DISABLED; fi
    printf setopts_test_6.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )setopts_test_6(\ |$) ]]; then setopts_test_6; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	setopts_test_main "${org_args[@]}"
    else
	setopts_test_main "${@}"
    fi
fi
# setopts-test-main ends here
