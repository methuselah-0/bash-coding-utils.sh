#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::sed_esc_key-test-main][sed_esc_key-test-main]]
org_args='()'
. bcu.sh
bcu__sed_esc_key(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() key_string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Escape a string that you want to pass to sed to have it replaced in
some other string/text, also referred to as the regexp string in the
sed manual.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset replaceme sed_escaped sometext
    replaceme="asdf[]][{}@%^"
    sed_escaped=$(bcu__sed_esc_key "$replaceme")
    sometext="lala${replaceme}lala"
    echo "$sometext" | sed "s/$sed_escaped/lulz/g"

Results:

    lalalulzlala

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:key_string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    #printf '%s' "${Input[0]}" | sed -e 's/\([\.\^\$\*\[\]\|]\)/\\&/g'
    
    local -a Arr=$(bcu__string_to_arr "${key_string}") || { printf '%s' "Failed to convert key_string to array: ${_STACK}" && bcu__stack "$@" && return 1; }

    for c in "${Arr[@]}"; do
	if [[ "$c" =~ (\.|\^|]|\$|\*|/|]|\[|\\) ]]; then
						     printf '\%s' "$c"
						  else printf '%s' "$c" ;fi ; done
						  }
sed_esc_key_test_f(){
    local a b
    local -a TestEscaped

    local -a Escaped=([0]="\\." [1]="\^" [2]="\\*" [3]="+" [4]="-" [5]="?" [6]="(" [7]=")" [8]="\\[" [9]="\\]" [10]="{" [11]="}" [12]="\\\\" [13]="|" [14]="\\\$")
    mapfile -t TestEscaped < <(for c in '.' '^' '$' '*' '+' '-' '?' '(' ')' '[' ']' '{' '}' '\' '|'; do bcu__sed_esc_key "$c" && echo; done | sort )
    mapfile -t Escaped < <(printf '%s\n' "${Escaped[@]}" | sort)
    a=$(if [[ "${Escaped[@]}" == "${TestEscaped[@]}" ]]; then echo true; else echo false; fi)

    junk="@@\''@echo $jnk\"\":{:}|:\"{}::{}::\":junk=\"@@\''@echo $jnk\"\":{:}|:\"{}::{}::\":@$@$%^#%$&%&^(^()*_+(+11234346534574769(&)*^()}|}|\">:?><><>,/.,.\,.][][121251$#"
    b=$(if diff -s <(a=$(bcu__sed_esc_key "$(printf '%s' "$junk")"); printf '%s' "$junk" | sed "s/$a/&/g") <(printf '%s' "$junk") &>/dev/null; then echo true; else echo false; fi)

    declare -p a b
}
sed_esc_key_test_1()(
    . <(sed_esc_key_test_f)
    [[ "${a}" == true ]] || { echo FAIL && return 1; }
    [[ "${b}" == true ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
sed_esc_key_test_main(){
    printf sed_esc_key_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )sed_esc_key_test_1(\ |$) ]]; then sed_esc_key_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	sed_esc_key_test_main "${org_args[@]}"
    else
	sed_esc_key_test_main "${@}"
    fi
fi
# sed_esc_key-test-main ends here
