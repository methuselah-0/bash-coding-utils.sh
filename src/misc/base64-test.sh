#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::base64-test-main][base64-test-main]]
org_args='()'
. bcu.sh
bcu__base64(){
    local _STACK z Options=() Input=() string s
    local -a Res=()
    [[ "$1" =~ (-h|--help) ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Encode or decode a string to or from base64 encoding


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__base64 abcd

Results:

    
    <record-1><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-1><record-2><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-2></file><file><record-1><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-1><record-2><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-2></file><file><record-1><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-1><record-2><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-2></file>YWJjZA==

EOF
		)
    }    
    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Encode=(e encode "" "Encode operands in base64. This is the default" 0)
    local Decode=(d decode "" "Decode operands in base64" 0)
    Options+=("(${Encode[*]@Q})" "(${Decode[*]@Q})" "(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || {
    printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ (-h|--help) ]] && return 0

    # Check args
    if ! command -v scm &>/dev/null; then
	enable -f "$GUIX_PROFILE"/lib/bash/libguile-bash.so scm &>/dev/null || { 
	    printf '%s' "Enabling guile-bash failed: " && bcu__stack "$@" && return 1; }; fi

    if [[ -n "$decode" ]]; then
	command -v scm-decode_from_base64 &>/dev/null \
	    || scm "${_misc_MOD_DIR}"/base64_guile_bash.scm &>/dev/null
	#    || scm "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"/base64_guile_bash.scm &>/dev/null
	for s in "${string[@]}"; do
	    Res+=("$(scm-decode_from_base64 "$s")"); done #2>/dev/null
    else
	command -v scm-encode_to_base64 &>/dev/null \
	    || scm "${_misc_MOD_DIR}"/base64_guile_bash.scm &>/dev/null
	#    || scm "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"/base64_guile_bash.scm &>/dev/null
	for s in "${string[@]}"; do
	    Res+=( "$(scm-encode_to_base64 "$s")" ); done #2>/dev/null
    fi
    if [[ -n "$null" ]]; then
	printf '%s\0' "${Res[@]}"
    else
	printf '%s\n' "${Res[@]}"; fi
}
base64_test_f(){
    local a
    a=$(bcu__base64 abcd)
    declare -p a
}
base64_test_1()(
    . <(base64_test_f)
    [[ "${a}" == 'YWJjZA==' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
base64_test_main(){
    printf base64_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )base64_test_1(\ |$) ]]; then base64_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	base64_test_main "${org_args[@]}"
    else
	base64_test_main "${@}"
    fi
fi
# base64-test-main ends here
