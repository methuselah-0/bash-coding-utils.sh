;; -*- mode: scheme; coding: utf-8 -*-
(add-to-load-path (dirname (current-filename)))
(use-modules 
 (base64)
 (rnrs bytevectors)
 (gnu bash)
 (ice-9 format)
 (system ffi)
 (srfi srfi-41))
(define-bash-function (scm-encode_to_base64 string)
  (display (base64-encode (string->utf8 string))))
(define-bash-function (scm-decode_from_base64 string)
  (display (base64-decode (string->utf8 string))))
