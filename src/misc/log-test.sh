#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::log-test-main][log-test-main]]
org_args='()'
. bcu.sh
bcu__log(){
    local _STACK z Options=() Input=()
    local facility severity version timestamp hostname application process_id message_id logfile structured_data
    local Facility Severity Version Timestamp Hostname Application Process_Id Message_Id Logfile Structured_Data
    local time host appname pid msgid


    local NILVALUE; NILVALUE="-"
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
log is an application logger for Bash adhering to RFC5424 (or attempts
to) and ensures that logs written to logfiles happen
sequentially. Sane defaults are provided as well as option flags to
override them.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__log -a myapp --msgid 45 | sed "s/$(printf '\357\273\277')/BOM/"
    bcu__log --msgid 45 -a myapp -- aa | sed "s/$(printf '\357\273\277')/BOM/"
    bcu__log -a myapp -d "$(bcu__sd_elem --id "apa@bepa" -a apa 'bepa\[]cepa"' -a test YES -a priority 154 -a user allan)" -- This is a test | sed "s/$(printf '\357\273\277')/BOM/"

Results:

    <15>1 2019-09-29T21:43:44.838283+0200 librem13v3guixsd myapp 23920 45 -
    <15>1 2019-09-29T21:43:44.886824+0200 librem13v3guixsd myapp 23920 45 - BOMaa
    <15>1 2019-09-29T21:43:45.002602+0200 librem13v3guixsd myapp 23920 - [apa@bepa apa="bepa\\[\]cepa\"" test="YES" priority="154" user="allan"] BOMThis is a test

EOF
		)
    }    
    # Options
    #myoption=(short long default "Description" argsnum required repeatable predicate)~
    Facility=(f facility '$(printf "%s" "${_BCU_LOG_DEFAULT_FACILITY:-1}")' "Facility code, a number in the range of 0-23. Set and export _LOG_DEFAULT_FACILITY to override the default value" 1)
    Severity=(s severity '$(printf "%s" "${_BCU_LOG_DEFAULT_SEVERITY:-7}")' "Facility code, a number in the range of 0-23. Set and export _LOG_DEFAULT_SEVERITY to override the default value" 1)
    Version=(v version '$(printf "%s" "${_BCU_LOG_DEFAULT_VERSION:-1}")' "What syslog version. Set and export _LOG_DEFAULT_VERSION to override the default value" 1)
    Timestamp=(t time '$(date +%Y-%m-%dT%H:%M:%S.%6N%z)' "Time that we created the log message" 1)
    Hostname=(h host '$(printf "%s" "$HOSTNAME")' "The machine we are logging for" 1)
    Application=(a appname '$(echo "${_BCU_LOG_APPNAME:-${BASH_SOURCE[0]}}")' "Name of the application we are logging for. Default is the variable set for _BCU_LOG_APPNAME and otherwise it is \${BASH_SOURCE[0]}" 1)
    Process_Id=(p pid '$(printf "%s" $$)' 'The current process id. Will use parent PID ($$) by default' 1)
    Message_Id=(m msgid '$(printf "%s" "$NILVALUE")' 'The MSGID SHOULD identify the type of message. For example, a firewall might use the MSGID "TCPIN" for incoming TCP traffic and the MSGID "TCPOUT" for outgoing TCP traffic. Messages with the same MSGID should reflect events of the same semantics. The MSGID itself is a string without further semantics. It is intended for filtering messages on a relay or collector.' 1)
    Logfile=(l logfile '$(echo "${_BCU_LOG_LOGFILE}")' "Save log message to file instead of printing it to stdout" 1)
    Structured_Data=(d structured_data "" "Structured data, preferably created with the sdelem function" 1 f t)

    Options=("(${Facility[*]@Q})" "(${Severity[*]@Q})" "(${Version[*]@Q})" "(${Timestamp[*]@Q})" "(${Hostname[*]@Q})" "(${Application[*]@Q})" "(${Process_Id[*]@Q})" "(${Message_Id[*]@Q})" "(${Logfile[*]@Q})" "(${Structured_Data[*]@Q})")

    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:f:msg)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || {
	printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    # Check args
    Input=("${msg[@]}")
    # local -i i
    # for ((i=1;i<=$#;i++)); do
    #     eval "narg=\"\${$i}\""
    #     if [[ "${narg}" == '--' ]]; then
    # 	    i+=1 && Input=("${@:$i}"); fi; done; fi

    local priority
    priority=$(( facility * 8 + severity ))

    local log_string
    read -r log_string < <(
	printf "<%s>%s %s %s %s %s %s " "$priority" "$version" "$time" "$host" "$appname" "$pid" "$msgid"
	if [[ -n "${structured_data[*]}" ]]; then
	    local data
	    for data in "${structured_data[@]}"; do
		printf '%s' "${data} "; done
	else
	    printf '%s' "- "; fi
    )

    # atomic writes since we might be calling log() in parallell
    [[ -n "$logfile" ]] && {
	(
	    flock 200
	    #printf '%s' "${log_string} BOM" $'\xEF\xBB\xBF' "${Input[*]}" $'\n' >> "$logfile"
	    if [[ -n "${Input[*]}" ]]; then
		printf '%s' "${log_string} " $'\xEF\xBB\xBF' "${msg[*]}" $'\n' >> "$logfile"
	    else
		printf '%s' "${log_string}" $'\n' >> "$logfile"; fi
	    # Following seds will find the BOM.
	    # printf '%s' apa $'\xEF\xBB\xBF' bepa | sed "s/$(printf '\357\273\277')/BOM/"
	    # sed "s/$(printf '\357\273\277')/BOMMATCH/" somefile 

	    #printf '%s\xEF\xBB\xBF%s' "" "${Input[*]}" $'\n'
	    #printf '%s\xef\xbb\xbf%s' "" "${Input[*]}" $'\n'

	) 200>/tmp/bcu__log_lock #200>~/.config/bash-coding-utils/bcu__log_lock
	return 0
    }

    if [[ -n "${Input[*]}" ]]; then
	printf '%s' "${log_string} " $'\xEF\xBB\xBF' "${msg[*]}" $'\n'
    else
	printf '%s' "${log_string}" $'\n'; fi

    #printf '%s'  >> "${logfile}"
    #printf '%s' "${log_string} BOM" "${Input[*]}" $'\n'
    #printf '%s' "${log_string}" $'\xEF\xBB\xBF' "${Input[*]}" $'\n'
    #printf '%s\xEF\xBB\xBF%s' "" "${Input[*]}" $'\n'
    #printf '%s\xef\xbb\xbf%s' "" "${Input[*]}" $'\n'

    #### OLD ###    
    # local msg
    # if ! [[ "$1" =~ (INFO|WARNING|ERROR) ]]; then
    #     printf '%s' "[ERROR]: " "log(): args: " "$@" $'\n'| tee -a "$logf"
    #     return 1; fi
    # if [[ -n "$2" ]]; then
    #     msg="[${1}]: ${2}\n"
    # else
    #     while read -r msgline; do
    # 	  msg+="[${1}]: ${msgline}\n"; done; fi
    # if [[ -n "$msg" ]]; then
    #     case "$1" in
    # 	  INFO|WARNING) printf '%b' "$msg" >> "$logf" ;;
    # 	      ERROR) printf '%b' "$msg" | tee -a "$logf" ;; esac ; fi
}
log_test_f(){
    local a b c 
    a=$(bcu__log -a myapp --msgid 45 -h localhost --pid 896 -t '2019-08-31T20:13:27.612745+0200' | sed "s/$(printf '\357\273\277')/BOM/")
    b=$(bcu__log --msgid 45 -a myapp -h localhost --pid 896 -t '2019-08-31T20:13:27.723119+0200' -- aa | sed "s/$(printf '\357\273\277')/BOM/")
    c=$(bcu__log -a myapp --pid 896 -h localhost -d "$(bcu__sd_elem --id "apa@bepa" -a apa 'bepa\[]cepa"' -a test YES -a priority 154 -a user allan)" -t '2019-08-31T20:13:27.927659+0200' -- This is a test | sed "s/$(printf '\357\273\277')/BOM/")
    declare -p a b c
}
log_test_1()(
    . <(log_test_f)
    [[ "${a}" == '<15>1 2019-08-31T20:13:27.612745+0200 localhost myapp 896 45 -' ]] || { echo FAIL && return 1; }
    [[ "${b}" == '<15>1 2019-08-31T20:13:27.723119+0200 localhost myapp 896 45 - BOMaa' ]] || { echo FAIL && return 1; }
    [[ "${c}" == '<15>1 2019-08-31T20:13:27.927659+0200 localhost myapp 896 - [apa@bepa apa="bepa\\[\]cepa\"" test="YES" priority="154" user="allan"] BOMThis is a test' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
log_test_main(){
    printf log_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )log_test_1(\ |$) ]]; then log_test_1; else echo DISABLED; fi
} 
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	log_test_main "${org_args[@]}"
    else
	log_test_main "${@}"
    fi
fi
# log-test-main ends here
