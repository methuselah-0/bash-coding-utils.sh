#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::sedp-test-main][sedp-test-main]]
org_args='()'
. bcu.sh
bcu__sedp(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
sedp is like sed but adds support for pcre-patterns by piping the
given input to perl. If the null option is provided, both input and
output will be null-separated.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    mapfile -t -d '' Arr < <(printf '%b\u0000' "apa\nbepa" bepa cepa | bcu__sedp -z -s 's/p/g/g')
    declare -p Arr
    echo apa | bcu__sedp -s 's/(?<=a)p(?=a)/s/g'

Results:

    declare -a Arr=([0]=$'aga\nbega' [1]="bega" [2]="cega")
    asa

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Substitution=(s substitution "" "The substitution string on the form 's/regex/replacement/g' to pass to perl" 1 t f)
    Options+=("(${null[*]@Q})" "(${Substitution[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    if [[ -n "$null" ]]; then
	printf '%s\u0000' "${string[@]}" | perl -0 -pe "$substitution"
    else
	printf '%s\n' "${string[0]}" | perl -pe  "$substitution"; fi
}
sedp_test_f(){
    local a
    a=$(if [[ "$(echo apa | bcu__sedp -s 's/(?<=a)p(?=a)/s/g')" == asa ]]; then echo true; else echo false; fi)
    declare -p a
}
sedp_test_1()(
    . <(sedp_test_f)
    [[ "${a}" == true ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
sedp_test_main(){
    printf sedp_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )sedp_test_1(\ |$) ]]; then sedp_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	sedp_test_main "${org_args[@]}"
    else
	sedp_test_main "${@}"
    fi
fi
# sedp-test-main ends here
