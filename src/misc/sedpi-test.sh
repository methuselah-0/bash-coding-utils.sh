#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::sedpi-test-main][sedpi-test-main]]
org_args='()'
. bcu.sh
bcu__sedpi(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() substitution file s f result
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
sedpi modifies a given file with a given pcre regexp


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset f
    f=$(mktemp /tmp/sedpi-test.XXXXXX)
    echo apa > "$f"
    bcu__sedpi -s 's/(?<=a)p(?=a)/s/g' -- "$f" 
    cat "$f"

Results:

    asa

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Substitution=(s substitution "" "Substitution command on the for 's/<regex>/<replacement>/g'" 1 t t)
    Options+=("(${null[*]@Q})" "(${Substitution[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:file)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    for s in "${substitution[@]}"; do
	for f in "${file[@]}"; do
	    result=$(perl -p -i -e  "$substitution" "$file" 2>&1) || { printf '%s' "Failed to substitute - $substitution - in file - $file - with result - $result - : ${_STACK}" && bcu__stack "$@" && return 1; }; done; done
}
sedpi_test_f(){
    local a f
    f=$(mktemp /tmp/sedpi-test.XXXXXX)
    echo apa > "$f"
    bcu__sedpi -s 's/(?<=a)p(?=a)/s/g' -- "$f" 
    a=$(if [[ "$(cat "$f")"  == asa ]]; then echo true; else echo false; fi)
    declare -p a
}
sedpi_test_1()(
    . <(sedpi_test_f)
    [[ "${a}" == true ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
sedpi_test_main(){
    printf sedpi_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )sedpi_test_1(\ |$) ]]; then sedpi_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	sedpi_test_main "${org_args[@]}"
    else
	sedpi_test_main "${@}"
    fi
fi
# sedpi-test-main ends here
