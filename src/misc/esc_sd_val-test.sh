#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::esc_sd_val-test-main][esc_sd_val-test-main]]
org_args='()'
. bcu.sh
bcu__esc_sd_val(){
    local _STACK z Options=() Input=() value_string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(
escape SD-value


		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__esc_sd_val 'apa]bepa"cepa\depa' && echo

Results:

    apa\]bepa\"cepa\\depa

EOF
		)
    }    
    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)    
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:value_string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || {
        printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    # Check args
    printf '%s' "$value_string" | sed 's/[]"\]/\\&/g'
}
esc_sd_val_test_f(){
    local a
    a=$(bcu__esc_sd_val 'apa]bepa"cepa\depa')
    declare -p a
}
esc_sd_val_test_1()(
    . <(esc_sd_val_test_f)
    [[ "${a}" == 'apa\]bepa\"cepa\\depa' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
esc_sd_val_test_main(){
    printf esc_sd_val_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )esc_sd_val_test_1(\ |$) ]]; then esc_sd_val_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	esc_sd_val_test_main "${org_args[@]}"
    else
	esc_sd_val_test_main "${@}"
    fi
fi
# esc_sd_val-test-main ends here
