#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::parallel-test-main][parallel-test-main]]
org_args='()'
. bcu.sh
bcu__parallel(){
bashp(){
    if [[ "$1" == --help ]] || [[ "$1" == -h ]]; then
	cat <<'EOF'
Usage: bashp [OPTIONS...] [<command>] [<argsmap>] [<args>]

Description:
Bash parallel can be used to parallelize bash functions or other
commands. It returns results as a bash array or json document
containing; each executed command and command arguments, stdout, stderr
and the return-code. It can be used in a nested way, see example.

Options:
	-h | --help		Display this help and exit
	-n | --size		[Optional] [Args:1] Number of jobs in parallel. Defaults to number of cpu's.
	-j | --json-args	[Optional] [Args:1] Read <command>, <argsmap>, and <args> from a json document, see example below.
	-k | --json-argsfile	[Optional] [Args:1] Same as --json-args but read it from a file.
	-p | --json-out		[Optional] [Args:1] Print results as a json document.
	-f | --argsfile		[Optional] [Args:1] Read the <args> bash array from a file or filedescriptor.

Example:

    alo(){ sleep 2 ; printf '%s\n' "$@" ; }
    Args=(apa bepa cepa '!@#$!$@%@$&$%*^$&(&#*@%$^!#$~')
    ArgsMap=(one {} {})
    declare -a Results=$(bashp --size 2 -- alo "(${ArgsMap[*]@Q})" "(${Args[*]@Q})")
    declare -a res1="${Results[1]}"
    declare -a res1_CommandAndArgs="${res1[0]}"
    res1_Command="${res1_CommandAndArgs[0]}"
    declare -a res1_CommandArgs="${res1_CommandAndArgs[1]}"
    res1_stdout="${res1[1]}"
    res1_stderr="${res1[2]}"
    res1_ret_code="${res1[3]}"
    alo2(){ bashp --json-args '{"command":"alo","args":'"$(jq -n '$ARGS.positional' --args "$@")"',"args-map":["{}","{}"]}' --json-out --size 2 ; }
    declare -p res1_Command res1_CommandArgs res1_stdout res1_stderr res1_ret_code && bashp --json-args '{"command":"alo2","args":["apa","bepa","cepa","depa"],"args-map":["staticarg","{}"]}' --size 2 --json-out | jq -r '[.[0,1].stdout|fromjson[]]'

Results:

    declare -- res1_Command="alo"
    declare -a res1_CommandArgs=([0]="one" [1]="cepa" [2]="!@#\$!\$@%@\$&\$%*^\$&(&#*@%\$^!#\$~")
    declare -- res1_stdout="one
    cepa
    !@#\$!\$@%@\$&\$%*^\$&(&#*@%\$^!#\$~
    "
    declare -- res1_stderr=""
    declare -- res1_ret_code="0"
    [
      {
        "command": "'alo' 'staticarg' 'apa'",
        "stdout": "staticarg\napa\n",
        "stderr": "",
        "return-code": "0"
      },
      {
        "command": "'alo' 'staticarg' 'bepa'",
        "stdout": "staticarg\nbepa\n",
        "stderr": "",
        "return-code": "0"
      }
    ]
EOF
	return 0
    fi
    local serial jsonargs json size argsfile
    while [[ -n "${1}" ]]; do
	if [[ "$1" == -o ]] || [[ "$1" == --serial ]]; then
	    serial="$1"
	    shift
	elif [[ "$1" == -j ]] || [[ "$1" == --json-args ]]; then
	    jsonargs="$2"
	    shift 2
	elif [[ "$1" == -k ]] || [[ "$1" == --json-argsfile ]]; then
	    jsonargs=$(jq -c . "$2") || { printf '%s\n' "Failed to to use $2 as json-argsfile" && return 1 ; }
	    shift 2
	elif [[ "$1" == -f ]] || [[ "$1" == --argsfile ]]; then
	    argsfile=$(cat "$2") || { printf '%s\n' "Failed to to use $2 as argsfile" && return 1 ; }
	    shift 2
	elif [[ "$1" == -p ]] || [[ "$1" == --json-out ]]; then
	    json="$1"
	    shift
	elif [[ "$1" == -n ]] || [[ "$1" == --size ]]; then
	    local -i size="$2" || return 1
	    shift 2
	elif [[ "$1" == -- ]]; then
	    shift && break
	else
	    break
	fi
    done

    if [[ -n "$argsfile" ]]; then
	local -a Args=$(printf '%s' "$argsfile") || { printf '%s\n' "Failed to to use $2 as argsfile" && return 1 ; }
	local command="$1"
	local -a CommandArgs="$2" || { printf '%s\n' "Failed to to use $2 as arguments map. It's not a valid bash list." && return 1 ; }
    elif [[ -n "$jsonargs" ]]; then
	bashp_jsonStripQuotes(){ local t0; while read -r t0; do t0="${t0%\"}"; t0="${t0#\"}"; printf '%s\n' "$t0"; done < <(jq '.');}
	command=$(jq -c '.command' <<<"$jsonargs" | bashp_jsonStripQuotes)
	mapfile -t Args < <(jq -c '.args[]' <<<"$jsonargs" | bashp_jsonStripQuotes)
	mapfile -t CommandArgs < <(jq -c '."args-map"[]' <<<"$jsonargs" | bashp_jsonStripQuotes)
	unset bashp_jsonStripQuotes
    else
	local command="$1"
	local -a CommandArgs="$2" || { printf '%s\n' "Failed to to use $2 as arguments map. It's not a valid bash list." && return 1 ; }
    fi
    [[ -n "${Args[@]}" ]] || { local -a Args="$3" || { printf '%s\n' "Failed to to use $3 as arguments. It's not a valid bash list." && return 1 ; } ; }

    # debug
    #declare -p Args command CommandArgs jsonargs serial json argsfile
    #return 0

    bashp_results_to_json(){
	local stdouttmp; stdouttmp=$(mktemp /tmp/temp.XXXXXXXX) || { printf '%s\n' "mapfork: Failed to create tempfile" && return 1 ; }
	local stderrtmp; stderrtmp=$(mktemp /tmp/temp.XXXXXXXX) || { printf '%s\n' "mapfork: Failed to create tempfile" && return 1 ; }
	local rettmp; rettmp=$(mktemp /tmp/temp.XXXXXXXX) || { printf '%s\n' "mapfork: Failed to create tempfile" && return 1 ; }
	local argstmp; argstmp=$(mktemp /tmp/temp.XXXXXXXX) || { printf '%s\n' "mapfork: Failed to create tempfile" && return 1 ; }
	# TODO: remove /tmp/mapfork_write_lock_$PPID"
	trap 'for a in $stdouttmp $stderrtmp $rettmp $argstmp ; do rm $a; done' EXIT SIGTERM RETURN
	local -a Results="$1" || return 1
	for ((i=0;i<${#Results[@]};i++)); do
	    local -a result="${Results[$i]}"
	    local -a cmd_args="${result[0]}"
	    cmd="${cmd_args[0]}"
	    local -a args="${cmd_args[1]}"
	    #declare -p result > /tmp/result-$$
	    printf '%s' "${result[1]}" > $stdouttmp
	    printf '%s' "${result[2]}" > $stderrtmp
	    printf '%s' "${result[3]}" > $rettmp
	    printf '%s' "${cmd[0]@Q} " "${args[*]@Q}" > $argstmp
	    jq -c -n --unbuffered \
	       --rawfile stdout $stdouttmp \
	       --rawfile stderr $stderrtmp \
	       --rawfile ret $rettmp \
	       --rawfile args $argstmp \
	       '. | { command: $args , stdout: $stdout , stderr: $stderr , "return-code": $ret }'; done | jq -Mcsj .
	return 0
    }

    local -a PipedArr
    local -i i

    # Open a fifo and keep it open until done
    local myfifo; myfifo=$(mktemp /tmp/temp.XXXXXXXX) || { printf '%s\n' "mapfork: Failed to create tempfile" && return 1 ; }
    [[ -e "$myfifo" ]] && rm "$myfifo" ; mkfifo "$myfifo"
    trap '[[ -e $myfifo ]] && rm $myfifo' EXIT SIGTERM RETURN
    sleep infinity > "$myfifo" &
    spid=$!

    local -a placeHolders=()
    for ((i=0;i<${#CommandArgs[@]};i++)); do
	[[ "${CommandArgs[$i]}" =~ ^\{\}$ ]] && placeHolders+=("$i") ;done

    local waitpid
    local -i myfd
    local -i myfd2
    local myfifo2; myfifo2=$(mktemp /tmp/temp.XXXXXXXX) || { printf '%s\n' "mapfork: Failed to create tempfile" && return 1 ; }
    rm "$myfifo2" && mkfifo "$myfifo2" && exec {myfd2}<>"$myfifo2" && rm "$myfifo2"
    # reference: https://unix.stackexchange.com/questions/103920/parallelize-a-bash-for-loop/216475#216475
    bashp_open_sem(){
	local myfifo3; myfifo3=$(mktemp /tmp/temp.XXXXXXXX) || { printf '%s\n' "mapfork: Failed to create tempfile" && return 1 ; }
	rm "$myfifo3" && mkfifo "$myfifo3"
	exec {myfd}<>"$myfifo3"
	rm "$myfifo3"
	local i=$1
	for((;i>0;i--)); do
	    printf '%s' 0 >&$myfd
	done
    }
    # reference: https://stackoverflow.com/questions/13806626/capture-both-stdout-and-stderr-in-bash
    bashp_nullWrap(){
	local -i i; i="$1"
	local myCommand="$2"
	local -a myCommandArgs="$3"
	local -a cmdAndArgs=("$myCommand" "(${myCommandArgs[*]@Q})")
	local myfifo="$4"
	local waitpid="$5"
	local stderr
	local stdout
	local stdret
	[[ -n "$5" ]] && until [[ ! -e /proc/"$waitpid" ]]; do sleep .2 ; done
	. <(\
	    { #stderr=$({ stdout=$(eval "$myCommand ${myCommandArgs[*]@Q}"); stdret=$?; } 2>&1 ;\
		stderr=$({ mapfile -d '' stdout < <(eval "$myCommand ${myCommandArgs[*]@Q}"); stdret=$?; } 2>&1 ;\
	               declare -p stdout >&2 ;\
		       declare -p stdret >&2) ;\
	      declare -p stderr;\
	    } 2>&1)
	local -a Arr=("(${cmdAndArgs[*]@Q})")
	Arr+=( "${stdout[0]}" )
	Arr+=("$stderr" "$stdret")
	#declare -p Arr > /tmp/resultarr-$$
	# For testing serial vs parallel:
	#printf '%s\n' "waitpid is $waitpid , cmdAndArgs: ${cmdAndArgs[@]}" >> logfile
	[[ -z "$5" ]] && printf '%s' 0 >&$myfd
	eval '( flock -x $myfd2 ; printf "${i}:%s\u0000" "(${Arr[*]@Q})" > "$myfifo" ; ) '"$myfd2"'>$bashpwritelocktmp'
    }
    local bashpwritelocktmp; bashpwritelocktmp=$(mktemp /tmp/temp.XXXXXXXX) || { printf '%s\n' "mapfork: Failed to create tempfile" && return 1 ; } 
    local -i cmdnum=0
    local ncpu x; ncpu=$(nproc)

    bashp_open_sem "${size:-$ncpu}"
    unset bashp_open_sem

    trap '[[ -e $bashpwritelocktmp ]] && rm $bashpwritelocktmp' EXIT SIGTERM RETURN

    for ((i=0;i<${#Args[@]};i+=0)); do
	# if we have placeholders in CommandArgs we need to take args
	# from Args to replace.
	if [[ ${#placeHolders[@]} -gt 0 ]]; then
	    for ii in "${placeHolders[@]}"; do
		CommandArgs["$ii"]="${Args[$i]}"
		i+=1; done; fi
	if [[ "$serial" = --serial ]] ; then
	    bashp_nullWrap "$i" "$command" "(${CommandArgs[*]@Q})" "$myfifo" "$waitpid" &
	    waitpid=$!
	    cmdnum+=1
	else
	    read -u $myfd -n 1 x
	    bashp_nullWrap "$i" "$command" "(${CommandArgs[*]@Q})" "$myfifo" &
	    cmdnum+=1
	fi
    done
    for ((i=0;i<$cmdnum;i+=1)) ; do
	local res
	# TODO: fix - may not need to create subshell
	#res=$(read -r -d '' temp <"$myfifo" && printf '%b' "$temp")
	res=$(read -r -d '' temp <"$myfifo" && echo -E "$temp")
	#declare -p res > /tmp/resultres-$$
	local -i resI
	resI="${res%%:*}"
	PipedArr[$resI]="${res#*:}"
    done
    kill -9 "$spid" # &>/dev/null
    wait "$spid" 2>/dev/null
    [[ -e "$myfifo" ]] && rm "$myfifo"
    unset bashp_nullWrap
    # reference: https://stackoverflow.com/questions/41966140/how-can-i-make-an-array-of-lists-or-similar-in-bash
    if [[ -n "$json" ]]; then
	#declare -p PipedArr > /tmp/resultpipedarr-$$
	if bashp_results_to_json "(${PipedArr[*]@Q})"; then
	    eval "exec $myfd>&-"
	    eval "exec $myfd2>&-"
	    unset bashp_results_to_json
	    rm "$bashpwritelocktmp"
	    return 0
	else
	    eval "exec $myfd>&-"
	    eval "exec $myfd2>&-"
	    unset bashp_results_to_json
	    rm "$bashpwritelocktmp"
	    return 1; fi
    else
	eval "exec $myfd>&-"
	eval "exec $myfd2>&-"
	unset bashp_results_to_json
	rm "$bashpwritelocktmp"
	printf '%s' "(${PipedArr[*]@Q})"; fi
}
    bashp "$@"
}
parallel_test_f(){
    local a
    alo(){ sleep 2 ; printf '%s\n' "$@" ; }
    local -a Args=(apa bepa cepa '!@#$!$@%@$&$%*^$&(&#*@%$^!#$~')
    local -a ArgsMap=(one {} {})
    local -a Results=$(bashp --size 2 -- alo "(${ArgsMap[*]@Q})" "(${Args[*]@Q})")
    local -a res1="${Results[1]}"
    local -a res1_CommandAndArgs="${res1[0]}"
    local res1_Command="${res1_CommandAndArgs[0]}"
    local -a res1_CommandArgs="${res1_CommandAndArgs[1]}"
    local res1_stdout="${res1[1]}"
    local res1_stderr="${res1[2]}"
    local res1_ret_code="${res1[3]}"
    alo2(){ bashp --json-args '{"command":"alo","args":'"$(jq -n '$ARGS.positional' --args "$@")"',"args-map":["{}","{}"]}' --json-out --size 2 ; }
    a=$(bcu__parallel --json-args '{"command":"alo2","args":["apa","bepa","cepa","depa"],"args-map":["staticarg","{}"]}' --size 2 --json-out | jq -r '[.[0,1].stdout|fromjson[]]|last|.command')
    unset alo
    unset alo2
    declare -p a
}
parallel_test_1()(
    . <(parallel_test_f)
    [[ "${a}" == "'alo' 'staticarg' 'bepa'" ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
parallel_test_main(){
    printf parallel_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )parallel_test_1(\ |$) ]]; then parallel_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
        parallel_test_main "${org_args[@]}"
    else
	parallel_test_main "${@}"
    fi
fi
# parallel-test-main ends here
