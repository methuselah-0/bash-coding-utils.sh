#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::die-test-main][die-test-main]]
org_args='()'
. bcu.sh
bcu__die(){
    local _STACK z Options=() Input=() return_code arg
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
die() prints a rather typical stacktrace and exits with given return code


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    dietest()( bcu__die -e 12 -- "$@"  ; ) ; dietest a b c
    echo $?

Results:

    'dietest' defined in main invoked on line 334 in main,
     'bcu__die' defined in misc.sh invoked on line 44 in main, with the args: 'a' 'b' 'c'
    12

EOF 
		)
    }    
    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)    
    local Exit=(e return_code "1" "Exit code. Default is 1" 1)
    Options+=("(${null[*]@Q})" "(${Exit[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:f:arg)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || {
	printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    # Check args
    [[ "$return_code" -lt 256 ]] || { printf '%s' "Exit code is not an integer: ${_STACK}" && bcu__stack "$@" && return 1; }

    bcu__stack "${arg[@]}"
    exit "${return_code}"
}
die_test_f(){
    local a b
    dietest()( bcu__die -e 12 -- "$@"  ; )
    a=$(dietest a b c)
    b=$(echo $?)
    declare -p a b 
}
die_test_1()(
    . <(die_test_f)
    [[ "${a}" =~ .*"'die_test_f' defined in".* ]] || { echo FAIL && return 1; }
    [[ "${b}" == "12" ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
die_test_main(){
    printf die_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )die_test_1(\ |$) ]]; then die_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	die_test_main "${org_args[@]}"
    else
	die_test_main "${@}"
    fi
fi
# die-test-main ends here
