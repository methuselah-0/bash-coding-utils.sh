#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::docs-test-main][docs-test-main]]
org_args='()'
. bcu.sh
bcu__docs()(
    local _STACK z Options=() Input=() browser
    [[ "$1" =~ (-h|--help) ]] && {
        local _DESCRIPTION _EXAMPLE
        # shellcheck disable=SC2034
        _DESCRIPTION=$(cat <<'EOF'
Open the full BCU documentation in a web-browser.


EOF
	)
        # shellcheck disable=SC2034
        _EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__docs icecat

Results:

    /home/user1/.guix-profile/bin/icecat
    

EOF
	)
    }
    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:f:browser)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || {
        printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ (-h|--help) ]] && return 0

    # Check args
    if [[ -n "$browser" ]]; then
	command -v "$browser" || { printf '%s' "Unable to find $browser in your PATH: ${_STACK}" && bcu__stack "$@" && return 1; }
	eval "$browser ${_BCU_SH_DIR}/docs/bcu.html"
    else
	command -v xdg-open || { printf '%s' "Unable to find a default browser: ${_STACK}" && bcu__stack "$@" && return 1; }
	xdg-open "${_BCU_SH_DIR}/docs/bcu.html"
    fi
)
docs_test_f(){
    local a
    a=$(command -v bcu__docs &>/dev/null && echo true)
    declare -p a
}
docs_test_1()(
    . <(docs_test_f)
    [[ "${a}" == true ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
docs_test_main(){
    printf docs_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )docs_test_1(\ |$) ]]; then docs_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
        docs_test_main "${org_args[@]}"
    else
	docs_test_main "${@}"
    fi
fi
# docs-test-main ends here
