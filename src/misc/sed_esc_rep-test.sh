#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::sed_esc_rep-test-main][sed_esc_rep-test-main]]
org_args='()'
. bcu.sh
bcu__sed_esc_rep(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() replacement_string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Escape a replacement string for sed.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset a b aesc
    a='apa\bepa/cepa&depa'
    b="lalalulzlala"
    aesc=$(bcu__sed_esc_rep "$a")
    echo "$b" | sed "s/lulz/$aesc/g"

Results:

    lalaapa\bepa/cepa&depalala

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:replacement_string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    printf '%s' "${replacement_string}" | sed -e 's/[\/&]/\\&/g'      
}
sed_esc_rep_test_f(){
    local a b aesc
    a='apa\bepa/cepa&depa'
    b="lalalulzlala"
    aesc=$(bcu__sed_esc_rep "$a")
    a=$(printf '%s' "$b" | sed "s/lulz/$aesc/g")
    declare -p a
}
sed_esc_rep_test_1()(
    . <(sed_esc_rep_test_f)
    [[ "${a}" == "lalaapa\bepa/cepa&depalala" ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
sed_esc_rep_test_main(){
    printf sed_esc_rep_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )sed_esc_rep_test_1(\ |$) ]]; then sed_esc_rep_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	sed_esc_rep_test_main "${org_args[@]}"
    else
	sed_esc_rep_test_main "${@}"
    fi
fi
# sed_esc_rep-test-main ends here
