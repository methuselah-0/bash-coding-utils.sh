#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::sd_elem-test-main][sd_elem-test-main]]
org_args='()'
. bcu.sh
bcu__sd_elem(){
    local _STACK z Options=() Input=()
    local id attribute ii name val
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Build a correctly formatted SD-element


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__sd_elem --id "apa@bepa" -a cepa depa -a epa 'fepa]"\' && echo

Results:

    
    <record-1><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-1><record-2><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-2></file><file><record-1><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-1><record-2><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-2></file><file><record-1><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-1><record-2><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-2></file><file><record-1><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-1><record-2><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-2></file><file><record-1><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-1><record-2><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-2></file><file><record-1><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-1><record-2><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-2></file>" epa="fepa\]\"\\<file><record-1><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-1><record-2><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-2></file><file><record-1><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-1><record-2><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-2></file><file><record-1><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-1><record-2><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-2></file><file><record-1><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-1><record-2><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-2></file><file><record-1><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-1><record-2><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-2></file><file><record-1><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-1><record-2><apa>val1</apa><bepa>val2</bepa><cepa>val3</cepa></record-2></file>"]

EOF
		)
    }    
    # Options
    local id=(i id "" "an sdid, e.g. exampleSDID@32473" 1 f f)
    local attribute=(a attribute "" 'an attribute name and value, e.g. eventSource Application' 2 t t)
    Options=("(${id[*]@Q})" "(${attribute[*]@Q})")
    local null=(z null "" "Read null-separated operands from stdin" 0)    
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=()

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || {
	printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    # Check args, TODO

    printf '[%s' "$id"
    for ((ii=0; ii<${#attribute[@]}; ii+=2)); do
	name="${attribute[ii]}"
	val="${attribute[$((ii+1))]}"
	val=$(bcu__esc_sd_val "$val")
	printf ' %s="%s"' "$name" "$val"; done
    printf '%s]' ""
}
sd_elem_test_f(){
    local a
    a=$(bcu__sd_elem --id "apa@bepa" -a cepa depa -a epa 'fepa]"\')
    declare -p a
}
sd_elem_test_1()(
    . <(sd_elem_test_f)
    [[ "${a}" == '[apa@bepa cepa="depa" epa="fepa\]\"\\"]' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
sd_elem_test_main(){
    printf sd_elem_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )sd_elem_test_1(\ |$) ]]; then sd_elem_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	sd_elem_test_main "${org_args[@]}"
    else
	sd_elem_test_main "${@}"
    fi
fi
# sd_elem-test-main ends here
