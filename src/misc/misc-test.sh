#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::misc-unit-tests][misc-unit-tests]]
org_args='()'
[[ "$_BCU_SH_LOADED" == YES ]] || . bcu.sh
_misc_MOD_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")
misc_private(){
    :

}
misc_test_main(){
. bcu.sh
bcu__sed_esc_key(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() key_string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Escape a string that you want to pass to sed to have it replaced in
some other string/text, also referred to as the regexp string in the
sed manual.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset replaceme sed_escaped sometext
    replaceme="asdf[]][{}@%^"
    sed_escaped=$(bcu__sed_esc_key "$replaceme")
    sometext="lala${replaceme}lala"
    echo "$sometext" | sed "s/$sed_escaped/lulz/g"

Results:

    lalalulzlala

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:key_string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    #printf '%s' "${Input[0]}" | sed -e 's/\([\.\^\$\*\[\]\|]\)/\\&/g'
    
    local -a Arr=$(bcu__string_to_arr "${key_string}") || { printf '%s' "Failed to convert key_string to array: ${_STACK}" && bcu__stack "$@" && return 1; }

    for c in "${Arr[@]}"; do
	if [[ "$c" =~ (\.|\^|]|\$|\*|/|]|\[|\\) ]]; then
						     printf '\%s' "$c"
						  else printf '%s' "$c" ;fi ; done
						  }
sed_esc_key_test_f(){
    local a b
    local -a TestEscaped

    local -a Escaped=([0]="\\." [1]="\^" [2]="\\*" [3]="+" [4]="-" [5]="?" [6]="(" [7]=")" [8]="\\[" [9]="\\]" [10]="{" [11]="}" [12]="\\\\" [13]="|" [14]="\\\$")
    mapfile -t TestEscaped < <(for c in '.' '^' '$' '*' '+' '-' '?' '(' ')' '[' ']' '{' '}' '\' '|'; do bcu__sed_esc_key "$c" && echo; done | sort )
    mapfile -t Escaped < <(printf '%s\n' "${Escaped[@]}" | sort)
    a=$(if [[ "${Escaped[@]}" == "${TestEscaped[@]}" ]]; then echo true; else echo false; fi)

    junk="@@\''@echo $jnk\"\":{:}|:\"{}::{}::\":junk=\"@@\''@echo $jnk\"\":{:}|:\"{}::{}::\":@$@$%^#%$&%&^(^()*_+(+11234346534574769(&)*^()}|}|\">:?><><>,/.,.\,.][][121251$#"
    b=$(if diff -s <(a=$(bcu__sed_esc_key "$(printf '%s' "$junk")"); printf '%s' "$junk" | sed "s/$a/&/g") <(printf '%s' "$junk") &>/dev/null; then echo true; else echo false; fi)

    declare -p a b
}
sed_esc_key_test_1()(
    . <(sed_esc_key_test_f)
    [[ "${a}" == true ]] || { echo FAIL && return 1; }
    [[ "${b}" == true ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
sed_esc_key_test_main(){
    printf sed_esc_key_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )sed_esc_key_test_1(\ |$) ]]; then sed_esc_key_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	sed_esc_key_test_main "${org_args[@]}"
    else
	sed_esc_key_test_main "${@}"
    fi
fi
. bcu.sh
bcu__sed_esc_rep(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() replacement_string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Escape a replacement string for sed.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset a b aesc
    a='apa\bepa/cepa&depa'
    b="lalalulzlala"
    aesc=$(bcu__sed_esc_rep "$a")
    echo "$b" | sed "s/lulz/$aesc/g"

Results:

    lalaapa\bepa/cepa&depalala

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:replacement_string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    printf '%s' "${replacement_string}" | sed -e 's/[\/&]/\\&/g'      
}
sed_esc_rep_test_f(){
    local a b aesc
    a='apa\bepa/cepa&depa'
    b="lalalulzlala"
    aesc=$(bcu__sed_esc_rep "$a")
    a=$(printf '%s' "$b" | sed "s/lulz/$aesc/g")
    declare -p a
}
sed_esc_rep_test_1()(
    . <(sed_esc_rep_test_f)
    [[ "${a}" == "lalaapa\bepa/cepa&depalala" ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
sed_esc_rep_test_main(){
    printf sed_esc_rep_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )sed_esc_rep_test_1(\ |$) ]]; then sed_esc_rep_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	sed_esc_rep_test_main "${org_args[@]}"
    else
	sed_esc_rep_test_main "${@}"
    fi
fi
. bcu.sh
bcu__sedp(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
sedp is like sed but adds support for pcre-patterns by piping the
given input to perl. If the null option is provided, both input and
output will be null-separated.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    mapfile -t -d '' Arr < <(printf '%b\u0000' "apa\nbepa" bepa cepa | bcu__sedp -z -s 's/p/g/g')
    declare -p Arr
    echo apa | bcu__sedp -s 's/(?<=a)p(?=a)/s/g'

Results:

    declare -a Arr=([0]=$'aga\nbega' [1]="bega" [2]="cega")
    asa

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Substitution=(s substitution "" "The substitution string on the form 's/regex/replacement/g' to pass to perl" 1 t f)
    Options+=("(${null[*]@Q})" "(${Substitution[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    if [[ -n "$null" ]]; then
	printf '%s\u0000' "${string[@]}" | perl -0 -pe "$substitution"
    else
	printf '%s\n' "${string[0]}" | perl -pe  "$substitution"; fi
}
sedp_test_f(){
    local a
    a=$(if [[ "$(echo apa | bcu__sedp -s 's/(?<=a)p(?=a)/s/g')" == asa ]]; then echo true; else echo false; fi)
    declare -p a
}
sedp_test_1()(
    . <(sedp_test_f)
    [[ "${a}" == true ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
sedp_test_main(){
    printf sedp_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )sedp_test_1(\ |$) ]]; then sedp_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	sedp_test_main "${org_args[@]}"
    else
	sedp_test_main "${@}"
    fi
fi
. bcu.sh
bcu__sedpi(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() substitution file s f result
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
sedpi modifies a given file with a given pcre regexp


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset f
    f=$(mktemp /tmp/sedpi-test.XXXXXX)
    echo apa > "$f"
    bcu__sedpi -s 's/(?<=a)p(?=a)/s/g' -- "$f" 
    cat "$f"

Results:

    asa

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Substitution=(s substitution "" "Substitution command on the for 's/<regex>/<replacement>/g'" 1 t t)
    Options+=("(${null[*]@Q})" "(${Substitution[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:file)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    for s in "${substitution[@]}"; do
	for f in "${file[@]}"; do
	    result=$(perl -p -i -e  "$substitution" "$file" 2>&1) || { printf '%s' "Failed to substitute - $substitution - in file - $file - with result - $result - : ${_STACK}" && bcu__stack "$@" && return 1; }; done; done
}
sedpi_test_f(){
    local a f
    f=$(mktemp /tmp/sedpi-test.XXXXXX)
    echo apa > "$f"
    bcu__sedpi -s 's/(?<=a)p(?=a)/s/g' -- "$f" 
    a=$(if [[ "$(cat "$f")"  == asa ]]; then echo true; else echo false; fi)
    declare -p a
}
sedpi_test_1()(
    . <(sedpi_test_f)
    [[ "${a}" == true ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
sedpi_test_main(){
    printf sedpi_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )sedpi_test_1(\ |$) ]]; then sedpi_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	sedpi_test_main "${org_args[@]}"
    else
	sedpi_test_main "${@}"
    fi
fi
. bcu.sh
bcu__trim(){
    local _STACK z Options=() Input=() leading trailing newlines padding text var
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE      
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Trim whitespace etc.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    printf '%b\u0000' 'apa \n ' ' \nbepa\nbepa\n' | bcu__trim -l -t -n -z
    printf '%s\n' 'apa  ' '  bepa' | bcu__trim -l -t -p

Results:

    apa 
     
    bepa
    bepa
    apa  
      bepa

EOF
		)
    }
    local null=(z null "" "Read null-separated operands from stdin. Print results null-separated as well" 0)
    local Leading=(l leading "" "Preserve leading whitespace" 0)
    local Trailing=(t trailing "" "Preserve trailing whitespace" 0)
    local Newlines=(n newlines "" "Don't replace newlines with a blankspace char" 0)
    local Padding=(p padding "" "Add a newline as padding. Default if given more than one input operand unless the null option is used. Can't be used with the null option" 0)
    Options+=("(${null[*]@Q})" "(${Leading[*]@Q})" "(${Trailing[*]@Q})" "(${Newlines[*]@Q})" "(${Padding[*]@Q})")
    # shellcheck disable=SC2034      
    local -a Operands=(0:t:text)
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    #declare -p Leading Trailing Newlines leading trailing newlines padding && return 0
    [[ "${#text[@]}" -gt 1 ]] && padding=1
    for var in "${text[@]}"; do
	# remove leading whitespace characters
	[[ -n "$leading" ]] || var="${var#"${var%%[![:space:]]*}"}"
	# remove trailing whitespace characters
	[[ -n "$trailing" ]] || var="${var%"${var##*[![:space:]]}"}"
	[[ -n "$newlines" ]] || var="${var//$'\n'/ }"
	if [[ "$padding" == 1 ]]; then
	    if [[ -n "$null" ]]; then
		printf '%s\u0000' "${var}"
	    else
		printf '%s\n' "${var}";fi
	else
	    if [[ -n "$null" ]]; then
		printf '%s\u0000' "${var}"
	    else
		printf '%s' "${var}"; fi; fi; done
}
trim_test_f(){
    local -a a b
    mapfile -t -d '' a < <(printf '%b\u0000' 'apa \n ' ' \nbepa\nbepa\n' | bcu__trim -z)
    mapfile -t b < <(printf '%s\n' 'apa  ' '  bepa' | bcu__trim -n -p)
    declare -p a b
}
trim_test_1()(
    . <(trim_test_f)
    [[ "${a[0]}" == apa ]] || { echo FAIL && return 1; }
    [[ "${a[1]}" == "bepa bepa" ]] || { echo FAIL && return 1; }
    [[ "${b[0]}" == apa ]] || { echo FAIL && return 1; }
    [[ "${b[1]}" == bepa ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
trim_test_main(){
    printf trim_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )trim_test_1(\ |$) ]]; then trim_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	trim_test_main "${org_args[@]}"
    else
	trim_test_main "${@}"
    fi
fi
. bcu.sh
bcu__is_valid_var_name(){ 
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() var_string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Check if a string is a valid bash variable name


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__is_valid_var_name _apa && echo true
    bcu__is_valid_var_name 1_apa || echo false

Results:

    true
    false

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:var_string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    [[ "$var_string" =~ ^[_[:alpha:]][_[:alpha:][:digit:]]*$ ]] || return 1
}
is_valid_var_name_test_f(){
    local a b
    a=$(bcu__is_valid_var_name _apa && echo true)
    b=$(bcu__is_valid_var_name 1_apa || echo false)
    declare -p a b
}
is_valid_var_name_test_1()(
    . <(is_valid_var_name_test_f)
    [[ "${a}" == true ]] || { echo FAIL && return 1; }
    [[ "${b}" == false ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
is_valid_var_name_test_main(){
    printf is_valid_var_name_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )is_valid_var_name_test_1(\ |$) ]]; then is_valid_var_name_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	is_valid_var_name_test_main "${org_args[@]}"
    else
	is_valid_var_name_test_main "${@}"
    fi
fi
. bcu.sh
bcu__prepend_line(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() newline file string line str
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Prepend a line to a file or create it if it doesn't
exist. prepend_line errors with a stacktrace if the given file is not
read- and writable or the file can't be created. Dependencies are the
sed and wc programs.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    rm b; cat b
    bcu__prepend_line -f b -- a
    bcu__prepend_line -f b -- b
    bcu__prepend_line -f b -n -- b
    cat b

Results:

    cat: b: No such file or directory
    b
    ba

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Newline=(n newline "" "Append a newline to the string you want to prepend" 0)
    local File=(f file "" "File to append to" 1)
    Options+=("(${null[*]@Q})" "(${Newline[*]@Q})" "(${File[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    if [[ ! -e "$file" ]]; then
	touch "$file" &>/dev/null || { printf '%s' "File $file does not exist and unable to create it: " && bcu__stack "$@" && return 1; }; fi

    [[ -w "$file" ]] || { printf '%s' "File is not writable: " && bcu__stack "$@" && return 1; }
    [[ -r "$file" ]] || { printf '%s' "File is not readable: " && bcu__stack "$@" && return 1; }
    [[ "$(wc -l < "$file")" -eq 0 ]] && echo >> "$file"

    if [[ -n "$newline" ]]; then
	for str in "${string[@]}"; do
	    line=$(bcu__sed_esc_rep "${str}") || { printf '%s' "Failed to escape replacement string - $str - with the following result - $line - : ${_STACK}" && bcu__stack "$@" && return 1; }
	    result=$(sed -i "1s/^/${line}\n/" "$file" 2>&1) || { printf '%s' "sed failed to prepend the line - $line - with the error - $result - : ${_STACK}" && bcu__stack "$@" && return 1; }; done
    else
	for str in "${string[@]}"; do
	    line=$(bcu__sed_esc_rep "${str}") || { printf '%s' "Failed to escape replacement string - $str - with the following result - $line - : ${_STACK}" && bcu__stack "$@" && return 1; }
	    result=$(sed -i "1s/^/${line}/" "$file" 2>&1) || { printf '%s' "sed failed to prepend the line - $line - with the error - $result - : ${_STACK}" && bcu__stack "$@" && return 1; }; done; fi
}
prepend_line_test_f(){
    local a b
    b=$(mktemp /tmp/tempfile.XXXXXX)
    rm "$b" &>/dev/null; cat "$b" &>/dev/null
    bcu__prepend_line -f "$b" -- a
    bcu__prepend_line -f "$b" -- b
    bcu__prepend_line -f "$b" -n -- b
    a=$(cat "$b")
    declare -p a
}
prepend_line_test_1()(
    . <(prepend_line_test_f)
    [[ "${a}" == $'b\nba' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
prepend_line_test_main(){
    printf prepend_line_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )prepend_line_test_1(\ |$) ]]; then prepend_line_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	prepend_line_test_main "${org_args[@]}"
    else
	prepend_line_test_main "${@}"
    fi
fi
bcu__setopts(){
    # shellcheck disable=SC2154
    #[[ -n "${Options[*]}" ]] || return 1

    # If we are invoked but not from a function, and are given the -h or
    # --help option flag, we consider the intent to be to get help on
    # using setopts itself.
    if { [[ "$1" == -h ]] || [[ "$1" == --help ]] ; } && [[ -z "$(caller 0)" ]]; then
	local _STACK z Options=() Input=() value_string
	[[ "$1" =~ ^(-h|--help)$ ]] && {
	    local _DESCRIPTION _EXAMPLE	
	    # shellcheck disable=SC2034
	    _DESCRIPTION=$(cat <<'EOF'
A more complete alternative to getopts. See the BCU manual and example
for usage info. setopts consumes the setopts_* namespace for functions
and variables and reads the Options and Operands array variables, so
be aware when using it.


EOF
			)			       
	    # shellcheck disable=SC2034	
	    _EXAMPLE=$(	
cat <<'EOF'
Example:

    . bcu.sh
    printMessages(){
        local debug message msg
        debug=(d debug 2 "Set the debug level" 1 f)
        local Operands=(0:t:message)
        local Options=("(${debug[*]@Q})") && bcu__setopts "$@"
        if [[ "$debug" -ge 2 ]]; then set -x ; else set +x; fi
        for msg in "${message[@]}"; do printf 'A message! %s\n' "$msg"; done
        echo Messages was printed with a debug level of $debug
    }
    printMessages -d 1 -- "Whazzaa" "Wazzzu" "Uzzaw" 2>&1
    printMessages --debug 2 -- "Whazzaa" "Wazzzu" "Uzzaw" 2>&1
    set +x

Results:

    A message! Whazzaa
    A message! Wazzzu
    A message! Uzzaw
    Messages was printed with a debug level of 1
    + for msg in "${message[@]}"
    s\n' Whazzaa
    A message! Whazzaa
    {message[@]}"
    s\n' Wazzzu
    A message! Wazzzu
    {message[@]}"
    s\n' Uzzaw
    A message! Uzzaw
    + echo Messages was printed with a debug level of 2
    Messages was printed with a debug level of 2
    + set +x
    

EOF
		    )
	}    
	Options=()
	# This code is mainly just a copy of the -h --help code from below. Sorry :/ TODO: fix!
	# print mandatory options in Usage: line.
	#printf 'Usage: %s [OPTION] [-- [OPERAND]]\n\n' "$(caller 0 | cut -f 2 -d ' ' --output-delimiter='')"
	# We test greater than 0 instead of gt 1 since we always have the help option.
	local setopts_options_string=$(if [[ ${#Options[@]} -gt 0 ]]; then printf '%s' '[OPTIONS...] '; elif [[ ${#Options[@]} -eq 1 ]]; then printf '%s' '[OPTION] '; fi )
	#printf "Usage: %s ${setopts_options_string}${setopts_operands_usage[*]}\n\n" "$(caller 0 | cut -f 2 -d ' ' --output-delimiter='')"

	printf "Usage: bcu__setopts ${setopts_options_string:-[OPTION] }%s\n\n" '"$@"'
	if [[ -v _DESCRIPTION ]]; then
	    # remove leading whitespace characters		  
	    _DESCRIPTION="${_DESCRIPTION#"${_DESCRIPTION%%[![:space:]]*}"}"
	    # remove trailing whitespace characters
	    _DESCRIPTION="${_DESCRIPTION%"${_DESCRIPTION##*[![:space:]]}"}"
	    printf '%s:\n%s\n\n' Description "$_DESCRIPTION"; fi
	printf '%s:\n\t%s | %s\t%s\n' "Options" "-h" "--help" "Display this help and exit"
	local setopts_opt
	for setopts_opt in "${Options[@]}"; do
	    local -a setopts_Option="${setopts_opt}"
	    local setopts_mandatory
	    setopts_mandatory=$(
		a=$(if [[ -n "${setopts_Option[5]}" ]]; then
			printf '%s' "${setopts_Option[5]}"; else
			printf '%s' f; fi)
		if [[ "$a" == t ]]; then
		    printf '%s' Required;
		else printf '%s' Optional; fi)
	    local setopts_args_num
	    setopts_args_num=$(a="${setopts_Option[4]}"; printf '%s' "${a:-1}")
	    # commented line is with displaying default value.
	    #printf '\t%s | %s\t[%s] [Args:%s] [Default:%s]\t%s\n' "-${setopts_Option[0]}" "--${setopts_Option[1]}" "$setopts_mandatory" "$setopts_args_num" "${setopts_Option[2]}" "${setopts_Option[3]}"; done
	    printf '\t%s | %s\t[%s] [Args:%s] %s\n' "-${setopts_Option[0]}" "--${setopts_Option[1]}" "$setopts_mandatory" "$setopts_args_num" "${setopts_Option[3]}"; done
	printf '%s\n' ""
	# Add examples to output if <function>_example is defined
	#local setopts_f_example
	#setopts_f_example=$(printf '%s_example' "$(caller 0 | cut -f 2 -d ' ' --output-delimiter='')")
	#if command -v "${setopts_f_example}" >/dev/null; then
	if [[ -v _EXAMPLE ]]; then
	    #printf '%s\n' "" && eval "${setopts_f_example}"
	    # remove leading whitespace characters		  
	    _EXAMPLE="${_EXAMPLE#"${_EXAMPLE%%[![:space:]]*}"}"
	    # remove trailing whitespace characters
	    _EXAMPLE="${_EXAMPLE%"${_EXAMPLE##*[![:space:]]}"}"
	    printf '%s\n' "$_EXAMPLE"
	    #trim -l -t -p -- "$_EXAMPLE" # && eval "${setopts_f_example}"
	    return 0
	else
	    return 0; fi; fi


    if [[ "$1" == -h ]] || [[ "$1" == --help ]]; then
	# Use <function>_usage if defined instead of the default auto-generated one.
	local setopts_f_usage;
	setopts_f_usage=$(printf '%s_usage' "$(caller 0 | cut -f 2 -d ' ' --output-delimiter='')")
	if command -v "${setopts_f_usage}" >/dev/null; then
	    eval "${setopts_f_usage}"
	    return 0
	else
	    #local setopts_operands_length="${#Operands[@]}"
	    local -a setopts_operands_usage=()
	    local -i setopts_i=0
	    for ((setopts_i=0; setopts_i<"${#Operands[@]}" ; setopts_i++ )); do
		setopts_op="${Operands[${setopts_i}]}"
		setopts_op_num="${setopts_op%%:*}"
		setopts_op_stat_and_var="${setopts_op##$setopts_op_num:}"
		setopts_op_stat="${setopts_op_stat_and_var%%:*}"
		setopts_op_var="${setopts_op_stat_and_var#$setopts_op_stat:}"
		if [[ "$setopts_op_stat" == t ]]; then
		    if [[ "$setopts_op_num" -eq 0 ]]; then
			setopts_operands_usage+=("<${setopts_op_var}> [<${setopts_op_var}>]...")
		    elif [[ "$setopts_op_num" -eq 1 ]]; then
			setopts_operands_usage+=("<${setopts_op_var}>")
		    elif [[ "$setopts_op_num" -gt 1 ]]; then
			for ((setopts_ii=0;$setopts_ii<$setopts_op_num;setopts_ii++)); do
			    setopts_operands_usage+=("<${setopts_op_var}-$(( setopts_ii + 1 ))>"); done; fi
		elif [[ "$setopts_op_stat" == f ]]; then
		    if [[ "$setopts_op_num" -eq 0 ]]; then
			setopts_operands_usage+=("[<${setopts_op_var}>]...")
		    elif [[ "$setopts_op_num" -eq 1 ]]; then
			setopts_operands_usage+=("[<${setopts_op_var}>]")
		    elif [[ "$setopts_op_num" -gt 1 ]]; then
			for ((setopts_ii=0;$setopts_ii<$setopts_op_num;setopts_ii++)); do
			    setopts_operands_usage+=("[<${setopts_op_var}-$(( setopts_ii + 1))>]");done; fi; fi; done

	    # print mandatory options in Usage: line.
	    #printf 'Usage: %s [OPTION] [-- [OPERAND]]\n\n' "$(caller 0 | cut -f 2 -d ' ' --output-delimiter='')"
	    # We test greater than 0 instead of gt 1 since we always have the help option.
	    local setopts_options_string=$(if [[ ${#Options[@]} -gt 0 ]]; then printf '%s' '[OPTIONS...] '; elif [[ ${#Options[@]} -eq 1 ]]; then printf '%s' '[OPTION] '; fi )
	    printf "Usage: %s ${setopts_options_string}${setopts_operands_usage[*]}\n\n" "$(caller 0 | cut -f 2 -d ' ' --output-delimiter='')"
	    if [[ -v _DESCRIPTION ]]; then
		# remove leading whitespace characters		  
		_DESCRIPTION="${_DESCRIPTION#"${_DESCRIPTION%%[![:space:]]*}"}"
		# remove trailing whitespace characters
		_DESCRIPTION="${_DESCRIPTION%"${_DESCRIPTION##*[![:space:]]}"}"
		printf '%s:\n%s\n\n' Description "$_DESCRIPTION"; fi
	    printf '%s:\n\t%s | %s\t%s\n' "Options" "-h" "--help" "Display this help and exit"
	    local setopts_opt
	    for setopts_opt in "${Options[@]}"; do
		local -a setopts_Option="${setopts_opt}"
		local setopts_mandatory
		setopts_mandatory=$(
		    a=$(if [[ -n "${setopts_Option[5]}" ]]; then
			    printf '%s' "${setopts_Option[5]}"; else
			    printf '%s' f; fi)
		    if [[ "$a" == t ]]; then
			printf '%s' Required;
		    else printf '%s' Optional; fi)
		local setopts_args_num
		setopts_args_num=$(a="${setopts_Option[4]}"; printf '%s' "${a:-1}")
		# commented line is with displaying default value.
		#printf '\t%s | %s\t[%s] [Args:%s] [Default:%s]\t%s\n' "-${setopts_Option[0]}" "--${setopts_Option[1]}" "$setopts_mandatory" "$setopts_args_num" "${setopts_Option[2]}" "${setopts_Option[3]}"; done
		printf '\t%s | %s\t[%s] [Args:%s] %s\n' "-${setopts_Option[0]}" "--${setopts_Option[1]}" "$setopts_mandatory" "$setopts_args_num" "${setopts_Option[3]}"; done
	    printf '%s\n' ""
	    # Add examples to output if <function>_example is defined
	    #local setopts_f_example
	    #setopts_f_example=$(printf '%s_example' "$(caller 0 | cut -f 2 -d ' ' --output-delimiter='')")
	    #if command -v "${setopts_f_example}" >/dev/null; then
	    if [[ -v _EXAMPLE ]]; then
		#printf '%s\n' "" && eval "${setopts_f_example}"
		# remove leading whitespace characters		  
		_EXAMPLE="${_EXAMPLE#"${_EXAMPLE%%[![:space:]]*}"}"
		# remove trailing whitespace characters
		_EXAMPLE="${_EXAMPLE%"${_EXAMPLE##*[![:space:]]}"}"
		printf '%s\n' "$_EXAMPLE"
		#trim -l -t -p -- "$_EXAMPLE" # && eval "${setopts_f_example}"
		return 0
	    else
		return 0; fi; fi; fi
    # end will hold end of option arguments, endless will be 1 if no '--' or options was provided.
    local -i end setopts_endless

    # p holds the number of processed args
    local -i setopts_p=0 

    # s is initially set to zero but shall be increased by 1 for each
    # option flag given as input.
    local -i setopts_s=0

    for opt in "${Options[@]}"; do
	local -a setopts_Option="${opt}"
	local setopts_short="${setopts_Option[0]}"
	local setopts_long="${setopts_Option[1]}"
	local setopts_default
	#setopts_default=$(printf '%s' "${setopts_Option[2]}")
	setopts_default="${setopts_Option[2]}"

	local description
	description="${setopts_Option[3]}"

	local -i setopts_args_num
	#setopts_args_num=$(
	#    if [[ -n "${setopts_Option[4]}" ]]; then
	#	  echo "${setopts_Option[4]}"
	#    else echo 1; fi)
	setopts_args_num="${setopts_Option[4]:-1}"

	local setopts_repeat
	# default is not repeatable
	setopts_repeat="${setopts_Option[6]:-f}" 
	#setopts_repeat="${setopts_repeat:-f}"

	local setopts_required
	# default is not required
	setopts_required="${setopts_Option[5]:-f}"
	#setopts_required="${setopts_required:-f}"

	# TODO: name of procedure to evaluate the option argument.
	local setopts_predicate
	setopts_predicate="${setopts_Option[6]}"

	# TODO: validate a,b,c,d.
	# Note: the eval here will also evaluate default values on the form '$(<somefunction or expression>)'
	if [[ ! $setopts_args_num -ge 2 ]] && [[ ! "$setopts_repeat" == t ]]; then
	    eval "unset ${setopts_Option[1]}"
	    # This is needed if for example the variable was previously declared as an int.
	    eval "unset ${setopts_long}; unset ${setopts_short}"
	    eval "${setopts_long}=\"${setopts_default}\"; ${setopts_short}=\"${setopts_default}\""
	else
	    local -n setopts_opt_array="${setopts_long}"
	    setopts_opt_array=()
	    local -a setopts_default="$setopts_default"
	    setopts_opt_array+=("${setopts_default[@]}"); fi


	local -i n=1

	# Will toggle setopts_set to 1 if an option flag is given (set) AND has been assigned a value. 
	local -i setopts_set=0

	#setopts_end=$(
	#    args=("$@")
	#    for ((ii=0; ii<=$#; ii++)); do
	#	[[ "${args[ii]}" == '--' ]] && printf '%s' "$ii"; done )

	# CHANGEHERE
	#[[ "$setopts_end" -eq 0 ]] && setopts_endless=1 && setopts_end=$#
	setopts_end=$#
	# start at the n where we left off perhaps?
	while [[ "$n" -le $setopts_end ]]; do
	    if [[ "${*:$n:1}" = "-${setopts_short}" ]] || [[ "${*:$n:1}" = "--${setopts_long}" ]]; then
		setopts_s+=1
		if [[ "$setopts_args_num" == 0 ]]; then eval "${setopts_short}=1; ${setopts_long}=1" && setopts_set=1 && continue 2; fi
		# TODO: error msg
		local setopts_args_n=$(($n+1))
		[[ -n "${*:$setopts_args_n}" ]] || return 1

		# if 1 args then create a string variable with the
		# long-option name, or add arg to array if it's a
		# repeatable option.
		if [[ $setopts_args_num -eq 1 ]]; then
		    if [[ "$setopts_repeat" == t ]]; then
			# empty array of default value if we got an option-argument
			if [[ "${setopts_opt_array[@]}" == "${setopts_default[@]}" ]]; then
			    setopts_opt_array=(); fi
			setopts_opt_array+=("${@:$setopts_args_n:$setopts_args_num}")
		    else
			eval "${setopts_long}=\"\${@:$setopts_args_n:$setopts_args_num}\""; fi
		    # "shift" the n-position the number of args we used for this option, but not counting the option-flag
		    n+=$setopts_args_num
		    setopts_set=1
		    setopts_p+=$((setopts_args_num))
		    # if 2 or more args create an array with the args
		elif [[ $setopts_args_num -ge 2 ]]; then
		    # empty array of default value if we got an option-argument
		    if [[ "${setopts_opt_array[@]}" == "${setopts_default[@]}" ]]; then
			setopts_opt_array=(); fi
		    # Add arguments starting from setopts_args_n
		    setopts_opt_array+=("${@:$setopts_args_n:$setopts_args_num}")
		    # "shift" the n-position the number of args we used for this option, but not counting the option-flag
		    n+=$setopts_args_num
		    setopts_set=1
		    setopts_p+=$((setopts_args_num))
		else
		    printf '%s\n' "Number of arguments was set to $setopts_args_num which is invalid" >&2 && return 1
		fi
	    fi
	    n+=1
	done
	if [[ "$setopts_required" == "t" ]] && [[ ! "$setopts_set" -ge 1 ]]; then
	    # TODO: error msg
	    printf '%s\n' "Required option $setopts_long is unset" >&2
	    return 1; fi
    done

    # TODO: Create a variable that holds the number of processed args
    # by counting in above while-loop.
    local -i setopts_op_start=0

    #if [[ "${@:$((p + 1)):1}" == '--' ]]; then
    #    setopts_endless=1; fi

    if [[ $setopts_endless -eq 1 ]]; then
	setopts_op_start=1
    else
	setopts_op_start=$(( setopts_end + 2)); fi

    #local op
    #for op in "${@:$setopts_op_start}"; do
    #Operands+=("$op"); done

    # CHANGEHERE
    if [[ "${@:$((setopts_p + setopts_s + 1)):1}" == -- ]] && [[ "$setopts_s" -ge 1 ]]; then
	setopts_op_start=$((setopts_p + setopts_s + 2))
    else
	setopts_op_start=$((setopts_p + setopts_s + 1)); fi

    # NEW STUFF BELOW
    [[ ! -n "$z" ]] && mapfile -t Input < <(printf '%s' "${Input[0]}")
    #declare -p Input >&2
    local -a setopts_Ops=()

    # First check whether we have specified operands for the function.
    if [[ -n "${Operands[@]}" ]]; then

	# If we have specified operands, check whether they come from
	# stdin (Input set by calling function) or via command-line
	# ($@)
	if [[ -n "${@:$setopts_op_start}" ]]; then
	    setopts_Ops=("${@:$setopts_op_start}")
	elif [[ -n "${Input[@]}" ]]; then
	    setopts_Ops=("${Input[@]}"); fi
	
	# We can't fail here because specified operands may be optional.
	# else     
	#    printf '%s' "Operands is specified as ${Operands[*]@Q} but no operands found in stdin or on the command-line: " && bcu__stack "$@" && return 1; fi

	# With given operands in Ops, and Operand-specifications in
	# Operands, parse the provided Ops.
	local -i setopts_i=0
	local setopts_op=""
	local -i setopts_op_num=0
	local setopts_op_stat_and_var=""
	local setopts_op_stat=""
	local setopts_op_var=""
	local -a setopts_OpsRange=()

	local -i setopts_ops_count=0
	local -i setopts_remaining=${#setopts_Ops[@]}

	for ((setopts_i=0; setopts_i<${#Operands[@]}; setopts_i++)); do
	    setopts_op="${Operands[$setopts_i]}"
	    setopts_op_num="${setopts_op%%:*}"
	    setopts_op_stat_and_var="${setopts_op##$setopts_op_num:}"
	    setopts_op_stat="${setopts_op_stat_and_var%%:*}"
	    setopts_op_var="${setopts_op_stat_and_var#$setopts_op_stat:}"

	    # Check that the number of operands field is a number.
	    [[ "${setopts_op_num}" =~ ^[0-9]+$ ]] || { printf '%s' "Number of operands - $setopts_op_num - is not a valid number: " && bcu__stack "$@" && return 1; }
	    # Ensure that the variable name field is a valid bash variable name
	    [[ "$setopts_op_var" =~ ^[_[:alpha:]][_[:alpha:][:digit:]]*$ ]] || { printf '%s' "$setopts_op_var is not a valid bash variable name: " && bcu__stack "$@" && return 1; }
	    #bcu__is_valid_var_name "$setopts_op_var" || { printf '%s' "$setopts_op_var is not a valid bash variable name: " && bcu__stack "$@" && return 1; }

	    # Ensure required or not required is specified correctly
	    { [[ "${setopts_op_stat}" == t ]] || [[ "${setopts_op_stat}" == f ]]; } || { printf '%s' "Invalid Operand specification - $op: " && bcu__stack "$@" && return 1; }

	    # If we have a required Option, there must be a sufficient
	    # number of remaining operands. This is checked depending
	    # on the setopts_op_num and remaning variables in below if-clauses
	    # that sets the var-names as well.

	    # number in array minus index
	    setopts_remaining=$(( ${#setopts_Ops[@]} - setopts_ops_count ))

	    # If endless number (i.e. 0) we create array
	    if [[ "${setopts_op_num}" == 0 ]]; then
		{ [[ ! "$setopts_remaining" -ge 1 ]] && [[ "$setopts_op_stat" == t ]] ;} && { printf '%s' "Missing operands. Unable to parse ${Operands[$i]}: " && bcu__stack "$@" && return 1; }
		setopts_OpsRange=("${setopts_Ops[@]:$setopts_ops_count}")
		eval "${setopts_op_var}=(${setopts_OpsRange[*]@Q})" && break
		# If just 1 we create a variable
	    elif [[ "${setopts_op_num}" == 1 ]]; then
		{ [[ ! "$setopts_remaining" -ge 1 ]] && [[ "$setopts_op_stat" == t ]] ; } && { printf '%s' "Missing operands. Unable to parse ${Operands[$i]}: " && bcu__stack "$@" && return 1; }
		eval "${setopts_op_var}=${setopts_Ops[$setopts_ops_count]@Q}"
		setopts_ops_count+=1
		#i+=1
		# Else it is some X number and we create an array and continue
	    else
	        { [[ ! "$setopts_remaining" -ge "$setopts_op_num" ]] && [[ "$setopts_op_stat" == t ]] ; } && { printf '%s' "Missing operands. Unable to parse ${Operands[$i]}: " && bcu__stack "$@" && return 1; }
		#[[ "$setopts_remaining" -ge "$setopts_op_num" ]] || { printf '%s' "Missing operands. Unable to parse ${Operands[$i]}: " && bcu__stack "$@" && return 1; }
		setopts_OpsRange=("${setopts_Ops[@]:$setopts_ops_count:$setopts_op_num}")
		eval "${setopts_op_var}=(${setopts_OpsRange[*]@Q})"
		setopts_ops_count+=$setopts_op_num; fi; done
	#i+=$((setopts_op_num - 1)); fi; done

	# If we haven't specified operands we set the Ops to be Input
	# which is from stdin (set by calling function), else to the
	# ramining command-line operands.
    else
	if [[ -n "${Input[@]}" ]]; then
	    setopts_Ops=("${Input[@]}")
	else
	    setopts_Ops=("${@:$setopts_op_start}"); fi; fi

    # Put all Ops in Operands, because either the Input will be
    # transferred there or the command-line operands.
    Operands=("${setopts_Ops[@]}")
    #declare -p Operands >&2
}
f(){
    local z; local -a Options
    local test=(t test "YES" "testrun, no commits" 1 t t)
    local debug=(d debug "1" "debug level, 1-3")
    local time=(w time '$(date)' "debug level, 1-3" 1)
    local messages=(m messages "(hulu)" "some 3 messages" 3 t)
    Options=("(${debug[*]@Q})" "(${test[*]@Q})" "(${messages[*]@Q})" "(${time[*]@Q})")
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")
    local -a Operands
    bcu__setopts "$@"
    local -a Input
    local z; local -a Options
    if [[ ! -t 0 ]]; then    	  
	if [[ -n "$z" ]]; then
	    mapfile -t -d '' Input
	else
	    mapfile -t Input; fi
    else
	Input=("${Operands[@]}"); fi
    declare -p test debug messages null Input z time Operands
}
f_example(){
    cat <<'EOF'
Example:

    f -t NO --debug 2 -m apa bepa 'cepa depa'
    f --test NOPE

Results:

    declare -a test=([0]="NO")
    declare -- debug="2"
    declare -a messages=([0]="apa" [1]="bepa" [2]="cepa depa")
    declare -- null=""
    declare -a Input=()
    declare -- z
    declare -- time="Sat 16 Oct 2021 07:39:10 PM CEST"
    declare -a Operands=()
    Required option messages is unset
    declare -a test=([0]="NOPE")
    declare -- debug="1"
    declare -a messages=([0]="hulu")
    declare -a null=([0]="z" [1]="null" [2]="" [3]="Read null-separated operands from stdin" [4]="0")
    declare -a Input=()
    declare -- z
    (date)" [3]="debug level, 1-3" [4]="1")
    declare -a Operands
    

EOF
}
g(){
    local z; local -a Options=()
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")
    local -a Operands
    local -a Input
    bcu__setopts "$@"
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    local -a Input
    if [[ ! -t 0 ]]; then    	  
	if [[ -n "$z" ]]; then
	    mapfile -t -d '' Input
	else
	    mapfile -t Input; fi
    else
	Input=("${Operands[@]}"); fi
    [[ ! -n "${Input[*]}" ]] && { bcu__stack "$@" && return 1 ; }	    
    declare -p Input Operands
}
h(){
    local _DESCRIPTION _EXAMPLE z
    local Options=()
    local z
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Newline=(n newline "" "Append a newline to the string you want to prepend" 0)
    local File=(f file "" "File to append to" 1)
    Options+=("(${null[*]@Q})" "(${Newline[*]@Q})" "(${File[*]@Q})")
    local -a Operands=(1:t:op1 8:t:MyOps)
    local -a Input
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: " && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    [[ ! -n "${Input[*]}" ]] && { bcu__stack "$@" && return 1 ; }
    declare -p Input Operands op1 MyOps
}
setopts_test_1()(
    . <(f -m apa bepa -t no)
    [[ "${test}" == no ]] || { echo FAIL && return 1 ; }
    echo PASS && return 0
)
setopts_test_2()(
    . <(f --test NOPE -m a b)
    [[ "$test" == NOPE ]] || { echo FAIL && return 1 ;}
    echo PASS && return 0
)
setopts_test_3()(
    . <(f -t NO --debug 2 -m apa bepa 'cepa depa')
    [[  "${#messages[@]}" == 3 ]] || { echo FAIL && return 1; }
    [[  "${messages[0]}" == apa ]] || { echo FAIL && return 1; }
    [[  "${messages[1]}" == bepa ]] || { echo FAIL && return 1; }
    [[  "${messages[2]}" == "cepa depa" ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
setopts_test_4()(
    . <(g apa bepa)
    [[ "${Input[0]}" == apa ]] || { echo FAIL && return 1; }
    . <(g -- apa bepa)
    { [[ "${Input[0]}" == -- ]] && [[ "${Input[1]}" == apa ]] ;} || { echo FAIL && return 1; }
    . <(g -z -- 0 -- apa bepa)
    [[ "${Input[0]}" == 0 ]] || { echo FAIL && return 1; }
    [[ "${Input[1]}" == -- ]] || { echo FAIL && return 1; }
    [[ "${Input[2]}" == apa ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
setopts_test_5()(
    . <(f -t yes -t no -m a b c -m d e f)
    [[ "${#messages[@]}" == 6 ]] || { echo FAIL && return 1; }
    [[ "${#test[@]}" == 2 ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
setopts_test_6()(
    local op1 a
    local -a MyOps 
    . <(printf '%b\n' 'apa\n bepa\n' cepa lala lolo a b c | h -n -- )
    [[ "${op1}" == apa ]] || { echo FAIL && return 1; }
    [[ "${#MyOps[@]}" == 8 ]] || { echo FAIL && return 1; }
    a=$(if printf '%b\n' 'apa\n bepa\n' cepa lala lolo a b | h -n -- >/dev/null; then echo true; else echo false; fi)
    [[ "$a" == false ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
setopts_test_main(){
    printf setopts_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )setopts_test_1(\ |$) ]]; then setopts_test_1; else echo DISABLED; fi
    printf setopts_test_2.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )setopts_test_2(\ |$) ]]; then setopts_test_2; else echo DISABLED; fi
    printf setopts_test_3.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )setopts_test_3(\ |$) ]]; then setopts_test_3; else echo DISABLED; fi
    printf setopts_test_4.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )setopts_test_4(\ |$) ]]; then setopts_test_4; else echo DISABLED; fi
    printf setopts_test_5.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )setopts_test_5(\ |$) ]]; then setopts_test_5; else echo DISABLED; fi
    printf setopts_test_6.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )setopts_test_6(\ |$) ]]; then setopts_test_6; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	setopts_test_main "${org_args[@]}"
    else
	setopts_test_main "${@}"
    fi
fi
. bcu.sh
bcu__die(){
    local _STACK z Options=() Input=() return_code arg
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
die() prints a rather typical stacktrace and exits with given return code


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    dietest()( bcu__die -e 12 -- "$@"  ; ) ; dietest a b c
    echo $?

Results:

    'dietest' defined in main invoked on line 334 in main,
     'bcu__die' defined in misc.sh invoked on line 44 in main, with the args: 'a' 'b' 'c'
    12

EOF
		)
    }    
    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)    
    local Exit=(e return_code "1" "Exit code. Default is 1" 1)
    Options+=("(${null[*]@Q})" "(${Exit[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:f:arg)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || {
	printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    # Check args
    [[ "$return_code" -lt 256 ]] || { printf '%s' "Exit code is not an integer: ${_STACK}" && bcu__stack "$@" && return 1; }

    bcu__stack "${arg[@]}"
    exit "${return_code}"
}
die_test_f(){
    local a b
    dietest()( bcu__die -e 12 -- "$@"  ; )
    a=$(dietest a b c)
    b=$(echo $?)
    declare -p a b 
}
die_test_1()(
    . <(die_test_f)
    [[ "${a}" =~ .*"'die_test_f' defined in".* ]] || { echo FAIL && return 1; }
    [[ "${b}" == "12" ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
die_test_main(){
    printf die_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )die_test_1(\ |$) ]]; then die_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	die_test_main "${org_args[@]}"
    else
	die_test_main "${@}"
    fi
fi
. bcu.sh
bcu__esc_sd_val(){
    local _STACK z Options=() Input=() value_string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(
escape SD-value


		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__esc_sd_val 'apa]bepa"cepa\depa' && echo

Results:

    apa\]bepa\"cepa\\depa

EOF
		)
    }    
    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)    
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:value_string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || {
        printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    # Check args
    printf '%s' "$value_string" | sed 's/[]"\]/\\&/g'
}
esc_sd_val_test_f(){
    local a
    a=$(bcu__esc_sd_val 'apa]bepa"cepa\depa')
    declare -p a
}
esc_sd_val_test_1()(
    . <(esc_sd_val_test_f)
    [[ "${a}" == 'apa\]bepa\"cepa\\depa' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
esc_sd_val_test_main(){
    printf esc_sd_val_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )esc_sd_val_test_1(\ |$) ]]; then esc_sd_val_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	esc_sd_val_test_main "${org_args[@]}"
    else
	esc_sd_val_test_main "${@}"
    fi
fi
. bcu.sh
bcu__sd_elem(){
    local _STACK z Options=() Input=()
    local id attribute ii name val
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Build a correctly formatted SD-element


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__sd_elem --id "apa@bepa" -a cepa depa -a epa 'fepa]"\' && echo

Results:

    [apa@bepa cepa="depa" epa="fepa\]\"\\"]

EOF
		)
    }    
    # Options
    local id=(i id "" "an sdid, e.g. exampleSDID@32473" 1 f f)
    local attribute=(a attribute "" 'an attribute name and value, e.g. eventSource Application' 2 t t)
    Options=("(${id[*]@Q})" "(${attribute[*]@Q})")
    local null=(z null "" "Read null-separated operands from stdin" 0)    
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=()

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || {
	printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    # Check args, TODO

    printf '[%s' "$id"
    for ((ii=0; ii<${#attribute[@]}; ii+=2)); do
	name="${attribute[ii]}"
	val="${attribute[$((ii+1))]}"
	val=$(bcu__esc_sd_val "$val")
	printf ' %s="%s"' "$name" "$val"; done
    printf '%s]' ""
}
sd_elem_test_f(){
    local a
    a=$(bcu__sd_elem --id "apa@bepa" -a cepa depa -a epa 'fepa]"\')
    declare -p a
}
sd_elem_test_1()(
    . <(sd_elem_test_f)
    [[ "${a}" == '[apa@bepa cepa="depa" epa="fepa\]\"\\"]' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
sd_elem_test_main(){
    printf sd_elem_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )sd_elem_test_1(\ |$) ]]; then sd_elem_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	sd_elem_test_main "${org_args[@]}"
    else
	sd_elem_test_main "${@}"
    fi
fi
. bcu.sh
bcu__log(){
    local _STACK z Options=() Input=()
    local facility severity version timestamp hostname application process_id message_id logfile structured_data
    local Facility Severity Version Timestamp Hostname Application Process_Id Message_Id Logfile Structured_Data
    local time host appname pid msgid


    local NILVALUE; NILVALUE="-"
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
log is an application logger for Bash adhering to RFC5424 (or attempts
to) and ensures that logs written to logfiles happen
sequentially. Sane defaults are provided as well as option flags to
override them.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__log -a myapp --msgid 45 | sed "s/$(printf '\357\273\277')/BOM/"
    bcu__log --msgid 45 -a myapp -- aa | sed "s/$(printf '\357\273\277')/BOM/"
    bcu__log -a myapp -d "$(bcu__sd_elem --id "apa@bepa" -a apa 'bepa\[]cepa"' -a test YES -a priority 154 -a user allan)" -- This is a test | sed "s/$(printf '\357\273\277')/BOM/"

Results:

    <15>1 2019-09-29T21:43:44.838283+0200 librem13v3guixsd myapp 23920 45 -
    <15>1 2019-09-29T21:43:44.886824+0200 librem13v3guixsd myapp 23920 45 - BOMaa
    <15>1 2019-09-29T21:43:45.002602+0200 librem13v3guixsd myapp 23920 - [apa@bepa apa="bepa\\[\]cepa\"" test="YES" priority="154" user="allan"] BOMThis is a test

EOF
		)
    }    
    # Options
    #myoption=(short long default "Description" argsnum required repeatable predicate)~
    Facility=(f facility '$(printf "%s" "${_BCU_LOG_DEFAULT_FACILITY:-1}")' "Facility code, a number in the range of 0-23. Set and export _LOG_DEFAULT_FACILITY to override the default value" 1)
    Severity=(s severity '$(printf "%s" "${_BCU_LOG_DEFAULT_SEVERITY:-7}")' "Facility code, a number in the range of 0-23. Set and export _LOG_DEFAULT_SEVERITY to override the default value" 1)
    Version=(v version '$(printf "%s" "${_BCU_LOG_DEFAULT_VERSION:-1}")' "What syslog version. Set and export _LOG_DEFAULT_VERSION to override the default value" 1)
    Timestamp=(t time '$(date +%Y-%m-%dT%H:%M:%S.%6N%z)' "Time that we created the log message" 1)
    Hostname=(h host '$(printf "%s" "$HOSTNAME")' "The machine we are logging for" 1)
    Application=(a appname '$(echo "${_BCU_LOG_APPNAME:-${BASH_SOURCE[0]}}")' "Name of the application we are logging for. Default is the variable set for _BCU_LOG_APPNAME and otherwise it is \${BASH_SOURCE[0]}" 1)
    Process_Id=(p pid '$(printf "%s" $$)' 'The current process id. Will use parent PID ($$) by default' 1)
    Message_Id=(m msgid '$(printf "%s" "$NILVALUE")' 'The MSGID SHOULD identify the type of message. For example, a firewall might use the MSGID "TCPIN" for incoming TCP traffic and the MSGID "TCPOUT" for outgoing TCP traffic. Messages with the same MSGID should reflect events of the same semantics. The MSGID itself is a string without further semantics. It is intended for filtering messages on a relay or collector.' 1)
    Logfile=(l logfile '$(echo "${_BCU_LOG_LOGFILE}")' "Save log message to file instead of printing it to stdout" 1)
    Structured_Data=(d structured_data "" "Structured data, preferably created with the sdelem function" 1 f t)

    Options=("(${Facility[*]@Q})" "(${Severity[*]@Q})" "(${Version[*]@Q})" "(${Timestamp[*]@Q})" "(${Hostname[*]@Q})" "(${Application[*]@Q})" "(${Process_Id[*]@Q})" "(${Message_Id[*]@Q})" "(${Logfile[*]@Q})" "(${Structured_Data[*]@Q})")

    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:f:msg)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || {
	printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    # Check args
    Input=("${msg[@]}")
    # local -i i
    # for ((i=1;i<=$#;i++)); do
    #     eval "narg=\"\${$i}\""
    #     if [[ "${narg}" == '--' ]]; then
    # 	    i+=1 && Input=("${@:$i}"); fi; done; fi

    local priority
    priority=$(( facility * 8 + severity ))

    local log_string
    read -r log_string < <(
	printf "<%s>%s %s %s %s %s %s " "$priority" "$version" "$time" "$host" "$appname" "$pid" "$msgid"
	if [[ -n "${structured_data[*]}" ]]; then
	    local data
	    for data in "${structured_data[@]}"; do
		printf '%s' "${data} "; done
	else
	    printf '%s' "- "; fi
    )

    # atomic writes since we might be calling log() in parallell
    [[ -n "$logfile" ]] && {
	(
	    flock 200
	    #printf '%s' "${log_string} BOM" $'\xEF\xBB\xBF' "${Input[*]}" $'\n' >> "$logfile"
	    if [[ -n "${Input[*]}" ]]; then
		printf '%s' "${log_string} " $'\xEF\xBB\xBF' "${msg[*]}" $'\n' >> "$logfile"
	    else
		printf '%s' "${log_string}" $'\n' >> "$logfile"; fi
	    # Following seds will find the BOM.
	    # printf '%s' apa $'\xEF\xBB\xBF' bepa | sed "s/$(printf '\357\273\277')/BOM/"
	    # sed "s/$(printf '\357\273\277')/BOMMATCH/" somefile 

	    #printf '%s\xEF\xBB\xBF%s' "" "${Input[*]}" $'\n'
	    #printf '%s\xef\xbb\xbf%s' "" "${Input[*]}" $'\n'

	) 200>/tmp/bcu__log_lock #200>~/.config/bash-coding-utils/bcu__log_lock
	return 0
    }

    if [[ -n "${Input[*]}" ]]; then
	printf '%s' "${log_string} " $'\xEF\xBB\xBF' "${msg[*]}" $'\n'
    else
	printf '%s' "${log_string}" $'\n'; fi

    #printf '%s'  >> "${logfile}"
    #printf '%s' "${log_string} BOM" "${Input[*]}" $'\n'
    #printf '%s' "${log_string}" $'\xEF\xBB\xBF' "${Input[*]}" $'\n'
    #printf '%s\xEF\xBB\xBF%s' "" "${Input[*]}" $'\n'
    #printf '%s\xef\xbb\xbf%s' "" "${Input[*]}" $'\n'

    #### OLD ###    
    # local msg
    # if ! [[ "$1" =~ (INFO|WARNING|ERROR) ]]; then
    #     printf '%s' "[ERROR]: " "log(): args: " "$@" $'\n'| tee -a "$logf"
    #     return 1; fi
    # if [[ -n "$2" ]]; then
    #     msg="[${1}]: ${2}\n"
    # else
    #     while read -r msgline; do
    # 	  msg+="[${1}]: ${msgline}\n"; done; fi
    # if [[ -n "$msg" ]]; then
    #     case "$1" in
    # 	  INFO|WARNING) printf '%b' "$msg" >> "$logf" ;;
    # 	      ERROR) printf '%b' "$msg" | tee -a "$logf" ;; esac ; fi
}
log_test_f(){
    local a b c 
    a=$(bcu__log -a myapp --msgid 45 -h localhost --pid 896 -t '2019-08-31T20:13:27.612745+0200' | sed "s/$(printf '\357\273\277')/BOM/")
    b=$(bcu__log --msgid 45 -a myapp -h localhost --pid 896 -t '2019-08-31T20:13:27.723119+0200' -- aa | sed "s/$(printf '\357\273\277')/BOM/")
    c=$(bcu__log -a myapp --pid 896 -h localhost -d "$(bcu__sd_elem --id "apa@bepa" -a apa 'bepa\[]cepa"' -a test YES -a priority 154 -a user allan)" -t '2019-08-31T20:13:27.927659+0200' -- This is a test | sed "s/$(printf '\357\273\277')/BOM/")
    declare -p a b c
}
log_test_1()(
    . <(log_test_f)
    [[ "${a}" == '<15>1 2019-08-31T20:13:27.612745+0200 localhost myapp 896 45 -' ]] || { echo FAIL && return 1; }
    [[ "${b}" == '<15>1 2019-08-31T20:13:27.723119+0200 localhost myapp 896 45 - BOMaa' ]] || { echo FAIL && return 1; }
    [[ "${c}" == '<15>1 2019-08-31T20:13:27.927659+0200 localhost myapp 896 - [apa@bepa apa="bepa\\[\]cepa\"" test="YES" priority="154" user="allan"] BOMThis is a test' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
log_test_main(){
    printf log_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )log_test_1(\ |$) ]]; then log_test_1; else echo DISABLED; fi
} 
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	log_test_main "${org_args[@]}"
    else
	log_test_main "${@}"
    fi
fi
. bcu.sh
bcu__timeout()(
    local _STACK z Options=() Input=() cmd spid rpids=()
    local -a Args
    [[ "$1" =~ (-h|--help) ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Run a command and kill it if it reaches a timeout. bcu__timeout can
run bash functions and preserves the exit status of the command in
case it completes before the timeout. bcu__timeout returns exit code
126 if the timeout is reached.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    f(){ printf '%s ' fstart ; sleep 2; printf '%s ' fdone ;}
    g()( printf '%s ' gstart ; sleep 2; printf '%s ' gdone ;)
    bcu__timeout 1 f ; echo $? ; echo
    bcu__timeout 3 f ; echo $? ; echo
    bcu__timeout 1 g ; echo $? ; echo 
    bcu__timeout 3 g ; echo $? ; echo

Results:

    fstart fdone 126
    fstart fdone 0
    gstart 126
    gstart gdone 0

EOF
		)
    }    
    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)    
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:s 1:t:cmd 0:f:Args)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || {
	printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ (-h|--help) ]] && return 0

    # Check args
    [[ "$s" =~ ^[0-9]+$ ]] || { printf '%s' "First arg is not a number. Should be the number of seconds before timeout of command: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$(type -t $cmd)" =~ ^(function|file)$ ]] || { printf '%s' "The cmd argument must be either a function or a file that is in the PATH environment variable: ${_STACK}" && bcu__stack "$@" && return 1; }

    list_descendants(){
	local children=$(ps -o pid= --ppid "$1")
	local pid
	for pid in $children ; do
	    list_descendants "$pid" ; done
	printf '%s ' "$children" ;} ;
    set +b ;

    # Run the timeout as a background process
    # save it's process id
    sleep "$s" &
    spid="$!"

    # Run the command as a background process
    "${cmd}" "${Args[@]}" &

    # Wait until either background job changes status
    wait -n
    
    # Record exit status of the background job that completed first
    local -i ecode=$?

    # If a running process id matches the sleep command, kill the
    # sleep process and we can assume that the exit code of the
    # command is what got stored in ecode. Then exit with that ecode.
    rpids=($(jobs -p))
    for p in "${rpids[@]}" ; do
	[[ "$p" == "$spid" ]] && \
	    kill -9 "${spid}" &>/dev/null && \
	    exit $ecode ; done

    # If no running pid matches the sleep command's pid, we have
    # reached the timout and we can kill the stuck process, and it's
    # descendants. We then exit 126, to indicate that the other command
    # did not finish. ref: https://shapeshed.com/unix-exit-codes/#what-exit-code-should-i-use
    kill -9 $(list_descendants "${rpids[@]}")
    exit 126
)
timeout_test_f()(
    local a b
    acmd()
    {
	( echo apa;
	  sleep 7 & sleep 10;
	  echo bepa;
	  exit 7 )
    }
    a=$(bcu__timeout 13 acmd &>/dev/null; echo $?)
    b=$(bcu__timeout 1 acmd &>/dev/null; echo $?)
    declare -p a b
)
timeout_test_1()(
    . <(timeout_test_f)
    [[ "${a}" == 7 ]] || { echo FAIL && return 1; }
    [[ "${b}" == 126 ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
timeout_test_main(){
    printf timeout_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )timeout_test_1(\ |$) ]]; then timeout_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	timeout_test_main "${org_args[@]}"
    else
	timeout_test_main "${@}"
    fi
fi
. bcu.sh
bcu__base64(){
    local _STACK z Options=() Input=() string s
    local -a Res=()
    [[ "$1" =~ (-h|--help) ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Encode or decode a string to or from base64 encoding


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__base64 abcd

Results:

    YWJjZA==

EOF
		)
    }    
    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Encode=(e encode "" "Encode operands in base64. This is the default" 0)
    local Decode=(d decode "" "Decode operands in base64" 0)
    Options+=("(${Encode[*]@Q})" "(${Decode[*]@Q})" "(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || {
    printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ (-h|--help) ]] && return 0

    # Check args
    if ! command -v scm &>/dev/null; then
	enable -f "$GUIX_PROFILE"/lib/bash/libguile-bash.so scm &>/dev/null || { 
	    printf '%s' "Enabling guile-bash failed: " && bcu__stack "$@" && return 1; }; fi

    if [[ -n "$decode" ]]; then
	command -v scm-decode_from_base64 &>/dev/null \
	    || scm "${_misc_MOD_DIR}"/base64_guile_bash.scm &>/dev/null
	#    || scm "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"/base64_guile_bash.scm &>/dev/null
	for s in "${string[@]}"; do
	    Res+=("$(scm-decode_from_base64 "$s")"); done #2>/dev/null
    else
	command -v scm-encode_to_base64 &>/dev/null \
	    || scm "${_misc_MOD_DIR}"/base64_guile_bash.scm &>/dev/null
	#    || scm "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"/base64_guile_bash.scm &>/dev/null
	for s in "${string[@]}"; do
	    Res+=( "$(scm-encode_to_base64 "$s")" ); done #2>/dev/null
    fi
    if [[ -n "$null" ]]; then
	printf '%s\0' "${Res[@]}"
    else
	printf '%s\n' "${Res[@]}"; fi
}
base64_test_f(){
    local a
    a=$(bcu__base64 abcd)
    declare -p a
}
base64_test_1()(
    . <(base64_test_f)
    [[ "${a}" == 'YWJjZA==' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
base64_test_main(){
    printf base64_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )base64_test_1(\ |$) ]]; then base64_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	base64_test_main "${org_args[@]}"
    else
	base64_test_main "${@}"
    fi
fi
. bcu.sh
bcu__parallel(){
bashp(){
    if [[ "$1" == --help ]] || [[ "$1" == -h ]]; then
	cat <<'EOF'
Usage: bashp [OPTIONS...] [<command>] [<argsmap>] [<args>]

Description:
Bash parallel can be used to parallelize bash functions or other
commands. It returns results as a bash array or json document
containing; each executed command and command arguments, stdout, stderr
and the return-code. It can be used in a nested way, see example.

Options:
	-h | --help		Display this help and exit
	-n | --size		[Optional] [Args:1] Number of jobs in parallel. Defaults to number of cpu's.
	-j | --json-args	[Optional] [Args:1] Read <command>, <argsmap>, and <args> from a json document, see example below.
	-k | --json-argsfile	[Optional] [Args:1] Same as --json-args but read it from a file.
	-p | --json-out		[Optional] [Args:1] Print results as a json document.
	-f | --argsfile		[Optional] [Args:1] Read the <args> bash array from a file or filedescriptor.

Example:

    alo(){ sleep 2 ; printf '%s\n' "$@" ; }
    Args=(apa bepa cepa '!@#$!$@%@$&$%*^$&(&#*@%$^!#$~')
    ArgsMap=(one {} {})
    declare -a Results=$(bashp --size 2 -- alo "(${ArgsMap[*]@Q})" "(${Args[*]@Q})")
    declare -a res1="${Results[1]}"
    declare -a res1_CommandAndArgs="${res1[0]}"
    res1_Command="${res1_CommandAndArgs[0]}"
    declare -a res1_CommandArgs="${res1_CommandAndArgs[1]}"
    res1_stdout="${res1[1]}"
    res1_stderr="${res1[2]}"
    res1_ret_code="${res1[3]}"
    alo2(){ bashp --json-args '{"command":"alo","args":'"$(jq -n '$ARGS.positional' --args "$@")"',"args-map":["{}","{}"]}' --json-out --size 2 ; }
    declare -p res1_Command res1_CommandArgs res1_stdout res1_stderr res1_ret_code && bashp --json-args '{"command":"alo2","args":["apa","bepa","cepa","depa"],"args-map":["staticarg","{}"]}' --size 2 --json-out | jq -r '[.[0,1].stdout|fromjson[]]'

Results:

    declare -- res1_Command="alo"
    declare -a res1_CommandArgs=([0]="one" [1]="cepa" [2]="!@#\$!\$@%@\$&\$%*^\$&(&#*@%\$^!#\$~")
    declare -- res1_stdout="one
    cepa
    !@#\$!\$@%@\$&\$%*^\$&(&#*@%\$^!#\$~
    "
    declare -- res1_stderr=""
    declare -- res1_ret_code="0"
    [
      {
        "command": "'alo' 'staticarg' 'apa'",
        "stdout": "staticarg\napa\n",
        "stderr": "",
        "return-code": "0"
      },
      {
        "command": "'alo' 'staticarg' 'bepa'",
        "stdout": "staticarg\nbepa\n",
        "stderr": "",
        "return-code": "0"
      }
    ]
EOF
	return 0
    fi
    local serial jsonargs json size argsfile
    while [[ -n "${1}" ]]; do
	if [[ "$1" == -o ]] || [[ "$1" == --serial ]]; then
	    serial="$1"
	    shift
	elif [[ "$1" == -j ]] || [[ "$1" == --json-args ]]; then
	    jsonargs="$2"
	    shift 2
	elif [[ "$1" == -k ]] || [[ "$1" == --json-argsfile ]]; then
	    jsonargs=$(jq -c . "$2") || { printf '%s\n' "Failed to to use $2 as json-argsfile" && return 1 ; }
	    shift 2
	elif [[ "$1" == -f ]] || [[ "$1" == --argsfile ]]; then
	    argsfile=$(cat "$2") || { printf '%s\n' "Failed to to use $2 as argsfile" && return 1 ; }
	    shift 2
	elif [[ "$1" == -p ]] || [[ "$1" == --json-out ]]; then
	    json="$1"
	    shift
	elif [[ "$1" == -n ]] || [[ "$1" == --size ]]; then
	    local -i size="$2" || return 1
	    shift 2
	elif [[ "$1" == -- ]]; then
	    shift && break
	else
	    break
	fi
    done

    if [[ -n "$argsfile" ]]; then
	local -a Args=$(printf '%s' "$argsfile") || { printf '%s\n' "Failed to to use $2 as argsfile" && return 1 ; }
	local command="$1"
	local -a CommandArgs="$2" || { printf '%s\n' "Failed to to use $2 as arguments map. It's not a valid bash list." && return 1 ; }
    elif [[ -n "$jsonargs" ]]; then
	bashp_jsonStripQuotes(){ local t0; while read -r t0; do t0="${t0%\"}"; t0="${t0#\"}"; printf '%s\n' "$t0"; done < <(jq '.');}
	command=$(jq -c '.command' <<<"$jsonargs" | bashp_jsonStripQuotes)
	mapfile -t Args < <(jq -c '.args[]' <<<"$jsonargs" | bashp_jsonStripQuotes)
	mapfile -t CommandArgs < <(jq -c '."args-map"[]' <<<"$jsonargs" | bashp_jsonStripQuotes)
	unset bashp_jsonStripQuotes
    else
	local command="$1"
	local -a CommandArgs="$2" || { printf '%s\n' "Failed to to use $2 as arguments map. It's not a valid bash list." && return 1 ; }
    fi
    [[ -n "${Args[@]}" ]] || { local -a Args="$3" || { printf '%s\n' "Failed to to use $3 as arguments. It's not a valid bash list." && return 1 ; } ; }

    # debug
    #declare -p Args command CommandArgs jsonargs serial json argsfile
    #return 0

    bashp_results_to_json(){
	local stdouttmp; stdouttmp=$(mktemp /tmp/temp.XXXXXXXX) || { printf '%s\n' "mapfork: Failed to create tempfile" && return 1 ; }
	local stderrtmp; stderrtmp=$(mktemp /tmp/temp.XXXXXXXX) || { printf '%s\n' "mapfork: Failed to create tempfile" && return 1 ; }
	local rettmp; rettmp=$(mktemp /tmp/temp.XXXXXXXX) || { printf '%s\n' "mapfork: Failed to create tempfile" && return 1 ; }
	local argstmp; argstmp=$(mktemp /tmp/temp.XXXXXXXX) || { printf '%s\n' "mapfork: Failed to create tempfile" && return 1 ; }
	# TODO: remove /tmp/mapfork_write_lock_$PPID"
	trap 'for a in $stdouttmp $stderrtmp $rettmp $argstmp ; do rm $a; done' EXIT SIGTERM RETURN
	local -a Results="$1" || return 1
	for ((i=0;i<${#Results[@]};i++)); do
	    local -a result="${Results[$i]}"
	    local -a cmd_args="${result[0]}"
	    cmd="${cmd_args[0]}"
	    local -a args="${cmd_args[1]}"
	    #declare -p result > /tmp/result-$$
	    printf '%s' "${result[1]}" > $stdouttmp
	    printf '%s' "${result[2]}" > $stderrtmp
	    printf '%s' "${result[3]}" > $rettmp
	    printf '%s' "${cmd[0]@Q} " "${args[*]@Q}" > $argstmp
	    jq -c -n --unbuffered \
	       --rawfile stdout $stdouttmp \
	       --rawfile stderr $stderrtmp \
	       --rawfile ret $rettmp \
	       --rawfile args $argstmp \
	       '. | { command: $args , stdout: $stdout , stderr: $stderr , "return-code": $ret }'; done | jq -Mcsj .
	return 0
    }

    local -a PipedArr
    local -i i

    # Open a fifo and keep it open until done
    local myfifo; myfifo=$(mktemp /tmp/temp.XXXXXXXX) || { printf '%s\n' "mapfork: Failed to create tempfile" && return 1 ; }
    [[ -e "$myfifo" ]] && rm "$myfifo" ; mkfifo "$myfifo"
    trap '[[ -e $myfifo ]] && rm $myfifo' EXIT SIGTERM RETURN
    sleep infinity > "$myfifo" &
    spid=$!

    local -a placeHolders=()
    for ((i=0;i<${#CommandArgs[@]};i++)); do
	[[ "${CommandArgs[$i]}" =~ ^\{\}$ ]] && placeHolders+=("$i") ;done

    local waitpid
    local -i myfd
    local -i myfd2
    local myfifo2; myfifo2=$(mktemp /tmp/temp.XXXXXXXX) || { printf '%s\n' "mapfork: Failed to create tempfile" && return 1 ; }
    rm "$myfifo2" && mkfifo "$myfifo2" && exec {myfd2}<>"$myfifo2" && rm "$myfifo2"
    # reference: https://unix.stackexchange.com/questions/103920/parallelize-a-bash-for-loop/216475#216475
    bashp_open_sem(){
	local myfifo3; myfifo3=$(mktemp /tmp/temp.XXXXXXXX) || { printf '%s\n' "mapfork: Failed to create tempfile" && return 1 ; }
	rm "$myfifo3" && mkfifo "$myfifo3"
	exec {myfd}<>"$myfifo3"
	rm "$myfifo3"
	local i=$1
	for((;i>0;i--)); do
	    printf '%s' 0 >&$myfd
	done
    }
    # reference: https://stackoverflow.com/questions/13806626/capture-both-stdout-and-stderr-in-bash
    bashp_nullWrap(){
	local -i i; i="$1"
	local myCommand="$2"
	local -a myCommandArgs="$3"
	local -a cmdAndArgs=("$myCommand" "(${myCommandArgs[*]@Q})")
	local myfifo="$4"
	local waitpid="$5"
	local stderr
	local stdout
	local stdret
	[[ -n "$5" ]] && until [[ ! -e /proc/"$waitpid" ]]; do sleep .2 ; done
	. <(\
	    { #stderr=$({ stdout=$(eval "$myCommand ${myCommandArgs[*]@Q}"); stdret=$?; } 2>&1 ;\
		stderr=$({ mapfile -d '' stdout < <(eval "$myCommand ${myCommandArgs[*]@Q}"); stdret=$?; } 2>&1 ;\
	               declare -p stdout >&2 ;\
		       declare -p stdret >&2) ;\
	      declare -p stderr;\
	    } 2>&1)
	local -a Arr=("(${cmdAndArgs[*]@Q})")
	Arr+=( "${stdout[0]}" )
	Arr+=("$stderr" "$stdret")
	#declare -p Arr > /tmp/resultarr-$$
	# For testing serial vs parallel:
	#printf '%s\n' "waitpid is $waitpid , cmdAndArgs: ${cmdAndArgs[@]}" >> logfile
	[[ -z "$5" ]] && printf '%s' 0 >&$myfd
	eval '( flock -x $myfd2 ; printf "${i}:%s\u0000" "(${Arr[*]@Q})" > "$myfifo" ; ) '"$myfd2"'>$bashpwritelocktmp'
    }
    local bashpwritelocktmp; bashpwritelocktmp=$(mktemp /tmp/temp.XXXXXXXX) || { printf '%s\n' "mapfork: Failed to create tempfile" && return 1 ; } 
    local -i cmdnum=0
    local ncpu x; ncpu=$(nproc)

    bashp_open_sem "${size:-$ncpu}"
    unset bashp_open_sem

    trap '[[ -e $bashpwritelocktmp ]] && rm $bashpwritelocktmp' EXIT SIGTERM RETURN

    for ((i=0;i<${#Args[@]};i+=0)); do
	# if we have placeholders in CommandArgs we need to take args
	# from Args to replace.
	if [[ ${#placeHolders[@]} -gt 0 ]]; then
	    for ii in "${placeHolders[@]}"; do
		CommandArgs["$ii"]="${Args[$i]}"
		i+=1; done; fi
	if [[ "$serial" = --serial ]] ; then
	    bashp_nullWrap "$i" "$command" "(${CommandArgs[*]@Q})" "$myfifo" "$waitpid" &
	    waitpid=$!
	    cmdnum+=1
	else
	    read -u $myfd -n 1 x
	    bashp_nullWrap "$i" "$command" "(${CommandArgs[*]@Q})" "$myfifo" &
	    cmdnum+=1
	fi
    done
    for ((i=0;i<$cmdnum;i+=1)) ; do
	local res
	# TODO: fix - may not need to create subshell
	#res=$(read -r -d '' temp <"$myfifo" && printf '%b' "$temp")
	res=$(read -r -d '' temp <"$myfifo" && echo -E "$temp")
	#declare -p res > /tmp/resultres-$$
	local -i resI
	resI="${res%%:*}"
	PipedArr[$resI]="${res#*:}"
    done
    kill -9 "$spid" # &>/dev/null
    wait "$spid" 2>/dev/null
    [[ -e "$myfifo" ]] && rm "$myfifo"
    unset bashp_nullWrap
    # reference: https://stackoverflow.com/questions/41966140/how-can-i-make-an-array-of-lists-or-similar-in-bash
    if [[ -n "$json" ]]; then
	#declare -p PipedArr > /tmp/resultpipedarr-$$
	if bashp_results_to_json "(${PipedArr[*]@Q})"; then
	    eval "exec $myfd>&-"
	    eval "exec $myfd2>&-"
	    unset bashp_results_to_json
	    rm "$bashpwritelocktmp"
	    return 0
	else
	    eval "exec $myfd>&-"
	    eval "exec $myfd2>&-"
	    unset bashp_results_to_json
	    rm "$bashpwritelocktmp"
	    return 1; fi
    else
	eval "exec $myfd>&-"
	eval "exec $myfd2>&-"
	unset bashp_results_to_json
	rm "$bashpwritelocktmp"
	printf '%s' "(${PipedArr[*]@Q})"; fi
}
    bashp "$@"
}
parallel_test_f(){
    local a
    alo(){ sleep 2 ; printf '%s\n' "$@" ; }
    local -a Args=(apa bepa cepa '!@#$!$@%@$&$%*^$&(&#*@%$^!#$~')
    local -a ArgsMap=(one {} {})
    local -a Results=$(bashp --size 2 -- alo "(${ArgsMap[*]@Q})" "(${Args[*]@Q})")
    local -a res1="${Results[1]}"
    local -a res1_CommandAndArgs="${res1[0]}"
    local res1_Command="${res1_CommandAndArgs[0]}"
    local -a res1_CommandArgs="${res1_CommandAndArgs[1]}"
    local res1_stdout="${res1[1]}"
    local res1_stderr="${res1[2]}"
    local res1_ret_code="${res1[3]}"
    alo2(){ bashp --json-args '{"command":"alo","args":'"$(jq -n '$ARGS.positional' --args "$@")"',"args-map":["{}","{}"]}' --json-out --size 2 ; }
    a=$(bcu__parallel --json-args '{"command":"alo2","args":["apa","bepa","cepa","depa"],"args-map":["staticarg","{}"]}' --size 2 --json-out | jq -r '[.[0,1].stdout|fromjson[]]|last|.command')
    unset alo
    unset alo2
    declare -p a
}
parallel_test_1()(
    . <(parallel_test_f)
    [[ "${a}" == "'alo' 'staticarg' 'bepa'" ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
parallel_test_main(){
    printf parallel_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )parallel_test_1(\ |$) ]]; then parallel_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
        parallel_test_main "${org_args[@]}"
    else
	parallel_test_main "${@}"
    fi
fi
. bcu.sh
bcu__docs()(
    local _STACK z Options=() Input=() browser
    [[ "$1" =~ (-h|--help) ]] && {
        local _DESCRIPTION _EXAMPLE
        # shellcheck disable=SC2034
        _DESCRIPTION=$(cat <<'EOF'
Open the full BCU documentation in a web-browser.


EOF
	)
        # shellcheck disable=SC2034
        _EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__docs icecat

Results:

    /home/user1/.guix-profile/bin/icecat
    

EOF
	)
    }
    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:f:browser)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || {
        printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ (-h|--help) ]] && return 0

    # Check args
    if [[ -n "$browser" ]]; then
	command -v "$browser" || { printf '%s' "Unable to find $browser in your PATH: ${_STACK}" && bcu__stack "$@" && return 1; }
	eval "$browser ${_BCU_SH_DIR}/docs/bcu.html"
    else
	command -v xdg-open || { printf '%s' "Unable to find a default browser: ${_STACK}" && bcu__stack "$@" && return 1; }
	xdg-open "${_BCU_SH_DIR}/docs/bcu.html"
    fi
)
docs_test_f(){
    local a
    a=$(command -v bcu__docs &>/dev/null && echo true)
    declare -p a
}
docs_test_1()(
    . <(docs_test_f)
    [[ "${a}" == true ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
docs_test_main(){
    printf docs_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )docs_test_1(\ |$) ]]; then docs_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
        docs_test_main "${org_args[@]}"
    else
	docs_test_main "${@}"
    fi
fi
local pydpid="$(cat /tmp/pydaemon/$_BCU_PYDAEMON_CHANNEL.sock.pid )"
kill "$pydpid"
wait "$pydpid" 2>/dev/null
}
declare -a org_args="${org_args}"
[[ -e "${_BCU_SH_DIR}"/disabled_tests.txt ]] && mapfile -t _BCU_TESTS_DISABLED <"${_BCU_SH_DIR}"/disabled_tests.txt
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	misc_test_main "${org_args[@]}"
    else
	misc_test_main "${@}"
    fi
fi
# misc-unit-tests ends here
