#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::is_valid_var_name-test-main][is_valid_var_name-test-main]]
org_args='()'
. bcu.sh
bcu__is_valid_var_name(){ 
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() var_string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Check if a string is a valid bash variable name


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__is_valid_var_name _apa && echo true
    bcu__is_valid_var_name 1_apa || echo false

Results:

    true
    false

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:var_string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    [[ "$var_string" =~ ^[_[:alpha:]][_[:alpha:][:digit:]]*$ ]] || return 1
}
is_valid_var_name_test_f(){
    local a b
    a=$(bcu__is_valid_var_name _apa && echo true)
    b=$(bcu__is_valid_var_name 1_apa || echo false)
    declare -p a b
}
is_valid_var_name_test_1()(
    . <(is_valid_var_name_test_f)
    [[ "${a}" == true ]] || { echo FAIL && return 1; }
    [[ "${b}" == false ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
is_valid_var_name_test_main(){
    printf is_valid_var_name_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )is_valid_var_name_test_1(\ |$) ]]; then is_valid_var_name_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	is_valid_var_name_test_main "${org_args[@]}"
    else
	is_valid_var_name_test_main "${@}"
    fi
fi
# is_valid_var_name-test-main ends here
