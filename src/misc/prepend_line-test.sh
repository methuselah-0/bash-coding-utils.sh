#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::prepend_line-test-main][prepend_line-test-main]]
org_args='()'
. bcu.sh
bcu__prepend_line(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() newline file string line str
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Prepend a line to a file or create it if it doesn't
exist. prepend_line errors with a stacktrace if the given file is not
read- and writable or the file can't be created. Dependencies are the
sed and wc programs.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    rm b; cat b
    bcu__prepend_line -f b -- a
    bcu__prepend_line -f b -- b
    bcu__prepend_line -f b -n -- b
    cat b

Results:

    cat: b: No such file or directory
    b
    ba

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Newline=(n newline "" "Append a newline to the string you want to prepend" 0)
    local File=(f file "" "File to append to" 1)
    Options+=("(${null[*]@Q})" "(${Newline[*]@Q})" "(${File[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    if [[ ! -e "$file" ]]; then
	touch "$file" &>/dev/null || { printf '%s' "File $file does not exist and unable to create it: " && bcu__stack "$@" && return 1; }; fi

    [[ -w "$file" ]] || { printf '%s' "File is not writable: " && bcu__stack "$@" && return 1; }
    [[ -r "$file" ]] || { printf '%s' "File is not readable: " && bcu__stack "$@" && return 1; }
    [[ "$(wc -l < "$file")" -eq 0 ]] && echo >> "$file"

    if [[ -n "$newline" ]]; then
	for str in "${string[@]}"; do
	    line=$(bcu__sed_esc_rep "${str}") || { printf '%s' "Failed to escape replacement string - $str - with the following result - $line - : ${_STACK}" && bcu__stack "$@" && return 1; }
	    result=$(sed -i "1s/^/${line}\n/" "$file" 2>&1) || { printf '%s' "sed failed to prepend the line - $line - with the error - $result - : ${_STACK}" && bcu__stack "$@" && return 1; }; done
    else
	for str in "${string[@]}"; do
	    line=$(bcu__sed_esc_rep "${str}") || { printf '%s' "Failed to escape replacement string - $str - with the following result - $line - : ${_STACK}" && bcu__stack "$@" && return 1; }
	    result=$(sed -i "1s/^/${line}/" "$file" 2>&1) || { printf '%s' "sed failed to prepend the line - $line - with the error - $result - : ${_STACK}" && bcu__stack "$@" && return 1; }; done; fi
}
prepend_line_test_f(){
    local a b
    b=$(mktemp /tmp/tempfile.XXXXXX)
    rm "$b" &>/dev/null; cat "$b" &>/dev/null
    bcu__prepend_line -f "$b" -- a
    bcu__prepend_line -f "$b" -- b
    bcu__prepend_line -f "$b" -n -- b
    a=$(cat "$b")
    declare -p a
}
prepend_line_test_1()(
    . <(prepend_line_test_f)
    [[ "${a}" == $'b\nba' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
prepend_line_test_main(){
    printf prepend_line_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )prepend_line_test_1(\ |$) ]]; then prepend_line_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	prepend_line_test_main "${org_args[@]}"
    else
	prepend_line_test_main "${@}"
    fi
fi
# prepend_line-test-main ends here
