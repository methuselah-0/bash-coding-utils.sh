#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::timeout-test-main][timeout-test-main]]
org_args='()'
. bcu.sh
bcu__timeout()(
    local _STACK z Options=() Input=() cmd spid rpids=()
    local -a Args
    [[ "$1" =~ (-h|--help) ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Run a command and kill it if it reaches a timeout. bcu__timeout can
run bash functions and preserves the exit status of the command in
case it completes before the timeout. bcu__timeout returns exit code
126 if the timeout is reached.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    f(){ printf '%s ' fstart ; sleep 2; printf '%s ' fdone ;}
    g()( printf '%s ' gstart ; sleep 2; printf '%s ' gdone ;)
    bcu__timeout 1 f ; echo $? ; echo
    bcu__timeout 3 f ; echo $? ; echo
    bcu__timeout 1 g ; echo $? ; echo 
    bcu__timeout 3 g ; echo $? ; echo

Results:

    fstart fdone 126
    fstart fdone 0
    gstart 126
    gstart gdone 0

EOF
		)
    }    
    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)    
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:s 1:t:cmd 0:f:Args)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || {
	printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ (-h|--help) ]] && return 0

    # Check args
    [[ "$s" =~ ^[0-9]+$ ]] || { printf '%s' "First arg is not a number. Should be the number of seconds before timeout of command: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$(type -t $cmd)" =~ ^(function|file)$ ]] || { printf '%s' "The cmd argument must be either a function or a file that is in the PATH environment variable: ${_STACK}" && bcu__stack "$@" && return 1; }

    list_descendants(){
	local children=$(ps -o pid= --ppid "$1")
	local pid
	for pid in $children ; do
	    list_descendants "$pid" ; done
	printf '%s ' "$children" ;} ;
    set +b ;

    # Run the timeout as a background process
    # save it's process id
    sleep "$s" &
    spid="$!"

    # Run the command as a background process
    "${cmd}" "${Args[@]}" &

    # Wait until either background job changes status
    wait -n
    
    # Record exit status of the background job that completed first
    local -i ecode=$?

    # If a running process id matches the sleep command, kill the
    # sleep process and we can assume that the exit code of the
    # command is what got stored in ecode. Then exit with that ecode.
    rpids=($(jobs -p))
    for p in "${rpids[@]}" ; do
	[[ "$p" == "$spid" ]] && \
	    kill -9 "${spid}" &>/dev/null && \
	    exit $ecode ; done

    # If no running pid matches the sleep command's pid, we have
    # reached the timout and we can kill the stuck process, and it's
    # descendants. We then exit 126, to indicate that the other command
    # did not finish. ref: https://shapeshed.com/unix-exit-codes/#what-exit-code-should-i-use
    kill -9 $(list_descendants "${rpids[@]}")
    exit 126
)
timeout_test_f()(
    local a b
    acmd()
    {
	( echo apa;
	  sleep 7 & sleep 10;
	  echo bepa;
	  exit 7 )
    }
    a=$(bcu__timeout 13 acmd &>/dev/null; echo $?)
    b=$(bcu__timeout 1 acmd &>/dev/null; echo $?)
    declare -p a b
)
timeout_test_1()(
    . <(timeout_test_f)
    [[ "${a}" == 7 ]] || { echo FAIL && return 1; }
    [[ "${b}" == 126 ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
timeout_test_main(){
    printf timeout_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )timeout_test_1(\ |$) ]]; then timeout_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	timeout_test_main "${org_args[@]}"
    else
	timeout_test_main "${@}"
    fi
fi
# timeout-test-main ends here
