#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::append-test-main][append-test-main]]
org_args='()'
. bcu.sh
bcu__append(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() string Strings
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Append a string to one or several other strings, then print them with
an appended newline.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    printf '%s\u0000' apa bepa | bcu__append -z -s some-string
    printf '%s\n' apa bepa | bcu__append -s '!@#$#!^#$&$%&(^$*($^*)($%#@!#!%'
    bcu__append -s some-string -- apa bepa cepa

Results:

    apasome-string
    bepasome-string
    apa!@#$#!^#$&$%&(^$*($^*)($%#@!#!%
    $#!^#$&$%&(^$*($^*)($%#@!#!%
    apasome-string
    bepasome-string
    cepasome-string

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local String=(s string "" "The string to append" 1 t f)
    Options+=("(${null[*]@Q})" "(${String[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:Strings)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    for str in "${Strings[@]}"; do
	printf "%s\n" "${str}""$string"; done
}
append_test_f(){
    local -a a b c
    mapfile -t a < <(printf '%s\u0000' apa bepa | bcu__append -z -s some-string)
    mapfile -t b < <(printf '%s\n' apa bepa | bcu__append -s some-string)
    mapfile -t c < <(bcu__append -s some-string -- apa bepa cepa)
    declare -p a b c
}
append_test_1()(
    . <(append_test_f)
    [[ "${a[0]}" == "apasome-string" ]] || { echo FAIL && return 1; }
    [[ "${a[1]}" == "bepasome-string" ]] || { echo FAIL && return 1; }
    [[ "${b[0]}" == "apasome-string" ]] || { echo FAIL && return 1; }
    [[ "${b[1]}" == "bepasome-string" ]] || { echo FAIL && return 1; }
    [[ "${c[0]}" == "apasome-string" ]] || { echo FAIL && return 1; }
    [[ "${c[1]}" == "bepasome-string" ]] || { echo FAIL && return 1; }
    [[ "${c[2]}" == "cepasome-string" ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
append_test_main(){
    printf append_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )append_test_1(\ |$) ]]; then append_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	append_test_main "${org_args[@]}"
    else
	append_test_main "${@}"
    fi
fi
# append-test-main ends here
