#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::string.sh][string.sh]]
[[ "$_BCU_SH_LOADED" == YES ]] || . bcu.sh
_string_MOD_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")
bcu__append(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() string Strings
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Append a string to one or several other strings, then print them with
an appended newline.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    printf '%s\u0000' apa bepa | bcu__append -z -s some-string
    printf '%s\n' apa bepa | bcu__append -s '!@#$#!^#$&$%&(^$*($^*)($%#@!#!%'
    bcu__append -s some-string -- apa bepa cepa

Results:

    apasome-string
    bepasome-string
    apa!@#$#!^#$&$%&(^$*($^*)($%#@!#!%
    $#!^#$&$%&(^$*($^*)($%#@!#!%
    apasome-string
    bepasome-string
    cepasome-string

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local String=(s string "" "The string to append" 1 t f)
    Options+=("(${null[*]@Q})" "(${String[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:Strings)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    for str in "${Strings[@]}"; do
	printf "%s\n" "${str}""$string"; done
}
bcu__prepend(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() string Strings str
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Prepend a string to one or several other strings, then print them with
an appended newline.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    printf '%s\u0000' apa bepa | bcu__prepend -z -s some-string
    printf '%s\n' apa bepa | bcu__prepend -s some-string
    bcu__prepend -s '!@#$@#%^#%^&' -- apa bepa '!@#$@%^#%*$^&*#$@%'
  
Results:

    some-stringapa
    some-stringbepa
    some-stringapa
    some-stringbepa
    !@#$@#%^#%^&apa
    $@#%^#%^&bepa
    $@#%^#%^&!@#$@%^#%*$^&*#$@%
      
EOF
		)
    }

    # Options
    local String=(s string "" "The string to prepend" 1 t f)
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})" "(${String[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:Strings)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    for str in "${Strings[@]}"; do
	printf "%s\n" "$string""${str}"; done
}
string_private(){
    :

}
string_main(){
    :
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	string_main "${org_args[@]}"
    else
	string_main "${@}"
    fi
fi
# string.sh ends here
