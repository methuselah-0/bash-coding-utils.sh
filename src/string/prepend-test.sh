#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::prepend-test-main][prepend-test-main]]
org_args='()'
. bcu.sh
bcu__prepend(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() string Strings str
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Prepend a string to one or several other strings, then print them with
an appended newline.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    printf '%s\u0000' apa bepa | bcu__prepend -z -s some-string
    printf '%s\n' apa bepa | bcu__prepend -s some-string
    bcu__prepend -s '!@#$@#%^#%^&' -- apa bepa '!@#$@%^#%*$^&*#$@%'
  
Results:

    some-stringapa
    some-stringbepa
    some-stringapa
    some-stringbepa
    !@#$@#%^#%^&apa
    $@#%^#%^&bepa
    $@#%^#%^&!@#$@%^#%*$^&*#$@%
      
EOF
		)
    }

    # Options
    local String=(s string "" "The string to prepend" 1 t f)
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})" "(${String[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:Strings)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    for str in "${Strings[@]}"; do
	printf "%s\n" "$string""${str}"; done
}
prepend_test_f(){
    local -a a b c
    mapfile -t a < <(printf '%s\u0000' apa bepa | bcu__prepend -z -s some-string)
    mapfile -t b < <(printf '%s\n' apa bepa | bcu__prepend -s some-string)
    mapfile -t c < <(bcu__prepend -s some-string -- apa bepa cepa)
    declare -p a b c
}
prepend_test_1()(
    . <(prepend_test_f)
    [[ "${a[0]}" == "some-stringapa" ]] || { echo FAIL && return 1; }
    [[ "${a[1]}" == "some-stringbepa" ]] || { echo FAIL && return 1; }
    [[ "${b[0]}" == "some-stringapa" ]] || { echo FAIL && return 1; }
    [[ "${b[1]}" == "some-stringbepa" ]] || { echo FAIL && return 1; }
    [[ "${c[0]}" == "some-stringapa" ]] || { echo FAIL && return 1; }
    [[ "${c[1]}" == "some-stringbepa" ]] || { echo FAIL && return 1; }
    [[ "${c[2]}" == "some-stringcepa" ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
prepend_test_main(){
    printf prepend_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )prepend_test_1(\ |$) ]]; then prepend_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	prepend_test_main "${org_args[@]}"
    else
	prepend_test_main "${@}"
    fi
fi
# prepend-test-main ends here
