#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::arr_is_subset-test-main][arr_is_subset-test-main]]
org_args='()'
. bcu.sh
bcu__arr_is_subset(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() array1 array2
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Return true if the elements of array1 constitutes a subset of the
elements of array2.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa bepa
    apa=(a b c)
    bepa=(a b c d)
    bcu__arr_is_subset -n -- apa bepa && echo true
    bcu__arr_is_subset "(${apa[*]@Q})" "(${bepa[*]@Q})" && echo true
    bcu__arr_is_subset -n -- bepa apa || echo false
    bcu__arr_is_subset "(${bepa[*]@Q})" "(${apa[*]@Q})" || echo false
    
  
Results:

    true
    true
    false
    false
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Namerefs=(n namerefs "" "Read array1 and array2 as namereferences" 0)
    Options+=("(${null[*]@Q})" "(${Namerefs[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:array1 1:t:array2)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    if [[ -n "$namerefs" ]]; then
	# shellcheck disable=SC2155
	local -a Arr=$(bcu__arr_diff -n --out first-only -- "${array1}" "${array2}")
    else
	local -a Input1="${array1}" || { printf '%s' "array1 is not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }

	local -a Input2="${array2}" || { printf '%s' "array2 is not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }

	local -a Arr=$(bcu__arr_diff --out first-only -- "$array1" "$array2" ); fi
    [[ -z "${Arr[*]}" ]] && return 0
    return 1
}
arr_is_subset_test_f(){
    unset apa bepa
    local a
    apa=(a b c)
    bepa=(a b c d)
    echo
    a=$(if bcu__arr_is_subset -n -- apa bepa; then echo true; else echo false; fi)
    b=$(if bcu__arr_is_subset "(${apa[*]@Q})" "(${bepa[*]@Q})"; then echo true; else echo false; fi)
    c=$(if bcu__arr_is_subset "(${bepa[*]@Q})" "(${apa[*]@Q})"; then echo true; else echo false; fi)
    declare -p a b c
}
arr_is_subset_test_1()(
    . <(arr_is_subset_test_f)
    [[ "${a}" == true ]] || { echo FAIL && return 1; }
    [[ "${b}" == true ]] || { echo FAIL && return 1; }
    [[ "${c}" == false ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
arr_is_subset_test_main(){
    printf arr_is_subset_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )arr_is_subset_test_1(\ |$) ]]; then arr_is_subset_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	arr_is_subset_test_main "${org_args[@]}"
    else
	arr_is_subset_test_main "${@}"
    fi
fi
# arr_is_subset-test-main ends here
