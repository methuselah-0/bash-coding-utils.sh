#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::map_args-test-main][map_args-test-main]]
org_args='()'
. bcu.sh
bcu__map_args(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() args elem array func split_elems split_args
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
A version of [[*map][map]] where the list elements are placed as first argument
to the function to be applied and the args after. Both args and
list-elements can be splitted (unquoted variable expansion).


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa bepa cepa depa
    apa=('%s\n' "  %sEXTRA\n")
    declare -a bepa=$(bcu__map_args -f printf -a "(${apa[*]@Q})" -- apa 'apa bepa' cepa)
    declare -a cepa=$(bcu__map_args -f printf -a "(${apa[*]@Q})" --split_elems -- apa 'apa bepa' cepa)
    declare -a depa=$(bcu__map_args -f printf -a "(${apa[*]@Q})" --split_elems --split_args -- apa 'apa bepa' cepa)
    echo
    declare -p bepa
    declare -p cepa
    declare -p depa
    echo
  
Results:

    declare -a bepa=([0]=$'apa\napa bepa\ncepa' [1]=$'  apaEXTRA\n  apa bepaEXTRA\n  cepaEXTRA')
    declare -a cepa=([0]=$'apa\napa bepa\ncepa' [1]=$'apaEXTRA\napa bepaEXTRA\ncepaEXTRA')
    declare -a depa=([0]=$'apa\napa\nbepa\ncepa' [1]=$'apaEXTRA\napaEXTRA\nbepaEXTRA\ncepaEXTRA')
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local SplitElems=(s split_elems "" "Whether to split the elements by unquoted expansion" 0 f f)
    local SplitArgs=(t split_args "" "Whether to split the input operands by unquoted expansion when provided as input arguments to the function" 0 f f)
    local Array=(a array "()" "The array to apply the function and args to" 1 t f)
    local Function=(f func "" "The function to apply" 1 t f)
    Options+=("(${null[*]@Q})" "(${SplitElems[*]@Q})" "(${SplitArgs[*]@Q})" "(${Array[*]@Q})" "(${Function[*]@Q})")
    
    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:f:args)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    local -a OutArray
    local -a Array="${array}"
    command -v "$func" &>/dev/null || bcu__stack "$@"

    if [[ -n "$split_elems" ]]; then
	if [[ -n "$split_args" ]]; then
	    for elem in "${Array[@]}"; do
		# shellcheck disable=SC2086
		# shellcheck disable=SC2068
		# intentionally split the elements by leaving $elem unquoted.
		OutArray+=("$("${func}" $elem ${args[@]})"); done
	else
	    for elem in "${Array[@]}"; do
		# shellcheck disable=SC2086
		# intentionally split the elements by leaving $elem unquoted.
		OutArray+=("$("$func" $elem "${args[@]}")"); done; fi
    else
	if [[ -n "$split_args" ]]; then
	    for elem in "${Array[@]}"; do
		# shellcheck disable=SC2068
		OutArray+=("$("$func" "$elem" ${args[@]})"); done
	else
	    for elem in "${Array[@]}"; do
		OutArray+=("$("$func" "$elem" "${args[@]}")"); done; fi; fi
    printf '%s' "(${OutArray[*]@Q})"
}
map_args_test_f(){
    local -a apa
    apa=('%s\n' "  %sEXTRA\n")
    local -a bepa=$(bcu__map_args -f printf -a "(${apa[*]@Q})" -- apa 'apa bepa' cepa)
    local -a cepa=$(bcu__map_args -f printf -a "(${apa[*]@Q})" --split_elems -- apa 'apa bepa' cepa)
    local -a depa=$(bcu__map_args -f printf -a "(${apa[*]@Q})" --split_elems --split_args -- apa 'apa bepa' cepa)

    declare -p bepa cepa depa
}
map_args_test_1()(
    . <(map_args_test_f)
    [[ "${bepa[0]}" =~ $'apa\napa bepa\ncepa' ]] || { echo FAIL && return 1; }
    [[ "${bepa[1]}" =~ $'  apaEXTRA\n  apa bepaEXTRA\n  cepaEXTRA' ]] || { echo FAIL && return 1; }
    [[ "${cepa[0]}" =~ $'apa\napa bepa\ncepa' ]] || { echo FAIL && return 1; }
    [[ "${cepa[1]}" =~ $'apaEXTRA\napa bepaEXTRA\ncepaEXTRA' ]] || { echo FAIL && return 1; }
    [[ "${depa[0]}" =~ $'apa\napa\nbepa\ncepa' ]] || { echo FAIL && return 1; }
    [[ "${depa[1]}" =~ $'apaEXTRA\napaEXTRA\nbepaEXTRA\ncepaEXTRA' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
map_args_test_main(){
    printf map_args_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )map_args_test_1(\ |$) ]]; then map_args_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	map_args_test_main "${org_args[@]}"
    else
	map_args_test_main "${@}"
    fi
fi
# map_args-test-main ends here
