#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::arr_pop-test-main][arr_pop-test-main]]
org_args='()'
. bcu.sh
bcu__arr_pop(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() array
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
arr_pop pulls the last element off of the given array.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa; apa=(one two three four)
    bcu__arr_pop -n -- apa && declare -p apa
    printf '%s' "(${apa[*]@Q})" | bcu__arr_pop && echo
  
Results:

    declare -a apa=([0]="one" [1]="two" [2]="three")
    ('three')
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Nameref=(n nameref "" "Read arg as a namereference. This option will not print the resulting array back to stdout" 0)
    Options+=("(${null[*]@Q})" "(${Nameref[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:array)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    if [[ -n "$nameref" ]]; then
	local -n Arr; Arr="${array}"
	local -i i
	i=$((${#Arr[@]}-1))
	unset Arr["$i"]
    else
	local -a Arr="${array}"
	local -i i
	i=$((${#Arr[@]}-1))
	local -a NewArr=("${Arr[@]: -1:$i}")
	printf '%s' "(${NewArr[*]@Q})"; fi
}
arr_pop_test_f(){
    local a b
    local -a apa=(one two three four)
    local -a bepa=(five 'six seven' eight)
    bcu__arr_pop -n -- apa bepa
    local -a bepa=$(bcu__arr_pop "(${bepa[*]@Q})")
    declare -p apa bepa
}
arr_pop_test_1()(
    . <(arr_pop_test_f)
    [[ "${apa[*]@Q}" == "'one' 'two' 'three'" ]] || { echo FAIL && return 1; }
    [[ "${bepa[@]}" == eight ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
arr_pop_test_main(){
    printf arr_pop_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )arr_pop_test_1(\ |$) ]]; then arr_pop_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	arr_pop_test_main "${org_args[@]}"
    else
	arr_pop_test_main "${@}"
    fi
fi
# arr_pop-test-main ends here
