#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::arr_tail-test-main][arr_tail-test-main]]
org_args='()'
. bcu.sh
bcu__arr_tail(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() array
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Return the tail of an array starting from a given index.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa bepa
    apa=(a b c d)
    declare -a bepa=$(bcu__arr_tail -i 1 -n -- apa)
    echo && declare -p bepa
  
Results:

    declare -a bepa=([0]="b" [1]="c" [2]="d")
      
EOF
		)
    }

    # Options
    local Index=(i index "" "Return an array with the elements in the input array starting from the i'th element" 1 t f)
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Nameref=(n nameref "" "Read array as a namereference" 0)
    Options+=("(${null[*]@Q})" "(${Index[*]@Q})" "(${Nameref[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:array)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    if [[ -n "$nameref" ]]; then
	local -n arr="${array}" || { printf '%s' "array is not a valid variable name: ${_STACK}" && bcu__stack "$@" && return 1; }
	
    else
	local -a arr="${array}" || { printf '%s' "array is not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }; fi

    local -i ii="$index" || { printf '%s' "index is not a valid integer: ${_STACK}" && bcu__stack "$@" && return 1; }

    printf '%s' "(""${arr[*]:$ii}"")"
}
arr_tail_test_f(){
    local -a apa
    apa=(a b c d)
    local -a a=$(bcu__arr_tail -n -i 2 -- apa)
    local -a b=$(bcu__arr_tail -i 2 -- "(${apa[*]@Q})")
    declare -p a
}
arr_tail_test_1()(
    . <(arr_tail_test_f)
    [[ "${a[0]}" == c ]] || { echo FAIL && return 1; }
    [[ "${a[1]}" == d ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
arr_tail_test_main(){
    printf arr_tail_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )arr_tail_test_1(\ |$) ]]; then arr_tail_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	arr_tail_test_main "${org_args[@]}"
    else
	arr_tail_test_main "${@}"
    fi
fi
# arr_tail-test-main ends here
