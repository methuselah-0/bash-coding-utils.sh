#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::string_to_arr-test-main][string_to_arr-test-main]]
org_args='()'
. bcu.sh
bcu__string_to_arr(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Convert a string to an indexed array.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    declare -a MyArr=$(bcu__string_to_arr MyString)
    declare -a MyArr2=$(bcu__string_to_arr string1 string2)
    echo && declare -p MyArr MyArr2
  
Results:

    declare -a MyArr=([0]="M" [1]="y" [2]="S" [3]="t" [4]="r" [5]="i" [6]="n" [7]="g")
    declare -a MyArr2=([0]="s" [1]="t" [2]="r" [3]="i" [4]="n" [5]="g" [6]="1")
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    # splits into array 
    # [[ "$string" =~ ${string//?/(.)} ]]
    [[ "${string}" =~ ${string//?/(.)} ]]       # splits into array
    local -a arr=( "${BASH_REMATCH[@]:1}" ) # copy array for later
    printf '%s' "(${arr[*]@Q})"
}
string_to_arr_test_f(){
    local -a a=$(bcu__string_to_arr apa bepa 'cepa depa')
    local -a b=$(printf '%s\n' apa bepa 'cepa depa' | bcu__string_to_arr)
    declare -p a b
}
string_to_arr_test_1()(
    . <(string_to_arr_test_f)
    [[ "${#a[@]}" == 3 ]] || { echo FAIL && return 1; }
    [[ $( printf '%s' "${a[@]}") == apa ]] || { echo FAIL && return 1; }
    [[ "${#b[@]}" == 3 ]] || { echo FAIL && return 1; }
    [[ $( printf '%s' "${b[@]}") == apa ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
string_to_arr_test_main(){
    printf string_to_arr_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )string_to_arr_test_1(\ |$) ]]; then string_to_arr_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	string_to_arr_test_main "${org_args[@]}"
    else
	string_to_arr_test_main "${@}"
    fi
fi
# string_to_arr-test-main ends here
