#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::map-test-main][map-test-main]]
org_args='()'
. bcu.sh
bcu__map(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() args split array func elem
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Map vill apply a function to a list's elements and return the list
ordered with the output of applying the function in place of the
list's original elements. Additional arguments to the function may be
given and are always placed "to the left" of the element in the list,
i.e. the list element is always the last input argument to the
function that is being applied. Note that the function is applied in a
shell substitution context, meaning e.g. that any trailing newlines in
the resulting output from applying the function to an element will be
trimmed (see example below).


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa bepa cepa
    apa=(a 'b c' d)
    declare -a bepa=$(bcu__map -f printf -a "(${apa[*]@Q})" -- '%s\n' 'apa bepa' cepa)
    declare -a cepa=$(bcu__map -f printf -a "(${apa[*]@Q})" --split -- '%s\n' 'apa bepa' cepa)
    declare -p bepa
    declare -p cepa
  
Results:

    declare -a bepa=([0]=$'apa bepa\ncepa\na' [1]=$'apa bepa\ncepa\nb c' [2]=$'apa bepa\ncepa\nd')
    declare -a cepa=([0]=$'apa bepa\ncepa\na' [1]=$'apa bepa\ncepa\nb\nc' [2]=$'apa bepa\ncepa\nd')
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Split=(s split "" "Whether to split the elements by unquoted expansion" 0 f f)
    local Array=(a array "()" "The array to apply the function and args to" 1 t f)
    local Function=(f func "" "The function to apply" 1 t f)
    Options+=("(${null[*]@Q})" "(${Split[*]@Q})" "(${Array[*]@Q})" "(${Function[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:args)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    local -i input_length; input_length=${#args[@]}

    if [[ -n "$split" ]]; then
	if command -v "$func" &>/dev/null; then
	    local -a Array="${array}"
	    local -a OutArray
	    for elem in "${Array[@]}"; do
		# shellcheck disable=SC2086
		# intentionally split the elements by leaving $elem unquoted.
		OutArray+=("$(${func} "${args[@]}" $elem)"); done
	    printf '%s' "(${OutArray[*]@Q})"
	else
	    printf '%s' "arg 1 in map not a function: ${_STACK}" && bcu__stack "$@" && return 1; fi
    else
	if command -v "$func" >/dev/null 2>/dev/null; then
	    local -a Array="${array}"
	    local -a OutArray
	    for elem in "${Array[@]}"; do
		OutArray+=("$(${func} "${args[@]}" "$elem")"); done
	    printf '%s' "(${OutArray[*]@Q})"
	else
	    printf '%s' "arg 1 in map not a function: ${_STACK}" && bcu__stack "$@" && return 1; fi; fi
}
map_test_f(){
    local a b
    local -a apa bepa
    apa=(a b c d)
    declare -a a=$(bcu__map -f echo -a "(${apa[*]@Q})" -- apa bepa cepa)
    declare -a b=$(printf '%s\n' apa bepa cepa | bcu__map -f echo -a "(${apa[*]@Q})")
    declare -p a b
}
map_test_1()(
    . <(map_test_f)
    [[ "${a[0]}" == "apa bepa cepa a" ]] || { echo FAIL && return 1; }
    [[ "${a[1]}" == "apa bepa cepa b" ]] || { echo FAIL && return 1; }
    [[ "${a[2]}" == "apa bepa cepa c" ]] || { echo FAIL && return 1; }
    [[ "${a[3]}" == "apa bepa cepa d" ]] || { echo FAIL && return 1; }
    [[ "${b[0]}" == "apa bepa cepa a" ]] || { echo FAIL && return 1; }
    [[ "${b[1]}" == "apa bepa cepa b" ]] || { echo FAIL && return 1; }
    [[ "${b[2]}" == "apa bepa cepa c" ]] || { echo FAIL && return 1; }
    [[ "${b[3]}" == "apa bepa cepa d" ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
map_test_main(){
    printf map_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )map_test_1(\ |$) ]]; then map_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	map_test_main "${org_args[@]}"
    else
	map_test_main "${@}"
    fi
fi
# map-test-main ends here
