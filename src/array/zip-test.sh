#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::zip-test-main][zip-test-main]]
org_args='()'
. bcu.sh
bcu__zip(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() array1 array2
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Combine two arrays into one


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa bepa
    apa=(a c e)
    bepa=(b d f)
    declare -a cepa=$(bcu__zip "(${apa[*]@Q})" "(${bepa[*]@Q})")
    echo
    declare -p cepa
  
Results:

    declare -a cepa=([0]="a" [1]="b" [2]="c" [3]="d" [4]="e" [5]="f")
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:array1 1:t:array2)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    local -a Zick="${array1}" || { printf '%s' "array1 is not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }

    local -a Zack="${array2}" || { printf '%s' "array2 is not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }

    local -a Out
    for ((i=0;i<"${#Zick[@]}";i++)); do
	Out+=("${Zick[$i]}" "${Zack[$i]}"); done
    printf '%s' "(${Out[*]@Q})"
}
zip_test_f(){
    local -a apa bepa cepa
    local a
    apa=(a c e)
    bepa=(b d f)
    declare -a a=$(bcu__zip "(${apa[*]@Q})" "(${bepa[*]@Q})")
    declare -p a
}
zip_test_1()(
    . <(zip_test_f)
    [[ "${a[@]}" == "a b c d e f" ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
zip_test_main(){
    printf zip_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )zip_test_1(\ |$) ]]; then zip_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	zip_test_main "${org_args[@]}"
    else
	zip_test_main "${@}"
    fi
fi
# zip-test-main ends here
