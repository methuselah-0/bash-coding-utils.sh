#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::is_assoc_and_unused-test-main][is_assoc_and_unused-test-main]]
org_args='()'
. bcu.sh
bcu__is_assoc_and_unused(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() variable_name
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
is_assoc_and_unused returns true only if a variable name is declared
an associative array and not being set to an empty or non-empty array.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa bepa cepa; declare -A apa bepa cepa
    apa[a]=b; unset apa[a]
    bepa[one]=two
    echo
    declare -p apa bepa cepa
    bcu__is_assoc_and_unused apa || echo false
    bcu__is_assoc_and_unused bepa || echo false
    bcu__is_assoc_and_unused cepa && echo true
  
Results:

    declare -A apa=()
    declare -A bepa=([one]="two" )
    declare -A cepa
    false
    false
    true
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:variable_name)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    if bcu__is_assoc "${variable_name}"; then
	local a; a=$(getReferencedVar "${variable_name}")
	#eval "[[ -z \${$a]+x} ]] && return 0"; fi
	[[ "$(declare -p "$a")" =~ ^'declare -A '"${a}"$ ]] && return 0; fi
    #eval "[[ -v \"$a\" ]] && return 0"; fi
    return 1
}
is_assoc_and_unused_test_f(){
    local -A apa bepa cepa
    local a b c
    apa=([a]="b" )
    bepa=()
    local -n depa=cepa
    a=$(if bcu__is_assoc_and_unused apa; then echo true; else echo false; fi)
    b=$(if bcu__is_assoc_and_unused bepa; then echo true; else echo false; fi)
    c=$(if bcu__is_assoc_and_unused cepa; then echo true; else echo false; fi)
    d=$(if bcu__is_assoc_and_unused depa; then echo true; else echo false; fi)
    declare -p a b c d # apa bepa cepa | tee -a >/dev/stderr 
}
is_assoc_and_unused_test_1()(
    . <(is_assoc_and_unused_test_f)
    [[ "${a}" == false ]] || { echo FAIL && return 1; }
    [[ "${b}" == false ]] || { echo FAIL && return 1; }
    [[ "${c}" == true ]] || { echo FAIL && return 1; }
    [[ "${d}" == true ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
is_assoc_and_unused_test_main(){
    printf is_assoc_and_unused_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )is_assoc_and_unused_test_1(\ |$) ]]; then is_assoc_and_unused_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	is_assoc_and_unused_test_main "${org_args[@]}"
    else
	is_assoc_and_unused_test_main "${@}"
    fi
fi
# is_assoc_and_unused-test-main ends here
