#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::arr_push-test-main][arr_push-test-main]]
org_args='()'
. bcu.sh
bcu__arr_push(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() Strings
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Add data to multiple arrays in one command.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa apa2 Stuff0 Stuff1 Stuff1_3 bepa
    apa=(one two 'three four')
    apa2=(two two two)
    bcu__arr_push -n -a apa -- apa bepa
    echo
    mapfile -t Stuff < <(printf '%s\n' "(${apa2[*]@Q})" | bcu__arr_push -a "(${apa[*]@Q})" -a "(${apa2[*]@Q})")
    declare -a Stuff0="${Stuff[0]}"
    declare -a Stuff1="${Stuff[1]}"
    declare -a Stuff1_3="${Stuff1[3]}"
    echo
    declare -p Stuff Stuff0 Stuff1 Stuff1_3
    bcu__arr_push -n -a bepa -- apa bepa
    echo
    declare -p bepa
  
Results:

    declare -a Stuff=([0]="('one' 'two' 'three four' 'apa' 'bepa' '('\\''two'\\'' '\\''two'\\'' '\\''two'\\'')')" [1]="('two' 'two' 'two' '('\\''two'\\'' '\\''two'\\'' '\\''two'\\'')')")
    declare -a Stuff0=([0]="one" [1]="two" [2]="three four" [3]="apa" [4]="bepa" [5]="('two' 'two' 'two')")
    declare -a Stuff1=([0]="two" [1]="two" [2]="two" [3]="('two' 'two' 'two')")
    declare -a Stuff1_3=([0]="two" [1]="two" [2]="two")
    declare -a bepa=([0]="apa" [1]="bepa")
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Arrays=(a Arrays "" "Variable name of an array to push arguments to" 1 t t)
    local Nameref=(n nameref "" "Read all given arrays as namereferences. With this option arr_push will not print the resulting array(s) back to stdout" 0)
    Options+=("(${null[*]@Q})" "(${Arrays[*]@Q})" "(${Nameref[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:Strings)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    if [[ -n "$nameref" ]]; then
	for Array in "${Arrays[@]}"; do
	    #local -a 'Arr=("${'"$Array"'[@]}")'
	    #local -n Arr="$Array"
	    local -n bcu__arr_push_Arr="$Array"
	    #Arr=("${Arr[@]}" "${Strings[@]}"); done
	    bcu__arr_push_Arr=("${bcu__arr_push_Arr[@]}" "${Strings[@]}"); done
    else
	for Array in "${Arrays[@]}"; do
	    local -a Arr="$Array"
	    Arr=("${Arr[@]}" "${Strings[@]}")
	    printf '%s\n' "(${Arr[*]@Q})"; done; fi
}
arr_push_test_f(){
    local -a apa bepa cepa
    apa=(one two 'three four')
    bcu__arr_push -n -a apa -- apa bepa
    local -a apa=$(bcu__arr_push -a "(${apa[*]@Q})" < <(printf '%s\n' apa bepa))

    bcu__arr_push -n -a bepa -- apa bepa

    bcu__arr_push -n -a cepa -- apa bepa
    declare -p apa bepa cepa
}
arr_push_test_1()(
    . <(arr_push_test_f)
    [[ "${#apa[@]}" == 7 ]] || { echo FAIL && return 1; }
    [[ "${#bepa[@]}" == 2 ]] || { echo FAIL && return 1; }
    [[ "${#cepa[@]}" == 2 ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
arr_push_test_main(){
    printf arr_push_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )arr_push_test_1(\ |$) ]]; then arr_push_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	arr_push_test_main "${org_args[@]}"
    else
	arr_push_test_main "${@}"
    fi
fi
# arr_push-test-main ends here
