#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::arr_sort-test-main][arr_sort-test-main]]
org_args='()'
. bcu.sh
bcu__arr_sort(){
    local _STACK z Options=() Input=() array line Sorted key
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Sort an array by length of its strings. Does not handle strings with
newlines. Prints results newline-separated.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset Arr
    mapfile -t Arr < <(printf '%s\n' apa bepa depta cepa)
    bcu__arr_sort "(${Arr[*]@Q})"
  
Results:

    apa
    bepa
    cepa
    depta
      
EOF
		)
    }
    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:array)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || {
	printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "" =~ ^(-h|--help)$ ]] && return 0

    local -a array="${array}"
    # Check args
    while read -r line; do
	if [ -z "${Sorted[${#line}]}" ] ; then          # does line length already exist?
	    Sorted[${#line}]="$line"                      # element for new length
	else
	    Sorted[${#line}]="${Sorted[${#line}]}\\n$line" # append to lines with equal length
	fi
    done < <(printf '%s\n' "${array[@]}")

    for key in ${!Sorted[*]}; do                      # iterate over existing indices
	printf '%b' "${Sorted[$key]}\\n"                       # echo lines with equal length
    done
}
arr_sort_test_f(){
    local a
    local -a Arr
    mapfile -t Arr < <(printf '%s\n' apa bepa depat cepa)
    mapfile -t a < <(bcu__arr_sort "(${Arr[*]@Q})")
    declare -p a
}
arr_sort_test_1()(
    . <(arr_sort_test_f)
    [[ "${a[0]}" == apa ]] || { echo FAIL && return 1; }
    [[ "${a[1]}" == bepa ]] || { echo FAIL && return 1; }
    [[ "${a[2]}" == cepa ]] || { echo FAIL && return 1; }
    [[ "${a[3]}" == depat ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
arr_sort_test_main(){
    printf arr_sort_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )arr_sort_test_1(\ |$) ]]; then arr_sort_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	arr_sort_test_main "${org_args[@]}"
    else
	arr_sort_test_main "${@}"
    fi
fi
# arr_sort-test-main ends here
