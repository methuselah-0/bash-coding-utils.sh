#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::arr_shift-test-main][arr_shift-test-main]]
org_args='()'
. bcu.sh
bcu__arr_shift(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() array
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
arr_shift removes the first element of the passed array and returns it
unless the namereference option is given. The namereference uses "arr"
locally, meaning that the array variable name used must not be named
"arr" if invoked with this option. The main benefit is that it
left-shifts the index of the array instead of just unsets index 0. If
the namereference options is not used, arr_shift prints an
array-declarable string back.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset myarr array2;
    myarr=(one two three)
    declare -a array2=$(bcu__arr_shift "(${myarr[*]@Q})")
    bcu__arr_shift -n -- myarr
    echo
    declare -p array2 myarr

Results:

    declare -a array2=([0]="two" [1]="three")
    declare -a myarr=([0]="two" [1]="three")

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Nameref=(n nameref "" "Read arg as a namereference. This option will not print the resulting array back to stdout" 0)
    Options+=("(${null[*]@Q})" "(${Nameref[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:array)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    if [[ -n "$nameref" ]]; then
	local -n arr="${array}"
	arr=("${arr[@]:1}")
    else
	local -a Arr="${array}"
	Arr=("${Arr[@]:1}")
	printf '%s' "(${Arr[*]@Q})"; fi
}
arr_shift_test_f(){
    local a b
    a=(one two three)
    local -a b=$(bcu__arr_shift "(${a[*]@Q})")
    bcu__arr_shift -n -- a
    declare -p a b
}
arr_shift_test_1()(
    . <(arr_shift_test_f)
    [[ "${a[0]}" == two ]] || { echo FAIL && return 1; }
    [[ "${a[1]}" == three ]] || { echo FAIL && return 1; }
    [[ "${b[0]}" == two ]] || { echo FAIL && return 1; }
    [[ "${b[1]}" == three ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
arr_shift_test_main(){
    printf arr_shift_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )arr_shift_test_1(\ |$) ]]; then arr_shift_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	arr_shift_test_main "${org_args[@]}"
    else
	arr_shift_test_main "${@}"
    fi
fi
# arr_shift-test-main ends here
