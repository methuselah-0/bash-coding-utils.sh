#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::arr_rev-test-main][arr_rev-test-main]]
org_args='()'
. bcu.sh
bcu__arr_rev(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() array arg
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Reverse arrays


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa bepa Apa_Rev Bepa_Rev Rev_2
    apa=(a b c d)
    bepa=(e f g h)
    bepa[8]=i
    declare -a Apa_Rev=$(bcu__arr_rev "(${apa[*]@Q})")
    declare -a Bepa_Rev=$(bcu__arr_rev "(${bepa[*]@Q})")
    mapfile -t Rev_2 < <(bcu__arr_rev "(${apa[*]@Q})" "(${bepa[*]@Q})")
    bcu__arr_rev -n -- apa bepa
    declare -p apa bepa Apa_Rev Bepa_Rev Rev_2
  
Results:

    declare -a apa=([0]="d" [1]="c" [2]="b" [3]="a")
    declare -a bepa=([0]="i" [1]="h" [2]="g" [3]="f" [4]="e")
    declare -a Apa_Rev=([0]="d" [1]="c" [2]="b" [3]="a")
    declare -a Bepa_Rev=([0]="i" [1]="h" [2]="g" [3]="f" [4]="e")
    declare -a Rev_2=([0]="('d' 'c' 'b' 'a')" [1]="('i' 'h' 'g' 'f' 'e')")
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Namerefs=(n namerefs "" "Read args as namereferences. If using this option, the resulting arrays will not be printed back to stdout" 0)
    Options+=("(${null[*]@Q})" "(${Namerefs[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:array)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    local -i i
    if [[ -n "$namerefs" ]]; then
	for arg in "${array[@]}"; do
	    local -a Result=()
	    local -n arr="$arg"
	    local -a Indices=( ${!arr[@]} )
	    for ((i=${#Indices[@]} - 1; i >= 0; i--)) ; do
		Result+=("${arr[Indices[i]]}")
	    done
	    arr=("${Result[@]}"); done
    else
	for arg in "${array[@]}"; do
	    local -a Result=()
	    local -a Arr="$arg"
	    #local -a Indices=( ${!arr[@]} )
	    for ((i=1; i <= ${#Arr[@]}; i++)) ; do
		Result+=("${Arr[@]: -$i:1}")
	    done;
	    printf '%s\n' "(${Result[*]@Q})"; done; fi
}
arr_rev_test_f(){
    local a b
    local -a apa=(a b c d e)
    a=$(if [[ "(${apa[*]@Q})" == "$(bcu__arr_rev "(${apa[*]@Q})" | bcu__arr_rev)" ]]; then echo true; else echo false; fi)
    b=$(declare -a temp="(${apa[*]@Q})"; bcu__arr_rev -n -- apa; bcu__arr_rev -n -- apa; if [[ "(${temp[*]@Q})" == "(${apa[*]@Q})" ]]; then echo true; else echo false; fi)
    local -a c=$(bcu__arr_rev -n -- apa; printf '%s' "(${apa[*]@Q})")
    declare -p a b c
}
arr_rev_test_1()(
    . <(arr_rev_test_f)
    [[ "${a}" == true ]] || { echo FAIL && return 1; }
    [[ "${b}" == true ]] || { echo FAIL && return 1; }
    [[ "${c[@]}" == "e d c b a" ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
arr_rev_test_main(){
    printf arr_rev_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )arr_rev_test_1(\ |$) ]]; then arr_rev_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	arr_rev_test_main "${org_args[@]}"
    else
	arr_rev_test_main "${@}"
    fi
fi
# arr_rev-test-main ends here
