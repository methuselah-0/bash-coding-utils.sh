#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::is_var_set-test-main][is_var_set-test-main]]
org_args='()'
. bcu.sh
bcu__is_var_set(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() variable_name
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Returns true if the input string resolves to a set variable and false
otherwise, including arrays.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa bepa cepa depa
    declare -A apa=([a]="b" )
    declare -A bepa[a]=b
    unset bepa[a]
    declare -A cepa
    echo
    declare -p apa bepa cepa depa
    for var in apa bepa cepa depa; do if bcu__is_var_set "$var"; then echo true; else echo false; fi ; done
    f(){ local -A arr; unset arr; declare -p arr; bcu__is_var_set arr || echo not set; }; f
  
Results:

    declare -A apa=([a]="b" )
    declare -A bepa=()
    declare -A cepa
    bash: declare: depa: not found
    true
    true
    true
    bash: declare: : not found
    false
    declare -- arr
    not set
    
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:variable_name)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    # if declare -p output declare -- <var> we must check [[ -v <var>
    # ]], due to bash's behaviour when unsetting a local variable in a
    # function.

    local a; a=$(getReferencedVar "${variable_name}")
    #eval "[[ -z \${$a]+x} ]] && return 0"; fi
    if [[ "$(declare -p "$a")" =~ ^declare\ --\ "$a"$ ]]; then
	[[ -v "$a" ]] && return 0
    else
	declare -p "${variable_name}" &>/dev/null; fi
}
is_var_set_test_f()(
    local apa bepa cepa depa a b c d e f g h
    for var in apa bepa cepa depa a b c d e f g h; do unset "$var"; done
    unset depa
    declare -A apa=([a]="b" )
    declare -A bepa[a]=b
    unset bepa[a]
    declare -A cepa
    #declare -p apa bepa cepa depa
    a=$(if bcu__is_var_set apa; then echo true; else echo false; fi)
    b=$(if bcu__is_var_set bepa; then echo true; else echo false; fi)
    c=$(if bcu__is_var_set cepa; then echo true; else echo false; fi)
    d=$(if bcu__is_var_set depa; then echo true; else echo false; fi)
    e=$(if printf '%s\n' apa | bcu__is_var_set ; then echo true; else echo false ; fi)
    f=$(if printf '%s\n' bepa | bcu__is_var_set ; then echo true; else echo false ; fi)
    g=$(if printf '%s\n' cepa | bcu__is_var_set ; then echo true; else echo false ; fi)
    h=$(if printf '%s\n' depa | bcu__is_var_set ; then echo true; else echo false ; fi)
    declare -p a b c d e f g h #| tee -a >/dev/stderr
)
is_var_set_test_1()(
    . <(is_var_set_test_f)
    [[ "${a}" == true ]] || { echo FAIL && return 1; }
    [[ "${b}" == true ]] || { echo FAIL && return 1; }
    [[ "${c}" == true ]] || { echo FAIL && return 1; }
    [[ "${d}" == false ]] || { echo FAIL && return 1; }
    [[ "${e}" == true ]] || { echo FAIL && return 1; }
    [[ "${f}" == true ]] || { echo FAIL && return 1; }
    [[ "${g}" == true ]] || { echo FAIL && return 1; }
    [[ "${h}" == false ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
is_var_set_test_main(){
    printf is_var_set_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )is_var_set_test_1(\ |$) ]]; then is_var_set_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	is_var_set_test_main "${org_args[@]}"
    else
	is_var_set_test_main "${@}"
    fi
fi
# is_var_set-test-main ends here
