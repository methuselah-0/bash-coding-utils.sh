#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::is_array-test-main][is_array-test-main]]
org_args='()'
. bcu.sh
bcu__is_array(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() variable_name
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Check whether a string that is a variable name either holds or
references an indexed array indirectly via namereferences.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa; declare -a apa
    unset bepa; declare -A bepa
    bcu__is_array apa && echo true
    bcu__is_array bepa || echo false
  
Results:

    true
    false
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:variable_name)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    local a; a=$(getReferencedVar "${variable_name}")
    [[ "$(declare -p "$a" 2>/dev/null)" == "declare -a"* ]] && return 0
    return 1
}
is_array_test_f(){
    local -a apa
    local a
    a=$(if bcu__is_array apa; then echo true; else echo false; fi)
    declare -p a
}
is_array_test_1()(
    . <(is_array_test_f)
    [[ "${a}" == true ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
is_array_test_main(){
    printf is_array_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )is_array_test_1(\ |$) ]]; then is_array_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	is_array_test_main "${org_args[@]}"
    else
	is_array_test_main "${@}"
    fi
fi
# is_array-test-main ends here
