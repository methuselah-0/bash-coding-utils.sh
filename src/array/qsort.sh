#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::qsort.sh][qsort.sh]]
. ctypes.sh
read -r inline
declare -a input="$inline" || bcu__stack "$@"
declare -i sortsize=${#input[@]}     # size of array

set -e

# int compare(const void *, const void *)
function compare {
    local -a x=(int)
    local -a y=(int)
    local -a result

    # extract the parameters
    unpack $2 x
    unpack $3 y

    # remove the prefix
    x=${x##*:}
    y=${y##*:}

    # calculate result
    result=(int:$((x - y)))

    # return result to caller
    pack $1 result

    return
}

# Generate a function pointer to compare that can be called from native code.
callback -n compare compare int pointer pointer

# Prefix the array with correct C type
for ((i = 0; i < ${#input[@]}; i++)); do
    values+=(int:${input[$i]})
done

# Allocate space for integers
dlcall -n buffer -r pointer malloc $((sortsize * 4))

# Pack our random array into that native array
pack $buffer values

# Now qsort can sort them
dlcall qsort $buffer long:$sortsize long:4 $compare

# Unpack the sorted array back into a bash array
unpack $buffer values

#printf '%s' "(${values[*]@Q})"
printf '%s\n' "${values[@]##*:}"
# qsort.sh ends here
