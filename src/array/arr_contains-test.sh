#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::arr_contains-test-main][arr_contains-test-main]]
org_args='()'
. bcu.sh
bcu__arr_contains(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() string str
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Return true if all given array(s) contain the given string(s).


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa; apa=(a b c d e)
    bcu__arr_contains -a "(${apa[*]@Q})" -- f || echo false
    bcu__arr_contains -a "(${apa[*]@Q})" -- f a || echo false
    bcu__arr_contains -a "(${apa[*]@Q})" -- c && echo true
    bcu__arr_contains -a "(${apa[*]@Q})" -- c d && echo true
    printf '%s\u0000' c d | bcu__arr_contains -a "(${apa[*]@Q})" -z && echo true
    printf '%s\n' c d | bcu__arr_contains -a "(${apa[*]@Q})" && echo true
  
Results:

    false
    false
    true
    true
    true
    true
      
EOF
		)
    }

    # Options
    local Array=(a array "()" "An array to check for content" 1 t t)
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})" "(${Array[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    

    local -i ii

    for Array in "${array[@]}"; do
	local -a Array="${Array}"
	for str in "${string[@]}"; do
	    local found=""
	    for ((ii=0;ii<${#Array[@]};ii++)); do
		[[ "${Array[$ii]}" == "$str" ]] && found=yes; done
	    [[ -z "$found" ]] && return 1; done; done
    return 0
}
arr_contains_test_f(){
    local -a apa
    local a b
    apa=(a b c d e)
    a=$(if bcu__arr_contains -a "(${apa[*]@Q})" -- c; then echo true; else echo false; fi)
    b=$(if bcu__arr_contains -a "(${apa[*]@Q})" -- f; then echo true; else echo false; fi)
    declare -p a b # | tee -a >/dev/stderr
}
arr_contains_test_1()(
    . <(arr_contains_test_f)
    [[ "${a}" == true ]] || { echo FAIL && return 1; }
    [[ "${b}" == false ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
arr_contains_test_main(){
    printf arr_contains_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )arr_contains_test_1(\ |$) ]]; then arr_contains_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	arr_contains_test_main "${org_args[@]}"
    else
	arr_contains_test_main "${@}"
    fi
fi
# arr_contains-test-main ends here
