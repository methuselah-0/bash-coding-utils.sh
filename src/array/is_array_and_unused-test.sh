#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::is_array_and_unused-test-main][is_array_and_unused-test-main]]
org_args='()'
. bcu.sh
bcu__is_array_and_unused(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() variable_name
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Returns true only if a variable name is declared an indexed array and
not being set to an empty or non-empty array.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa; unset bepa; declare -a apa; declare -a bepa; bepa=(one)
    bcu__is_array_and_unused apa && echo true
    bcu__is_array_and_unused bepa || echo false
  
Results:

    true
    false
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:variable_name)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    if bcu__is_array "${variable_name}"; then
	local a; a=$(getReferencedVar "${variable_name}")
	#eval "[[ -z \${$a]+x} ]] && return 0"; fi
	[[ "$(declare -p "$a")" =~ ^'declare -a '"${a}"$ ]] && return 0; fi
    return 1
}
is_array_and_unused_test_f(){
    local -a apa bepa cepa
    apa=(one)
    bepa=()
    local a b c
    a=$(if bcu__is_array_and_unused apa; then echo true; else echo false; fi)
    b=$(if bcu__is_array_and_unused bepa; then echo true; else echo false; fi)
    c=$(if bcu__is_array_and_unused cepa; then echo true; else echo false; fi)
    declare -p a b c
}
is_array_and_unused_test_1()(
    . <(is_array_and_unused_test_f)
    [[ "${a}" == false ]] || { echo FAIL && return 1; }
    [[ "${b}" == false ]] || { echo FAIL && return 1; }
    [[ "${c}" == true ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
is_array_and_unused_test_main(){
    printf is_array_and_unused_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )is_array_and_unused_test_1(\ |$) ]]; then is_array_and_unused_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	is_array_and_unused_test_main "${org_args[@]}"
    else
	is_array_and_unused_test_main "${@}"
    fi
fi
# is_array_and_unused-test-main ends here
