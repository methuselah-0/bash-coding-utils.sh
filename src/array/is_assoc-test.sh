#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::is_assoc-test-main][is_assoc-test-main]]
org_args='()'
. bcu.sh
bcu__is_assoc(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() variable_name
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
is_assoc checks whether a given string is a variable that holds an
associative array, or a reference to it. It checks recursively whether
any namereferences eventually references an associative array before
deciding that a given input string is or isn't an associative array.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa; declare -A apa
    declare -n a=apa
    bcu__is_assoc a && echo true
    unset bepa; declare -A bepa
    bcu__is_assoc bepa && echo true
  
Results:

    true
    true
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:variable_name)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    [[ "$(declare -p "$(getReferencedVar "${variable_name}")" 2>/dev/null)" == "declare -A"* ]] && return 0
    return 1
}
is_assoc_test_f(){
    local -A apa
    local a b
    local -a bepa
    a=$(if bcu__is_assoc apa; then echo true; else echo false; fi)
    b=$(if bcu__is_assoc bepa; then echo true; else echo false; fi)
    declare -p a b
}
is_assoc_test_1()(
    . <(is_assoc_test_f)
    [[ "${a}" == true ]] || { echo FAIL && return 1; }
    [[ "${b}" == false ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
is_assoc_test_main(){
    printf is_assoc_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )is_assoc_test_1(\ |$) ]]; then is_assoc_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	is_assoc_test_main "${org_args[@]}"
    else
	is_assoc_test_main "${@}"
    fi
fi
# is_assoc-test-main ends here
