#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::arr_head-test-main][arr_head-test-main]]
org_args='()'
. bcu.sh
bcu__arr_head(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() array include nameref
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Return the head of an array up to and including a specified number of
elements.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa bepa cepa depa
    apa=(one two three four)
    bepa=(two three four five)
    declare -a cepa=$(printf '%s' "(${apa[*]@Q})" | bcu__arr_head -i 3)
    declare -a depa=$(printf '%s\n' "(${apa[*]@Q})" "(${bepa[*]@Q})" | bcu__arr_diff --out intersection | bcu__arr_head -i 1 --)
    echo && declare -p cepa depa
  
Results:

    declare -a cepa=([0]="one" [1]="two" [2]="three")
    declare -a depa=([0]="two")
      
EOF
		)
    }

    # Options
    local Include=(i include "" "Return an array with i number of elements included in the output array starting from the first element" 1 t f)
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Nameref=(n nameref "" "Read arg as a namereference" 0)
    Options+=("(${null[*]@Q})" "(${Include[*]@Q})" "(${Nameref[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:array)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    if [[ -n "$nameref" ]]; then
	local -n arr="${array}" &>/dev/null || { printf '%s' "array does not reference an array: ${_STACK}" && bcu__stack "$@" && return 1; }
	printf '%s' "(${arr[@]:0:$ii})"
    else
	local -a Array="${array}" &>/dev/null || { printf '%s' "array is not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }
	local -i ii="$include" &>/dev/null || { printf '%s' "Option argument to include is not a valid integer: ${_STACK}" && bcu__stack "$@" && return 1; }
	printf '%s' "(""${Array[*]:0:$ii}"")"; fi
}
arr_head_test_f(){
    local -a apa bepa
    local a b
    apa=(one two three four)
    bepa=(two three four five)
    local -a a=$(printf '%s' "(${apa[*]@Q})" | bcu__arr_head -i 2)
    local -a b=$(printf '%s\n' "(two three four)" | bcu__arr_head -i 2)
    declare -p a b
}
arr_head_test_1()(
    . <(arr_head_test_f)
    [[ "${a[0]}" == "one" ]] || { echo FAIL && return 1; }
    [[ "${a[1]}" == "two" ]] || { echo FAIL && return 1; }
    [[ "${b[0]}" == "two" ]] || { echo FAIL && return 1; }
    [[ "${b[1]}" == "three" ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
arr_head_test_main(){
    printf arr_head_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )arr_head_test_1(\ |$) ]]; then arr_head_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	arr_head_test_main "${org_args[@]}"
    else
	arr_head_test_main "${@}"
    fi
fi
# arr_head-test-main ends here
