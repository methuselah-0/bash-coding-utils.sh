#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::arr_diff-test-main][arr_diff-test-main]]
org_args='()'
. bcu.sh
bcu__arr_diff(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() array1 array2
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
diff arrays as sets and return the diff.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa bepa cepa depa epa fepa
    apa=(a b c)
    bepa=(a b)
    cepa=(b c d)
    declare -a depa=$(bcu__arr_diff -n --out first-only -- apa bepa)
    declare -a epa=$(bcu__arr_diff -n --out second-only -- bepa cepa)
    declare -a fepa=$(printf '%s\n' bepa cepa | bcu__arr_diff -n --out second-only)
    declare -a gepa=$(bcu__arr_diff -n --out intersection -- bepa cepa)
    declare -p depa epa fepa gepa
  
Results:

    declare -a depa=([0]="c")
    declare -a epa=([0]="c" [1]="d")
    declare -a fepa=([0]="c" [1]="d")
    declare -a gepa=([0]="b")
      
EOF
		)
    }

    # Options
    local -a Out=(o out "" 'Valid args are "first-only", "second-only" and "intersection"' 1 t f)
    local z
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Namerefs=(n namerefs "" "Read args as namereferences" 0)
    Options+=("(${null[*]@Q})" "(${Out[*]@Q})" "(${Namerefs[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:array1 1:t:array2)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    if [[ "$out" == intersection ]]; then
	local Result=()

	if [[ -n "$namerefs" ]]; then
	    local -n Input1="${array1}"
	    local -n Input2="${array2}"
	else
	    local -a Input1="${array1}"
	    local -a Input2="${array2}"; fi

	local item1 item2
	for item1 in "${Input1[@]}"; do
	    for item2 in "${Input2[@]}"; do
		if [[ "$item1" == "$item2" ]]; then
		    Result+=("$item1")
		fi
	    done
	done
	[[ -n "$Result" ]] && printf '%s' "(${Result[*]@Q})" && return 0
	return 1
    fi

    # Create local arrays depending on namerefs vs
    # array-quote-expanded input
    if [[ -n "$namerefs" ]]; then
	if [[ "$out" == first-only ]]; then
	    # shellcheck disable=2034
	    declare -n arr_diffL_LArr="${array1}"
	    # shellcheck disable=2034
	    declare -n arr_diffL_RArr="${array2}"
	elif [[ "$out" == second-only ]]; then
	    # shellcheck disable=2034
	    declare -n arr_diffL_LArr="${array2}"
	    # shellcheck disable=2034
	    declare -n arr_diffL_RArr="${array1}"
	else
	    bcu__stack "$@"; fi
    else
	local -a Input1="${array1}"
	local -a Input2="${array2}"
	if [[ "$out" == first-only ]]; then
	    local -a arr_diffL_LArr="(${Input1[*]@Q})"
	    local -a arr_diffL_RArr="(${Input2[*]@Q})"
	elif [[ "$out" == second-only ]]; then
	    local -a arr_diffL_LArr="(${Input2[*]@Q})"
	    local -a arr_diffL_RArr="(${Input1[*]@Q})"; fi; fi
    local -n LArr=arr_diffL_LArr
    local -n RArr=arr_diffL_RArr

    local -a DontExist=()
    for obj in "${LArr[@]}"; do
	local found="no"
	for ((i=0;i<"${#RArr[@]}";i++)); do
	    if [[ "${RArr[$i]}" = "$obj" ]]; then
		found="yes"; fi; done
	if [[ "$found" = "no" ]]; then
	    DontExist+=("$obj"); fi; done
    
    printf '%s' "(${DontExist[*]@Q})"
    [[ -n  "$namerefs" ]] && { unset arr_diffL_LArr && unset arr_diffL_RArr ; }
}
arr_diff_test_f(){
    local apa bepa cepa
    apa=(a b c)
    bepa=(a b)
    cepa=(b c d)
    local -a a=$(bcu__arr_diff -n --out first-only -- apa bepa)
    local -a b=$(bcu__arr_diff -n --out second-only -- bepa cepa)
    local -a c=$(printf '%s\n' bepa cepa | bcu__arr_diff -n --out second-only)
    local -a d=$(bcu__arr_diff -n --out intersection -- bepa cepa)
    local -a e=$(bcu__arr_diff --out intersection -- "(${bepa[*]@Q})" "(${cepa[*]@Q})")
    declare -p a b c d e
}
arr_diff_test_1()(
    . <(arr_diff_test_f)
    [[ "${#a[@]}" == 1 ]] || { echo FAIL && return 1; }
    [[ "${a[0]}" == c ]] || { echo FAIL && return 1; }
    [[ "${#b[@]}" == 2 ]] || { echo FAIL && return 1; }
    [[ "${b[0]}" == c ]] || { echo FAIL && return 1; }
    [[ "${b[1]}" == d ]] || { echo FAIL && return 1; }
    [[ "${#c[@]}" == 2 ]] || { echo FAIL && return 1; }
    [[ "${c[0]}" == c ]] || { echo FAIL && return 1; }
    [[ "${c[1]}" == d ]] || { echo FAIL && return 1; }
    [[ "${d[0]}" == b ]] || { echo FAIL && return 1; }
    [[ "${e[0]}" == b ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
arr_diff_test_main(){
    printf arr_diff_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )arr_diff_test_1(\ |$) ]]; then arr_diff_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	arr_diff_test_main "${org_args[@]}"
    else
	arr_diff_test_main "${@}"
    fi
fi
# arr_diff-test-main ends here
