#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::array.sh][array.sh]]
[[ "$_BCU_SH_LOADED" == YES ]] || . bcu.sh
_array_MOD_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")
bcu__is_assoc(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() variable_name
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
is_assoc checks whether a given string is a variable that holds an
associative array, or a reference to it. It checks recursively whether
any namereferences eventually references an associative array before
deciding that a given input string is or isn't an associative array.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa; declare -A apa
    declare -n a=apa
    bcu__is_assoc a && echo true
    unset bepa; declare -A bepa
    bcu__is_assoc bepa && echo true
  
Results:

    true
    true
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:variable_name)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    [[ "$(declare -p "$(getReferencedVar "${variable_name}")" 2>/dev/null)" == "declare -A"* ]] && return 0
    return 1
}
bcu__is_array(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() variable_name
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Check whether a string that is a variable name either holds or
references an indexed array indirectly via namereferences.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa; declare -a apa
    unset bepa; declare -A bepa
    bcu__is_array apa && echo true
    bcu__is_array bepa || echo false
  
Results:

    true
    false
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:variable_name)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    local a; a=$(getReferencedVar "${variable_name}")
    [[ "$(declare -p "$a" 2>/dev/null)" == "declare -a"* ]] && return 0
    return 1
}
bcu__is_var_set(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() variable_name
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Returns true if the input string resolves to a set variable and false
otherwise, including arrays.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa bepa cepa depa
    declare -A apa=([a]="b" )
    declare -A bepa[a]=b
    unset bepa[a]
    declare -A cepa
    echo
    declare -p apa bepa cepa depa
    for var in apa bepa cepa depa; do if bcu__is_var_set "$var"; then echo true; else echo false; fi ; done
    f(){ local -A arr; unset arr; declare -p arr; bcu__is_var_set arr || echo not set; }; f
  
Results:

    declare -A apa=([a]="b" )
    declare -A bepa=()
    declare -A cepa
    bash: declare: depa: not found
    true
    true
    true
    false
    declare -- arr
    not set
    
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:variable_name)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    # if declare -p output declare -- <var> we must check [[ -v <var>
    # ]], due to bash's behaviour when unsetting a local variable in a
    # function.

    local a; a=$(getReferencedVar "${variable_name}")
    #eval "[[ -z \${$a]+x} ]] && return 0"; fi
    if [[ "$(declare -p "$a" 2>/dev/null)" =~ ^declare\ --\ "$a"$ ]]; then
	[[ -v "$a" ]] && return 0
    else
	declare -p "${variable_name}" &>/dev/null; fi
}
bcu__is_assoc_and_unused(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() variable_name
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
is_assoc_and_unused returns true only if a variable name is declared
an associative array and not being set to an empty or non-empty array.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa bepa cepa; declare -A apa bepa cepa
    apa[a]=b; unset apa[a]
    bepa[one]=two
    echo
    declare -p apa bepa cepa
    bcu__is_assoc_and_unused apa || echo false
    bcu__is_assoc_and_unused bepa || echo false
    bcu__is_assoc_and_unused cepa && echo true
  
Results:

    declare -A apa=()
    declare -A bepa=([one]="two" )
    declare -A cepa
    false
    false
    true
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:variable_name)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    if bcu__is_assoc "${variable_name}"; then
	local a; a=$(getReferencedVar "${variable_name}")
	#eval "[[ -z \${$a]+x} ]] && return 0"; fi
	[[ "$(declare -p "$a")" =~ ^'declare -A '"${a}"$ ]] && return 0; fi
    #eval "[[ -v \"$a\" ]] && return 0"; fi
    return 1
}
bcu__is_array_and_unused(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() variable_name
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Returns true only if a variable name is declared an indexed array and
not being set to an empty or non-empty array.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa; unset bepa; declare -a apa; declare -a bepa; bepa=(one)
    bcu__is_array_and_unused apa && echo true
    bcu__is_array_and_unused bepa || echo false
  
Results:

    true
    false
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:variable_name)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    if bcu__is_array "${variable_name}"; then
	local a; a=$(getReferencedVar "${variable_name}")
	#eval "[[ -z \${$a]+x} ]] && return 0"; fi
	[[ "$(declare -p "$a")" =~ ^'declare -a '"${a}"$ ]] && return 0; fi
    return 1
}
bcu__string_to_arr(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Convert a string to an indexed array.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    declare -a MyArr=$(bcu__string_to_arr MyString)
    declare -a MyArr2=$(bcu__string_to_arr string1 string2)
    echo && declare -p MyArr MyArr2
  
Results:

    declare -a MyArr=([0]="M" [1]="y" [2]="S" [3]="t" [4]="r" [5]="i" [6]="n" [7]="g")
    declare -a MyArr2=([0]="s" [1]="t" [2]="r" [3]="i" [4]="n" [5]="g" [6]="1")
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    # splits into array 
    # [[ "$string" =~ ${string//?/(.)} ]]
    [[ "${string}" =~ ${string//?/(.)} ]]       # splits into array
    local -a arr=( "${BASH_REMATCH[@]:1}" ) # copy array for later
    printf '%s' "(${arr[*]@Q})"
}
bcu__arr_push(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() Strings
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Add data to multiple arrays in one command.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa apa2 Stuff0 Stuff1 Stuff1_3 bepa
    apa=(one two 'three four')
    apa2=(two two two)
    bcu__arr_push -n -a apa -- apa bepa
    echo
    mapfile -t Stuff < <(printf '%s\n' "(${apa2[*]@Q})" | bcu__arr_push -a "(${apa[*]@Q})" -a "(${apa2[*]@Q})")
    declare -a Stuff0="${Stuff[0]}"
    declare -a Stuff1="${Stuff[1]}"
    declare -a Stuff1_3="${Stuff1[3]}"
    echo
    declare -p Stuff Stuff0 Stuff1 Stuff1_3
    bcu__arr_push -n -a bepa -- apa bepa
    echo
    declare -p bepa
  
Results:

    declare -a Stuff=([0]="('one' 'two' 'three four' 'apa' 'bepa' '('\\''two'\\'' '\\''two'\\'' '\\''two'\\'')')" [1]="('two' 'two' 'two' '('\\''two'\\'' '\\''two'\\'' '\\''two'\\'')')")
    declare -a Stuff0=([0]="one" [1]="two" [2]="three four" [3]="apa" [4]="bepa" [5]="('two' 'two' 'two')")
    declare -a Stuff1=([0]="two" [1]="two" [2]="two" [3]="('two' 'two' 'two')")
    declare -a Stuff1_3=([0]="two" [1]="two" [2]="two")
    declare -a bepa=([0]="apa" [1]="bepa")
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Arrays=(a Arrays "" "Variable name of an array to push arguments to" 1 t t)
    local Nameref=(n nameref "" "Read all given arrays as namereferences. With this option arr_push will not print the resulting array(s) back to stdout" 0)
    Options+=("(${null[*]@Q})" "(${Arrays[*]@Q})" "(${Nameref[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:Strings)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    if [[ -n "$nameref" ]]; then
	for Array in "${Arrays[@]}"; do
	    #local -a 'Arr=("${'"$Array"'[@]}")'
	    #local -n Arr="$Array"
	    local -n bcu__arr_push_Arr="$Array"
	    #Arr=("${Arr[@]}" "${Strings[@]}"); done
	    bcu__arr_push_Arr=("${bcu__arr_push_Arr[@]}" "${Strings[@]}"); done
    else
	for Array in "${Arrays[@]}"; do
	    local -a Arr="$Array"
	    Arr=("${Arr[@]}" "${Strings[@]}")
	    printf '%s\n' "(${Arr[*]@Q})"; done; fi
}
bcu__arr_pop(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() array
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
arr_pop pulls the last element off of the given array.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa; apa=(one two three four)
    bcu__arr_pop -n -- apa && declare -p apa
    printf '%s' "(${apa[*]@Q})" | bcu__arr_pop && echo
  
Results:

    declare -a apa=([0]="one" [1]="two" [2]="three")
    ('three')
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Nameref=(n nameref "" "Read arg as a namereference. This option will not print the resulting array back to stdout" 0)
    Options+=("(${null[*]@Q})" "(${Nameref[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:array)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    if [[ -n "$nameref" ]]; then
	local -n Arr; Arr="${array}"
	local -i i
	i=$((${#Arr[@]}-1))
	unset Arr["$i"]
    else
	local -a Arr="${array}"
	local -i i
	i=$((${#Arr[@]}-1))
	local -a NewArr=("${Arr[@]: -1:$i}")
	printf '%s' "(${NewArr[*]@Q})"; fi
}
bcu__arr_shift(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() array
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
arr_shift removes the first element of the passed array and returns it
unless the namereference option is given. The namereference uses "arr"
locally, meaning that the array variable name used must not be named
"arr" if invoked with this option. The main benefit is that it
left-shifts the index of the array instead of just unsets index 0. If
the namereference options is not used, arr_shift prints an
array-declarable string back.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset myarr array2;
    myarr=(one two three)
    declare -a array2=$(bcu__arr_shift "(${myarr[*]@Q})")
    bcu__arr_shift -n -- myarr
    echo
    declare -p array2 myarr

Results:

    declare -a array2=([0]="two" [1]="three")
    declare -a myarr=([0]="two" [1]="three")

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Nameref=(n nameref "" "Read arg as a namereference. This option will not print the resulting array back to stdout" 0)
    Options+=("(${null[*]@Q})" "(${Nameref[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:array)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    if [[ -n "$nameref" ]]; then
	local -n arr="${array}"
	arr=("${arr[@]:1}")
    else
	local -a Arr="${array}"
	Arr=("${Arr[@]:1}")
	printf '%s' "(${Arr[*]@Q})"; fi
}
bcu__arr_unshift(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() Array String Arr
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
arr_unshift prepends args to one or several arrays. It may consider
the given arrays as variable names instead of quote-expanded
arrays. If given more than 1 input array that is quote-expanded,
arr_unshift prints the output arrays as array-declarable strings
newline-separated.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset myarr; myarr=(one two)
    bcu__arr_unshift -n -a myarr -- three four
    echo && declare -p myarr
  
Results:

    declare -a myarr=([0]="three" [1]="four" [2]="one" [3]="two")
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Arrays=(a Array "" "Variable name of an array to prepend arguments to" 1 t t)
    local Nameref=(n nameref "" "Read arg as a namereference. This option will not print the resulting array back to stdout" 0)
    Options+=("(${null[*]@Q})" "(${Nameref[*]@Q})" "(${Arrays[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:String)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    if [[ -n "$nameref" ]]; then
	for Arr in "${Array[@]}"; do
	    declare -n arr="$Arr"
	    arr=("${String[@]}" "${arr[@]}"); done
    else
	if [[ "${#Array[@]}" -gt 1 ]]; then
	    for Arr in "${Array[@]}"; do
		local -a Arr="${Arr}"
		printf '%s' "(${Args[*]@Q} ${Arr[*]@Q})" $'\n'; done
	else
	    local -a Arr="${Array[0]}"
	    printf '%s' "(${Args[*]@Q} ${Arr[*]@Q})"; fi; fi
}
bcu__arr_head(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() array include nameref
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Return the head of an array up to and including a specified number of
elements.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa bepa cepa depa
    apa=(one two three four)
    bepa=(two three four five)
    declare -a cepa=$(printf '%s' "(${apa[*]@Q})" | bcu__arr_head -i 3)
    declare -a depa=$(printf '%s\n' "(${apa[*]@Q})" "(${bepa[*]@Q})" | bcu__arr_diff --out intersection | bcu__arr_head -i 1 --)
    echo && declare -p cepa depa
  
Results:

    declare -a cepa=([0]="one" [1]="two" [2]="three")
    declare -a depa=([0]="two")
      
EOF
		)
    }

    # Options
    local Include=(i include "" "Return an array with i number of elements included in the output array starting from the first element" 1 t f)
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Nameref=(n nameref "" "Read arg as a namereference" 0)
    Options+=("(${null[*]@Q})" "(${Include[*]@Q})" "(${Nameref[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:array)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    if [[ -n "$nameref" ]]; then
	local -n arr="${array}" &>/dev/null || { printf '%s' "array does not reference an array: ${_STACK}" && bcu__stack "$@" && return 1; }
	printf '%s' "(${arr[@]:0:$ii})"
    else
	local -a Array="${array}" &>/dev/null || { printf '%s' "array is not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }
	local -i ii="$include" &>/dev/null || { printf '%s' "Option argument to include is not a valid integer: ${_STACK}" && bcu__stack "$@" && return 1; }
	printf '%s' "(""${Array[*]:0:$ii}"")"; fi
}
bcu__arr_tail(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() array
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Return the tail of an array starting from a given index.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa bepa
    apa=(a b c d)
    declare -a bepa=$(bcu__arr_tail -i 1 -n -- apa)
    echo && declare -p bepa
  
Results:

    declare -a bepa=([0]="b" [1]="c" [2]="d")
      
EOF
		)
    }

    # Options
    local Index=(i index "" "Return an array with the elements in the input array starting from the i'th element" 1 t f)
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Nameref=(n nameref "" "Read array as a namereference" 0)
    Options+=("(${null[*]@Q})" "(${Index[*]@Q})" "(${Nameref[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:array)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    if [[ -n "$nameref" ]]; then
	local -n arr="${array}" || { printf '%s' "array is not a valid variable name: ${_STACK}" && bcu__stack "$@" && return 1; }
	
    else
	local -a arr="${array}" || { printf '%s' "array is not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }; fi

    local -i ii="$index" || { printf '%s' "index is not a valid integer: ${_STACK}" && bcu__stack "$@" && return 1; }

    printf '%s' "(""${arr[*]:$ii}"")"
}
bcu__arr_is_subset(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() array1 array2
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Return true if the elements of array1 constitutes a subset of the
elements of array2.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa bepa
    apa=(a b c)
    bepa=(a b c d)
    bcu__arr_is_subset -n -- apa bepa && echo true
    bcu__arr_is_subset "(${apa[*]@Q})" "(${bepa[*]@Q})" && echo true
    bcu__arr_is_subset -n -- bepa apa || echo false
    bcu__arr_is_subset "(${bepa[*]@Q})" "(${apa[*]@Q})" || echo false
    
  
Results:

    true
    true
    false
    false
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Namerefs=(n namerefs "" "Read array1 and array2 as namereferences" 0)
    Options+=("(${null[*]@Q})" "(${Namerefs[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:array1 1:t:array2)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    if [[ -n "$namerefs" ]]; then
	# shellcheck disable=SC2155
	local -a Arr=$(bcu__arr_diff -n --out first-only -- "${array1}" "${array2}")
    else
	local -a Input1="${array1}" || { printf '%s' "array1 is not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }

	local -a Input2="${array2}" || { printf '%s' "array2 is not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }

	local -a Arr=$(bcu__arr_diff --out first-only -- "$array1" "$array2" ); fi
    [[ -z "${Arr[*]}" ]] && return 0
    return 1
}
bcu__arr_diff(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() array1 array2
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
diff arrays as sets and return the diff.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa bepa cepa depa epa fepa
    apa=(a b c)
    bepa=(a b)
    cepa=(b c d)
    declare -a depa=$(bcu__arr_diff -n --out first-only -- apa bepa)
    declare -a epa=$(bcu__arr_diff -n --out second-only -- bepa cepa)
    declare -a fepa=$(printf '%s\n' bepa cepa | bcu__arr_diff -n --out second-only)
    declare -a gepa=$(bcu__arr_diff -n --out intersection -- bepa cepa)
    declare -p depa epa fepa gepa
  
Results:

    declare -a depa=([0]="c")
    declare -a epa=([0]="c" [1]="d")
    declare -a fepa=([0]="c" [1]="d")
    declare -a gepa=([0]="b")
      
EOF
		)
    }

    # Options
    local -a Out=(o out "" 'Valid args are "first-only", "second-only" and "intersection"' 1 t f)
    local z
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Namerefs=(n namerefs "" "Read args as namereferences" 0)
    Options+=("(${null[*]@Q})" "(${Out[*]@Q})" "(${Namerefs[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:array1 1:t:array2)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    if [[ "$out" == intersection ]]; then
	local Result=()

	if [[ -n "$namerefs" ]]; then
	    local -n Input1="${array1}"
	    local -n Input2="${array2}"
	else
	    local -a Input1="${array1}"
	    local -a Input2="${array2}"; fi

	local item1 item2
	for item1 in "${Input1[@]}"; do
	    for item2 in "${Input2[@]}"; do
		if [[ "$item1" == "$item2" ]]; then
		    Result+=("$item1")
		fi
	    done
	done
	[[ -n "$Result" ]] && printf '%s' "(${Result[*]@Q})" && return 0
	return 1
    fi

    # Create local arrays depending on namerefs vs
    # array-quote-expanded input
    if [[ -n "$namerefs" ]]; then
	if [[ "$out" == first-only ]]; then
	    # shellcheck disable=2034
	    declare -n arr_diffL_LArr="${array1}"
	    # shellcheck disable=2034
	    declare -n arr_diffL_RArr="${array2}"
	elif [[ "$out" == second-only ]]; then
	    # shellcheck disable=2034
	    declare -n arr_diffL_LArr="${array2}"
	    # shellcheck disable=2034
	    declare -n arr_diffL_RArr="${array1}"
	else
	    bcu__stack "$@"; fi
    else
	local -a Input1="${array1}"
	local -a Input2="${array2}"
	if [[ "$out" == first-only ]]; then
	    local -a arr_diffL_LArr="(${Input1[*]@Q})"
	    local -a arr_diffL_RArr="(${Input2[*]@Q})"
	elif [[ "$out" == second-only ]]; then
	    local -a arr_diffL_LArr="(${Input2[*]@Q})"
	    local -a arr_diffL_RArr="(${Input1[*]@Q})"; fi; fi
    local -n LArr=arr_diffL_LArr
    local -n RArr=arr_diffL_RArr

    local -a DontExist=()
    for obj in "${LArr[@]}"; do
	local found="no"
	for ((i=0;i<"${#RArr[@]}";i++)); do
	    if [[ "${RArr[$i]}" = "$obj" ]]; then
		found="yes"; fi; done
	if [[ "$found" = "no" ]]; then
	    DontExist+=("$obj"); fi; done
    
    printf '%s' "(${DontExist[*]@Q})"
    [[ -n  "$namerefs" ]] && { unset arr_diffL_LArr && unset arr_diffL_RArr ; }
}
bcu__arr_contains(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() string str
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Return true if all given array(s) contain the given string(s).


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa; apa=(a b c d e)
    bcu__arr_contains -a "(${apa[*]@Q})" -- f || echo false
    bcu__arr_contains -a "(${apa[*]@Q})" -- f a || echo false
    bcu__arr_contains -a "(${apa[*]@Q})" -- c && echo true
    bcu__arr_contains -a "(${apa[*]@Q})" -- c d && echo true
    printf '%s\u0000' c d | bcu__arr_contains -a "(${apa[*]@Q})" -z && echo true
    printf '%s\n' c d | bcu__arr_contains -a "(${apa[*]@Q})" && echo true
  
Results:

    false
    false
    true
    true
    true
    true
      
EOF
		)
    }

    # Options
    local Array=(a array "()" "An array to check for content" 1 t t)
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})" "(${Array[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    

    local -i ii

    for Array in "${array[@]}"; do
	local -a Array="${Array}"
	for str in "${string[@]}"; do
	    local found=""
	    for ((ii=0;ii<${#Array[@]};ii++)); do
		[[ "${Array[$ii]}" == "$str" ]] && found=yes; done
	    [[ -z "$found" ]] && return 1; done; done
    return 0
}
bcu__arr_rev(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() array arg
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Reverse arrays


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa bepa Apa_Rev Bepa_Rev Rev_2
    apa=(a b c d)
    bepa=(e f g h)
    bepa[8]=i
    declare -a Apa_Rev=$(bcu__arr_rev "(${apa[*]@Q})")
    declare -a Bepa_Rev=$(bcu__arr_rev "(${bepa[*]@Q})")
    mapfile -t Rev_2 < <(bcu__arr_rev "(${apa[*]@Q})" "(${bepa[*]@Q})")
    bcu__arr_rev -n -- apa bepa
    declare -p apa bepa Apa_Rev Bepa_Rev Rev_2
  
Results:

    declare -a apa=([0]="d" [1]="c" [2]="b" [3]="a")
    declare -a bepa=([0]="i" [1]="h" [2]="g" [3]="f" [4]="e")
    declare -a Apa_Rev=([0]="d" [1]="c" [2]="b" [3]="a")
    declare -a Bepa_Rev=([0]="i" [1]="h" [2]="g" [3]="f" [4]="e")
    declare -a Rev_2=([0]="('d' 'c' 'b' 'a')" [1]="('i' 'h' 'g' 'f' 'e')")
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Namerefs=(n namerefs "" "Read args as namereferences. If using this option, the resulting arrays will not be printed back to stdout" 0)
    Options+=("(${null[*]@Q})" "(${Namerefs[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:array)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    local -i i
    if [[ -n "$namerefs" ]]; then
	for arg in "${array[@]}"; do
	    local -a Result=()
	    local -n arr="$arg"
	    local -a Indices=( ${!arr[@]} )
	    for ((i=${#Indices[@]} - 1; i >= 0; i--)) ; do
		Result+=("${arr[Indices[i]]}")
	    done
	    arr=("${Result[@]}"); done
    else
	for arg in "${array[@]}"; do
	    local -a Result=()
	    local -a Arr="$arg"
	    #local -a Indices=( ${!arr[@]} )
	    for ((i=1; i <= ${#Arr[@]}; i++)) ; do
		Result+=("${Arr[@]: -$i:1}")
	    done;
	    printf '%s\n' "(${Result[*]@Q})"; done; fi
}
bcu__arr_sort(){
    local _STACK z Options=() Input=() array line Sorted key
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Sort an array by length of its strings. Does not handle strings with
newlines. Prints results newline-separated.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset Arr
    mapfile -t Arr < <(printf '%s\n' apa bepa depta cepa)
    bcu__arr_sort "(${Arr[*]@Q})"
  
Results:

    apa
    bepa
    cepa
    depta
      
EOF
		)
    }
    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:array)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || {
	printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "" =~ ^(-h|--help)$ ]] && return 0

    local -a array="${array}"
    # Check args
    while read -r line; do
	if [ -z "${Sorted[${#line}]}" ] ; then          # does line length already exist?
	    Sorted[${#line}]="$line"                      # element for new length
	else
	    Sorted[${#line}]="${Sorted[${#line}]}\\n$line" # append to lines with equal length
	fi
    done < <(printf '%s\n' "${array[@]}")

    for key in ${!Sorted[*]}; do                      # iterate over existing indices
	printf '%b' "${Sorted[$key]}\\n"                       # echo lines with equal length
    done
}
bcu__map(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() args split array func elem
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Map vill apply a function to a list's elements and return the list
ordered with the output of applying the function in place of the
list's original elements. Additional arguments to the function may be
given and are always placed "to the left" of the element in the list,
i.e. the list element is always the last input argument to the
function that is being applied. Note that the function is applied in a
shell substitution context, meaning e.g. that any trailing newlines in
the resulting output from applying the function to an element will be
trimmed (see example below).


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa bepa cepa
    apa=(a 'b c' d)
    declare -a bepa=$(bcu__map -f printf -a "(${apa[*]@Q})" -- '%s\n' 'apa bepa' cepa)
    declare -a cepa=$(bcu__map -f printf -a "(${apa[*]@Q})" --split -- '%s\n' 'apa bepa' cepa)
    declare -p bepa
    declare -p cepa
  
Results:

    declare -a bepa=([0]=$'apa bepa\ncepa\na' [1]=$'apa bepa\ncepa\nb c' [2]=$'apa bepa\ncepa\nd')
    declare -a cepa=([0]=$'apa bepa\ncepa\na' [1]=$'apa bepa\ncepa\nb\nc' [2]=$'apa bepa\ncepa\nd')
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Split=(s split "" "Whether to split the elements by unquoted expansion" 0 f f)
    local Array=(a array "()" "The array to apply the function and args to" 1 t f)
    local Function=(f func "" "The function to apply" 1 t f)
    Options+=("(${null[*]@Q})" "(${Split[*]@Q})" "(${Array[*]@Q})" "(${Function[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:args)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    local -i input_length; input_length=${#args[@]}

    if [[ -n "$split" ]]; then
	if command -v "$func" &>/dev/null; then
	    local -a Array="${array}"
	    local -a OutArray
	    for elem in "${Array[@]}"; do
		# shellcheck disable=SC2086
		# intentionally split the elements by leaving $elem unquoted.
		OutArray+=("$(${func} "${args[@]}" $elem)"); done
	    printf '%s' "(${OutArray[*]@Q})"
	else
	    printf '%s' "arg 1 in map not a function: ${_STACK}" && bcu__stack "$@" && return 1; fi
    else
	if command -v "$func" >/dev/null 2>/dev/null; then
	    local -a Array="${array}"
	    local -a OutArray
	    for elem in "${Array[@]}"; do
		OutArray+=("$(${func} "${args[@]}" "$elem")"); done
	    printf '%s' "(${OutArray[*]@Q})"
	else
	    printf '%s' "arg 1 in map not a function: ${_STACK}" && bcu__stack "$@" && return 1; fi; fi
}
bcu__map_args(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() args elem array func split_elems split_args
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
A version of [[*map][map]] where the list elements are placed as first argument
to the function to be applied and the args after. Both args and
list-elements can be splitted (unquoted variable expansion).


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa bepa cepa depa
    apa=('%s\n' "  %sEXTRA\n")
    declare -a bepa=$(bcu__map_args -f printf -a "(${apa[*]@Q})" -- apa 'apa bepa' cepa)
    declare -a cepa=$(bcu__map_args -f printf -a "(${apa[*]@Q})" --split_elems -- apa 'apa bepa' cepa)
    declare -a depa=$(bcu__map_args -f printf -a "(${apa[*]@Q})" --split_elems --split_args -- apa 'apa bepa' cepa)
    echo
    declare -p bepa
    declare -p cepa
    declare -p depa
    echo
  
Results:

    declare -a bepa=([0]=$'apa\napa bepa\ncepa' [1]=$'  apaEXTRA\n  apa bepaEXTRA\n  cepaEXTRA')
    declare -a cepa=([0]=$'apa\napa bepa\ncepa' [1]=$'apaEXTRA\napa bepaEXTRA\ncepaEXTRA')
    declare -a depa=([0]=$'apa\napa\nbepa\ncepa' [1]=$'apaEXTRA\napaEXTRA\nbepaEXTRA\ncepaEXTRA')
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local SplitElems=(s split_elems "" "Whether to split the elements by unquoted expansion" 0 f f)
    local SplitArgs=(t split_args "" "Whether to split the input operands by unquoted expansion when provided as input arguments to the function" 0 f f)
    local Array=(a array "()" "The array to apply the function and args to" 1 t f)
    local Function=(f func "" "The function to apply" 1 t f)
    Options+=("(${null[*]@Q})" "(${SplitElems[*]@Q})" "(${SplitArgs[*]@Q})" "(${Array[*]@Q})" "(${Function[*]@Q})")
    
    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:f:args)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    local -a OutArray
    local -a Array="${array}"
    command -v "$func" &>/dev/null || bcu__stack "$@"

    if [[ -n "$split_elems" ]]; then
	if [[ -n "$split_args" ]]; then
	    for elem in "${Array[@]}"; do
		# shellcheck disable=SC2086
		# shellcheck disable=SC2068
		# intentionally split the elements by leaving $elem unquoted.
		OutArray+=("$("${func}" $elem ${args[@]})"); done
	else
	    for elem in "${Array[@]}"; do
		# shellcheck disable=SC2086
		# intentionally split the elements by leaving $elem unquoted.
		OutArray+=("$("$func" $elem "${args[@]}")"); done; fi
    else
	if [[ -n "$split_args" ]]; then
	    for elem in "${Array[@]}"; do
		# shellcheck disable=SC2068
		OutArray+=("$("$func" "$elem" ${args[@]})"); done
	else
	    for elem in "${Array[@]}"; do
		OutArray+=("$("$func" "$elem" "${args[@]}")"); done; fi; fi
    printf '%s' "(${OutArray[*]@Q})"
}
bcu__zip(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() array1 array2
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Combine two arrays into one


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa bepa
    apa=(a c e)
    bepa=(b d f)
    declare -a cepa=$(bcu__zip "(${apa[*]@Q})" "(${bepa[*]@Q})")
    echo
    declare -p cepa
  
Results:

    declare -a cepa=([0]="a" [1]="b" [2]="c" [3]="d" [4]="e" [5]="f")
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:array1 1:t:array2)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    local -a Zick="${array1}" || { printf '%s' "array1 is not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }

    local -a Zack="${array2}" || { printf '%s' "array2 is not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }

    local -a Out
    for ((i=0;i<"${#Zick[@]}";i++)); do
	Out+=("${Zick[$i]}" "${Zack[$i]}"); done
    printf '%s' "(${Out[*]@Q})"
}
bcu__zip_assoc(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() array1 array2
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Combine two indexed arrays into one assoc-declarable string


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa apa2 bepa bepa2 cepa depa epa
    apa=(a b c d)
    apa2=(a b c d e f)
    bepa=(one two three four)
    bepa2=(one two three four five six)
    declare -A cepa=$(bcu__zip_assoc "(${apa[*]@Q})" "(${bepa[*]@Q})")
    declare -A depa=$(bcu__zip_assoc "(${apa2[*]@Q})" "(${bepa[*]@Q})")
    declare -A epa=$(bcu__zip_assoc "(${apa[*]@Q})" "(${bepa2[*]@Q})")
    echo
    declare -p cepa depa epa
    echo

Results:

    declare -A cepa=([d]="four" [c]="three" [b]="two" [a]="one" )
    declare -A depa=([f]="" [e]="" [d]="four" [c]="three" [b]="two" [a]="one" )
    declare -A epa=([d]="four" [c]="three" [b]="two" [a]="one" )

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:array1 1:t:array2)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    local -a Zick="${array1}" || { printf '%s' "array1 is not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }

    local -a Zack="${array2}" || { printf '%s' "array2 is not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }

    local -A Zip_Assoc_ref
    #$(a=$(declare -p "$1"); printf '%s' "${a#*=}")
    #declare -p Bepa
    local -i i
    for ((i=0; i<${#Zick[@]}; i++)); do
	Zip_Assoc_ref["${Zick[$i]}"]="${Zack[$i]}"
    done
    local a; a=$(declare -p Zip_Assoc_ref)
    printf '%s' "${a#*=}"
}
bcu__quicksort(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() array
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
This is just an experiment and shouldn't be used. It's terribly slow
and only works for numbers currently. It does, however, return results
newline-separated and numerically sorted.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset values Sorted
    declare -i sortsize=12800
    for ((i = 0; i < sortsize; i++)); do
        values+=($RANDOM); done
    mapfile -t Sorted < <(bcu__quicksort "(${values[*]@Q})")
    mapfile -t Sorted < <(printf '%s\n' "${values[@]}" | sort --numeric)
    printf '%s\n' "${Sorted[@]}" | head

Results:

    0
    2
    2
    4
    6
    9
    11
    12
    15
    16
    

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:array)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    local -a myvalues="${array}" || { printf '%s' "array is not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }

    bash "${_array_MOD_DIR}/qsort.sh" < <(printf '%s' "(${myvalues[*]@Q})")
}
array_private(){
    :

}
array_main(){
    :
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	array_main "${org_args[@]}"
    else
	array_main "${@}"
    fi
fi
# array.sh ends here
