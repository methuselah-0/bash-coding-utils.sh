#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::arr_unshift-test-main][arr_unshift-test-main]]
org_args='()'
. bcu.sh
bcu__arr_unshift(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() Array String Arr
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
arr_unshift prepends args to one or several arrays. It may consider
the given arrays as variable names instead of quote-expanded
arrays. If given more than 1 input array that is quote-expanded,
arr_unshift prints the output arrays as array-declarable strings
newline-separated.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset myarr; myarr=(one two)
    bcu__arr_unshift -n -a myarr -- three four
    echo && declare -p myarr
  
Results:

    declare -a myarr=([0]="three" [1]="four" [2]="one" [3]="two")
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Arrays=(a Array "" "Variable name of an array to prepend arguments to" 1 t t)
    local Nameref=(n nameref "" "Read arg as a namereference. This option will not print the resulting array back to stdout" 0)
    Options+=("(${null[*]@Q})" "(${Nameref[*]@Q})" "(${Arrays[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:String)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    if [[ -n "$nameref" ]]; then
	for Arr in "${Array[@]}"; do
	    declare -n arr="$Arr"
	    arr=("${String[@]}" "${arr[@]}"); done
    else
	if [[ "${#Array[@]}" -gt 1 ]]; then
	    for Arr in "${Array[@]}"; do
		local -a Arr="${Arr}"
		printf '%s' "(${Args[*]@Q} ${Arr[*]@Q})" $'\n'; done
	else
	    local -a Arr="${Array[0]}"
	    printf '%s' "(${Args[*]@Q} ${Arr[*]@Q})"; fi; fi
}
arr_unshift_test_f(){
    #  unset myarr; myarr=(one two)
    #  bcu__arr_unshift -n -a myarr -- three four
    #  echo && declare -p myarr
    # 3 4 1 2
    local myarr
    myarr=(one two)
    bcu__arr_unshift -n -a myarr -- three 'four five'
    declare -p myarr
}
arr_unshift_test_1()(
    . <(arr_unshift_test_f)
    [[ "${#myarr[@]}" == 4 ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
arr_unshift_test_main(){
    printf arr_unshift_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )arr_unshift_test_1(\ |$) ]]; then arr_unshift_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	arr_unshift_test_main "${org_args[@]}"
    else
	arr_unshift_test_main "${@}"
    fi
fi
# arr_unshift-test-main ends here
