#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::quicksort-test-main][quicksort-test-main]]
org_args='()'
. bcu.sh
bcu__quicksort(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() array
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
This is just an experiment and shouldn't be used. It's terribly slow
and only works for numbers currently. It does, however, return results
newline-separated and numerically sorted.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset values Sorted
    declare -i sortsize=12800
    for ((i = 0; i < sortsize; i++)); do
        values+=($RANDOM); done
    mapfile -t Sorted < <(bcu__quicksort "(${values[*]@Q})")
    mapfile -t Sorted < <(printf '%s\n' "${values[@]}" | sort --numeric)
    printf '%s\n' "${Sorted[@]}" | head

Results:

    0
    2
    2
    4
    6
    9
    11
    12
    15
    16
    

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:array)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    local -a myvalues="${array}" || { printf '%s' "array is not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }

    bash "${_array_MOD_DIR}/qsort.sh" < <(printf '%s' "(${myvalues[*]@Q})")
}
quicksort_test_f(){
    # Generate an array of random values
    local -a Sorted
    local -i sortsize=12800
    local -i i
    for ((i = 0; i < sortsize; i++)); do
	values+=($RANDOM)
    done
    mapfile -t Sorted < <(bcu__quicksort "(${values[*]@Q})")
    declare -p Sorted
}
quicksort_test_1()(
    . <(quicksort_test_f)
    # Verify the random values are sorted
    sort --check --numeric <(IFS=$'\n'; echo "${Sorted[*]}") || { echo FAIL && return 1 ;}
    echo PASS && return 0
)
quicksort_test_main(){
    printf quicksort_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )quicksort_test_1(\ |$) ]]; then quicksort_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	quicksort_test_main "${org_args[@]}"
    else
	quicksort_test_main "${@}"
    fi
fi
# quicksort-test-main ends here
