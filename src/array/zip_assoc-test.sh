#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::zip_assoc-test-main][zip_assoc-test-main]]
org_args='()'
. bcu.sh
bcu__zip_assoc(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() array1 array2
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Combine two indexed arrays into one assoc-declarable string


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset apa apa2 bepa bepa2 cepa depa epa
    apa=(a b c d)
    apa2=(a b c d e f)
    bepa=(one two three four)
    bepa2=(one two three four five six)
    declare -A cepa=$(bcu__zip_assoc "(${apa[*]@Q})" "(${bepa[*]@Q})")
    declare -A depa=$(bcu__zip_assoc "(${apa2[*]@Q})" "(${bepa[*]@Q})")
    declare -A epa=$(bcu__zip_assoc "(${apa[*]@Q})" "(${bepa2[*]@Q})")
    echo
    declare -p cepa depa epa
    echo

Results:

    declare -A cepa=([d]="four" [c]="three" [b]="two" [a]="one" )
    declare -A depa=([f]="" [e]="" [d]="four" [c]="three" [b]="two" [a]="one" )
    declare -A epa=([d]="four" [c]="three" [b]="two" [a]="one" )

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:array1 1:t:array2)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    local -a Zick="${array1}" || { printf '%s' "array1 is not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }

    local -a Zack="${array2}" || { printf '%s' "array2 is not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }

    local -A Zip_Assoc_ref
    #$(a=$(declare -p "$1"); printf '%s' "${a#*=}")
    #declare -p Bepa
    local -i i
    for ((i=0; i<${#Zick[@]}; i++)); do
	Zip_Assoc_ref["${Zick[$i]}"]="${Zack[$i]}"
    done
    local a; a=$(declare -p Zip_Assoc_ref)
    printf '%s' "${a#*=}"
}
zip_assoc_test_f(){
    local -a apa apa2 bepa bepa2
    local a b c
    apa=(a b c d)
    apa2=(a b c d e f)
    bepa=(one two three four)
    bepa2=(one two three four five six)
    local -A a=$(bcu__zip_assoc "(${apa[*]@Q})" "(${bepa[*]@Q})")
    local -A b=$(bcu__zip_assoc "(${apa2[*]@Q})" "(${bepa[*]@Q})")
    local -A c=$(bcu__zip_assoc "(${apa[*]@Q})" "(${bepa2[*]@Q})")
    declare -p a b c
}
zip_assoc_test_1()(
    . <(zip_assoc_test_f)
    [[ "${a[a]}" == one ]] || { echo FAIL && return 1; }
    [[ "${a[b]}" == two ]] || { echo FAIL && return 1; }
    [[ "${a[c]}" == three ]] || { echo FAIL && return 1; }
    [[ "${a[d]}" == four ]] || { echo FAIL && return 1; }
    [[ "${b[e]}" == "" ]] || { echo FAIL && return 1; }
    [[ "${b[f]}" == "" ]] || { echo FAIL && return 1; }
    [[ "${!c[@]}" =~ [abcd\ ]{7} ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
zip_assoc_test_main(){
    printf zip_assoc_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )zip_assoc_test_1(\ |$) ]]; then zip_assoc_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	zip_assoc_test_main "${org_args[@]}"
    else
	zip_assoc_test_main "${@}"
    fi
fi
# zip_assoc-test-main ends here
