#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::calc-test-main][calc-test-main]]
org_args='()'
. bcu.sh
calc()(
    math_private
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() tag_equal_text
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Calc implements calculation functions and for example you can use it
to calculate the double or the square of a given number.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
calc_example(){
    cat <<'EOF'
Example:

    calc -d 3
    calc -s 3

Results:

    bash: calc: command not found
    bash: calc: command not found

EOF
}
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local -a Doublenum=(d doublenum "" "Double the provided integer" 0)
    local -a Squarenum=(s squarenum "" "Square the provided integer" 0)
    Options+=("(${null[*]@Q})" "(${Doublenum[*]@Q})" "(${Squarenum[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:integer)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    local -i int="$integer" || { printf '%s' "Not a valid integer: $integer: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ -n "$doublenum" ]] && double $integer && return 0
    [[ -n "$squarenum" ]] && square $integer && return 0
)
calc_test_f(){
    local a
    a=$(calc -d 3)
    declare -p a
}
calc_test_1()(
    . <(calc_test_f)
    [[ "${a}" == 6 ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
calc_test_main(){
    printf calc_test_1.. ; calc_test_1
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	calc_test_main "${org_args[@]}"
    else
	calc_test_main "${@}"
    fi
fi
# calc-test-main ends here
