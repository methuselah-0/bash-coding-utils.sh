#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::square-test-main][square-test-main]]
org_args='()'
. bcu.sh
square(){
    local z; local -a Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")
    local -a Operands
    bcu__setopts "$@"
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    local -a Input
    if [[ ! -t 0 ]]; then
        if [[ -n "$z" ]]; then
            mapfile -t -d '' Input
	else
            mapfile -t Input; fi
    else
        Input=("${Operands[@]}"); fi
    [[ ! -n "${Input[*]}" ]] && { bcu__stack "$@" && return 1 ; }
    echo $((${Input[0]}*${Input[0]}))
}
square_test_f(){
    local a
    a=$(square 5)
    declare -p a
}
square_test_1()(
    . <(square_test_f)
    [[ "${a}" == 25 ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
square_test_main(){
    printf square_test_1.. ; square_test_1
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	square_test_main "${org_args[@]}"
    else
	square_test_main "${@}"
    fi
fi
# square-test-main ends here
