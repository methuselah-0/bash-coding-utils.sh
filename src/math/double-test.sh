#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::double-test-main][double-test-main]]
org_args='()'
. bcu.sh
double(){
    echo "$(($1*2))"
}
double_test_f(){
    local a
    a=$(double 5)
    declare -p a
}
double_test_1()(
    . <(double_test_f)
    [[ "${a}" == 10 ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
double_test_main(){
    printf double_test_1.. ; double_test_1
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	double_test_main "${org_args[@]}"
    else
	double_test_main "${@}"
    fi
fi
# double-test-main ends here
