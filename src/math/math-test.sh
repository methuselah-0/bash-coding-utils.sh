#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::math-unit-tests][math-unit-tests]]
org_args='()'
. bcu.sh
_math_MOD_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")
calc()(
    math_private
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() tag_equal_text
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Calc implements calculation functions and for example you can use it
to calculate the double or the square of a given number.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
calc_example(){
    cat <<'EOF'
Example:

    calc -d 3
    calc -s 3

Results:

    bash: calc: command not found
    bash: calc: command not found

EOF
}
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local -a Doublenum=(d doublenum "" "Double the provided integer" 0)
    local -a Squarenum=(s squarenum "" "Square the provided integer" 0)
    Options+=("(${null[*]@Q})" "(${Doublenum[*]@Q})" "(${Squarenum[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:integer)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    local -i int="$integer" || { printf '%s' "Not a valid integer: $integer: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ -n "$doublenum" ]] && double $integer && return 0
    [[ -n "$squarenum" ]] && square $integer && return 0
)
calc_example(){
    cat <<'EOF'
Example:

    calc -d 3
    calc -s 3

Results:

    bash: calc: command not found
    bash: calc: command not found

EOF
}
math_private(){
    :
double(){
    local z; local -a Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")
    local -a Operands
    bcu__setopts "$@"
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    local -a Input
    if [[ ! -t 0 ]]; then
        if [[ -n "$z" ]]; then
            mapfile -t -d '' Input
	else
            mapfile -t Input; fi
    else
        Input=("${Operands[@]}"); fi
    [[ ! -n "${Input[*]}" ]] && { bcu__stack "$@" && return 1 ; }
    echo $((2*${Input[0]}))
}
double_example(){
    cat <<'EOF'
Example:

    double 5

Results:

    bash: double: command not found

EOF
}
square(){
    local z; local -a Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")
    local -a Operands
    bcu__setopts "$@"
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    local -a Input
    if [[ ! -t 0 ]]; then
        if [[ -n "$z" ]]; then
            mapfile -t -d '' Input
	else
            mapfile -t Input; fi
    else
        Input=("${Operands[@]}"); fi
    [[ ! -n "${Input[*]}" ]] && { bcu__stack "$@" && return 1 ; }
    echo $((${Input[0]}*${Input[0]}))
}
square_example(){
    cat <<'EOF'
Example:

    square 5

Results:

    bash: square: command not found
   
EOF
}
}
math_test_main(){
. bcu.sh
calc()(
    math_private
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() tag_equal_text
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Calc implements calculation functions and for example you can use it
to calculate the double or the square of a given number.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
calc_example(){
    cat <<'EOF'
Example:

    calc -d 3
    calc -s 3

Results:

    bash: calc: command not found
    bash: calc: command not found

EOF
}
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local -a Doublenum=(d doublenum "" "Double the provided integer" 0)
    local -a Squarenum=(s squarenum "" "Square the provided integer" 0)
    Options+=("(${null[*]@Q})" "(${Doublenum[*]@Q})" "(${Squarenum[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:integer)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    local -i int="$integer" || { printf '%s' "Not a valid integer: $integer: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ -n "$doublenum" ]] && double $integer && return 0
    [[ -n "$squarenum" ]] && square $integer && return 0
)
calc_test_f(){
    local a
    a=$(calc -d 3)
    declare -p a
}
calc_test_1()(
    . <(calc_test_f)
    [[ "${a}" == 6 ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
calc_test_main(){
    printf calc_test_1.. ; calc_test_1
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	calc_test_main "${org_args[@]}"
    else
	calc_test_main "${@}"
    fi
fi
. bcu.sh
double(){
    echo "$(($1*2))"
}
double_test_f(){
    local a
    a=$(double 5)
    declare -p a
}
double_test_1()(
    . <(double_test_f)
    [[ "${a}" == 10 ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
double_test_main(){
    printf double_test_1.. ; double_test_1
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	double_test_main "${org_args[@]}"
    else
	double_test_main "${@}"
    fi
fi
. bcu.sh
square(){
    local z; local -a Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")
    local -a Operands
    bcu__setopts "$@"
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    local -a Input
    if [[ ! -t 0 ]]; then
        if [[ -n "$z" ]]; then
            mapfile -t -d '' Input
	else
            mapfile -t Input; fi
    else
        Input=("${Operands[@]}"); fi
    [[ ! -n "${Input[*]}" ]] && { bcu__stack "$@" && return 1 ; }
    echo $((${Input[0]}*${Input[0]}))
}
square_test_f(){
    local a
    a=$(square 5)
    declare -p a
}
square_test_1()(
    . <(square_test_f)
    [[ "${a}" == 25 ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
square_test_main(){
    printf square_test_1.. ; square_test_1
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	square_test_main "${org_args[@]}"
    else
	square_test_main "${@}"
    fi
fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	math_test_main "${org_args[@]}"
    else
	math_test_main "${@}"
    fi
fi
# math-unit-tests ends here
