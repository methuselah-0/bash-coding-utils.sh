#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::url_decode-test-main][url_decode-test-main]]
org_args='()'
[[ "$_BCU_SH_LOADED" == YES ]] || . bcu.sh
bcu__url_decode(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() url_data
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Decode url_encoded data


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__url_decode 'https://api.apify.org%3FSELECT+apa+WHERE+apa%3D%27bepa%2Dcepa%27' && echo
  
Results:

    https://api.apify.org?SELECT apa WHERE apa='bepa-cepa'
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)    
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:url_data)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: $_STACK" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    local url_encoded="${url_data//+/ }";
    printf '%b' "${url_encoded//%/\\x}"
}
url_decode_test_f(){
    local a
    a=$(bcu__url_decode 'https%3A%2F%2Fapa%2Ecom%2F%3Fwhatis%26SELECT+this+WHERE+this%3Dit')
    declare -p a
}
url_decode_test_1()(
    . <(url_decode_test_f)
    [[ "${a}" == "https://apa.com/?whatis&SELECT this WHERE this=it" ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
url_decode_test_main(){
    printf url_decode_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )url_decode_test_1(\ |$) ]]; then url_decode_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	url_decode_test_main "${org_args[@]}"
    else
	url_decode_test_main "${@}"
    fi
fi
# url_decode-test-main ends here
