#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::web.sh][web.sh]]
[[ "$_BCU_SH_LOADED" == YES ]] || . bcu.sh
_web_MOD_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")
bcu__ip_of(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() host
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Get the resolving ip address of a hostname.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__ip_of localhost
  
Results:

    127.0.0.1
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)    
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:host)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: $_STACK" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    #echo $host && return 0
    local ip; ip=$(ping -c 1 "${host}" | head -n 1 | pcregrep -o '[0-9\.]{7,}')
    bcu__is_valid_ip_no_cidr "$ip" &>/dev/null || { printf '%s' "ping did not return a valid ip - $ip -: ${_STACK}" && bcu__stack "$@" && return 1; }
    printf '%s\n' "$ip" && return 0
}
bcu__url_encode(){
    local _STACK z Options=() Input=() url_data
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Encode a url


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    api_call_url(){
        local target query
        target='https://api.apify.org'
        query="?SELECT apa WHERE apa='bepa-cepa'"
        printf '%s' "$target" $(bcu__url_encode "$query")
    }
    api_call_url && echo
  
Results:

    https://api.apify.org%3FSELECT+apa+WHERE+apa%3D%27bepa%2Dcepa%27
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")
    
    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:url_data)
    
    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: $_STACK" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    local x="${url_data}" ; local y=()
    local -i i
    for (( i=0 ; i < ${#x} ; i++ ))
    do local c=${x:i:1}
       case $c in
	   [a-zA-Z0-9~_-\.]) y[$i]="$c" ;; # correct
	   ' ') y[$i]='+' ;;
	   *) y[$i]=$(printf '%%%02X' "'$c") # %% means 1 literal %, and %02X is 2 character hex format specifier, i.e. %HH format.
       esac
    done ; printf '%s' "${y[@]}"
}
bcu__url_decode(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() url_data
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Decode url_encoded data


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__url_decode 'https://api.apify.org%3FSELECT+apa+WHERE+apa%3D%27bepa%2Dcepa%27' && echo
  
Results:

    https://api.apify.org?SELECT apa WHERE apa='bepa-cepa'
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)    
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:url_data)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: $_STACK" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    local url_encoded="${url_data//+/ }";
    printf '%b' "${url_encoded//%/\\x}"
}
web_private(){
    :

}
web_main(){
    :
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
        web_main "${org_args[@]}"
    else
        web_main "${@}"
    fi
fi
# web.sh ends here
