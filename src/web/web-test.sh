#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::web-unit-tests][web-unit-tests]]
org_args='()'
. bcu.sh
_web_MOD_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")
web_private(){
    :

}
web_test_main(){
[[ "$_BCU_SH_LOADED" == YES ]] || . bcu.sh
bcu__ip_of(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() host
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Get the resolving ip address of a hostname.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__ip_of localhost
  
Results:

    127.0.0.1
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)    
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:host)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: $_STACK" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    #echo $host && return 0
    local ip; ip=$(ping -c 1 "${host}" | head -n 1 | pcregrep -o '[0-9\.]{7,}')
    bcu__is_valid_ip_no_cidr "$ip" &>/dev/null || { printf '%s' "ping did not return a valid ip - $ip -: ${_STACK}" && bcu__stack "$@" && return 1; }
    printf '%s\n' "$ip" && return 0
}
ip_of_test_f(){
    local a
    a=$(bcu__ip_of localhost)
    declare -p a
}
ip_of_test_1()(
    . <(ip_of_test_f)
    [[ "${a}" == 127.0.0.1 ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
ip_of_test_main(){
    printf ip_of_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )ip_of_test_1(\ |$) ]]; then ip_of_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	ip_of_test_main "${org_args[@]}"
    else
	ip_of_test_main "${@}"
    fi
fi
[[ "$_BCU_SH_LOADED" == YES ]] || . bcu.sh
bcu__url_encode(){
    local _STACK z Options=() Input=() url_data
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Encode a url


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    api_call_url(){
        local target query
        target='https://api.apify.org'
        query="?SELECT apa WHERE apa='bepa-cepa'"
        printf '%s' "$target" $(bcu__url_encode "$query")
    }
    api_call_url && echo
  
Results:

    https://api.apify.org%3FSELECT+apa+WHERE+apa%3D%27bepa%2Dcepa%27
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")
    
    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:url_data)
    
    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: $_STACK" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    local x="${url_data}" ; local y=()
    local -i i
    for (( i=0 ; i < ${#x} ; i++ ))
    do local c=${x:i:1}
       case $c in
	   [a-zA-Z0-9~_-\.]) y[$i]="$c" ;; # correct
	   ' ') y[$i]='+' ;;
	   *) y[$i]=$(printf '%%%02X' "'$c") # %% means 1 literal %, and %02X is 2 character hex format specifier, i.e. %HH format.
       esac
    done ; printf '%s' "${y[@]}"
}
url_encode_test_f(){
    local a
    a=$(bcu__url_encode 'https://apa.com/?whatis&SELECT this WHERE this=it')
    declare -p a
}
url_encode_test_1()(
    . <(url_encode_test_f)
    [[ "${a}" == 'https%3A%2F%2Fapa%2Ecom%2F%3Fwhatis%26SELECT+this+WHERE+this%3Dit' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
url_encode_test_main(){
    printf url_encode_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )url_encode_test_1(\ |$) ]]; then url_encode_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	url_encode_test_main "${org_args[@]}"
    else
	url_encode_test_main "${@}"
    fi
fi
[[ "$_BCU_SH_LOADED" == YES ]] || . bcu.sh
bcu__url_decode(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() url_data
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Decode url_encoded data


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__url_decode 'https://api.apify.org%3FSELECT+apa+WHERE+apa%3D%27bepa%2Dcepa%27' && echo
  
Results:

    https://api.apify.org?SELECT apa WHERE apa='bepa-cepa'
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)    
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:url_data)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: $_STACK" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    local url_encoded="${url_data//+/ }";
    printf '%b' "${url_encoded//%/\\x}"
}
url_decode_test_f(){
    local a
    a=$(bcu__url_decode 'https%3A%2F%2Fapa%2Ecom%2F%3Fwhatis%26SELECT+this+WHERE+this%3Dit')
    declare -p a
}
url_decode_test_1()(
    . <(url_decode_test_f)
    [[ "${a}" == "https://apa.com/?whatis&SELECT this WHERE this=it" ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
url_decode_test_main(){
    printf url_decode_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )url_decode_test_1(\ |$) ]]; then url_decode_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	url_decode_test_main "${org_args[@]}"
    else
	url_decode_test_main "${@}"
    fi
fi
local pydpid="$(cat /tmp/pydaemon/$_BCU_PYDAEMON_CHANNEL.sock.pid )"
kill "$pydpid"
wait "$pydpid" 2>/dev/null
}
declare -a org_args="${org_args}"
[[ -e "${_BCU_SH_DIR}"/disabled_tests.txt ]] && mapfile -t _BCU_TESTS_DISABLED <"${_BCU_SH_DIR}"/disabled_tests.txt
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
        web_test_main "${org_args[@]}"
    else
        web_test_main "${@}"
    fi
fi
# web-unit-tests ends here
