#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::ip_of-test-main][ip_of-test-main]]
org_args='()'
[[ "$_BCU_SH_LOADED" == YES ]] || . bcu.sh
bcu__ip_of(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() host
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Get the resolving ip address of a hostname.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__ip_of localhost
  
Results:

    127.0.0.1
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)    
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:host)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: $_STACK" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    #echo $host && return 0
    local ip; ip=$(ping -c 1 "${host}" | head -n 1 | pcregrep -o '[0-9\.]{7,}')
    bcu__is_valid_ip_no_cidr "$ip" &>/dev/null || { printf '%s' "ping did not return a valid ip - $ip -: ${_STACK}" && bcu__stack "$@" && return 1; }
    printf '%s\n' "$ip" && return 0
}
ip_of_test_f(){
    local a
    a=$(bcu__ip_of localhost)
    declare -p a
}
ip_of_test_1()(
    . <(ip_of_test_f)
    [[ "${a}" == 127.0.0.1 ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
ip_of_test_main(){
    printf ip_of_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )ip_of_test_1(\ |$) ]]; then ip_of_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	ip_of_test_main "${org_args[@]}"
    else
	ip_of_test_main "${@}"
    fi
fi
# ip_of-test-main ends here
