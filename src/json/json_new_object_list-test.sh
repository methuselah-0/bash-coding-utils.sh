#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::json_new_object_list-test-main][json_new_object_list-test-main]]
org_args='()'
. bcu.sh
bcu__json_new_object_list(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() string_key value
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
json_new_object_list is only used for creating a list of
string-objects with the same key. If you instead want to create a list
of your json objects, invoke ~printf '%s\n' "${JsonObjects[@]}" | jq
-s -c~.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__json_new_object_list apa bepa cepa depa | jq -Mc

Results:

    [{"apa":"bepa"},{"apa":"cepa"},{"apa":"depa"}]

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:string_key 0:t:value)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    #printf '%s' "$@" | jq -c -s . ; fi
    # local str="["

    for val in "${value[@]}"; do
	bcu__json_new_string_object "$string_key" "$val"; done | jq -c -s

    # for obj in "${Objects[@]}"; do
    # 	str+="${obj},"; done
    # printf '%s' "${str%,}]"
}
json_new_object_list_test_f(){
    local a
    a=$(bcu__json_new_object_list apa bepa cepa depa)
    declare -p a
}
json_new_object_list_test_1()(
    . <(json_new_object_list_test_f)
    [[ "${a}" == '[{"apa":"bepa"},{"apa":"cepa"},{"apa":"depa"}]' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_new_object_list_test_main(){
    printf json_new_object_list_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_new_object_list_test_1(\ |$) ]]; then json_new_object_list_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_new_object_list_test_main "${org_args[@]}"
    else
	json_new_object_list_test_main "${@}"
    fi
fi
# json_new_object_list-test-main ends here
