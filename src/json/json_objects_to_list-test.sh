#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::json_objects_to_list-test-main][json_objects_to_list-test-main]]
org_args='()'
. bcu.sh
bcu__json_objects_to_list(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() json_object
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Put json objects in a json list


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__json_objects_to_list "$(bcu__json_elems_to_object "$(bcu__json_new_elem -n apa -- 5)")" "$(bcu__json_new_string_object bepa cepa)" | jq -Mc

Results:

    [{"apa":5},{"bepa":"cepa"}]

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:json_object)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    # {Object} .. {Object} -> [{jsonObject}]
    # stdin input corresponds to just running: jq -s -c .
    jq -c -n '$ARGS.positional' --jsonargs "${json_object[@]}"
    #printf '%s' "$@" | jq -s -c .
    # local str
    # for obj in "$@"; do
    # 	str+="${obj},"; done
    # # TODO:
    # printf '%s' "[${str%,}]"
}
json_objects_to_list_test_f(){
    local a
    a=$(bcu__json_objects_to_list '{"apa":5}' '{"bepa":"cepa"}')
    declare -p a
}
json_objects_to_list_test_1()(
    . <(json_objects_to_list_test_f)
    [[ "${a}" == '[{"apa":5},{"bepa":"cepa"}]' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_objects_to_list_test_main(){
    printf json_objects_to_list_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_objects_to_list_test_1(\ |$) ]]; then json_objects_to_list_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_objects_to_list_test_main "${org_args[@]}"
    else
	json_objects_to_list_test_main "${@}"
    fi
fi
# json_objects_to_list-test-main ends here
