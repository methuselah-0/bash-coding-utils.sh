#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::json_new_elem-test-main][json_new_elem-test-main]]
org_args='()'
. bcu.sh
bcu__json_new_elem(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() name json_data
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Create a new named element


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__json_new_elem -n apa -- '[{"name":"apa","value":"bepa"}]' && echo

Results:

    "apa":[{"name":"apa","value":"bepa"}]

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Name=(n name "" "Name of the new json element")
    Options+=("(${null[*]@Q})" "(${Name[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:json_data)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || {
	printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    #printf '%s' "\"${1}\":${2}"
    bcu__is_valid_json "$json_data" || {
	printf '%s' "json_data is not valid json: ${_STACK}" && bcu__stack "$@" && return 1; }

    bcu__json_new_object_elem -n "$name" "${json_data}" #| jq -c ".${Input[0]}"
}
json_new_elem_test_f(){
    local a
    a=$(bcu__json_new_elem -n apa -- '[{"name":"apa","value":"bepa"}]')
    declare -p a
}
json_new_elem_test_1()(
    . <(json_new_elem_test_f)
    [[ "${a}" == '"apa":[{"name":"apa","value":"bepa"}]' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_new_elem_test_main(){
    printf json_new_elem_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_new_elem_test_1(\ |$) ]]; then json_new_elem_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_new_elem_test_main "${org_args[@]}"
    else
	json_new_elem_test_main "${@}"
    fi
fi
# json_new_elem-test-main ends here
