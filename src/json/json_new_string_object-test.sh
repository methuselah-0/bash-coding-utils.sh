#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::json_new_string_object-test-main][json_new_string_object-test-main]]
org_args='()'
. bcu.sh
bcu__json_new_string_object(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() key_string value_string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Create a json object with strings as key and value


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__json_new_string_object 'ap"a' 'be"pa' | jq -Mc
    bcu__json_new_string_object apa bepa | jq -Mc

Results:

    {"ap\"a":"be\"pa"}
    {"apa":"bepa"}

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:key_string 1:t:value_string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    #printf '%s' "{\"${1}\":\"${2}\"}"
    local result
    if result=$(printf '%s' '{"temp": {} }' | jq -c --arg a "${key_string}" --arg b "${value_string}" '.temp[$a] = $b | .temp'); then
	printf '%s\n' "$result"
    else
	printf '%s' "failed creating object - $result - : ${_STACK}" && bcu__stack "$@" && return 1; fi
}
json_new_string_object_test_f(){
    local a
    a=$(bcu__json_new_string_object 'ap"a' 'be"pa')
    declare -p a
}
json_new_string_object_test_1()(
    . <(json_new_string_object_test_f)
    [[ "${a}" == '{"ap\"a":"be\"pa"}' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_new_string_object_test_main(){
    printf json_new_string_object_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_new_string_object_test_1(\ |$) ]]; then json_new_string_object_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_new_string_object_test_main "${org_args[@]}"
    else
	json_new_string_object_test_main "${@}"
    fi
fi
# json_new_string_object-test-main ends here
