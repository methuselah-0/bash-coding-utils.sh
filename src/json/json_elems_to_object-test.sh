#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::json_elems_to_object-test-main][json_elems_to_object-test-main]]
org_args='()'
. bcu.sh
bcu__json_elems_to_object(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() json_element
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Given a number of json elements, json_elems_to_object wraps them into a
well-formed json object.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__json_elems_to_object "$(bcu__json_new_string_elem apa1 bepa)" "$(bcu__json_new_object_elem -n apa2 -- "$(bcu__json_name_value_list apa=bepa bepa=cepa)")" | jq -Mc && echo

Results:

    {"apa1":"bepa","apa2":[{"name":"apa","value":"bepa"},{"name":"bepa","value":"cepa"}]}

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:json_element)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    local -a Objects
    local obj
    for obj in "${json_element[@]}"; do
	Objects+=("${obj}" ','); done

    # # TODO:
    #printf '%s' "{${str%,}}"
    local a
    a="${Objects[@]}"
    printf '%s' "{${a%,}}" | jq -c

    # #jq -s -c '.[] | { . }'
    # local -a Objects
    # local obj
    # if [[ ! -t 0 ]]; then
    # 	while read -r obj; do
    # 	    Objects+=("$obj" ','); done
    # else
    # 	for obj in "$@"; do
    # 	    Objects+=("${obj}" ','); done; fi

    # # # TODO:
    # #printf '%s' "{${str%,}}"
    # local a
    # a="${Objects[@]}"
    # printf '%s' "{${a%,}}" | jq -c
}
json_elems_to_object_test_f(){
    local a
    a=$(bcu__json_elems_to_object '"apa":[{"name":"apa","value":"bepa"}' '{"name":"bepa","value":"cepa"}]')
    declare -p a
}
json_elems_to_object_test_1()(
    . <(json_elems_to_object_test_f)
    [[ "${a}" == '{"apa":[{"name":"apa","value":"bepa"},{"name":"bepa","value":"cepa"}]}' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_elems_to_object_test_main(){
    printf json_elems_to_object_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_elems_to_object_test_1(\ |$) ]]; then json_elems_to_object_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_elems_to_object_test_main "${org_args[@]}"
    else
	json_elems_to_object_test_main "${@}"
    fi
fi
# json_elems_to_object-test-main ends here
