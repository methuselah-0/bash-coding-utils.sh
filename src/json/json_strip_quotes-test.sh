#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::json_strip_quotes-test-main][json_strip_quotes-test-main]]
org_args='()'
. bcu.sh
bcu__json_strip_quotes(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() json_string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Strip the quotes off of a well-escaped and quoted json string


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    printf '%s\n' $(bcu__json_elems_to_object $(bcu__json_new_string_elem apa bepa)) | bcu__json_strip_quotes
    printf '%s\u0000' apa bepa cepa | bcu__json_to_json_string -z | bcu__json_strip_quotes

Results:

    {"apa":"bepa"}
    apa bepa cepa

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:json_string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    local t0
    while read -r t0 ; do
	t0="${t0#\"}"; t0="${t0%\"}"; printf '%s\n' "$t0" ; done < <(printf '%s' "${json_string[@]}" | jq -c '.')
}
json_strip_quotes_test_f(){
    local a b
    a=$(printf '%s' '"apa\nbepa\ncepa\n"' | bcu__json_strip_quotes)
    b=$(bcu__json_strip_quotes '"apa\nbepa\ncepa\n"')
    declare -p a b
}
json_strip_quotes_test_1()(
    . <(json_strip_quotes_test_f)
    [[ "${a}" == 'apa\nbepa\ncepa\n' ]] || { echo FAIL && return 1; }
    [[ "${b}" == 'apa\nbepa\ncepa\n' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_strip_quotes_test_main(){
    printf json_strip_quotes_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_strip_quotes_test_1(\ |$) ]]; then json_strip_quotes_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_strip_quotes_test_main "${org_args[@]}"
    else
	json_strip_quotes_test_main "${@}"
    fi
fi
# json_strip_quotes-test-main ends here
