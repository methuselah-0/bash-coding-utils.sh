#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::json_new_string_elem-test-main][json_new_string_elem-test-main]]
org_args='()'
. bcu.sh
bcu__json_new_string_elem(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() key_string value_string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Create a new json string element


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__json_new_string_elem 'a}pa' 'be}pa' && echo
    bcu__json_new_string_elem apa bepa && echo
    bcu__json_elems_to_object $(bcu__json_new_string_elem apa bepa) | jq -Mc

Results:

    "a}pa":"be}pa"
    "apa":"bepa"
    {"apa":"bepa"}

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:key_string 1:t:value_string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    local a b c
    if a=$(echo '{"temp": {} }' \
	       | jq -c --arg a "${key_string}" --arg b "${value_string}" '.temp[$a] = $b | .temp'); then
	b="${a%\}}"
	c="${b#\{}"
	printf '%s' "$c"
    else
	printf '%s' "Failed creating a string element - $a - : ${_STACK}" && bcu__stack "$@" && return 1; fi

    #printf '%s' "\"${1}\":\"${2:-$val}\""
}
json_new_string_elem_test_f(){
    local a
    a=$(bcu__json_new_string_elem 'a}pa' 'be}pa')
    declare -p a
}
json_new_string_elem_test_1()(
    . <(json_new_string_elem_test_f)
    [[ "${a}" == '"a}pa":"be}pa"' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_new_string_elem_test_main(){
    printf json_new_string_elem_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_new_string_elem_test_1(\ |$) ]]; then json_new_string_elem_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_new_string_elem_test_main "${org_args[@]}"
    else
	json_new_string_elem_test_main "${@}"
    fi
fi
# json_new_string_elem-test-main ends here
