#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::json_set_field-test-main][json_set_field-test-main]]
org_args='()'
. bcu.sh
bcu__json_set_field(){
    local _STACK z Options=() Input=() Object obj
    [[ "$1" =~ (-h|--help) ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Set a json field key and value. Adds the field to the given object if
the key doesn't exist, else it replaces the value associated with the
given key.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__json_set_field -k apa -v bepa -- "{}"

Results:

    {"apa":"bepa"}

EOF
		)
    }
    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Key=(k key "" "Json key string" 1 t)
    local Value=(v value "" "Json value string" 1 t)
    Options+=("(${null[*]@Q})" "(${Key[*]@Q})" "(${Value[*]@Q})" )

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:Object)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || {
	printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ (-h|--help) ]] && return 0

    # Check args, or be lazy
    #declare -p key value Objects
    for obj in "${Object[@]}"; do
	jq -Mcn --unbuffered --arg k "$key" --arg v "$value" --argjson o "$obj" ' $o + { ( $k ) : $v } '; done
}
json_set_field_test_f(){
    local a
    a=$(bcu__json_set_field -k apa -v bepa -- "{}" | jq -Mc)
    b=$(bcu__json_set_field -k apa -v notbepa -- '{"apa":"bepa"}' | jq -Mc)
    declare -p a b
}
json_set_field_test_1()(
    . <(json_set_field_test_f)
    [[ "${a}" == '{"apa":"bepa"}' ]] || { echo FAIL && return 1; }
    [[ "${b}" == '{"apa":"notbepa"}' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_set_field_test_main(){
    printf json_set_field_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_set_field_test_1(\ |$) ]]; then json_set_field_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_set_field_test_main "${org_args[@]}"
    else
	json_set_field_test_main "${@}"
    fi
fi
# json_set_field-test-main ends here
