#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::is_valid_json-test-main][is_valid_json-test-main]]
org_args='()'
. bcu.sh
bcu__is_valid_json(){
    local _STACK z Options=() Input=() json_data
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Check if a string is a valid json string


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__is_valid_json '["a"]' && echo true
    bcu__is_valid_json '["a]' || echo false

Results:

    true
    false

EOF
		)
    }
    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:json_data)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "" =~ ^(-h|--help)$ ]] && return 0

    # Check args
    for arg in "${json_data}"; do
	jq -n . --argjson b "$arg" &>/dev/null || return 1; done
}
is_valid_json_test_f(){
    local a
    a=$(bcu__is_valid_json '["a"]' && echo true)
    b=$(bcu__is_valid_json '["a]' || echo false)
    declare -p a b
}
is_valid_json_test_1()(
    . <(is_valid_json_test_f)
    [[ "${a}" == true ]] || { echo FAIL && return 1; }
    [[ "${b}" == false ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
is_valid_json_test_main(){
    printf is_valid_json_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )is_valid_json_test_1(\ |$) ]]; then is_valid_json_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	is_valid_json_test_main "${org_args[@]}"
    else
	is_valid_json_test_main "${@}"
    fi
fi
# is_valid_json-test-main ends here
