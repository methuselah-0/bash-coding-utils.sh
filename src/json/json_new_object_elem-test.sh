#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::json_new_object_elem-test-main][json_new_object_elem-test-main]]
org_args='()'
. bcu.sh
bcu__json_new_object_elem(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() name json_object
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Create a new named json-object


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__json_new_object_elem -n apa -- '{"bepa":"cepa"}' && echo
    echo '{"bepa":"cepa"}' | bcu__json_new_object_elem -n apa -- && echo

Results:

    "apa":{"bepa":"cepa"}
    "apa":{"bepa":"cepa"}

EOF
		)
    }

    # Options
    local Name=(n name "" "The name of the new object" 1 t f)
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})" "(${Name[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:json_object)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    #local -a Json
    local s t
    #Json=("${Input[1]}")
    #printf '%s' '{"temp": {} }' | jq -c --arg a "${Input[0]}" --argjson b "$(printf '%s' "${Json[@]}" | jq -c -s .[])" '.temp[$a] = $b | .temp'

    #s=$(printf '%s' '{"temp": {} }' | jq -c --arg a "${Input[0]}" --argjson b "$(printf '%s' "${Json[@]}" | jq -c -s .[])" '.temp[$a] = $b | .temp')

    s=$(printf '%s' '{"temp": {} }' | jq -c --arg a "${name}" --argjson b "$(printf '%s' "${json_object}" | jq -c -s .[])" '.temp[$a] = $b | .temp')

    t=$(printf '%s' "${s%\}}")
    printf '%s' "${t#\{}"

    #      printf '%s' '{"temp": {} }' | jq -c --arg a "${Input[0]}" --argjson b "$(printf '%s' "${Json[@]}" | jq -c -s .[])" '.temp[$a] = $b | .temp'
    #printf '%s' "\"${1}\":{${2}}"
}
json_new_object_elem_test_f(){
    local a
    a=$(bcu__json_new_object_elem -n apa -- '{"bepa":"cepa"}')
    declare -p a
}
json_new_object_elem_test_1()(
    . <(json_new_object_elem_test_f)
    [[ "${a}" == '"apa":{"bepa":"cepa"}' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_new_object_elem_test_main(){
    printf json_new_object_elem_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_new_object_elem_test_1(\ |$) ]]; then json_new_object_elem_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_new_object_elem_test_main "${org_args[@]}"
    else
	json_new_object_elem_test_main "${@}"
    fi
fi
# json_new_object_elem-test-main ends here
