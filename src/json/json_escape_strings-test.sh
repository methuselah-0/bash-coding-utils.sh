#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::json_escape_strings-test-main][json_escape_strings-test-main]]
org_args='()'
. bcu.sh
bcu__json_escape_strings(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Escape strings to json format leaving the quotes out. Use
json_escape_string if you need to escape given strings into one
single json-escaped string.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__json_escape_strings "apa{}][]\" bepa" apa bepa

Results:

    apa{}][]\" bepa
    apa
    bepa

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    bcu__json_to_json_strings "${string[@]}" | bcu__json_strip_quotes
}
json_escape_strings_test_f(){
    local a
    mapfile -t a < <(bcu__json_escape_strings apa bepa '}{,""')
    declare -p a
}
json_escape_strings_test_1()(
    . <(json_escape_strings_test_f)
    [[ "${a[0]}" == apa ]] || { echo FAIL && return 1; }
    [[ "${a[1]}" == bepa ]] || { echo FAIL && return 1; }
    [[ "${a[2]}" == '}{,\"\"' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_escape_strings_test_main(){
    printf json_escape_strings_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_escape_strings_test_1(\ |$) ]]; then json_escape_strings_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_escape_strings_test_main "${org_args[@]}"
    else
	json_escape_strings_test_main "${@}"
    fi
fi
# json_escape_strings-test-main ends here
