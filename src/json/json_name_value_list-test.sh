#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::json_name_value_list-test-main][json_name_value_list-test-main]]
org_args='()'
. bcu.sh
bcu__json_name_value_list(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() name_value
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Create a list of name-value objects


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__json_name_value_list apa=bepa 'bepa=ce"p"a' | jq -Mc

Results:

    [{"name":"apa","value":"bepa"},{"name":"bepa","value":"ce\"p\"a"}]

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:name_value)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    local str name value obj
    str="["
    local -a Objects
    #local -a Args=("${Input[@]}")
    #for cf in "${Args[@]}"; do
    for cf in "${name_value[@]}"; do
	name="${cf%%=*}"
	value="${cf##*=}"
	Objects+=("$(bcu__json_elems_to_object "$(bcu__json_new_string_elem name "$name")" "$(bcu__json_new_string_elem value "$value")")"); done
    
    for obj in "${Objects[@]}"; do
	str+="${obj},"; done
    printf '%s' "${str%,}]" | jq -c
    # json_name_value_list(){ # ElemName {ElemValue} .. {ElemValue} -> [{jsonObjects}]
    #     local str="["
    #     declare -n jsonAssocListList="$1"
    #     for elemName "${!jsonAssocListList[@]}"; do
    # 	Objects+=($(json_new_string_object "$elemName" "${jsonAssocListList[$elemName]}")); done
    
    #     for obj in "${Objects[@]}"; do
    # 	str+="${obj},"; done
    #     printf '%s' "${str%,}]"
    # }
}
json_name_value_list_test_f(){
    local a
    a=$(bcu__json_name_value_list apa=bepa 'bepa=ce"p"a' | jq -Mc)
    declare -p a
}
json_name_value_list_test_1()(
    . <(json_name_value_list_test_f)
    [[ "${a}" == '[{"name":"apa","value":"bepa"},{"name":"bepa","value":"ce\"p\"a"}]' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_name_value_list_test_main(){
    printf json_name_value_list_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_name_value_list_test_1(\ |$) ]]; then json_name_value_list_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_name_value_list_test_main "${org_args[@]}"
    else
	json_name_value_list_test_main "${@}"
    fi
fi
# json_name_value_list-test-main ends here
