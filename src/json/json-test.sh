#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::json-unit-tests][json-unit-tests]]
org_args='()'
[[ "$_BCU_SH_LOADED" == YES ]] || . bcu.sh
_json_MOD_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")
json_private(){
    :

}
json_test_main(){
. bcu.sh
bcu__json_new_string_object(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() key_string value_string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Create a json object with strings as key and value


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__json_new_string_object 'ap"a' 'be"pa' | jq -Mc
    bcu__json_new_string_object apa bepa | jq -Mc

Results:

    {"ap\"a":"be\"pa"}
    {"apa":"bepa"}

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:key_string 1:t:value_string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    #printf '%s' "{\"${1}\":\"${2}\"}"
    local result
    if result=$(printf '%s' '{"temp": {} }' | jq -c --arg a "${key_string}" --arg b "${value_string}" '.temp[$a] = $b | .temp'); then
	printf '%s\n' "$result"
    else
	printf '%s' "failed creating object - $result - : ${_STACK}" && bcu__stack "$@" && return 1; fi
}
json_new_string_object_test_f(){
    local a
    a=$(bcu__json_new_string_object 'ap"a' 'be"pa')
    declare -p a
}
json_new_string_object_test_1()(
    . <(json_new_string_object_test_f)
    [[ "${a}" == '{"ap\"a":"be\"pa"}' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_new_string_object_test_main(){
    printf json_new_string_object_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_new_string_object_test_1(\ |$) ]]; then json_new_string_object_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_new_string_object_test_main "${org_args[@]}"
    else
	json_new_string_object_test_main "${@}"
    fi
fi
. bcu.sh
bcu__json_new_string_elem(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() key_string value_string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Create a new json string element


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__json_new_string_elem 'a}pa' 'be}pa' && echo
    bcu__json_new_string_elem apa bepa && echo
    bcu__json_elems_to_object $(bcu__json_new_string_elem apa bepa) | jq -Mc

Results:

    "a}pa":"be}pa"
    "apa":"bepa"
    {"apa":"bepa"}

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:key_string 1:t:value_string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    local a b c
    if a=$(echo '{"temp": {} }' \
	       | jq -c --arg a "${key_string}" --arg b "${value_string}" '.temp[$a] = $b | .temp'); then
	b="${a%\}}"
	c="${b#\{}"
	printf '%s' "$c"
    else
	printf '%s' "Failed creating a string element - $a - : ${_STACK}" && bcu__stack "$@" && return 1; fi

    #printf '%s' "\"${1}\":\"${2:-$val}\""
}
json_new_string_elem_test_f(){
    local a
    a=$(bcu__json_new_string_elem 'a}pa' 'be}pa')
    declare -p a
}
json_new_string_elem_test_1()(
    . <(json_new_string_elem_test_f)
    [[ "${a}" == '"a}pa":"be}pa"' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_new_string_elem_test_main(){
    printf json_new_string_elem_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_new_string_elem_test_1(\ |$) ]]; then json_new_string_elem_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_new_string_elem_test_main "${org_args[@]}"
    else
	json_new_string_elem_test_main "${@}"
    fi
fi
. bcu.sh
bcu__json_strip_quotes(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() json_string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Strip the quotes off of a well-escaped and quoted json string


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    printf '%s\n' $(bcu__json_elems_to_object $(bcu__json_new_string_elem apa bepa)) | bcu__json_strip_quotes
    printf '%s\u0000' apa bepa cepa | bcu__json_to_json_string -z | bcu__json_strip_quotes

Results:

    {"apa":"bepa"}
    apa bepa cepa

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:json_string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    local t0
    while read -r t0 ; do
	t0="${t0#\"}"; t0="${t0%\"}"; printf '%s\n' "$t0" ; done < <(printf '%s' "${json_string[@]}" | jq -c '.')
}
json_strip_quotes_test_f(){
    local a b
    a=$(printf '%s' '"apa\nbepa\ncepa\n"' | bcu__json_strip_quotes)
    b=$(bcu__json_strip_quotes '"apa\nbepa\ncepa\n"')
    declare -p a b
}
json_strip_quotes_test_1()(
    . <(json_strip_quotes_test_f)
    [[ "${a}" == 'apa\nbepa\ncepa\n' ]] || { echo FAIL && return 1; }
    [[ "${b}" == 'apa\nbepa\ncepa\n' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_strip_quotes_test_main(){
    printf json_strip_quotes_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_strip_quotes_test_1(\ |$) ]]; then json_strip_quotes_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_strip_quotes_test_main "${org_args[@]}"
    else
	json_strip_quotes_test_main "${@}"
    fi
fi
. bcu.sh
bcu__json_to_json_string(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Escape a string to json format. Use json_to_json_strings if you need
to escape several strings in one go.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__json_to_json_string 'apa{}[]]""] bepa' 'cepa' | jq -Mc
    printf '%s\n' a b c | bcu__json_to_json_string -z | jq -Mc
  
Results:

    "apa{}[]]\"\"] bepa cepa"
    "a\nb\nc\n"

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    # corresponds to just running jq -Rs .
    jq -n '$ARGS.positional[]' --args "${string[*]}"
}
json_to_json_string_test_f(){
    local a b
    a=$(bcu__json_to_json_string '"apa' bepa)
    b=$(printf '%s\u0000' apa bepa | bcu__json_to_json_string -z)
    declare -p a b
}
json_to_json_string_test_1()(
    . <(json_to_json_string_test_f)
    [[ "${a}" == '"\"apa bepa"' ]] || { echo FAIL && return 1; }
    [[ "${b}" == '"apa bepa"' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_to_json_string_test_main(){
    printf json_to_json_string_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_to_json_string_test_1(\ |$) ]]; then json_to_json_string_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_to_json_string_test_main "${org_args[@]}"
    else
	json_to_json_string_test_main "${@}"
    fi
fi
. bcu.sh
bcu__json_to_json_strings(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Escape strings to json format. Use json_to_json_string if you need to
escape given strings into one single json-escaped string.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__json_to_json_strings '"a"pa' bepa | jq -Mc
    printf '%s\n' 'ap"a' bepa | bcu__json_to_json_strings | jq -Mc

Results:

    "\"a\"pa"
    "bepa"
    "ap\"a"
    "bepa"

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    # corresponds to just running jq -R .
    jq -n '$ARGS.positional[]' --args "${string[@]}"
}
json_to_json_strings_test_f(){
    local -a a
    mapfile -t a < <(bcu__json_to_json_strings apa bepa)
    declare -p a
}
json_to_json_strings_test_1()(
    . <(json_to_json_strings_test_f)
    [[ "${a[0]}" == '"apa"' ]] || { echo FAIL && return 1; }
    [[ "${a[1]}" == '"bepa"' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_to_json_strings_test_main(){
    printf json_to_json_strings_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_to_json_strings_test_1(\ |$) ]]; then json_to_json_strings_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_to_json_strings_test_main "${org_args[@]}"
    else
	json_to_json_strings_test_main "${@}"
    fi
fi
. bcu.sh
bcu__json_escape_string(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Escape given strings to one json formatted string, leaving the quotes
out. Use json_escape_strings if you need to escape the given strings
into their own json-escaped strings instead of concatenating them.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    a="apa{}][]\" bepa"
    bcu__json_escape_string "$a"
    bcu__json_elems_to_object "$(bcu__json_new_string_elem mystring "$(bcu__json_escape_string "$a")")" | jq -Mc
    bcu__json_elems_to_object "$(bcu__json_new_string_elem mystring "$(bcu__json_escape_string "$a")")" | jq -rMc .mystring

Results:

    apa{}][]\" bepa
    {"mystring":"apa{}][]\\\" bepa"}
    apa{}][]\" bepa

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    bcu__json_to_json_string "${string[*]}" | bcu__json_strip_quotes
}
json_escape_string_test_f(){
    local a
    a=$(bcu__json_escape_string apa bepa '}{,""')
    declare -p a
}
json_escape_string_test_1()(
    . <(json_escape_string_test_f)
    [[ "${a}" == 'apa bepa }{,\"\"' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_escape_string_test_main(){
    printf json_escape_string_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_escape_string_test_1(\ |$) ]]; then json_escape_string_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_escape_string_test_main "${org_args[@]}"
    else
	json_escape_string_test_main "${@}"
    fi
fi
. bcu.sh
bcu__json_escape_strings(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Escape strings to json format leaving the quotes out. Use
json_escape_string if you need to escape given strings into one
single json-escaped string.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__json_escape_strings "apa{}][]\" bepa" apa bepa

Results:

    apa{}][]\" bepa
    apa
    bepa

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    bcu__json_to_json_strings "${string[@]}" | bcu__json_strip_quotes
}
json_escape_strings_test_f(){
    local a
    mapfile -t a < <(bcu__json_escape_strings apa bepa '}{,""')
    declare -p a
}
json_escape_strings_test_1()(
    . <(json_escape_strings_test_f)
    [[ "${a[0]}" == apa ]] || { echo FAIL && return 1; }
    [[ "${a[1]}" == bepa ]] || { echo FAIL && return 1; }
    [[ "${a[2]}" == '}{,\"\"' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_escape_strings_test_main(){
    printf json_escape_strings_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_escape_strings_test_1(\ |$) ]]; then json_escape_strings_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_escape_strings_test_main "${org_args[@]}"
    else
	json_escape_strings_test_main "${@}"
    fi
fi
. bcu.sh
bcu__json_new_object_list(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() string_key value
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
json_new_object_list is only used for creating a list of
string-objects with the same key. If you instead want to create a list
of your json objects, invoke ~printf '%s\n' "${JsonObjects[@]}" | jq
-s -c~.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__json_new_object_list apa bepa cepa depa | jq -Mc

Results:

    [{"apa":"bepa"},{"apa":"cepa"},{"apa":"depa"}]

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:string_key 0:t:value)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    #printf '%s' "$@" | jq -c -s . ; fi
    # local str="["

    for val in "${value[@]}"; do
	bcu__json_new_string_object "$string_key" "$val"; done | jq -c -s

    # for obj in "${Objects[@]}"; do
    # 	str+="${obj},"; done
    # printf '%s' "${str%,}]"
}
json_new_object_list_test_f(){
    local a
    a=$(bcu__json_new_object_list apa bepa cepa depa)
    declare -p a
}
json_new_object_list_test_1()(
    . <(json_new_object_list_test_f)
    [[ "${a}" == '[{"apa":"bepa"},{"apa":"cepa"},{"apa":"depa"}]' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_new_object_list_test_main(){
    printf json_new_object_list_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_new_object_list_test_1(\ |$) ]]; then json_new_object_list_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_new_object_list_test_main "${org_args[@]}"
    else
	json_new_object_list_test_main "${@}"
    fi
fi
. bcu.sh
bcu__json_new_object_elem(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() name json_object
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Create a new named json-object


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__json_new_object_elem -n apa -- '{"bepa":"cepa"}' && echo
    echo '{"bepa":"cepa"}' | bcu__json_new_object_elem -n apa -- && echo

Results:

    "apa":{"bepa":"cepa"}
    "apa":{"bepa":"cepa"}

EOF
		)
    }

    # Options
    local Name=(n name "" "The name of the new object" 1 t f)
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})" "(${Name[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:json_object)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    #local -a Json
    local s t
    #Json=("${Input[1]}")
    #printf '%s' '{"temp": {} }' | jq -c --arg a "${Input[0]}" --argjson b "$(printf '%s' "${Json[@]}" | jq -c -s .[])" '.temp[$a] = $b | .temp'

    #s=$(printf '%s' '{"temp": {} }' | jq -c --arg a "${Input[0]}" --argjson b "$(printf '%s' "${Json[@]}" | jq -c -s .[])" '.temp[$a] = $b | .temp')

    s=$(printf '%s' '{"temp": {} }' | jq -c --arg a "${name}" --argjson b "$(printf '%s' "${json_object}" | jq -c -s .[])" '.temp[$a] = $b | .temp')

    t=$(printf '%s' "${s%\}}")
    printf '%s' "${t#\{}"

    #      printf '%s' '{"temp": {} }' | jq -c --arg a "${Input[0]}" --argjson b "$(printf '%s' "${Json[@]}" | jq -c -s .[])" '.temp[$a] = $b | .temp'
    #printf '%s' "\"${1}\":{${2}}"
}
json_new_object_elem_test_f(){
    local a
    a=$(bcu__json_new_object_elem -n apa -- '{"bepa":"cepa"}')
    declare -p a
}
json_new_object_elem_test_1()(
    . <(json_new_object_elem_test_f)
    [[ "${a}" == '"apa":{"bepa":"cepa"}' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_new_object_elem_test_main(){
    printf json_new_object_elem_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_new_object_elem_test_1(\ |$) ]]; then json_new_object_elem_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_new_object_elem_test_main "${org_args[@]}"
    else
	json_new_object_elem_test_main "${@}"
    fi
fi
. bcu.sh
bcu__json_name_value_list(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() name_value
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Create a list of name-value objects


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__json_name_value_list apa=bepa 'bepa=ce"p"a' | jq -Mc

Results:

    [{"name":"apa","value":"bepa"},{"name":"bepa","value":"ce\"p\"a"}]

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:name_value)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    local str name value obj
    str="["
    local -a Objects
    #local -a Args=("${Input[@]}")
    #for cf in "${Args[@]}"; do
    for cf in "${name_value[@]}"; do
	name="${cf%%=*}"
	value="${cf##*=}"
	Objects+=("$(bcu__json_elems_to_object "$(bcu__json_new_string_elem name "$name")" "$(bcu__json_new_string_elem value "$value")")"); done
    
    for obj in "${Objects[@]}"; do
	str+="${obj},"; done
    printf '%s' "${str%,}]" | jq -c
    # json_name_value_list(){ # ElemName {ElemValue} .. {ElemValue} -> [{jsonObjects}]
    #     local str="["
    #     declare -n jsonAssocListList="$1"
    #     for elemName "${!jsonAssocListList[@]}"; do
    # 	Objects+=($(json_new_string_object "$elemName" "${jsonAssocListList[$elemName]}")); done
    
    #     for obj in "${Objects[@]}"; do
    # 	str+="${obj},"; done
    #     printf '%s' "${str%,}]"
    # }
}
json_name_value_list_test_f(){
    local a
    a=$(bcu__json_name_value_list apa=bepa 'bepa=ce"p"a' | jq -Mc)
    declare -p a
}
json_name_value_list_test_1()(
    . <(json_name_value_list_test_f)
    [[ "${a}" == '[{"name":"apa","value":"bepa"},{"name":"bepa","value":"ce\"p\"a"}]' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_name_value_list_test_main(){
    printf json_name_value_list_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_name_value_list_test_1(\ |$) ]]; then json_name_value_list_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_name_value_list_test_main "${org_args[@]}"
    else
	json_name_value_list_test_main "${@}"
    fi
fi
. bcu.sh
bcu__is_valid_json(){
    local _STACK z Options=() Input=() json_data
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Check if a string is a valid json string


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__is_valid_json '["a"]' && echo true
    bcu__is_valid_json '["a]' || echo false

Results:

    true
    false

EOF
		)
    }
    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:json_data)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "" =~ ^(-h|--help)$ ]] && return 0

    # Check args
    for arg in "${json_data}"; do
	jq -n . --argjson b "$arg" &>/dev/null || return 1; done
}
is_valid_json_test_f(){
    local a
    a=$(bcu__is_valid_json '["a"]' && echo true)
    b=$(bcu__is_valid_json '["a]' || echo false)
    declare -p a b
}
is_valid_json_test_1()(
    . <(is_valid_json_test_f)
    [[ "${a}" == true ]] || { echo FAIL && return 1; }
    [[ "${b}" == false ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
is_valid_json_test_main(){
    printf is_valid_json_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )is_valid_json_test_1(\ |$) ]]; then is_valid_json_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	is_valid_json_test_main "${org_args[@]}"
    else
	is_valid_json_test_main "${@}"
    fi
fi
. bcu.sh
bcu__json_new_elem(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() name json_data
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Create a new named element


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__json_new_elem -n apa -- '[{"name":"apa","value":"bepa"}]' && echo

Results:

    "apa":[{"name":"apa","value":"bepa"}]

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Name=(n name "" "Name of the new json element")
    Options+=("(${null[*]@Q})" "(${Name[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:json_data)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || {
	printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    #printf '%s' "\"${1}\":${2}"
    bcu__is_valid_json "$json_data" || {
	printf '%s' "json_data is not valid json: ${_STACK}" && bcu__stack "$@" && return 1; }

    bcu__json_new_object_elem -n "$name" "${json_data}" #| jq -c ".${Input[0]}"
}
json_new_elem_test_f(){
    local a
    a=$(bcu__json_new_elem -n apa -- '[{"name":"apa","value":"bepa"}]')
    declare -p a
}
json_new_elem_test_1()(
    . <(json_new_elem_test_f)
    [[ "${a}" == '"apa":[{"name":"apa","value":"bepa"}]' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_new_elem_test_main(){
    printf json_new_elem_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_new_elem_test_1(\ |$) ]]; then json_new_elem_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_new_elem_test_main "${org_args[@]}"
    else
	json_new_elem_test_main "${@}"
    fi
fi
. bcu.sh
bcu__json_set_object_field(){
    local _STACK z Options=() Input=() Object key value obj
    [[ "$1" =~ (-h|--help) ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Set a json field key and value. Adds the field to the given object if
the key doesn't exist, else it replaces the value associated with the
given key. The value must be a valid json object, not just a string.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__json_set_object_field -k key -v "{\"key\":\"value\"}" -- "{}"

Results:

    {"key":{"key":"value"}}

EOF
		)
    }
    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Key=(k key "" "Json key string" 1 t)
    local Value=(v value "" "Json value string" 1 t)
    Options+=("(${null[*]@Q})" "(${Key[*]@Q})" "(${Value[*]@Q})" )

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:Object)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || {
        printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ (-h|--help) ]] && return 0

    # Check args, or be lazy
    for obj in "${Object[@]}"; do
	jq -Mcn --unbuffered --arg k "$key" --argjson v "$value" --argjson o "$obj" ' $o + { ( $k ) : $v } '; done
}
json_set_object_field_test_f(){
    local a
    a=$(bcu__json_set_object_field -k key -v "{\"key\":\"value\"}" -- "{}")
    declare -p a
}
json_set_object_field_test_1()(
    . <(json_set_object_field_test_f)
    [[ "${a}" == '{"key":{"key":"value"}}' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_set_object_field_test_main(){
    printf json_set_object_field_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_set_object_field_test_1(\ |$) ]]; then json_set_object_field_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_set_object_field_test_main "${org_args[@]}"
    else
	json_set_object_field_test_main "${@}"
    fi
fi
. bcu.sh
bcu__json_set_field(){
    local _STACK z Options=() Input=() Object obj
    [[ "$1" =~ (-h|--help) ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Set a json field key and value. Adds the field to the given object if
the key doesn't exist, else it replaces the value associated with the
given key.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__json_set_field -k apa -v bepa -- "{}"

Results:

    {"apa":"bepa"}

EOF
		)
    }
    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Key=(k key "" "Json key string" 1 t)
    local Value=(v value "" "Json value string" 1 t)
    Options+=("(${null[*]@Q})" "(${Key[*]@Q})" "(${Value[*]@Q})" )

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:Object)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || {
	printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ (-h|--help) ]] && return 0

    # Check args, or be lazy
    #declare -p key value Objects
    for obj in "${Object[@]}"; do
	jq -Mcn --unbuffered --arg k "$key" --arg v "$value" --argjson o "$obj" ' $o + { ( $k ) : $v } '; done
}
json_set_field_test_f(){
    local a
    a=$(bcu__json_set_field -k apa -v bepa -- "{}" | jq -Mc)
    b=$(bcu__json_set_field -k apa -v notbepa -- '{"apa":"bepa"}' | jq -Mc)
    declare -p a b
}
json_set_field_test_1()(
    . <(json_set_field_test_f)
    [[ "${a}" == '{"apa":"bepa"}' ]] || { echo FAIL && return 1; }
    [[ "${b}" == '{"apa":"notbepa"}' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_set_field_test_main(){
    printf json_set_field_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_set_field_test_1(\ |$) ]]; then json_set_field_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_set_field_test_main "${org_args[@]}"
    else
	json_set_field_test_main "${@}"
    fi
fi
. bcu.sh
bcu__json_elems_to_object(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() json_element
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Given a number of json elements, json_elems_to_object wraps them into a
well-formed json object.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__json_elems_to_object "$(bcu__json_new_string_elem apa1 bepa)" "$(bcu__json_new_object_elem -n apa2 -- "$(bcu__json_name_value_list apa=bepa bepa=cepa)")" | jq -Mc && echo

Results:

    {"apa1":"bepa","apa2":[{"name":"apa","value":"bepa"},{"name":"bepa","value":"cepa"}]}

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:json_element)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    local -a Objects
    local obj
    for obj in "${json_element[@]}"; do
	Objects+=("${obj}" ','); done

    # # TODO:
    #printf '%s' "{${str%,}}"
    local a
    a="${Objects[@]}"
    printf '%s' "{${a%,}}" | jq -c

    # #jq -s -c '.[] | { . }'
    # local -a Objects
    # local obj
    # if [[ ! -t 0 ]]; then
    # 	while read -r obj; do
    # 	    Objects+=("$obj" ','); done
    # else
    # 	for obj in "$@"; do
    # 	    Objects+=("${obj}" ','); done; fi

    # # # TODO:
    # #printf '%s' "{${str%,}}"
    # local a
    # a="${Objects[@]}"
    # printf '%s' "{${a%,}}" | jq -c
}
json_elems_to_object_test_f(){
    local a
    a=$(bcu__json_elems_to_object '"apa":[{"name":"apa","value":"bepa"}' '{"name":"bepa","value":"cepa"}]')
    declare -p a
}
json_elems_to_object_test_1()(
    . <(json_elems_to_object_test_f)
    [[ "${a}" == '{"apa":[{"name":"apa","value":"bepa"},{"name":"bepa","value":"cepa"}]}' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_elems_to_object_test_main(){
    printf json_elems_to_object_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_elems_to_object_test_1(\ |$) ]]; then json_elems_to_object_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_elems_to_object_test_main "${org_args[@]}"
    else
	json_elems_to_object_test_main "${@}"
    fi
fi
. bcu.sh
bcu__json_objects_to_list(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() json_object
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Put json objects in a json list


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__json_objects_to_list "$(bcu__json_elems_to_object "$(bcu__json_new_elem -n apa -- 5)")" "$(bcu__json_new_string_object bepa cepa)" | jq -Mc

Results:

    [{"apa":5},{"bepa":"cepa"}]

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:json_object)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    # {Object} .. {Object} -> [{jsonObject}]
    # stdin input corresponds to just running: jq -s -c .
    jq -c -n '$ARGS.positional' --jsonargs "${json_object[@]}"
    #printf '%s' "$@" | jq -s -c .
    # local str
    # for obj in "$@"; do
    # 	str+="${obj},"; done
    # # TODO:
    # printf '%s' "[${str%,}]"
}
json_objects_to_list_test_f(){
    local a
    a=$(bcu__json_objects_to_list '{"apa":5}' '{"bepa":"cepa"}')
    declare -p a
}
json_objects_to_list_test_1()(
    . <(json_objects_to_list_test_f)
    [[ "${a}" == '[{"apa":5},{"bepa":"cepa"}]' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_objects_to_list_test_main(){
    printf json_objects_to_list_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_objects_to_list_test_1(\ |$) ]]; then json_objects_to_list_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_objects_to_list_test_main "${org_args[@]}"
    else
	json_objects_to_list_test_main "${@}"
    fi
fi
. bcu.sh
bcu__json_strings_to_list(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Put strings in a json list.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__json_strings_to_list 'asdf[]{}{"""{asdf' bepa cepa | jq -Mc

Results:

    ["asdf[]{}{\"\"\"{asdf","bepa","cepa"]

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    # local str
    # for obj in "$@"; do
    # 	str+="\"${obj}\","; done
    # # TODO:
    # printf '%s' "[${str%,}]"

    # local -a Args
    # if [[ ! -t 0 ]]; then
    # 	mapfile -t Args
    # else
    # 	Args=("$@"); fi
    jq -c -n --unbuffered '$ARGS.positional' --args "${string[@]}"
}
json_strings_to_list_test_f(){
    local a
    a=$(bcu__json_strings_to_list 'asdf[]{}{"""{asdf' bepa cepa)
    declare -p a
}
json_strings_to_list_test_1()(
    . <(json_strings_to_list_test_f)
    [[ "${a}" == '["asdf[]{}{\"\"\"{asdf","bepa","cepa"]' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_strings_to_list_test_main(){
    printf json_strings_to_list_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_strings_to_list_test_1(\ |$) ]]; then json_strings_to_list_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_strings_to_list_test_main "${org_args[@]}"
    else
	json_strings_to_list_test_main "${@}"
    fi
fi
. bcu.sh
bcu__json_diff(){
    local _STACK z Options=() Input=() key keys file1 file2 f
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
diff two json documents based on one or several json element keys


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    read -r -d '' a < <(cat <<'EOF'
      [
          {
    	"name": "Cynthia",
    	"surname": "Craig",
    	"isActive": true,
    	"balance": "$2,426.88"
          },
          {
    	"name": "Elise",
    	"surname": "Long",
    	"isActive": false,
    	"balance": "$1,892.72"
          },
          {
    	"name": "Hyde",
    	"surname": "Adkins",
    	"isActive": true,
    	"balance": "$1,769.34"
          },
          {
    	"name": "Matthews",
    	"surname": "Jefferson",
    	"isActive": true,
    	"balance": "$1,991.42"
          },
          {
    	"name": "Kris",
    	"surname": "Norris",
    	"isActive": false,
    	"balance": "$2,137.11"
          }
      ]
    EOF
    		   )
    read -r -d '' b < <(cat <<'EOF'
    		    [
    			{
    			    "name": "Cynthia",
    			    "surname": "Craig"
    			},
    			{
    			    "name": "Kris",
    			    "surname": "Norris"
    			}
    		    ]
    EOF
    		   )
    echo "$a" > a
    echo "$b" > b
    ab=$(bcu__json_diff a b surname name | jq -M)
    rm a; rm b; echo "$ab"

Results:

    [
      {
        "name": "Elise",
        "surname": "Long",
        "isActive": false,
    1,892.72"
      },
      {
        "name": "Hyde",
        "surname": "Adkins",
        "isActive": true,
    1,769.34"
      },
      {
        "name": "Matthews",
        "surname": "Jefferson",
        "isActive": true,
    1,991.42"
      }
    ]
    

EOF
		)
    }
    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:file1 1:t:file2 0:t:key)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || {
	printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    # Check args
    for f in "$file1" "$file2"; do
        [[ -e "$f" ]] || { printf '%s' "File $f does not exist: ${_STACK}" && bcu__stack "$@" && return 1; }
        [[ -r "$f" ]] || { printf '%s' "File $f is not readable: ${_STACK}" && bcu__stack "$@" && return 1; }
    done
    
    local jqscript
    read -r keys < <(printf '%s, ' "${key[@]}")
    read -r keys < <(printf '{%s}' "${keys,*}")
    read -r -d '' jqscript < <(cat <<'EOF'
    #The following solution is intended to be generic, efficient and as simple as possible subject to the first two objectives.
    #Genericity

    #For genericity, let us suppose that $one and $two are two arrays of
    #JSON entities, and that we wish to find those items, $x, in $one such
    #that ($x|filter) does not appear in map($two | filter), where filter
    #is an arbitrary filter. (In the present instance, it is
    #{iplnetdev_ip_addr, name}.)

    #The solution uses INDEX/1, which was added to jq after the official
    #1.5 release, so we begin by reproducing its definition:

    def INDEX(stream; idx_expr):
      reduce stream as $row ({};
	.[$row|idx_expr|
	  if type != "string" then tojson
	  else .
	  end] |= $row);
    def INDEX(idx_expr): INDEX(.[]; idx_expr);

    #Efficiency

    #For efficiency, we will need to use a JSON object as a dictionary;
    #since keys must be strings, we will need to ensure that when
    #converting an object to a string, the objects are normalized. For
    #this, we define normalize as follows:

    # Normalize the input with respect to the order of keys in objects
    def normalize:
      . as $in
      | if type == "object" then reduce keys[] as $key
	     ( {}; . + { ($key):  ($in[$key] | normalize) } ) 
	elif type == "array" then map( normalize )
	else .
	end;

    #To construct the dictionary, we simply apply (normalize|tojson):
    def todict(filter):
      INDEX(filter| normalize | tojson);

    #The solution

    #The solution is now quite simple:

    # select those items from the input stream for which 
    # (normalize|tojson) is NOT in dict:
    def MINUS(filter; $dict):
     select( $dict[filter | normalize | tojson] | not);

    def difference($one; $two; filter):
      ($two | todict(filter)) as $dict
      | $one[] | MINUS( filter; $dict );
EOF
			       cat <<EOF
    [ difference( \$one; \$two; $keys ) ]
EOF
			      )
    jq -n --argfile one "$file1" --argfile two "$file2" -f <(printf '%s\n' "$jqscript")
}
json_diff_test_f(){
    local a b c
    
    read -r -d '' a < <(cat <<'EOF'
    [
	{
	  "name": "Cynthia",
	  "surname": "Craig",
	  "isActive": true,
	  "balance": "$2,426.88"
	},
	{
	  "name": "Elise",
	  "surname": "Long",
	  "isActive": false,
	  "balance": "$1,892.72"
	},
	{
	  "name": "Hyde",
	  "surname": "Adkins",
	  "isActive": true,
	  "balance": "$1,769.34"
	},
	{
	  "name": "Matthews",
	  "surname": "Jefferson",
	  "isActive": true,
	  "balance": "$1,991.42"
	},
	{
	  "name": "Kris",
	  "surname": "Norris",
	  "isActive": false,
	  "balance": "$2,137.11"
	}
    ]
EOF
		       )
    read -r -d '' b < <(cat <<'EOF'
    [
	{
	  "name": "Cynthia",
	  "surname": "Craig"
	},
	{
	  "name": "Kris",
	  "surname": "Norris"
	}
    ]
EOF
		       )
    local tmp1=$(mktemp /tmp/a.XXXXXX)
    echo "$a" > "$tmp1"
    local tmp2=$(mktemp /tmp/b.XXXXXX)
    echo "$b" > "$tmp2"
    mapfile -t c < <(bcu__json_diff "$tmp1" "$tmp2" surname name | jq -Mr .[].name | sort)
    rm "$tmp1"; rm "$tmp2"
    declare -p c
}
json_diff_test_1()(
    . <(json_diff_test_f)
    [[ "${c[0]}" == Elise ]] || { echo FAIL && return 1; }
    [[ "${c[1]}" == Hyde ]] || { echo FAIL && return 1; }
    [[ "${c[2]}" == Matthews ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_diff_test_main(){
    printf json_diff_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_diff_test_1(\ |$) ]]; then json_diff_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_diff_test_main "${org_args[@]}"
    else
	json_diff_test_main "${@}"
    fi
fi
local pydpid="$(cat /tmp/pydaemon/$_BCU_PYDAEMON_CHANNEL.sock.pid )"
kill "$pydpid"
wait "$pydpid" 2>/dev/null
}
declare -a org_args="${org_args}"
[[ -e "${_BCU_SH_DIR}"/disabled_tests.txt ]] && mapfile -t _BCU_TESTS_DISABLED <"${_BCU_SH_DIR}"/disabled_tests.txt
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_test_main "${org_args[@]}"
    else
	json_test_main "${@}"
    fi
fi
# json-unit-tests ends here
