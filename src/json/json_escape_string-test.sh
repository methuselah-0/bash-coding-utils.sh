#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::json_escape_string-test-main][json_escape_string-test-main]]
org_args='()'
. bcu.sh
bcu__json_escape_string(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Escape given strings to one json formatted string, leaving the quotes
out. Use json_escape_strings if you need to escape the given strings
into their own json-escaped strings instead of concatenating them.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    a="apa{}][]\" bepa"
    bcu__json_escape_string "$a"
    bcu__json_elems_to_object "$(bcu__json_new_string_elem mystring "$(bcu__json_escape_string "$a")")" | jq -Mc
    bcu__json_elems_to_object "$(bcu__json_new_string_elem mystring "$(bcu__json_escape_string "$a")")" | jq -rMc .mystring

Results:

    apa{}][]\" bepa
    {"mystring":"apa{}][]\\\" bepa"}
    apa{}][]\" bepa

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    bcu__json_to_json_string "${string[*]}" | bcu__json_strip_quotes
}
json_escape_string_test_f(){
    local a
    a=$(bcu__json_escape_string apa bepa '}{,""')
    declare -p a
}
json_escape_string_test_1()(
    . <(json_escape_string_test_f)
    [[ "${a}" == 'apa bepa }{,\"\"' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_escape_string_test_main(){
    printf json_escape_string_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_escape_string_test_1(\ |$) ]]; then json_escape_string_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_escape_string_test_main "${org_args[@]}"
    else
	json_escape_string_test_main "${@}"
    fi
fi
# json_escape_string-test-main ends here
