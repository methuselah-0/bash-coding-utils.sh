#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::json_to_json_string-test-main][json_to_json_string-test-main]]
org_args='()'
. bcu.sh
bcu__json_to_json_string(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Escape a string to json format. Use json_to_json_strings if you need
to escape several strings in one go.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__json_to_json_string 'apa{}[]]""] bepa' 'cepa' | jq -Mc
    printf '%s\n' a b c | bcu__json_to_json_string -z | jq -Mc
  
Results:

    "apa{}[]]\"\"] bepa cepa"
    "a\nb\nc\n"

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    # corresponds to just running jq -Rs .
    jq -n '$ARGS.positional[]' --args "${string[*]}"
}
json_to_json_string_test_f(){
    local a b
    a=$(bcu__json_to_json_string '"apa' bepa)
    b=$(printf '%s\u0000' apa bepa | bcu__json_to_json_string -z)
    declare -p a b
}
json_to_json_string_test_1()(
    . <(json_to_json_string_test_f)
    [[ "${a}" == '"\"apa bepa"' ]] || { echo FAIL && return 1; }
    [[ "${b}" == '"apa bepa"' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_to_json_string_test_main(){
    printf json_to_json_string_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_to_json_string_test_1(\ |$) ]]; then json_to_json_string_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_to_json_string_test_main "${org_args[@]}"
    else
	json_to_json_string_test_main "${@}"
    fi
fi
# json_to_json_string-test-main ends here
