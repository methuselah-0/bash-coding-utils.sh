#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::json_diff-test-main][json_diff-test-main]]
org_args='()'
. bcu.sh
bcu__json_diff(){
    local _STACK z Options=() Input=() key keys file1 file2 f
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
diff two json documents based on one or several json element keys


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    read -r -d '' a < <(cat <<'EOF'
      [
          {
    	"name": "Cynthia",
    	"surname": "Craig",
    	"isActive": true,
    	"balance": "$2,426.88"
          },
          {
    	"name": "Elise",
    	"surname": "Long",
    	"isActive": false,
    	"balance": "$1,892.72"
          },
          {
    	"name": "Hyde",
    	"surname": "Adkins",
    	"isActive": true,
    	"balance": "$1,769.34"
          },
          {
    	"name": "Matthews",
    	"surname": "Jefferson",
    	"isActive": true,
    	"balance": "$1,991.42"
          },
          {
    	"name": "Kris",
    	"surname": "Norris",
    	"isActive": false,
    	"balance": "$2,137.11"
          }
      ]
    EOF
    		   )
    read -r -d '' b < <(cat <<'EOF'
    		    [
    			{
    			    "name": "Cynthia",
    			    "surname": "Craig"
    			},
    			{
    			    "name": "Kris",
    			    "surname": "Norris"
    			}
    		    ]
    EOF
    		   )
    echo "$a" > a
    echo "$b" > b
    ab=$(bcu__json_diff a b surname name | jq -M)
    rm a; rm b; echo "$ab"

Results:

    [
      {
        "name": "Elise",
        "surname": "Long",
        "isActive": false,
    1,892.72"
      },
      {
        "name": "Hyde",
        "surname": "Adkins",
        "isActive": true,
    1,769.34"
      },
      {
        "name": "Matthews",
        "surname": "Jefferson",
        "isActive": true,
    1,991.42"
      }
    ]
    

EOF
		)
    }
    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:file1 1:t:file2 0:t:key)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || {
	printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    # Check args
    for f in "$file1" "$file2"; do
        [[ -e "$f" ]] || { printf '%s' "File $f does not exist: ${_STACK}" && bcu__stack "$@" && return 1; }
        [[ -r "$f" ]] || { printf '%s' "File $f is not readable: ${_STACK}" && bcu__stack "$@" && return 1; }
    done
    
    local jqscript
    read -r keys < <(printf '%s, ' "${key[@]}")
    read -r keys < <(printf '{%s}' "${keys,*}")
    read -r -d '' jqscript < <(cat <<'EOF'
    #The following solution is intended to be generic, efficient and as simple as possible subject to the first two objectives.
    #Genericity

    #For genericity, let us suppose that $one and $two are two arrays of
    #JSON entities, and that we wish to find those items, $x, in $one such
    #that ($x|filter) does not appear in map($two | filter), where filter
    #is an arbitrary filter. (In the present instance, it is
    #{iplnetdev_ip_addr, name}.)

    #The solution uses INDEX/1, which was added to jq after the official
    #1.5 release, so we begin by reproducing its definition:

    def INDEX(stream; idx_expr):
      reduce stream as $row ({};
	.[$row|idx_expr|
	  if type != "string" then tojson
	  else .
	  end] |= $row);
    def INDEX(idx_expr): INDEX(.[]; idx_expr);

    #Efficiency

    #For efficiency, we will need to use a JSON object as a dictionary;
    #since keys must be strings, we will need to ensure that when
    #converting an object to a string, the objects are normalized. For
    #this, we define normalize as follows:

    # Normalize the input with respect to the order of keys in objects
    def normalize:
      . as $in
      | if type == "object" then reduce keys[] as $key
	     ( {}; . + { ($key):  ($in[$key] | normalize) } ) 
	elif type == "array" then map( normalize )
	else .
	end;

    #To construct the dictionary, we simply apply (normalize|tojson):
    def todict(filter):
      INDEX(filter| normalize | tojson);

    #The solution

    #The solution is now quite simple:

    # select those items from the input stream for which 
    # (normalize|tojson) is NOT in dict:
    def MINUS(filter; $dict):
     select( $dict[filter | normalize | tojson] | not);

    def difference($one; $two; filter):
      ($two | todict(filter)) as $dict
      | $one[] | MINUS( filter; $dict );
EOF
			       cat <<EOF
    [ difference( \$one; \$two; $keys ) ]
EOF
			      )
    jq -n --argfile one "$file1" --argfile two "$file2" -f <(printf '%s\n' "$jqscript")
}
json_diff_test_f(){
    local a b c
    
    read -r -d '' a < <(cat <<'EOF'
    [
	{
	  "name": "Cynthia",
	  "surname": "Craig",
	  "isActive": true,
	  "balance": "$2,426.88"
	},
	{
	  "name": "Elise",
	  "surname": "Long",
	  "isActive": false,
	  "balance": "$1,892.72"
	},
	{
	  "name": "Hyde",
	  "surname": "Adkins",
	  "isActive": true,
	  "balance": "$1,769.34"
	},
	{
	  "name": "Matthews",
	  "surname": "Jefferson",
	  "isActive": true,
	  "balance": "$1,991.42"
	},
	{
	  "name": "Kris",
	  "surname": "Norris",
	  "isActive": false,
	  "balance": "$2,137.11"
	}
    ]
EOF
		       )
    read -r -d '' b < <(cat <<'EOF'
    [
	{
	  "name": "Cynthia",
	  "surname": "Craig"
	},
	{
	  "name": "Kris",
	  "surname": "Norris"
	}
    ]
EOF
		       )
    local tmp1=$(mktemp /tmp/a.XXXXXX)
    echo "$a" > "$tmp1"
    local tmp2=$(mktemp /tmp/b.XXXXXX)
    echo "$b" > "$tmp2"
    mapfile -t c < <(bcu__json_diff "$tmp1" "$tmp2" surname name | jq -Mr .[].name | sort)
    rm "$tmp1"; rm "$tmp2"
    declare -p c
}
json_diff_test_1()(
    . <(json_diff_test_f)
    [[ "${c[0]}" == Elise ]] || { echo FAIL && return 1; }
    [[ "${c[1]}" == Hyde ]] || { echo FAIL && return 1; }
    [[ "${c[2]}" == Matthews ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
json_diff_test_main(){
    printf json_diff_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )json_diff_test_1(\ |$) ]]; then json_diff_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	json_diff_test_main "${org_args[@]}"
    else
	json_diff_test_main "${@}"
    fi
fi
# json_diff-test-main ends here
