#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::org-publish][org-publish]]
main(){
org_html_themes_dir=$(readlink -f "${BASH_SOURCE[0]%/*}/../../submodules/org-html-themes")
theme="theme-bigblow-local" # theme-readtheorg-local, etc.
git_dir="$(readlink -f "${1:-test}")"
docs_dir="$(readlink -f "${git_dir}"/docs)"
declare -a Exclude_Dirs=("${docs_dir}" "${git_dir}/submodules")
declare -a Extensions=(odt pdf docx html png jpg md mu)
export UPDATE_SRC=YES # orgmk setting
Converters=('org2html') #'org2gfm' 'org2odt')
for conv in "${Converters[@]}"; do command -v "$conv" || { echo "$conv not found in PATH, cant continue. Please install orgmk and make sure it's in your PATH." && return 1 ; } ; done
org_html_themes_dir_rel_git_dir=$(realpath --relative-to="$git_dir" "$org_html_themes_dir")
#org_html_themes_dir_rel_git_dir="${org_html_themes_dir##$git_dir}"
#[[ -n "${2}" ]] && org_html_themes_dir_rel_git_dir="$2"
echo git_dir: "$git_dir"
echo org_html_themes_dir: "$org_html_themes_dir"
echo org_html_themes_dir_rel_git_dir: "$org_html_themes_dir_rel_git_dir"
#return 0
declare -a Not_Paths=()
for dir in "${Exclude_Dirs[@]}"; do
    Not_Paths+=('-not' '-path' "${dir}/*"); done
mapfile -t -d '' Files < <(find "$git_dir" "${Not_Paths[@]}" -not -path "*${BASH_SOURCE[0]%/*}/*" -not -path "*${org_html_themes_dir}/*" -type f -xtype f -regex '.*\.org' -print0)
mapfile -t -d '' Dirs < <(for f in "${Files[@]}" ; do printf '%s\u0000' "${f%/*}" ; done | sort -z -u)
printf '%s\n' "${Files[@]}" "${Dirs[@]}"
for dir in "${Dirs[@]}"; do end="${dir#${git_dir}}"; new="${docs_dir}${end}" && mkdir -p "${new:-${docs_dir}}" && { [[ ! -e "${new}/org-html-themes" ]] && ln -s "${org_html_themes_dir}" "${new}/org-html-themes" ; } ; done
for dir in "${Dirs[@]}"; do
    [[ -e "${dir}/org-html-themes" ]] \
	&& [[ ! "$(readlink -f "${dir}/org-html-themes")" = "$(readlink -f ${org_html_themes_dir})" ]] \
	&& printf '%s\n' "The file ${dir}/org-html-themes is incorrectly symlinked to $(readlink -f "${dir}/org-html-themes") when it should be symlinked to ${org_html_themes_dir}. Please move it in order to finish the export." \
	&& return 1
    #   if [[ ! -e "${dir}/org-html-themes" ]]; then
    if [[ ! "$(readlink -f "${dir}/org-html-themes")" = "${org_html_themes_dir}" ]]; then
	echo now running ln -sf "${org_html_themes_dir}" "${dir}/org-html-themes" >&2	
	ln -sf $(readlink -f "${org_html_themes_dir}") "${dir}/org-html-themes"; fi; done
# [[ "$(readlink -f "${org_html_themes_dir}")" = "${dir}/org-html-themes" ]] || ln -sf "$(readlink -f "${org_html_themes_dir}")" "${dir}/org-html-themes"; done
#Files=("test.org")
Options=()
Options+=('#+OPTIONS: ^:{}')
Options+=('#+OPTIONS: H:15 num:15 toc:15')
Options+=('#+HTML_HEAD: <style> #content{max-width:1800px;}</style>')
Options+=('#+HTML_HEAD: <style> p{max-width:800px;}</style>')
Options+=('#+HTML_HEAD: <style> li{max-width:800px;}</style>')
Options+=('#+HTML_HEAD: <style> .p-img{max-width:768px; text-align: left; margin-left:0px; display:block;}</style>')
Options+=('#+HTML_HEAD: <style>.outline-text-2, .outline-text-3, .outline-text-4, .outline-text-5, .outline-text-6, .outline-text-7, .outline-text-8, .outline-text-9, .outline-text-10, .outline-3 > ul, .outline-4 > ol, #text-footnotes { margin-left: 100px; }</style>')
Options+=('#+HTML_HEAD: <style> img{display:inline-block ; max-width:100% ; height:auto ;}</style>')
Options+=('#+HTML_HEAD: <style> table[class="left-table"]{margin-left:0px;}</style>')
Options+=('#+HTML_HEAD: <style> table[class="right-table"]{margin-right:0px;}</style>')
Options+=('#+HTML_HEAD: <style> #postamble{text-align:center ;}</style>')
Options+=('#+HTML_HEAD: <style> pre.src{overflow-x: auto ;}</style>')
Options+=('#+HTML_HEAD: <script> var HS_STARTUP_FOLDED = true; </script>')
Options+=("#+SETUPFILE: org-html-themes/setup/${theme}.setup")
#sedEsc(){ echo "$1" | sed -e 's/[]\/$*.^[]/\\&/g' ; }
ereEsc(){ printf '%s' "$1" | sed 's/[.[\*^$()+?{|]/\\&/g';}
breEsc(){ printf '%s' "$string" | sed 's/[.[\*^$]/\\&/g';}
sed_esc_key(){ printf '%s' "$1" | sed -e 's/\([\.\^\$\*\[\]\|]\)/\\&/g' ; }
sed_esc_key2(){ printf '%s' "$1" | sed -e 's/\([[\/.*]\|\]\)/\\&/g';}
sed_esc_rep(){ printf '%s' "$1" | sed -e 's/[\/&]/\\&/g' ;}
requote(){ local char ; printf '%s' "$1" | while IFS= read -r -d '' -n1 char; do printf '[%s]' "$char"; done ; }
for f in "${Files[@]}"; do for o in "${Options[@]}"; do reg=$(ereEsc "$o"); if ! grep -E "^${reg}" "$f"; then sed -i "1s/^/$(sed_esc_rep "$o")\n/" "$f"; fi; done; done
massconvert()
{
    # mapfile -t Files < <(find "$1" -maxdepth 1 -type f -regex '.*\.org$');
    for converter in "${Converters[@]}";
    do
	echo Converting with $converter;
	for f in "${Files[@]}";
	do
	    printf '%s\n' "Converting $f with $converter" >&2 
	    eval "$converter $f" &
	done;
	wait;
    done;
    echo Done with org-convert;
    echo Converting to docx;
    { grep -q docx <<<"${Extensions[@]}" && command -v soffice >/dev/null ; } && find "$1" -type f -regex '.*\.odt$' -exec soffice --headless --convert-to docx {} \;
}
massconvert "${git_dir}"
for file in "${Files[@]}"; do
    for ext in "${Extensions[@]}" ; do
	file_dir="${file%/*}" ;
	new="${docs_dir}${file_dir#${git_dir}}/" && mv "${file%org}${ext}" "$new" ; done ; done

for file in "${Files[@]}"; do
    for ext in "${Extensions[@]}" ; do
	file_dir="${file%/*}" ;
	new="${docs_dir}${file_dir#${git_dir}}/" && find "${file_dir}" -maxdepth 1 -type f -iname "*.${ext}" -exec cp {} "$new" \; ; done ; done
printf '%s\n' "Moving latex-stuff.."
for file in "${Files[@]}"; do
    file_dir="${file%/*}"
    new="${docs_dir}${file_dir#${git_dir}}"
    [[ -e "${file_dir}/ltximg" ]] && rm -rf "${new}/ltximg" && mv "${file_dir}/ltximg" "$new"/
    [[ -e "${file_dir}/ltxpng" ]] && rm -rf "${new}/ltxpng" && mv "${file_dir}/ltxpng" "$new"/
    find "${file_dir}" -maxdepth 1 -type f -regex '/.*/latex[^/]*' -exec mv {} "$new"/ \;
done
printf '%s\n' "Finished moving latex-stuff.."
mapfile -t -d '' Paths < <(find docs -regex '.*/org-html-themes' -type l -print0)
org_html_themes_dir_rel_git_dir="/${org_html_themes_dir_rel_git_dir}"
# TODO: this stuff is cluttered, should probably use realpath
# --relative-to=x stuff multiple times here instead.
for p in "${Paths[@]}"; do
    cd "${p%/*}"
    echo "Changing org-html-themes symlink in path: " "${p%/*}"

    # replace with .. for the number of subdirs
    read -r p_rel < <(printf '%s' "/${p}" | perl -pe 's/(?<=\/).*?(?=\/)/\.\./g')
    p_rel="${p_rel#/}"
    #ls
    #echo "$p_rel"
    #echo "${p_rel//org-html-themes/submodules\/org-html-themes}"
    [[ -e org-html-themes ]] && rm org-html-themes
    #ln -sf "${p_rel//org-html-themes/submodules\/org-html-themes}" org-html-themes
    echo ln -sf "${p_rel//\/org-html-themes/$org_html_themes_dir_rel_git_dir}" org-html-themes
    ln -sf "${p_rel//\/org-html-themes/$org_html_themes_dir_rel_git_dir}" org-html-themes
    cd - ; done
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	main "${org_args[@]}"
    else
	main "${@}"
    fi
fi
# org-publish ends here
