#!/usr/bin/guile \
-e main -s
!#
;; #!/usr/bin/guile \
(add-to-load-path "src/utils")
(add-to-load-path (dirname (current-filename)))
(use-modules (sxml simple)
             (sxml ssax)
             (sxml xpath) ; sxpath
	     (csv csv)
             (srfi srfi-1)
             (ice-9 match)
             (ice-9 rdelim))

(define file (cadr (program-arguments)))
;;(display file)
;;(display "\n")

(define XMLCSVFile (call-with-input-file file csv->xml))
;;(display XMLCSVFile)
;;(display "\nafterxmlcsvfile\n")

(define XMLFile (string-append "<file>" XMLCSVFile "</file>"))
(display XMLFile)
;;(display "\nafterxmlfile\n")

(define SXMLFile (xml->sxml XMLFile))
;; (display SXMLFile)
;; (display "\naftersxmlfile\n")

(define Destination ((sxpath '(//(file_xml) (*) (apa) *any*)) SXMLFile))    
;; (display Destination)
;; (display "\nafterdestination\n")
;; (display (sxml->string SXMLFile))
;; (display "\naftersxmltostring xmlfile\n")
