#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::csv.sh][csv.sh]]
[[ "$_BCU_SH_LOADED" == YES ]] || . bcu.sh
_csv_MOD_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")
bcu__csv_to_xml(){
    local _STACK z Options=() Input=() file
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Convert a csv file to xml.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    #bcu__csv_to_xml <(printf '%s\n' apa,bepa,cepa val1,val2,val3 val1,val2,val3) | xmllint --format -
    tmpfile=$(mktemp /tmp/bcu__csv_to_xml_test.XXXXXX)
    printf '%s\n' apa,bepa,cepa val1,val2,val3 val1,val2,val3 > "$tmpfile"
    bcu__csv_to_xml "$tmpfile"
    rm "$tmpfile"

Results:

    

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:file)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: $_STACK" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    # Check args
    [[ -e "$file" ]] || { printf '%s' "File $file does not exist: " && bcu__stack "$@" && return 1; }
    [[ -r "$file" ]] || { printf '%s' "File $file is not readable: " && bcu__stack "$@" && return 1; }

    if ! command -v scm &>/dev/null; then
        enable -f "$GUIX_PROFILE"/lib/bash/libguile-bash.so scm &>/dev/null || { printf '%s' "Enabling guile-bash failed: " && bcu__stack "$@" && return 1; }; fi
        #enable -f "$GUIX_PROFILE"/lib/bash/libguile-bash.so scm || { printf '%s' "Enabling guile-bash failed: " && bcu__stack "$@" && return 1; }; fi
    command -v scm-csv_to_xml &>/dev/null \
        || { export GUILE_AUTO_COMPILE=0 ; scm "${_csv_MOD_DIR}"/scm-csv_to_xml.scm ; }
    scm-csv_to_xml "$file"
    #guile "$_csv_MOD_DIR"/csv2xml.scm "${file}"
}
csv_private(){
    :

}
csv_main(){
    :
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	csv_main "${org_args[@]}"
    else
	csv_main "${@}"
    fi
fi
# csv.sh ends here
