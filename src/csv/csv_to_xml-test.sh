#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::csv_to_xml-test-main][csv_to_xml-test-main]]
org_args='()'
. bcu.sh
bcu__csv_to_xml(){
    local _STACK z Options=() Input=() file
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Convert a csv file to xml.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    #bcu__csv_to_xml <(printf '%s\n' apa,bepa,cepa val1,val2,val3 val1,val2,val3) | xmllint --format -
    tmpfile=$(mktemp /tmp/bcu__csv_to_xml_test.XXXXXX)
    printf '%s\n' apa,bepa,cepa val1,val2,val3 val1,val2,val3 > "$tmpfile"
    bcu__csv_to_xml "$tmpfile"
    rm "$tmpfile"

Results:

    

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:file)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: $_STACK" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    # Check args
    [[ -e "$file" ]] || { printf '%s' "File $file does not exist: " && bcu__stack "$@" && return 1; }
    [[ -r "$file" ]] || { printf '%s' "File $file is not readable: " && bcu__stack "$@" && return 1; }

    if ! command -v scm &>/dev/null; then
	enable -f "$GUIX_PROFILE"/lib/bash/libguile-bash.so scm &>/dev/null || { printf '%s' "Enabling guile-bash failed: " && bcu__stack "$@" && return 1; }; fi
    #enable -f "$GUIX_PROFILE"/lib/bash/libguile-bash.so scm || { printf '%s' "Enabling guile-bash failed: " && bcu__stack "$@" && return 1; }; fi
    #command -v scm-csv_to_xml &>/dev/null
    command -v scm-csv_to_xml \
        || scm "${_csv_MOD_DIR}"/scm-csv_to_xml.scm &>/dev/null
    scm-csv_to_xml "$file" 2>/dev/null
    #guile "$_csv_MOD_DIR"/csv2xml.scm "${file}"
}
csv_to_xml_test_f(){
    local a
    local tmpfile
    tmpfile=$(mktemp /tmp/bcu__csv_to_xml_test.XXXXXX) 
    printf '%s\n' apa,bepa,cepa "val1,'val2 and val2',val3" 'val1,"val2, and val2",val3' > "$tmpfile"
    a=$(bcu__csv_to_xml "$tmpfile" | xmllint --format -)
    declare -p a
}
csv_to_xml_test_1()(
    . <(csv_to_xml_test_f)
    local a b
    b=$(
	cat <<EOF
<?xml version="1.0"?>
<file>
  <record-1>
    <apa>val1</apa>
    <bepa>'val2 and val2'</bepa>
    <cepa>val3</cepa>
  </record-1>
  <record-2>
    <apa>val1</apa>
    <bepa>val2, and val2</bepa>
    <cepa>val3</cepa>
  </record-2>
</file>
EOF
     )
    [[ "${a}" == "$b" ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
csv_to_xml_test_main(){
    printf csv_to_xml_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )csv_to_xml_test_1(\ |$) ]]; then csv_to_xml_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	csv_to_xml_test_main "${org_args[@]}"
    else
	csv_to_xml_test_main "${@}"
    fi
fi
# csv_to_xml-test-main ends here
