#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::is_ip_subset_py-test-main][is_ip_subset_py-test-main]]
org_args='()'
. bcu.sh
bcu__is_ip_subset_py(){
    # shellcheck disable=SC2034
    local z Options=() Input=() IPs1 IPs2 pystring
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Check if a list of ip addresses is a subset of another. Allowed
address formats for elements in the lists are IPv4: 1.2.3.4-5.6.7.8,
1.2.3.4, 1.2.3.4/32, 1.2.3.4/24.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    set1=('192.168.1.1-192.168.1.254' '10.0.0.1/31' '1.1.1.1/25' '1.2.3.4')
    set2=('192.168.1.1-192.168.1.254' '1.1.1.1/26' '1.2.3.4')
    bcu__is_ip_subset "(${set2[*]@Q})" "(${set1[*]@Q})" && echo true
  
Results:

    true
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:IPs1 1:t:IPs2)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    local -a S1="$IPs1" || { printf '%s' "IPs1 not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }

    local -a S2="$IPs2" || { printf '%s' "IPs2 not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }

    mapfile -t S1 < <(bcu__ip_types_to_cidrs "(${S1[*]@Q})") || { printf '%s' "IPs1 not valid ip elements: ${_STACK}" && bcu__stack "$@" && return 1; }

    mapfile -t S2 < <(bcu__ip_types_to_cidrs "(${S2[*]@Q})") || { printf '%s' "IPs1 not valid ip elements: ${_STACK}" && bcu__stack "$@" && return 1; }
    local a b Set1 Set2 result
    a=$(a=$(printf '%s, ' ${S1[*]@Q}); printf '%s' "${a%, }")
    b=$(b=$(printf '%s, ' ${S2[*]@Q}); printf '%s' "${b%, }")
    Set1=$(printf '([%s])' "$a")
    Set2=$(printf '([%s])' "$b")
    #declare -p Set1
    #declare -p Set2
    pystring=$(cat <<EOF
import netaddr
from netaddr import IPSet
s1 = IPSet$Set1
#s1 = IPSet(['192.0.2.0/24', '192.0.4.0/24'])
s2 = IPSet$Set2
#s2 = IPSet(['192.0.2.0', '192.0.4.0'])
res = s1.issubset(s2)
print(res)
EOF
	    )
    result=$(pypipe "$_BCU_PYDAEMON_CHANNEL" "$pystring")
    case "$result" in
	True) return 0;;
	False) return 1;;
	*) return 1;;
    esac
}
is_ip_subset_py_test_f(){
    local set1 set2 a b
    set1=('192.168.1.1-192.168.1.254' '10.0.0.1/31' '1.1.1.1/25' '1.2.3.4')
    set2=('192.168.1.1-192.168.1.254' '1.1.1.1/26' '1.2.3.4')
    a=$(if bcu__is_ip_subset_py "(${set2[*]@Q})" "(${set1[*]@Q})"; then echo true; else echo false; fi)
    b=$(if bcu__is_ip_subset_py "(${set1[*]@Q})" "(${set2[*]@Q})"; then echo true; else echo false; fi)
    declare -p a b
}
is_ip_subset_py_test_1()(
    . <(is_ip_subset_py_test_f)
    [[ "${a}" == true ]] || { echo FAIL && return 1; }
    [[ "${b}" == false ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
is_ip_subset_py_test_main(){
    printf is_ip_subset_py_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )is_ip_subset_py_test_1(\ |$) ]]; then is_ip_subset_py_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	is_ip_subset_py_test_main "${org_args[@]}"
    else
	is_ip_subset_py_test_main "${@}"
    fi
fi
# is_ip_subset_py-test-main ends here
