#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::is_valid_ip_no_cidr-test-main][is_valid_ip_no_cidr-test-main]]
org_args='()'
. bcu.sh
bcu__is_valid_ip_no_cidr(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() ip_address
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Returns true if the given ip is a valid ip address in dot-decimal
format and not in cidr-notation.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__is_valid_ip_no_cidr 1.2.3.4/32 || echo not valid
    bcu__is_valid_ip_no_cidr 1.2.3.4 && echo valid
  
Results:

    not valid
    valid
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:ip_address)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: $_STACK" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    if [[ "${ip_address}" =~ ^([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})$ ]]; then
	for i in {1..4}; do
	    [[ ! "${BASH_REMATCH[$i]}" -le 255 ]] && return 1; done
	return 0; fi
    return 1
}
is_valid_ip_no_cidr_test_f(){
    local a
    a=$(if bcu__is_valid_ip_no_cidr 1.2.3.4 ; then echo true ; else echo false ; fi )
    b=$(if bcu__is_valid_ip_no_cidr 1.2.3.4/32 ; then echo true ; else echo false ; fi )
    declare -p a b
}
is_valid_ip_no_cidr_test_1()(
    . <(is_valid_ip_no_cidr_test_f)
    [[ "${a}" == true ]] || { echo FAIL && return 1; }
    [[ "${b}" == false ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
is_valid_ip_no_cidr_test_main(){
    printf is_valid_ip_no_cidr_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )is_valid_ip_no_cidr_test_1(\ |$) ]]; then is_valid_ip_no_cidr_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	is_valid_ip_no_cidr_test_main "${org_args[@]}"
    else
	is_valid_ip_no_cidr_test_main "${@}"
    fi
fi
# is_valid_ip_no_cidr-test-main ends here
