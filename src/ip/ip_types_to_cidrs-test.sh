#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::ip_types_to_cidrs-test-main][ip_types_to_cidrs-test-main]]
org_args='()'
. bcu.sh
bcu__ip_types_to_cidrs(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() IPs
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Takes an array-declarable string with elements being either an
individual ip (potentially in cidr-notation), a subnet in
cidr-notation, an ip address range in the format a.b.c.d-e.f.g.h, or a
valid fqdn string and returns an array-declarable string with the
corresponding cidr subnets covering all ip addresses. FQDN strings
will be looked up in DNS.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    CIDRset=('192.168.1.1-192.168.1.254' '10.0.0.1/31' '1.1.1.1/25' '1.2.3.4')
    bcu__ip_types_to_cidrs "(${CIDRset[*]@Q})"
  
Results:

    192.168.1.1/32
    192.168.1.2/31
    192.168.1.4/30
    192.168.1.8/29
    192.168.1.16/28
    192.168.1.32/27
    192.168.1.64/26
    192.168.1.128/26
    192.168.1.192/27
    192.168.1.224/28
    192.168.1.240/29
    192.168.1.248/30
    192.168.1.252/31
    192.168.1.254/32
    10.0.0.1/31
    1.1.1.1/25
    1.2.3.4/32
    
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:IPs)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    local -a IPs="${IPs}"
    local -a CIDRs=()
    local -i ii
    for ((ii=0;ii<${#IPs[@]};ii++)); do
	if bcu__is_valid_ip_no_cidr "${IPs[$ii]}"; then
	    CIDRs+=("${IPs[$ii]}/32")
	elif bcu__is_valid_subnet "${IPs[$ii]}" || bcu__is_valid_ip "${IPs[$ii]}"; then
	    CIDRs+=("${IPs[$ii]}")
	elif bcu__is_valid_ip_range "${IPs[$ii]}"; then
	    #Hostranges+=("${Names[$ii]}")
	    # command substitution inside array below is intentionally left unquoted.
	    CIDRs+=( $(bcu__ip_range_to_cidrs "${IPs[$ii]%-*}" "${IPs[$ii]#*-}") )
	elif bcu__is_valid_fqdn "${IPs[$ii]}"; then
	    # "has ipv6 address" is printed instead of "has address" for ipv6 addresses.
	    read -r ip < <(host -W 1 "${IPs[$ii]}" | pcregrep -o '(?<=(has address )).*$')
	    if bcu__is_valid_ip "${ip}"; then
		CIDRs+=("${ip}/32")
	    else
		printf '%s' "Element  $ii - ${IPs[$ii]} - is not a valid ip type or its fqdn could not be resolved to a valid ip version 4 address: ${_STACK}" && bcu__stack "$@"
		return 1; fi
	else
	    printf '%s' "Element  $ii - ${IPs[$ii]} - is not a valid ip type: ${_STACK}" && bcu__stack "$@" && return 1; fi ; done
    #IPs+=("${Values[$ii]}"); fi; done
    printf '%s\n' "${CIDRs[@]}"
}
ip_types_to_cidrs_test_f(){
    local -a a CIDRset
    CIDRset=('192.168.1.1-192.168.1.254' '10.0.0.1/31' '1.1.1.1/25' '1.2.3.4')
    mapfile -t a < <(bcu__ip_types_to_cidrs "(${CIDRset[*]@Q})")
    declare -p a
}
ip_types_to_cidrs_test_1()(
    . <(ip_types_to_cidrs_test_f)
    local result
    result=$(
	cat <<'EOF'
192.168.1.1/32
192.168.1.2/31
192.168.1.4/30
192.168.1.8/29
192.168.1.16/28
192.168.1.32/27
192.168.1.64/26
192.168.1.128/26
192.168.1.192/27
192.168.1.224/28
192.168.1.240/29
192.168.1.248/30
192.168.1.252/31
192.168.1.254/32
10.0.0.1/31
1.1.1.1/25
1.2.3.4/32
EOF
	  )
    diff -s <(printf '%s\n' "${a[@]}") <(echo "$result") >/dev/null 2>/dev/null || { echo FAIL && return 1; }
    echo PASS && return 0
)
ip_types_to_cidrs_test_main(){
    printf ip_types_to_cidrs_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )ip_types_to_cidrs_test_1(\ |$) ]]; then ip_types_to_cidrs_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	ip_types_to_cidrs_test_main "${org_args[@]}"
    else
	ip_types_to_cidrs_test_main "${@}"
    fi
fi
# ip_types_to_cidrs-test-main ends here
