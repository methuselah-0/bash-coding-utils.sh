#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::ip_to_hex-test-main][ip_to_hex-test-main]]
org_args='()'
. bcu.sh
bcu__ip_to_hex(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() ip
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Convert a dot-decimal ip address string to a hex string.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__ip_to_hex 1.1.1.1
  
Results:

    1010101
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:ip)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    bcu__is_valid_ip "$ip" || { printf '%s' "Not a valid ip address: ${_STACK}" && bcu__stack "$@" && return 1; }

    # Do not quote ${ip//./ } below.
    # shellcheck disable=SC2086
    printf '%02X' ${ip//./ } | tr '[:upper:]' '[:lower:]'
    echo
}
ip_to_hex_test_f(){
    local a
    a=$(bcu__ip_to_hex 1.1.1.1)
    declare -p a
}
ip_to_hex_test_1()(
    . <(ip_to_hex_test_f)
    [[ "${a}" == 01010101 ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
ip_to_hex_test_main(){
    printf ip_to_hex_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )ip_to_hex_test_1(\ |$) ]]; then ip_to_hex_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	ip_to_hex_test_main "${org_args[@]}"
    else
	ip_to_hex_test_main "${@}"
    fi
fi
# ip_to_hex-test-main ends here
