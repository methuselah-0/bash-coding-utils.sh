#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::has_ip_intersection_py-test-main][has_ip_intersection_py-test-main]]
org_args='()'
. bcu.sh
bcu__has_ip_intersection_py(){
    # shellcheck disable=SC2034
    local z Options=() Input=() IPs1 IPs2 a b result Set1 Set2 pystring
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Check if a list of ip addresses intersects one-another. Allowed
address formats for elements in the lists are IPv4: 1.2.3.4-5.6.7.8,
1.2.3.4, 1.2.3.4/32, 1.2.3.4/24.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    apa=(1.1.1.1/28 1.1.1.10)
    bepa=(1.1.2.1/28 1.1.1.14)
    bcu__has_ip_intersection_py "(${apa[*]@Q})" "(${bepa[*]@Q})" && echo true
  
Results:

    true
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:IPs1 1:t:IPs2)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    local -a S1="${IPs1}" || { printf '%s' "IPs1 not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }

    local -a S2="${IPs2}" || { printf '%s' "IPs1 not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }

    mapfile -t S1 < <(bcu__ip_types_to_cidrs "(${S1[*]@Q})") || { printf '%s' "IPs1 has incorrectly formatted ip address elements: ${_STACK}" && bcu__stack "$@" && return 1; }
    mapfile -t S2 < <(bcu__ip_types_to_cidrs "(${S2[*]@Q})") || { printf '%s' "IPs1 has incorrectly formatted ip address elements: ${_STACK}" && bcu__stack "$@" && return 1; }

    a=$(a=$(printf '%s, ' ${S1[*]@Q}); printf '%s' "${a%, }")
    b=$(b=$(printf '%s, ' ${S2[*]@Q}); printf '%s' "${b%, }")
    Set1=$(printf '([%s])' "$a")
    Set2=$(printf '([%s])' "$b")
    #declare -p Set1
    #declare -p Set2
    local result
    pystring=$(cat <<EOF
import netaddr
from netaddr import IPSet
s1 = IPSet$Set1
#s1 = IPSet(['192.0.2.0/24', '192.0.4.0/24'])
s2 = IPSet$Set2
#s2 = IPSet(['192.0.2.0', '192.0.4.0'])
res = s1.intersection(s2)
print(res)
EOF
	    )
    result=$(pypipe "$_BCU_PYDAEMON_CHANNEL" "$pystring")
    #printf '%s\n' "$result"
    case "$result" in
	'IPSet([])') return 1;;
	*) return 0;;
    esac
}
has_ip_intersection_py_test_f(){
    local a b apa bepa
    apa=(1.1.1.1/28 1.1.1.10)
    bepa=(1.1.2.1/28 1.1.1.14)
    cepa=(10.1.1.1)
    #bcu__has_ip_intersection "(${apa[*]@Q})" "(${bepa[*]@Q})"
    a=$(bcu__has_ip_intersection_py "(${apa[*]@Q})" "(${bepa[*]@Q})" && echo true)
    b=$(bcu__has_ip_intersection_py "(${cepa[*]@Q})" "(${bepa[*]@Q})" || echo false)
    declare -p a b
}
has_ip_intersection_py_test_1()(
    . <(has_ip_intersection_py_test_f)
    [[ "${a}" == true ]] || { echo FAIL && return 1; }
    [[ "${b}" == false ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
has_ip_intersection_py_test_main(){
    printf has_ip_intersection_py_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )has_ip_intersection_py_test_1(\ |$) ]]; then has_ip_intersection_py_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	has_ip_intersection_py_test_main "${org_args[@]}"
    else
	has_ip_intersection_py_test_main "${@}"
    fi
fi
# has_ip_intersection_py-test-main ends here
