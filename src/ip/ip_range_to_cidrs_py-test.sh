#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::ip_range_to_cidrs_py-test-main][ip_range_to_cidrs_py-test-main]]
org_args='()'
. bcu.sh
bcu__ip_range_to_cidrs_py(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() range_start range_end pystring
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Takes an array-declarable string with elements being either an
individual ip (potentially in cidr-notation), a subnet in
cidr-notation or an ip address range in the format a.b.c.d-e.f.g.h,
and returns an array-declarable string with the corresponding cidr
subnets covering all ip addresses.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__ip_range_to_cidrs_py 10.0.0.128 10.0.0.254
  
Results:

    10.0.0.128/26
    10.0.0.192/27
    10.0.0.224/28
    10.0.0.240/29
    10.0.0.248/30
    10.0.0.252/31
    10.0.0.254/32
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:range_start 1:t:range_end)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    pystring=$(cat <<EOF
import netaddr
for net in (netaddr.iprange_to_cidrs("$range_start", "$range_end")):
    print(net)
EOF
	    )
    pypipe "$_BCU_PYDAEMON_CHANNEL" "$pystring"
}
ip_range_to_cidrs_py_test_f(){
    local -a a
    mapfile -t a < <(bcu__ip_range_to_cidrs_py 10.0.0.128 10.0.0.254)
    declare -p a
}
ip_range_to_cidrs_py_test_1()(
    . <(ip_range_to_cidrs_py_test_f)
    [[ "${a[0]}" == 10.0.0.128/26 ]] || { echo FAIL && return 1; }
    [[ "${a[1]}" == 10.0.0.192/27 ]] || { echo FAIL && return 1; }
    [[ "${a[2]}" == 10.0.0.224/28 ]] || { echo FAIL && return 1; }
    [[ "${a[3]}" == 10.0.0.240/29 ]] || { echo FAIL && return 1; }
    [[ "${a[4]}" == 10.0.0.248/30 ]] || { echo FAIL && return 1; }
    [[ "${a[5]}" == 10.0.0.252/31 ]] || { echo FAIL && return 1; }
    [[ "${a[6]}" == 10.0.0.254/32 ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
ip_range_to_cidrs_py_test_main(){
    printf ip_range_to_cidrs_py_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )ip_range_to_cidrs_py_test_1(\ |$) ]]; then ip_range_to_cidrs_py_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	ip_range_to_cidrs_py_test_main "${org_args[@]}"
    else
	ip_range_to_cidrs_py_test_main "${@}"
    fi
fi
# ip_range_to_cidrs_py-test-main ends here
