#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::get_subnet_ips-test-main][get_subnet_ips-test-main]]
org_args='()'
. bcu.sh
bcu__get_subnet_ips(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() cidr
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Print ip address for a given CIDR subnet range. Correct subnet base of
given CIDR address range is not validated.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__get_subnet_ips 10.1.1.0/31
  
Results:

    10.1.1.0
    10.1.1.1
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:cidr)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    { bcu__is_valid_subnet "$cidr" \
	  || bcu__is_valid_ip "${cidr}" ; } \
	|| { printf '%s' "cidr is not a valid ip or cidr subnet: ${_STACK}" && bcu__stack "$@" && return 1; }

    nmap -sL -n "${cidr}" | grep "Nmap scan report" | awk '{print $NF}'
}
get_subnet_ips_test_f(){
    local -a a
    mapfile -t a < <(bcu__get_subnet_ips 10.1.1.0/31)
    declare -p a
}
get_subnet_ips_test_1()(
    . <(get_subnet_ips_test_f)
    [[ "${a[0]}" == 10.1.1.0 ]] || { echo FAIL && return 1; }
    [[ "${a[1]}" == 10.1.1.1 ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
get_subnet_ips_test_main(){
    printf get_subnet_ips_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )get_subnet_ips_test_1(\ |$) ]]; then get_subnet_ips_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	get_subnet_ips_test_main "${org_args[@]}"
    else
	get_subnet_ips_test_main "${@}"
    fi
fi
# get_subnet_ips-test-main ends here
