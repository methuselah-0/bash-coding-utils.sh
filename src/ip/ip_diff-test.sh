#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::ip_diff-test-main][ip_diff-test-main]]
org_args='()'
. bcu.sh
bcu__ip_diff(){
    local _STACK z Options=() Input=() IPs1 IPs2
    [[ "$1" =~ (-h|--help) ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Calculate the difference between two ip sets.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__ip_diff '(1.1.2.22-1.1.2.55 1.1.1.0/24 1.1.1.139)' '(1.1.1.0/25 1.1.2.28)'
    declare -a a='(1.1.2.22-1.1.2.55 1.1.1.0/24 1.1.1.139)'
    declare -a b='(1.1.1.0/25 1.1.2.28)'
    bcu__ip_diff -n -- a b

Results:

    ('1.1.1.128/25' '1.1.2.22/31' '1.1.2.24/30' '1.1.2.29/32' '1.1.2.30/31' '1.1.2.32/28' '1.1.2.48/29')
    ('1.1.1.128/25' '1.1.2.22/31' '1.1.2.24/30' '1.1.2.29/32' '1.1.2.30/31' '1.1.2.32/28' '1.1.2.48/29')

EOF
		)
    }
    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Nameref=(n nameref "" "Read the IP arrays as namereferences" 0)
    Options+=("(${Nameref[*]@Q})" "(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:IPs1 1:t:IPs2)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || {
	printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ (-h|--help) ]] && return 0

    # Check args
    if [[ -n "$nameref" ]]; then
	#for arrname in "${IPs1}" "${IPs2}"; do
	#local -a 'Arr=("${'"$Array"'[@]}")'
	local -n S1="${IPs1}"
	local -n S2="${IPs2}"
    else
	local -a S1="${IPs1}" || { printf '%s' "IPs1 not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }
	local -a S2="${IPs2}" || { printf '%s' "IPs1 not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }; fi

    mapfile -t S1 < <(bcu__ip_types_to_cidrs "(${S1[*]@Q})") || { printf '%s' "IPs1 has incorrectly formatted ip address elements: ${_STACK}" && bcu__stack "$@" && return 1; }
    mapfile -t S2 < <(bcu__ip_types_to_cidrs "(${S2[*]@Q})") || { printf '%s' "IPs1 has incorrectly formatted ip address elements: ${_STACK}" && bcu__stack "$@" && return 1; }

    a=$(a=$(printf '%s, ' ${S1[*]@Q}); printf '%s' "${a%, }")
    b=$(b=$(printf '%s, ' ${S2[*]@Q}); printf '%s' "${b%, }")
    Set1=$(printf '([%s])' "$a")
    Set2=$(printf '([%s])' "$b")
    #declare -p Set1
    #declare -p Set2
    local result
    pystring=$(cat <<EOF
import netaddr
from netaddr import IPSet
s1 = IPSet$Set1
#s1 = IPSet(['192.0.2.0/24', '192.0.4.0/24'])
s2 = IPSet$Set2
#s2 = IPSet(['192.0.2.0', '192.0.4.0'])
res = s1 - s2
print(res)
EOF
	    )
    result=$(pypipe "$_BCU_PYDAEMON_CHANNEL" "$pystring")
    result="${result#IPSet}"
    printf '%s\n' "(${result//[,()\[\]]})"
    # case "$result" in
    # 	'IPSet([])') return 1;;
    # 	*) return 0;;
    # esac
}
ip_diff_test_f(){
    local -a c=$(declare -a a='(1.1.2.22-1.1.2.55 1.1.1.0/24 1.1.1.139)' ; declare -a b='(1.1.1.0/25 1.1.2.28)'; bcu__ip_diff -n -- a b)
    local d=$(printf '%s\n' "${c[@]}")
    declare -p d
}
ip_diff_test_1()(
    . <(ip_diff_test_f)
    e=$(cat <<'EOF'
1.1.1.128/25
1.1.2.22/31
1.1.2.24/30
1.1.2.29/32
1.1.2.30/31
1.1.2.32/28
1.1.2.48/29
EOF
     )
    [[ "${d}" == "$e" ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
ip_diff_test_main(){
    printf ip_diff_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )ip_diff_test_1(\ |$) ]]; then ip_diff_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	ip_diff_test_main "${org_args[@]}"
    else
	ip_diff_test_main "${@}"
    fi
fi
# ip_diff-test-main ends here
