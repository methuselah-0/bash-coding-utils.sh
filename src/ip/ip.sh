#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::ip.sh][ip.sh]]
[[ "$_BCU_SH_LOADED" == YES ]] || . bcu.sh
_ip_MOD_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")
bcu__is_valid_hostname(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() host_name
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Validates hostnames conforming to RFC1123, not RFC952, except no dots
allowed, thus not allowing valid hostnames that contains a domain-name
ending.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__is_valid_hostname gnu && echo isvalid
    bcu__is_valid_hostname gnu.org || echo isinvalid
  
Results:

    isvalid
    isinvalid
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)    
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:host_name)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: $_STACK" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    { [[ "$host_name" =~ ^[a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9]+$ ]] && [[ $(fold -w1 <<<"$host_name" | wc -l) -le 63 ]] ; } && return 0
}
bcu__is_valid_ip(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() ip_address
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Returns true if the given ip is a valid single ip address and false otherwise.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    if bcu__is_valid_ip 1.2.3.4; then echo valid ; else echo not valid ; fi
    if bcu__is_valid_ip 1.2.3.4/32; then echo valid ; else echo not valid ; fi
    if bcu__is_valid_ip 1.2.3.456; then echo valid ; else echo not valid ; fi
  
Results:

    valid
    valid
    not valid
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)    
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:ip_address)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: $_STACK" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    if [[ "$ip_address" =~ ^([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})/32$ ]] || \
	   [[ "$ip_address" =~ ^([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})$ ]]; then
	local i
	for i in {1..4}; do
	    [[ ! "${BASH_REMATCH[$i]}" -le 255 ]] && return 1; done
	return 0; fi
    return 1
}
bcu__is_valid_ip_no_cidr(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() ip_address
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Returns true if the given ip is a valid ip address in dot-decimal
format and not in cidr-notation.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__is_valid_ip_no_cidr 1.2.3.4/32 || echo not valid
    bcu__is_valid_ip_no_cidr 1.2.3.4 && echo valid
  
Results:

    not valid
    valid
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:ip_address)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: $_STACK" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    if [[ "${ip_address}" =~ ^([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})$ ]]; then
	for i in {1..4}; do
	    [[ ! "${BASH_REMATCH[$i]}" -le 255 ]] && return 1; done
	return 0; fi
    return 1
}
bcu__is_valid_subnet(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() address
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Validates CIDR-format of given input, ignoring whether the input has a
correct subnet base.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__is_valid_subnet 1.2.3.4/24 && echo valid
    bcu__is_valid_subnet 1.2.3.4/33 || echo not valid
    bcu__is_valid_subnet 1.2.3.4 || echo not valid
  
Results:

    valid
    not valid
    not valid
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:address)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: $_STACK" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    if bcu__is_valid_ip "${address%/*}" && [[ "${address#*/}" -ge 0 ]] 2>/dev/null && [[ "${address#*/}" -le 31 ]] 2>/dev/null; then
	return 0
    else
	return 1; fi
}
bcu__is_valid_ip_range(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() ip_address_range
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
"a.b.c.d-e.f.g.h" -> Bool. Does not return error in case the second ip
in the range is a previous ip or "earlier" ip.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__is_valid_ip_range 1.2.3.4-5.6.7.8 && echo valid
    bcu__is_valid_ip_range 1.2.3.4-5.6.7.8.9 || echo not valid
    bcu__is_valid_ip_range 1.2.3.4-1.2.3.3 && echo valid
  
Results:

    valid
    not valid
    valid
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)    
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:ip_address_range)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: $_STACK" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    if [[ "${ip_address_range}" =~ ^([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})-([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})$ ]]; then
	local ip1="${BASH_REMATCH[1]}"
	local ip2="${BASH_REMATCH[2]}"
	if bcu__is_valid_ip "${ip1}" && bcu__is_valid_ip "${ip2}"; then
	    return 0;
	else
	    return 1; fi
    else
	return 1; fi
}
bcu__is_valid_fqdn(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Validate that a given input string is an fqdn.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__is_valid_fqdn apa.bepa.com && echo valid
    bcu__is_valid_fqdn apa || echo notvalid
    bcu__is_valid_fqdn -r apa && echo valid
  
Results:

    valid
    notvalid
    valid
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Relaxed=(r relaxed_tld "" "This option makes the tld part optional" 0 f f)
    Options+=("(${null[*]@Q})" "(${Relaxed[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: $_STACK" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    #host -N 1 "${Input[0]}" &>/dev/null

    local regex_with_tld='(?=^.{4,253}$)(^((?!-)[a-zA-Z0-9-]{1,63}(?<!-)\.)+[a-zA-Z]{2,63}$)'
    local regex_optional_tld='(?=^.{1,253}$)(^(((?!-)[a-zA-Z0-9-]{1,63}(?<!-))|((?!-)[a-zA-Z0-9-]{1,63}(?<!-)\.)+[a-zA-Z]{2,63})$)'
    local regex
    if [[ -n "$relaxed_tld" ]]; then
	local regex="$regex_optional_tld"
    else
	local regex="$regex_with_tld"; fi

    pcregrep "$regex" <<<"${string}" &>/dev/null && return 0
}
bcu__get_subnet_ips(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() cidr
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Print ip address for a given CIDR subnet range. Correct subnet base of
given CIDR address range is not validated.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__get_subnet_ips 10.1.1.0/31
  
Results:

    10.1.1.0
    10.1.1.1
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:cidr)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    { bcu__is_valid_subnet "$cidr" \
	  || bcu__is_valid_ip "${cidr}" ; } \
	|| { printf '%s' "cidr is not a valid ip or cidr subnet: ${_STACK}" && bcu__stack "$@" && return 1; }

    nmap -sL -n "${cidr}" | grep "Nmap scan report" | awk '{print $NF}'
}
bcu__ip_range_to_cidrs(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() range_start range_end
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Print the CIDR's of a given ip address range.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__ip_range_to_cidrs 10.0.0.0 10.0.0.1
  
Results:

    10.0.0.0/31
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:range_start 1:t:range_end)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    bcu__is_valid_ip "$range_start" || { printf '%s' "Start ip is not valid: ${_STACK}" && bcu__stack "$@" && return 1; }
    bcu__is_valid_ip "$range_end" || { printf '%s' "End ip is not valid: ${_STACK}" && bcu__stack "$@" && return 1; }

    printf '%s ' "${range_start}" "${range_end}" | gawk -f "${_ip_MOD_DIR}/ipcalcs.awk" -f <(echo "{print range2cidr(ip2dec(sanitize(\"${range_start}\")),ip2dec(sanitize(\"${range_end}\")))}")
}
bcu__ip_range_to_cidrs_py(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() range_start range_end pystring
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Takes an array-declarable string with elements being either an
individual ip (potentially in cidr-notation), a subnet in
cidr-notation or an ip address range in the format a.b.c.d-e.f.g.h,
and returns an array-declarable string with the corresponding cidr
subnets covering all ip addresses.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__ip_range_to_cidrs_py 10.0.0.128 10.0.0.254
  
Results:

    10.0.0.128/26
    10.0.0.192/27
    10.0.0.224/28
    10.0.0.240/29
    10.0.0.248/30
    10.0.0.252/31
    10.0.0.254/32
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:range_start 1:t:range_end)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    pystring=$(cat <<EOF
import netaddr
for net in (netaddr.iprange_to_cidrs("$range_start", "$range_end")):
    print(net)
EOF
	    )
    pypipe "$_BCU_PYDAEMON_CHANNEL" "$pystring"
}
bcu__ip_types_to_cidrs_py(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() IPs
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Takes an array-declarable string with elements being either an
individual ip (potentially in cidr-notation), a subnet in
cidr-notation, an ip address range in the format a.b.c.d-e.f.g.h, or a
valid fqdn string and returns an array-declarable string with the
corresponding cidr subnets covering all ip addresses. FQDN strings
will be looked up in DNS.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    CIDRset=('192.168.1.1-192.168.1.254' '10.0.0.1/31' '1.1.1.1/25' '1.2.3.4')
    bcu__ip_types_to_cidrs "(${CIDRset[*]@Q})"
  
Results:

    192.168.1.1/32
    192.168.1.2/31
    192.168.1.4/30
    192.168.1.8/29
    192.168.1.16/28
    192.168.1.32/27
    192.168.1.64/26
    192.168.1.128/26
    192.168.1.192/27
    192.168.1.224/28
    192.168.1.240/29
    192.168.1.248/30
    192.168.1.252/31
    192.168.1.254/32
    10.0.0.1/31
    1.1.1.1/25
    1.2.3.4/32
    
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:IPs)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    local -a IPs="${IPs}"
    local -a CIDRs=()
    local ip
    local -i ii
    for ((ii=0;ii<${#IPs[@]};ii++)); do
	if bcu__is_valid_ip_no_cidr "${IPs[$ii]}"; then
	    CIDRs+=("${IPs[$ii]}/32")
	elif bcu__is_valid_subnet "${IPs[$ii]}" || bcu__is_valid_ip "${IPs[$ii]}"; then
	    CIDRs+=("${IPs[$ii]}")
	elif bcu__is_valid_ip_range "${IPs[$ii]}"; then
	    #Hostranges+=("${Names[$ii]}")
	    # command substitution inside array below is intentionally left unquoted.
	    CIDRs+=( $(bcu__ip_range_to_cidrs_py "${IPs[$ii]%-*}" "${IPs[$ii]#*-}") )
	elif bcu__is_valid_fqdn "${IPs[$ii]}"; then
	    # "has ipv6 address" is printed instead of "has address" for ipv6 addresses.
	    read -r ip < <(host -W 1 "${IPs[$ii]}" | pcregrep -o '(?<=(has address )).*$')
	    if bcu__is_valid_ip "${ip}"; then
		CIDRs+=("${ip}/32");
	    else
		printf '%s' "Element  $ii - ${IPs[$ii]} - is not a valid ip type or its fqdn could not be resolved to a valid ip version 4 address: ${_STACK}" && bcu__stack "$@"
		return 1; fi
	else
	    printf '%s' "Element  $ii - ${IPs[$ii]} - is not a valid ip type or fqdn: ${_STACK}" && bcu__stack "$@" && return 1; fi ; done
    #IPs+=("${Values[$ii]}"); fi; done
    printf '%s\n' "${CIDRs[@]}"
}
bcu__ip_types_to_cidrs(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() IPs
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Takes an array-declarable string with elements being either an
individual ip (potentially in cidr-notation), a subnet in
cidr-notation, an ip address range in the format a.b.c.d-e.f.g.h, or a
valid fqdn string and returns an array-declarable string with the
corresponding cidr subnets covering all ip addresses. FQDN strings
will be looked up in DNS.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    CIDRset=('192.168.1.1-192.168.1.254' '10.0.0.1/31' '1.1.1.1/25' '1.2.3.4')
    bcu__ip_types_to_cidrs "(${CIDRset[*]@Q})"
  
Results:

    192.168.1.1/32
    192.168.1.2/31
    192.168.1.4/30
    192.168.1.8/29
    192.168.1.16/28
    192.168.1.32/27
    192.168.1.64/26
    192.168.1.128/26
    192.168.1.192/27
    192.168.1.224/28
    192.168.1.240/29
    192.168.1.248/30
    192.168.1.252/31
    192.168.1.254/32
    10.0.0.1/31
    1.1.1.1/25
    1.2.3.4/32
    
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:IPs)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    local -a IPs="${IPs}"
    local -a CIDRs=()
    local -i ii
    for ((ii=0;ii<${#IPs[@]};ii++)); do
	if bcu__is_valid_ip_no_cidr "${IPs[$ii]}"; then
	    CIDRs+=("${IPs[$ii]}/32")
	elif bcu__is_valid_subnet "${IPs[$ii]}" || bcu__is_valid_ip "${IPs[$ii]}"; then
	    CIDRs+=("${IPs[$ii]}")
	elif bcu__is_valid_ip_range "${IPs[$ii]}"; then
	    #Hostranges+=("${Names[$ii]}")
	    # command substitution inside array below is intentionally left unquoted.
	    CIDRs+=( $(bcu__ip_range_to_cidrs "${IPs[$ii]%-*}" "${IPs[$ii]#*-}") )
	elif bcu__is_valid_fqdn "${IPs[$ii]}"; then
	    # "has ipv6 address" is printed instead of "has address" for ipv6 addresses.
	    read -r ip < <(host -W 1 "${IPs[$ii]}" | pcregrep -o '(?<=(has address )).*$')
	    if bcu__is_valid_ip "${ip}"; then
		CIDRs+=("${ip}/32")
	    else
		printf '%s' "Element  $ii - ${IPs[$ii]} - is not a valid ip type or its fqdn could not be resolved to a valid ip version 4 address: ${_STACK}" && bcu__stack "$@"
		return 1; fi
	else
	    printf '%s' "Element  $ii - ${IPs[$ii]} - is not a valid ip type: ${_STACK}" && bcu__stack "$@" && return 1; fi ; done
    #IPs+=("${Values[$ii]}"); fi; done
    printf '%s\n' "${CIDRs[@]}"
}
bcu__ip_to_hex(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() ip
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Convert a dot-decimal ip address string to a hex string.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__ip_to_hex 1.1.1.1
  
Results:

    1010101
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:ip)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    bcu__is_valid_ip "$ip" || { printf '%s' "Not a valid ip address: ${_STACK}" && bcu__stack "$@" && return 1; }

    # Do not quote ${ip//./ } below.
    # shellcheck disable=SC2086
    printf '%02X' ${ip//./ } | tr '[:upper:]' '[:lower:]'
    echo
}
bcu__hex_to_ip(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() ip
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Convert IPv4 address in hex format to dot-decimal format.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__hex_to_ip 01010101 && echo
  
Results:

    1.1.1.1
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:ip)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    printf '%s' "$ip" | fold -w 2 | { mapfile -t IP ; printf "%d." "0x${IP[0]}" "0x${IP[1]}" "0x${IP[2]}" "0x${IP[3]}" | sed 's/\.$//' ; }
}
bcu__sort_ip(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() IPs result
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Sort IPv4 efficiently using ~sort~ and as a filter (from stdin).


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    printf '%s\n' 1.1.1.1 3.3.3.3 2.2.2.2 | bcu__sort_ip
    bcu__sort_ip 1.1.1.1 3.3.3.3 2.2.2

Results:

    1.1.1.1
    2.2.2.2
    3.3.3.3
    IP - 2.2.2 - is not a valid ip and can't be sorted: 'bcu__sort_ip' defined in ip.sh invoked on line 141 in /home/user1/VirtualHome/src/bash-coding-utils/src/ip/ip.sh, with the args: '1.1.1.1' '3.3.3.3' '2.2.2'

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:IPs)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    for ip in "${IPs[@]}"; do
	bcu__is_valid_ip "$ip" || { printf '%s' "IP - $ip - is not a valid ip and can't be sorted: ${_STACK}" && bcu__stack "$@" && return 1; }; done

    result=$(printf '%s\n' "${IPs[@]}" | sort -t. -k1,1n -k2,2n -k3,3n -k4,4n) || { printf '%s' "Failed to sort given IPs: $result - ${_STACK}" && bcu__stack "$@" && return 1; }
    printf '%s\n' "$result"
}
bcu__is_ip_subset(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() IPs1 IPs2
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Check if a list of ip addresses is a subset of another. Allowed
address formats for elements in the lists are IPv4: 1.2.3.4-5.6.7.8,
1.2.3.4, 1.2.3.4/32, 1.2.3.4/24.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    set1=('192.168.1.1-192.168.1.254' '10.0.0.1/31' '1.1.1.1/25' '1.2.3.4')
    set2=('192.168.1.1-192.168.1.254' '1.1.1.1/26' '1.2.3.4')
    bcu__is_ip_subset "(${set2[*]@Q})" "(${set1[*]@Q})" && echo true
  
Results:

    true
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:IPs1 1:t:IPs2)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    # Check args
    local -a IPs1="$IPs1" || { printf '%s' "IPs1 not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }

    local -a IPs2="$IPs2" || { printf '%s' "IPs2 not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }

    # TODO: use ip_types_to_cidrs here just in case
    local -a S1 S2
    mapfile -t S1 < <(bcu__ip_types_to_cidrs "${IPs1[@]}") || { printf '%s' "IPs1 not in correct ip format: ${_STACK}" && bcu__stack "$@" && return 1; }

    mapfile -t S2 < <(bcu__ip_types_to_cidrs "${IPs2[@]}") || { printf '%s' "IPs2 not in correct ip format: ${_STACK}" && bcu__stack "$@" && return 1; }

    local res1=
    for s1 in "${S1[@]}"; do
	res1=
	for s2 in "${S2[@]}" ; do
	    # Since the php script comparison below don't give true
	    # when given the same individual ip we check that case
	    # here:
	    [[ "${s1%/*}" = "${s2%/*}" ]] && {
		local a="${s1#*/}"
		local b="${s2#*/}"
		if bcu__is_valid_ip_no_cidr "$a"; then a="${a}/32"; fi
		if bcu__is_valid_ip_no_cidr "$b"; then b="${a}/32"; fi
		[[ "${a#*/}" -ge "${b#*/}" ]] && res1=ISCONTAINED
	    }
	    if php "${_ip_MOD_DIR}/ipcalcs.php" -w "${s1}" "${s2}" >/dev/null; then
		res1=ISCONTAINED
	    fi
	done
	# if s1 wasn't contained in any of the S2 subnets, S1 can't be
	# a subset of S2.
	[[ -z "$res1" ]] && return 1
    done
    return 0
}
bcu__is_ip_subset_py(){
    # shellcheck disable=SC2034
    local z Options=() Input=() IPs1 IPs2 pystring
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Check if a list of ip addresses is a subset of another. Allowed
address formats for elements in the lists are IPv4: 1.2.3.4-5.6.7.8,
1.2.3.4, 1.2.3.4/32, 1.2.3.4/24.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    set1=('192.168.1.1-192.168.1.254' '10.0.0.1/31' '1.1.1.1/25' '1.2.3.4')
    set2=('192.168.1.1-192.168.1.254' '1.1.1.1/26' '1.2.3.4')
    bcu__is_ip_subset "(${set2[*]@Q})" "(${set1[*]@Q})" && echo true
  
Results:

    true
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:IPs1 1:t:IPs2)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    local -a S1="$IPs1" || { printf '%s' "IPs1 not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }

    local -a S2="$IPs2" || { printf '%s' "IPs2 not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }

    mapfile -t S1 < <(bcu__ip_types_to_cidrs "(${S1[*]@Q})") || { printf '%s' "IPs1 not valid ip elements: ${_STACK}" && bcu__stack "$@" && return 1; }

    mapfile -t S2 < <(bcu__ip_types_to_cidrs "(${S2[*]@Q})") || { printf '%s' "IPs1 not valid ip elements: ${_STACK}" && bcu__stack "$@" && return 1; }
    local a b Set1 Set2 result
    a=$(a=$(printf '%s, ' ${S1[*]@Q}); printf '%s' "${a%, }")
    b=$(b=$(printf '%s, ' ${S2[*]@Q}); printf '%s' "${b%, }")
    Set1=$(printf '([%s])' "$a")
    Set2=$(printf '([%s])' "$b")
    #declare -p Set1
    #declare -p Set2
    pystring=$(cat <<EOF
import netaddr
from netaddr import IPSet
s1 = IPSet$Set1
#s1 = IPSet(['192.0.2.0/24', '192.0.4.0/24'])
s2 = IPSet$Set2
#s2 = IPSet(['192.0.2.0', '192.0.4.0'])
res = s1.issubset(s2)
print(res)
EOF
	    )
    result=$(pypipe "$_BCU_PYDAEMON_CHANNEL" "$pystring")
    case "$result" in
	True) return 0;;
	False) return 1;;
	*) return 1;;
    esac
}
bcu__has_ip_intersection(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() IPs1 IPs2 json
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Check if a list of ip addresses intersects one-another. Allowed
address formats for elements in the lists are IPv4: 1.2.3.4-5.6.7.8,
1.2.3.4, 1.2.3.4/32, 1.2.3.4/24.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    apa=(1.1.1.1/28 1.1.1.10)
    bepa=(1.1.2.1/28 1.1.1.14)
    bcu__has_ip_intersection "(${apa[*]@Q})" "(${bepa[*]@Q})" && echo true
  
Results:

    true
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local json=(j json "" "Read input as json lists" 0)
    Options+=("(${null[*]@Q})" "(${json[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:IPs1 1:t:IPs2)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    if [[ -n "$json" ]]; then
	mapfile -t S1 < <(jq -r .[] <<< "${IPs1}")
	mapfile -t S2 < <(jq -r .[] <<< "${IPs2}")
    else
	local -a S1="${IPs1}"
	local -a S2="${IPs2}"; fi
    
    local res1 s1 s2
    for s1 in "${S1[@]}"; do
	res1=
	for s2 in "${S2[@]}" ; do
	    if php "${_ip_MOD_DIR}"/ipcalcs.php -i "${s1}" "${s2}" >/dev/null; then
		res1=ISINTERSECT
	    fi
	done
	# if s1 intersected any of the S2 subnets we return true
	[[ -n "$res1" ]] && return 0
    done
    return 1
}
bcu__has_ip_intersection_py(){
    # shellcheck disable=SC2034
    local z Options=() Input=() IPs1 IPs2 a b result Set1 Set2 pystring
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Check if a list of ip addresses intersects one-another. Allowed
address formats for elements in the lists are IPv4: 1.2.3.4-5.6.7.8,
1.2.3.4, 1.2.3.4/32, 1.2.3.4/24.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    apa=(1.1.1.1/28 1.1.1.10)
    bepa=(1.1.2.1/28 1.1.1.14)
    bcu__has_ip_intersection_py "(${apa[*]@Q})" "(${bepa[*]@Q})" && echo true
  
Results:

    true
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:IPs1 1:t:IPs2)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    local -a S1="${IPs1}" || { printf '%s' "IPs1 not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }

    local -a S2="${IPs2}" || { printf '%s' "IPs1 not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }

    mapfile -t S1 < <(bcu__ip_types_to_cidrs "(${S1[*]@Q})") || { printf '%s' "IPs1 has incorrectly formatted ip address elements: ${_STACK}" && bcu__stack "$@" && return 1; }
    mapfile -t S2 < <(bcu__ip_types_to_cidrs "(${S2[*]@Q})") || { printf '%s' "IPs1 has incorrectly formatted ip address elements: ${_STACK}" && bcu__stack "$@" && return 1; }

    a=$(a=$(printf '%s, ' ${S1[*]@Q}); printf '%s' "${a%, }")
    b=$(b=$(printf '%s, ' ${S2[*]@Q}); printf '%s' "${b%, }")
    Set1=$(printf '([%s])' "$a")
    Set2=$(printf '([%s])' "$b")
    #declare -p Set1
    #declare -p Set2
    local result
    pystring=$(cat <<EOF
import netaddr
from netaddr import IPSet
s1 = IPSet$Set1
#s1 = IPSet(['192.0.2.0/24', '192.0.4.0/24'])
s2 = IPSet$Set2
#s2 = IPSet(['192.0.2.0', '192.0.4.0'])
res = s1.intersection(s2)
print(res)
EOF
	    )
    result=$(pypipe "$_BCU_PYDAEMON_CHANNEL" "$pystring")
    #printf '%s\n' "$result"
    case "$result" in
	'IPSet([])') return 1;;
	*) return 0;;
    esac
}
bcu__ip_diff(){
    local _STACK z Options=() Input=() IPs1 IPs2
    [[ "$1" =~ (-h|--help) ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Calculate the difference between two ip sets.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__ip_diff '(1.1.2.22-1.1.2.55 1.1.1.0/24 1.1.1.139)' '(1.1.1.0/25 1.1.2.28)'
    declare -a a='(1.1.2.22-1.1.2.55 1.1.1.0/24 1.1.1.139)'
    declare -a b='(1.1.1.0/25 1.1.2.28)'
    bcu__ip_diff -n -- a b

Results:

    ('1.1.1.128/25' '1.1.2.22/31' '1.1.2.24/30' '1.1.2.29/32' '1.1.2.30/31' '1.1.2.32/28' '1.1.2.48/29')
    ('1.1.1.128/25' '1.1.2.22/31' '1.1.2.24/30' '1.1.2.29/32' '1.1.2.30/31' '1.1.2.32/28' '1.1.2.48/29')

EOF
		)
    }
    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Nameref=(n nameref "" "Read the IP arrays as namereferences" 0)
    Options+=("(${Nameref[*]@Q})" "(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:IPs1 1:t:IPs2)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || {
	printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ (-h|--help) ]] && return 0

    # Check args
    if [[ -n "$nameref" ]]; then
	#for arrname in "${IPs1}" "${IPs2}"; do
	#local -a 'Arr=("${'"$Array"'[@]}")'
	local -n S1="${IPs1}"
	local -n S2="${IPs2}"
    else
	local -a S1="${IPs1}" || { printf '%s' "IPs1 not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }
	local -a S2="${IPs2}" || { printf '%s' "IPs1 not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }; fi

    mapfile -t S1 < <(bcu__ip_types_to_cidrs "(${S1[*]@Q})") || { printf '%s' "IPs1 has incorrectly formatted ip address elements: ${_STACK}" && bcu__stack "$@" && return 1; }
    mapfile -t S2 < <(bcu__ip_types_to_cidrs "(${S2[*]@Q})") || { printf '%s' "IPs1 has incorrectly formatted ip address elements: ${_STACK}" && bcu__stack "$@" && return 1; }

    a=$(a=$(printf '%s, ' ${S1[*]@Q}); printf '%s' "${a%, }")
    b=$(b=$(printf '%s, ' ${S2[*]@Q}); printf '%s' "${b%, }")
    Set1=$(printf '([%s])' "$a")
    Set2=$(printf '([%s])' "$b")
    #declare -p Set1
    #declare -p Set2
    local result
    pystring=$(cat <<EOF
import netaddr
from netaddr import IPSet
s1 = IPSet$Set1
#s1 = IPSet(['192.0.2.0/24', '192.0.4.0/24'])
s2 = IPSet$Set2
#s2 = IPSet(['192.0.2.0', '192.0.4.0'])
res = s1 - s2
print(res)
EOF
	    )
    result=$(pypipe "$_BCU_PYDAEMON_CHANNEL" "$pystring")
    result="${result#IPSet}"
    printf '%s\n' "(${result//[,()\[\]]})"
    # case "$result" in
    # 	'IPSet([])') return 1;;
    # 	*) return 0;;
    # esac
}
ip_private(){
    :

}
ip_main(){
    :
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	ip_main "${org_args[@]}"
    else
	ip_main "${@}"
    fi
fi
# ip.sh ends here
