#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::has_ip_intersection-test-main][has_ip_intersection-test-main]]
org_args='()'
. bcu.sh
bcu__has_ip_intersection(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() IPs1 IPs2 json
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Check if a list of ip addresses intersects one-another. Allowed
address formats for elements in the lists are IPv4: 1.2.3.4-5.6.7.8,
1.2.3.4, 1.2.3.4/32, 1.2.3.4/24.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    apa=(1.1.1.1/28 1.1.1.10)
    bepa=(1.1.2.1/28 1.1.1.14)
    bcu__has_ip_intersection "(${apa[*]@Q})" "(${bepa[*]@Q})" && echo true
  
Results:

    true
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local json=(j json "" "Read input as json lists" 0)
    Options+=("(${null[*]@Q})" "(${json[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:IPs1 1:t:IPs2)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    if [[ -n "$json" ]]; then
	mapfile -t S1 < <(jq -r .[] <<< "${IPs1}")
	mapfile -t S2 < <(jq -r .[] <<< "${IPs2}")
    else
	local -a S1="${IPs1}"
	local -a S2="${IPs2}"; fi
    
    local res1 s1 s2
    for s1 in "${S1[@]}"; do
	res1=
	for s2 in "${S2[@]}" ; do
	    if php "${_ip_MOD_DIR}"/ipcalcs.php -i "${s1}" "${s2}" >/dev/null; then
		res1=ISINTERSECT
	    fi
	done
	# if s1 intersected any of the S2 subnets we return true
	[[ -n "$res1" ]] && return 0
    done
    return 1
}
has_ip_intersection_test_f(){
    local a
    local -a apa bepa
    apa=(1.1.1.1/28 1.1.1.10)
    bepa=(1.1.2.1/28 1.1.1.14)
    a=$(if bcu__has_ip_intersection "(${apa[*]@Q})" "(${bepa[*]@Q})"; then echo true; else echo false; fi)
    declare -p a
}
has_ip_intersection_test_1()(
    . <(has_ip_intersection_test_f)
    [[ "${a}" == true ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
has_ip_intersection_test_main(){
    printf has_ip_intersection_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )has_ip_intersection_test_1(\ |$) ]]; then has_ip_intersection_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	has_ip_intersection_test_main "${org_args[@]}"
    else
	has_ip_intersection_test_main "${@}"
    fi
fi
# has_ip_intersection-test-main ends here
