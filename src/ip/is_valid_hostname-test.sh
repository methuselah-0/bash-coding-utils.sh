#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::is_valid_hostname-test-main][is_valid_hostname-test-main]]
org_args='()'
. bcu.sh
bcu__is_valid_hostname(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() host_name
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Validates hostnames conforming to RFC1123, not RFC952, except no dots
allowed, thus not allowing valid hostnames that contains a domain-name
ending.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__is_valid_hostname gnu && echo isvalid
    bcu__is_valid_hostname gnu.org || echo isinvalid
  
Results:

    isvalid
    isinvalid
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)    
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:host_name)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: $_STACK" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    { [[ "$host_name" =~ ^[a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9]+$ ]] && [[ $(fold -w1 <<<"$host_name" | wc -l) -le 63 ]] ; } && return 0
}
is_valid_hostname_test_f(){
    local a
    a=$(if bcu__is_valid_hostname gnu >/dev/null ; then echo true ; else echo false ; fi)
    declare -p a
}
is_valid_hostname_test_1()(
    . <(is_valid_hostname_test_f)
    [[ "${a}" == true ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
is_valid_hostname_test_main(){
    printf is_valid_hostname_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )is_valid_hostname_test_1(\ |$) ]]; then is_valid_hostname_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	is_valid_hostname_test_main "${org_args[@]}"
    else
	is_valid_hostname_test_main "${@}"
    fi
fi
# is_valid_hostname-test-main ends here
