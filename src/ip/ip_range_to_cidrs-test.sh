#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::ip_range_to_cidrs-test-main][ip_range_to_cidrs-test-main]]
org_args='()'
. bcu.sh
bcu__ip_range_to_cidrs(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() range_start range_end
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Print the CIDR's of a given ip address range.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__ip_range_to_cidrs 10.0.0.0 10.0.0.1
  
Results:

    10.0.0.0/31
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:range_start 1:t:range_end)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    bcu__is_valid_ip "$range_start" || { printf '%s' "Start ip is not valid: ${_STACK}" && bcu__stack "$@" && return 1; }
    bcu__is_valid_ip "$range_end" || { printf '%s' "End ip is not valid: ${_STACK}" && bcu__stack "$@" && return 1; }

    printf '%s ' "${range_start}" "${range_end}" | gawk -f "${_ip_MOD_DIR}/ipcalcs.awk" -f <(echo "{print range2cidr(ip2dec(sanitize(\"${range_start}\")),ip2dec(sanitize(\"${range_end}\")))}")
}
ip_range_to_cidrs_test_f(){
    local a
    a=$(bcu__ip_range_to_cidrs 10.0.0.0 10.0.0.1)
    declare -p a
}
ip_range_to_cidrs_test_1()(
    . <(ip_range_to_cidrs_test_f)
    [[ "${a}" == "10.0.0.0/31" ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
ip_range_to_cidrs_test_main(){
    printf ip_range_to_cidrs_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )ip_range_to_cidrs_test_1(\ |$) ]]; then ip_range_to_cidrs_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	ip_range_to_cidrs_test_main "${org_args[@]}"
    else
	ip_range_to_cidrs_test_main "${@}"
    fi
fi
# ip_range_to_cidrs-test-main ends here
