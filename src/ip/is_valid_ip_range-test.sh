#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::is_valid_ip_range-test-main][is_valid_ip_range-test-main]]
org_args='()'
. bcu.sh
bcu__is_valid_ip_range(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() ip_address_range
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
"a.b.c.d-e.f.g.h" -> Bool. Does not return error in case the second ip
in the range is a previous ip or "earlier" ip.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__is_valid_ip_range 1.2.3.4-5.6.7.8 && echo valid
    bcu__is_valid_ip_range 1.2.3.4-5.6.7.8.9 || echo not valid
    bcu__is_valid_ip_range 1.2.3.4-1.2.3.3 && echo valid
  
Results:

    valid
    not valid
    valid
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)    
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:ip_address_range)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: $_STACK" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    if [[ "${ip_address_range}" =~ ^([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})-([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})$ ]]; then
	local ip1="${BASH_REMATCH[1]}"
	local ip2="${BASH_REMATCH[2]}"
	if bcu__is_valid_ip "${ip1}" && bcu__is_valid_ip "${ip2}"; then
	    return 0;
	else
	    return 1; fi
    else
	return 1; fi
}
is_valid_ip_range_test_f(){
    local a b
    a=$(if bcu__is_valid_ip_range 1.2.3.4-5.6.7.8; then echo true; else echo false; fi)
    b=$(if bcu__is_valid_ip_range 1.2.3.4-555.6.7.8; then echo true; else echo false; fi)
    declare -p a b
}
is_valid_ip_range_test_1()(
    . <(is_valid_ip_range_test_f)
    [[ "${a}" == true ]] || { echo FAIL && return 1; }
    [[ "${b}" == false ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
is_valid_ip_range_test_main(){
    printf is_valid_ip_range_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )is_valid_ip_range_test_1(\ |$) ]]; then is_valid_ip_range_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	is_valid_ip_range_test_main "${org_args[@]}"
    else
	is_valid_ip_range_test_main "${@}"
    fi
fi
# is_valid_ip_range-test-main ends here
