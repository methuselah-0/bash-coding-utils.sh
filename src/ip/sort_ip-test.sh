#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::sort_ip-test-main][sort_ip-test-main]]
org_args='()'
. bcu.sh
bcu__sort_ip(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() IPs result
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Sort IPv4 efficiently using ~sort~ and as a filter (from stdin).


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    printf '%s\n' 1.1.1.1 3.3.3.3 2.2.2.2 | bcu__sort_ip
    bcu__sort_ip 1.1.1.1 3.3.3.3 2.2.2

Results:

    1.1.1.1
    2.2.2.2
    3.3.3.3
    IP - 2.2.2 - is not a valid ip and can't be sorted: 'bcu__sort_ip' defined in ip.sh invoked on line 141 in /home/user1/VirtualHome/src/bash-coding-utils/src/ip/ip.sh, with the args: '1.1.1.1' '3.3.3.3' '2.2.2'

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:IPs)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    for ip in "${IPs[@]}"; do
	bcu__is_valid_ip "$ip" || { printf '%s' "IP - $ip - is not a valid ip and can't be sorted: ${_STACK}" && bcu__stack "$@" && return 1; }; done

    result=$(printf '%s\n' "${IPs[@]}" | sort -t. -k1,1n -k2,2n -k3,3n -k4,4n) || { printf '%s' "Failed to sort given IPs: $result - ${_STACK}" && bcu__stack "$@" && return 1; }
    printf '%s\n' "$result"
}
sort_ip_test_f(){
    local -a a
    mapfile -t a < <(bcu__sort_ip 1.1.1.1 3.3.3.3 2.2.2.2)
    declare -p a
}
sort_ip_test_1()(
    . <(sort_ip_test_f)
    [[ "${a[0]}" == 1.1.1.1 ]] || { echo FAIL && return 1; }
    [[ "${a[1]}" == 2.2.2.2 ]] || { echo FAIL && return 1; }
    [[ "${a[2]}" == 3.3.3.3 ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
sort_ip_test_main(){
    printf sort_ip_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )sort_ip_test_1(\ |$) ]]; then sort_ip_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	sort_ip_test_main "${org_args[@]}"
    else
	sort_ip_test_main "${@}"
    fi
fi
# sort_ip-test-main ends here
