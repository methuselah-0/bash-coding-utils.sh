#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::is_ip_subset-test-main][is_ip_subset-test-main]]
org_args='()'
. bcu.sh
bcu__is_ip_subset(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() IPs1 IPs2
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Check if a list of ip addresses is a subset of another. Allowed
address formats for elements in the lists are IPv4: 1.2.3.4-5.6.7.8,
1.2.3.4, 1.2.3.4/32, 1.2.3.4/24.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    set1=('192.168.1.1-192.168.1.254' '10.0.0.1/31' '1.1.1.1/25' '1.2.3.4')
    set2=('192.168.1.1-192.168.1.254' '1.1.1.1/26' '1.2.3.4')
    bcu__is_ip_subset "(${set2[*]@Q})" "(${set1[*]@Q})" && echo true
  
Results:

    true
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:IPs1 1:t:IPs2)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    # Check args
    local -a IPs1="$IPs1" || { printf '%s' "IPs1 not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }

    local -a IPs2="$IPs2" || { printf '%s' "IPs2 not an array-declarable string: ${_STACK}" && bcu__stack "$@" && return 1; }

    # TODO: use ip_types_to_cidrs here just in case
    local -a S1 S2
    mapfile -t S1 < <(bcu__ip_types_to_cidrs "${IPs1[@]}") || { printf '%s' "IPs1 not in correct ip format: ${_STACK}" && bcu__stack "$@" && return 1; }

    mapfile -t S2 < <(bcu__ip_types_to_cidrs "${IPs2[@]}") || { printf '%s' "IPs2 not in correct ip format: ${_STACK}" && bcu__stack "$@" && return 1; }

    local res1=
    for s1 in "${S1[@]}"; do
	res1=
	for s2 in "${S2[@]}" ; do
	    # Since the php script comparison below don't give true
	    # when given the same individual ip we check that case
	    # here:
	    [[ "${s1%/*}" = "${s2%/*}" ]] && {
		local a="${s1#*/}"
		local b="${s2#*/}"
		if bcu__is_valid_ip_no_cidr "$a"; then a="${a}/32"; fi
		if bcu__is_valid_ip_no_cidr "$b"; then b="${a}/32"; fi
		[[ "${a#*/}" -ge "${b#*/}" ]] && res1=ISCONTAINED
	    }
	    if php "${_ip_MOD_DIR}/ipcalcs.php" -w "${s1}" "${s2}" >/dev/null; then
		res1=ISCONTAINED
	    fi
	done
	# if s1 wasn't contained in any of the S2 subnets, S1 can't be
	# a subset of S2.
	[[ -z "$res1" ]] && return 1
    done
    return 0
}
is_ip_subset_test_f(){
    local a
    local set1=('192.168.1.1-192.168.1.254' '10.0.0.1/31' '1.1.1.1/25' '1.2.3.4')
    local set2=('192.168.1.1-192.168.1.254' '1.1.1.1/26' '1.2.3.4')
    a=$(if bcu__is_ip_subset "(${set2[*]@Q})" "(${set1[*]@Q})"; then echo true; else echo false; fi)
    declare -p a
}
is_ip_subset_test_1()(
    . <(is_ip_subset_test_f)
    [[ "${a}" == true ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
is_ip_subset_test_main(){
    printf is_ip_subset_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )is_ip_subset_test_1(\ |$) ]]; then is_ip_subset_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	is_ip_subset_test_main "${org_args[@]}"
    else
	is_ip_subset_test_main "${@}"
    fi
fi
# is_ip_subset-test-main ends here
