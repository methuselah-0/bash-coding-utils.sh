#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::hex_to_ip-test-main][hex_to_ip-test-main]]
org_args='()'
. bcu.sh
bcu__hex_to_ip(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() ip
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Convert IPv4 address in hex format to dot-decimal format.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__hex_to_ip 01010101 && echo
  
Results:

    1.1.1.1
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:ip)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    printf '%s' "$ip" | fold -w 2 | { mapfile -t IP ; printf "%d." "0x${IP[0]}" "0x${IP[1]}" "0x${IP[2]}" "0x${IP[3]}" | sed 's/\.$//' ; }
}
hex_to_ip_test_f(){
    local a
    a=$(bcu__hex_to_ip 01010101)
    declare -p a
}
hex_to_ip_test_1()(
    . <(hex_to_ip_test_f)
    [[ "${a}" == 1.1.1.1 ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
hex_to_ip_test_main(){
    printf hex_to_ip_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )hex_to_ip_test_1(\ |$) ]]; then hex_to_ip_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	hex_to_ip_test_main "${org_args[@]}"
    else
	hex_to_ip_test_main "${@}"
    fi
fi
# hex_to_ip-test-main ends here
