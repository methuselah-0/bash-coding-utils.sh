#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::is_valid_fqdn-test-main][is_valid_fqdn-test-main]]
org_args='()'
. bcu.sh
bcu__is_valid_fqdn(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE	
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Validate that a given input string is an fqdn.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__is_valid_fqdn apa.bepa.com && echo valid
    bcu__is_valid_fqdn apa || echo notvalid
    bcu__is_valid_fqdn -r apa && echo valid
  
Results:

    valid
    notvalid
    valid
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Relaxed=(r relaxed_tld "" "This option makes the tld part optional" 0 f f)
    Options+=("(${null[*]@Q})" "(${Relaxed[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: $_STACK" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    #host -N 1 "${Input[0]}" &>/dev/null

    local regex_with_tld='(?=^.{4,253}$)(^((?!-)[a-zA-Z0-9-]{1,63}(?<!-)\.)+[a-zA-Z]{2,63}$)'
    local regex_optional_tld='(?=^.{1,253}$)(^(((?!-)[a-zA-Z0-9-]{1,63}(?<!-))|((?!-)[a-zA-Z0-9-]{1,63}(?<!-)\.)+[a-zA-Z]{2,63})$)'
    local regex
    if [[ -n "$relaxed_tld" ]]; then
	local regex="$regex_optional_tld"
    else
	local regex="$regex_with_tld"; fi

    pcregrep "$regex" <<<"${string}" &>/dev/null && return 0
}
is_valid_fqdn_test_f(){
    local a
    a=$(if bcu__is_valid_fqdn apa.bepa.com; then echo true; else echo false; fi)
    b=$(if bcu__is_valid_fqdn 911.gov; then echo true; else echo false; fi)
    c=$(if bcu__is_valid_fqdn 911; then echo true; else echo false; fi)
    d=$(if bcu__is_valid_fqdn 'a-.com'; then echo true; else echo false; fi)
    e=$(if bcu__is_valid_fqdn '-a.com'; then echo true; else echo false; fi)
    f=$(if bcu__is_valid_fqdn a.com; then echo true; else echo false; fi)
    g=$(if bcu__is_valid_fqdn a.66; then echo true; else echo false; fi)
    h=$(if bcu__is_valid_fqdn my_host.com; then echo true; else echo false; fi)
    i=$(if bcu__is_valid_fqdn 'typical-hostname33.whatever.co.uk'; then echo true; else echo false; fi)
    j=$(if bcu__is_valid_fqdn -r -- 911; then echo true; else echo false; fi)
    declare -p a b c d e f g h i j
}
is_valid_fqdn_test_1()(
    . <(is_valid_fqdn_test_f)
    [[ "${a}" == true ]] || { echo FAIL && return 1; }
    [[ "${b}" == true ]] || { echo FAIL && return 1; }
    [[ "${c}" == false ]] || { echo FAIL && return 1; }
    [[ "${d}" == false ]] || { echo FAIL && return 1; }
    [[ "${e}" == false ]] || { echo FAIL && return 1; }
    [[ "${f}" == true ]] || { echo FAIL && return 1; }
    [[ "${g}" == false ]] || { echo FAIL && return 1; }
    [[ "${h}" == false ]] || { echo FAIL && return 1; }
    [[ "${i}" == true ]] || { echo FAIL && return 1; }
    [[ "${j}" == true ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
is_valid_fqdn_test_main(){
    printf is_valid_fqdn_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )is_valid_fqdn_test_1(\ |$) ]]; then is_valid_fqdn_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	is_valid_fqdn_test_main "${org_args[@]}"
    else
	is_valid_fqdn_test_main "${@}"
    fi
fi
# is_valid_fqdn-test-main ends here
