#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::is_valid_subnet-test-main][is_valid_subnet-test-main]]
org_args='()'
. bcu.sh
bcu__is_valid_subnet(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() address
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Validates CIDR-format of given input, ignoring whether the input has a
correct subnet base.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__is_valid_subnet 1.2.3.4/24 && echo valid
    bcu__is_valid_subnet 1.2.3.4/33 || echo not valid
    bcu__is_valid_subnet 1.2.3.4 || echo not valid
  
Results:

    valid
    not valid
    not valid
      
EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:address)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: $_STACK" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    if bcu__is_valid_ip "${address%/*}" && [[ "${address#*/}" -ge 0 ]] 2>/dev/null && [[ "${address#*/}" -le 31 ]] 2>/dev/null; then
	return 0
    else
	return 1; fi
}
is_valid_subnet_test_f(){
    local a b
    a=$(if bcu__is_valid_subnet 1.2.3.4/24; then echo true; else echo false; fi)
    b=$(if bcu__is_valid_subnet 1.2.3.4/33; then echo true; else echo false; fi)
    declare -p a b
}
is_valid_subnet_test_1()(
    . <(is_valid_subnet_test_f)
    [[ "${a}" == true ]] || { echo FAIL && return 1; }
    [[ "${b}" == false ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
is_valid_subnet_test_main(){
    printf is_valid_subnet_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )is_valid_subnet_test_1(\ |$) ]]; then is_valid_subnet_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	is_valid_subnet_test_main "${org_args[@]}"
    else
	is_valid_subnet_test_main "${@}"
    fi
fi
# is_valid_subnet-test-main ends here
