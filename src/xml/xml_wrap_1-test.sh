#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::xml_wrap_1-test-main][xml_wrap_1-test-main]]
org_args='()'
. bcu.sh
bcu__xml_wrap_1(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() build_arg
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Build xml-data with the wrap-1 configured header variables


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset _NS1 _NS2 _SOAP_PREENVELOPE1 _SOAP_PREENVELOPE2 _SOAP_POSTENVELOPE1 _SOAP_POSTENVELOPE2 _BODY_HEADER
    _NS1="${_NS1:-apa}"
    _SOAP_PREENVELOPE1=$( if [[ -n "${_SOAP_PREENVELOPE1}" ]]; then printf '%s' "${_SOAP_PREENVELOPE1}"; else printf '%s' "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:${_NS1}='https://www.some-product.com/something1'><soapenv:Header/><soapenv:Body>"; fi; )
    _SOAP_POSTENVELOPE1="${_SOAP_POSTENVELOPE1:-</soapenv:Body></soapenv:Envelope>}"
    
    _NS2="${_NS2:-bepa}"
    _SOAP_PREENVELOPE2=$( if [[ -n "${_SOAP_PREENVELOPE2}" ]]; then printf '%s' "${_SOAP_PREENVELOPE2}"; else printf '%s' "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:${_NS2}='https://www.some-product.com/something2'><soapenv:Header/><soapenv:Body>" ; fi; )
    _SOAP_POSTENVELOPE2="${_SOAP_POSTENVELOPE2:-</soapenv:Body></soapenv:Envelope>}"
    _BODY_HEADER="${_BODY_HEADER:-<BEPAHeader><version>1</version></BEPAHeader>}"
    bcu__xml_wrap_1 -open=log lala=lolo -close=log | xmllint --format -

Results:

    <?xml version="1.0"?>
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:apa="https://www.some-product.com/something1">
      <soapenv:Header/>
      <soapenv:Body>
        <apa:log>
          <apa:lala>lolo</apa:lala>
        </apa:log>
      </soapenv:Body>
    </soapenv:Envelope>

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:build_arg)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    local body
    if body="$(bcu__xml_build "${_NS1}" "${build_arg[@]}")"; then
	printf '%s' "${_SOAP_PREENVELOPE1}""$body""${_SOAP_POSTENVELOPE1}"
    else
	printf '%s' "Failed to build the xml data: ${_STACK}" && bcu__stack "$@" && return 1; fi
}
xml_wrap_1_test_f(){
    local a
    unset _NS1 _NS2 _SOAP_PREENVELOPE1 _SOAP_PREENVELOPE2 _SOAP_POSTENVELOPE1 _SOAP_POSTENVELOPE2 _BODY_HEADER
    _NS1="${_NS1:-apa}"
    _SOAP_PREENVELOPE1=$( if [[ -n "${_SOAP_PREENVELOPE1}" ]]; then printf '%s' "${_SOAP_PREENVELOPE1}"; else printf '%s' "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:${_NS1}='https://www.some-product.com/something1'><soapenv:Header/><soapenv:Body>"; fi; )
    _SOAP_POSTENVELOPE1="${_SOAP_POSTENVELOPE1:-</soapenv:Body></soapenv:Envelope>}"
    
    _NS2="${_NS2:-bepa}"
    _SOAP_PREENVELOPE2=$( if [[ -n "${_SOAP_PREENVELOPE2}" ]]; then printf '%s' "${_SOAP_PREENVELOPE2}"; else printf '%s' "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:${_NS2}='https://www.some-product.com/something2'><soapenv:Header/><soapenv:Body>" ; fi; )
    _SOAP_POSTENVELOPE2="${_SOAP_POSTENVELOPE2:-</soapenv:Body></soapenv:Envelope>}"
    _BODY_HEADER="${_BODY_HEADER:-<BEPAHeader><version>1</version></BEPAHeader>}"
    a=$(bcu__xml_wrap_1 -open=log lala=lolo -close=log)
    declare -p a
}
xml_wrap_1_test_1()(
    . <(xml_wrap_1_test_f)
    local b
    b="<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:apa='https://www.some-product.com/something1'><soapenv:Header/><soapenv:Body><apa:log><apa:lala>lolo</apa:lala></apa:log></soapenv:Body></soapenv:Envelope>"
    [[ "${a}" == "${b}" ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
xml_wrap_1_test_main(){
    printf xml_wrap_1_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )xml_wrap_1_test_1(\ |$) ]]; then xml_wrap_1_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	xml_wrap_1_test_main "${org_args[@]}"
    else
	xml_wrap_1_test_main "${@}"
    fi
fi
# xml_wrap_1-test-main ends here
