#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::xml_encode-test-main][xml_encode-test-main]]
org_args='()'
. bcu.sh
bcu__xml_encode(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Escape data to be placed between xml tags. xml_encode does not escape CDATA.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__xml_encode '<>"&apa' && echo

Results:

    &lt;&gt;&quot;&amp;apa

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    local y=()
    local -i i
    for (( i=0 ; i < ${#string} ; i++ ))
    do local c=${string:$i:1}
       case $c in
	   '&') y[$i]='&amp;' ;;
	   '<') y[$i]='&lt;' ;;
	   '>') y[$i]='&gt;' ;;
	   "'") y[$i]='&apos;' ;;
	   '"') y[$i]='&quot;' ;;
	   *) y[$i]="$c" # %02X is 2 character hex format specifier, i.e. %HH format.
       esac
    done ; printf '%s' "${y[@]}"
}
xml_encode_test_f(){
    local a
    a=$(bcu__xml_encode '<>"&apa')
    declare -p a
}
xml_encode_test_1()(
    . <(xml_encode_test_f)
    [[ "${a}" == '&lt;&gt;&quot;&amp;apa' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
xml_encode_test_main(){
    printf xml_encode_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )xml_encode_test_1(\ |$) ]]; then xml_encode_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	xml_encode_test_main "${org_args[@]}"
    else
	xml_encode_test_main "${@}"
    fi
fi
# xml_encode-test-main ends here
