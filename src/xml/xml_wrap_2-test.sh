#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::xml_wrap_2-test-main][xml_wrap_2-test-main]]
org_args='()'
. bcu.sh
bcu__xml_wrap_2(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() build_arg
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Build xml-data with the wrap-2 configured header variables


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset _NS1 _NS2 _SOAP_PREENVELOPE1 _SOAP_PREENVELOPE2 _SOAP_POSTENVELOPE1 _SOAP_POSTENVELOPE2 _BODY_HEADER
    _NS1="${_NS1:-apa}"
    _SOAP_PREENVELOPE1=$( if [[ -n "${_SOAP_PREENVELOPE1}" ]]; then printf '%s' "${_SOAP_PREENVELOPE1}"; else printf '%s' "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:${_NS1}='https://www.some-product.com/something1'><soapenv:Header/><soapenv:Body>"; fi; )
    _SOAP_POSTENVELOPE1="${_SOAP_POSTENVELOPE1:-</soapenv:Body></soapenv:Envelope>}"
    
    _NS2="${_NS2:-bepa}"
    _SOAP_PREENVELOPE2=$( if [[ -n "${_SOAP_PREENVELOPE2}" ]]; then printf '%s' "${_SOAP_PREENVELOPE2}"; else printf '%s' "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:${_NS2}='https://www.some-product.com/something2'><soapenv:Header/><soapenv:Body>" ; fi; )
    _SOAP_POSTENVELOPE2="${_SOAP_POSTENVELOPE2:-</soapenv:Body></soapenv:Envelope>}"
    _BODY_HEADER="${_BODY_HEADER:-<BEPAHeader><version>1</version></BEPAHeader>}"
    bcu__xml_wrap_2 -open=login user=apa -close=login | xmllint --format -

Results:

    <?xml version="1.0"?>
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bepa="https://www.some-product.com/something2">
      <soapenv:Header/>
      <soapenv:Body>
        <bepa:login>
          <BEPAHeader>
            <version>1</version>
          </BEPAHeader>
          <bepa:user>apa</bepa:user>
        </bepa:login>
      </soapenv:Body>
    </soapenv:Envelope>
    

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:build_arg)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    # body=$(multiWrap "${_NS2}" "$@")
    # bodyWithBodyHeader="${body}${_BODY_HEADER}"
    # out=$(printf '%s' "${_SOAP_PREENVELOPE2}${bodyWithBodyHeader}${_SOAP_POSTENVELOPE2}")
    # printf '%s' "${out}"

    local body
    if body=$(bcu__xml_build "${_NS2}" "${build_arg[@]}"); then
	local preout="${_SOAP_PREENVELOPE2}${body}${_SOAP_POSTENVELOPE2}"
	local sub
	if sub=$(pcregrep -o '<.*?>' <<< "$preout" | tail -n +4 | head -n 1); then
	    local bh
	    if bh=$(bcu__sed_esc_rep "${_BODY_HEADER}"); then
		local out
		if out=$(sed "s/$sub/${sub}${bh}/g" <<< "$preout"); then
		    printf '%s' "${out}"
		else
		    printf '%s' "Failed attaching the body header: ${_STACK}" && bcu__stack "$@" && return 1; fi
	    else
		printf '%s' "Failed escaping the body header for sed: ${_STACK}" && bcu__stack "$@" && return 1; fi
	else
	    printf '%s' "Failed grepping tags from built xml-data: ${_STACK}" && bcu__stack "$@" && return 1; fi
    else
	printf '%s' "Failed building xml-data: ${_STACK}" && bcu__stack "$@" && return 1; fi
}
xml_wrap_2_test_f(){
    local a
    unset _NS1 _NS2 _SOAP_PREENVELOPE1 _SOAP_PREENVELOPE2 _SOAP_POSTENVELOPE1 _SOAP_POSTENVELOPE2 _BODY_HEADER
    _NS1="${_NS1:-apa}"
    _SOAP_PREENVELOPE1=$( if [[ -n "${_SOAP_PREENVELOPE1}" ]]; then printf '%s' "${_SOAP_PREENVELOPE1}"; else printf '%s' "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:${_NS1}='https://www.some-product.com/something1'><soapenv:Header/><soapenv:Body>"; fi; )
    _SOAP_POSTENVELOPE1="${_SOAP_POSTENVELOPE1:-</soapenv:Body></soapenv:Envelope>}"
    
    _NS2="${_NS2:-bepa}"
    _SOAP_PREENVELOPE2=$( if [[ -n "${_SOAP_PREENVELOPE2}" ]]; then printf '%s' "${_SOAP_PREENVELOPE2}"; else printf '%s' "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:${_NS2}='https://www.some-product.com/something2'><soapenv:Header/><soapenv:Body>" ; fi; )
    _SOAP_POSTENVELOPE2="${_SOAP_POSTENVELOPE2:-</soapenv:Body></soapenv:Envelope>}"
    _BODY_HEADER="${_BODY_HEADER:-<BEPAHeader><version>1</version></BEPAHeader>}"
      a=$(bcu__xml_wrap_2 -open=login user=apa -close=login | xmllint --format -)
      declare -p a
  }
xml_wrap_2_test_1()(
    . <(xml_wrap_2_test_f)
    local b
    b=$(
	cat <<EOF
<?xml version="1.0"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bepa="https://www.some-product.com/something2">
  <soapenv:Header/>
  <soapenv:Body>
    <bepa:login>
      <BEPAHeader>
        <version>1</version>
      </BEPAHeader>
      <bepa:user>apa</bepa:user>
    </bepa:login>
  </soapenv:Body>
</soapenv:Envelope>
EOF
     )
    [[ "${a}" == "$b" ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
xml_wrap_2_test_main(){
    printf xml_wrap_2_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )xml_wrap_2_test_1(\ |$) ]]; then xml_wrap_2_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	xml_wrap_2_test_main "${org_args[@]}"
    else
	xml_wrap_2_test_main "${@}"
    fi
fi
# xml_wrap_2-test-main ends here
