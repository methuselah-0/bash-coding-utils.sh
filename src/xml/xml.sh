#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::xml.sh][xml.sh]]
[[ "$_BCU_SH_LOADED" == YES ]] || . bcu.sh
_xml_MOD_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")
unset _NS1 _NS2 _SOAP_PREENVELOPE1 _SOAP_PREENVELOPE2 _SOAP_POSTENVELOPE1 _SOAP_POSTENVELOPE2 _BODY_HEADER
_NS1="${_NS1:-apa}"
_SOAP_PREENVELOPE1=$( if [[ -n "${_SOAP_PREENVELOPE1}" ]]; then printf '%s' "${_SOAP_PREENVELOPE1}"; else printf '%s' "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:${_NS1}='https://www.some-product.com/something1'><soapenv:Header/><soapenv:Body>"; fi; )
_SOAP_POSTENVELOPE1="${_SOAP_POSTENVELOPE1:-</soapenv:Body></soapenv:Envelope>}"

_NS2="${_NS2:-bepa}"
_SOAP_PREENVELOPE2=$( if [[ -n "${_SOAP_PREENVELOPE2}" ]]; then printf '%s' "${_SOAP_PREENVELOPE2}"; else printf '%s' "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:${_NS2}='https://www.some-product.com/something2'><soapenv:Header/><soapenv:Body>" ; fi; )
_SOAP_POSTENVELOPE2="${_SOAP_POSTENVELOPE2:-</soapenv:Body></soapenv:Envelope>}"
_BODY_HEADER="${_BODY_HEADER:-<BEPAHeader><version>1</version></BEPAHeader>}"
[[ -r ~/.config/bash-coding-utils/xml.config ]] && source ~/.config/bash-coding-utils/xml.config
# it needs to set the following variables
export _SOAP_PREENVELOPE1
export _SOAP_POSTENVELOPE1
export _NS1
export _SOAP_PREENVELOPE2
export _SOAP_POSTENVELOPE2
export _NS2
export _BODY_HEADER

_CURL_OPTIONS=("-k" "-sS")
_CTYPE='Content-Type: text/xml;charset=UTF-8'
_NS_RESPONSE="ns1"
export _CURL_OPTIONS
export _CTYPE
export _NS_RESPONSE
bcu__xml_encode(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() string
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Escape data to be placed between xml tags. xml_encode does not escape CDATA.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__xml_encode '<>"&apa' && echo

Results:

    &lt;&gt;&quot;&amp;apa

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:string)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    local y=()
    local -i i
    for (( i=0 ; i < ${#string} ; i++ ))
    do local c=${string:$i:1}
       case $c in
	   '&') y[$i]='&amp;' ;;
	   '<') y[$i]='&lt;' ;;
	   '>') y[$i]='&gt;' ;;
	   "'") y[$i]='&apos;' ;;
	   '"') y[$i]='&quot;' ;;
	   *) y[$i]="$c" # %02X is 2 character hex format specifier, i.e. %HH format.
       esac
    done ; printf '%s' "${y[@]}"
}
bcu__xml_build(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() build_arg namespace
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
xml_build can be used to build N-level hierarchical xml-tag
structures. It does not support attributes or CDATA. See the example.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__xml_build NS -open=country 'name=Sweden With A <tag>' king=Gustav -open=neighbours name=Norway name=Denmark name=Finland -close=neighbours -close=country | xmllint --format - 2>/dev/null

Results:

    <?xml version="1.0"?>
    <NS:country>
      <NS:name>Sweden With A &lt;tag&gt;</NS:name>
      <NS:king>Gustav</NS:king>
      <NS:neighbours>
        <NS:name>Norway</NS:name>
        <NS:name>Denmark</NS:name>
        <NS:name>Finland</NS:name>
      </NS:neighbours>
    </NS:country>
    

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:namespace 0:t:build_arg)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    xml_private
    _NS="${namespace}"
    local arg
    for arg in "${build_arg[@]}"; do
	if [[ "$arg" =~ ^-open= ]]; then
	    bcu__open "${arg#*=}"
	elif [[ "$arg" =~ ^-close= ]]; then
	    bcu__close "${arg#*=}"
	else
	    bcu__wrap "${arg}" ; fi; done
}
bcu__xml_wrap_1(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() build_arg
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Build xml-data with the wrap-1 configured header variables


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset _NS1 _NS2 _SOAP_PREENVELOPE1 _SOAP_PREENVELOPE2 _SOAP_POSTENVELOPE1 _SOAP_POSTENVELOPE2 _BODY_HEADER
    _NS1="${_NS1:-apa}"
    _SOAP_PREENVELOPE1=$( if [[ -n "${_SOAP_PREENVELOPE1}" ]]; then printf '%s' "${_SOAP_PREENVELOPE1}"; else printf '%s' "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:${_NS1}='https://www.some-product.com/something1'><soapenv:Header/><soapenv:Body>"; fi; )
    _SOAP_POSTENVELOPE1="${_SOAP_POSTENVELOPE1:-</soapenv:Body></soapenv:Envelope>}"
    
    _NS2="${_NS2:-bepa}"
    _SOAP_PREENVELOPE2=$( if [[ -n "${_SOAP_PREENVELOPE2}" ]]; then printf '%s' "${_SOAP_PREENVELOPE2}"; else printf '%s' "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:${_NS2}='https://www.some-product.com/something2'><soapenv:Header/><soapenv:Body>" ; fi; )
    _SOAP_POSTENVELOPE2="${_SOAP_POSTENVELOPE2:-</soapenv:Body></soapenv:Envelope>}"
    _BODY_HEADER="${_BODY_HEADER:-<BEPAHeader><version>1</version></BEPAHeader>}"
    bcu__xml_wrap_1 -open=log lala=lolo -close=log | xmllint --format -

Results:

    <?xml version="1.0"?>
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:apa="https://www.some-product.com/something1">
      <soapenv:Header/>
      <soapenv:Body>
        <apa:log>
          <apa:lala>lolo</apa:lala>
        </apa:log>
      </soapenv:Body>
    </soapenv:Envelope>

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:build_arg)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    local body
    if body="$(bcu__xml_build "${_NS1}" "${build_arg[@]}")"; then
	printf '%s' "${_SOAP_PREENVELOPE1}""$body""${_SOAP_POSTENVELOPE1}"
    else
	printf '%s' "Failed to build the xml data: ${_STACK}" && bcu__stack "$@" && return 1; fi
}
bcu__xml_wrap_2(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() build_arg
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Build xml-data with the wrap-2 configured header variables


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    unset _NS1 _NS2 _SOAP_PREENVELOPE1 _SOAP_PREENVELOPE2 _SOAP_POSTENVELOPE1 _SOAP_POSTENVELOPE2 _BODY_HEADER
    _NS1="${_NS1:-apa}"
    _SOAP_PREENVELOPE1=$( if [[ -n "${_SOAP_PREENVELOPE1}" ]]; then printf '%s' "${_SOAP_PREENVELOPE1}"; else printf '%s' "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:${_NS1}='https://www.some-product.com/something1'><soapenv:Header/><soapenv:Body>"; fi; )
    _SOAP_POSTENVELOPE1="${_SOAP_POSTENVELOPE1:-</soapenv:Body></soapenv:Envelope>}"
    
    _NS2="${_NS2:-bepa}"
    _SOAP_PREENVELOPE2=$( if [[ -n "${_SOAP_PREENVELOPE2}" ]]; then printf '%s' "${_SOAP_PREENVELOPE2}"; else printf '%s' "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:${_NS2}='https://www.some-product.com/something2'><soapenv:Header/><soapenv:Body>" ; fi; )
    _SOAP_POSTENVELOPE2="${_SOAP_POSTENVELOPE2:-</soapenv:Body></soapenv:Envelope>}"
    _BODY_HEADER="${_BODY_HEADER:-<BEPAHeader><version>1</version></BEPAHeader>}"
    bcu__xml_wrap_2 -open=login user=apa -close=login | xmllint --format -

Results:

    <?xml version="1.0"?>
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bepa="https://www.some-product.com/something2">
      <soapenv:Header/>
      <soapenv:Body>
        <bepa:login>
          <BEPAHeader>
            <version>1</version>
          </BEPAHeader>
          <bepa:user>apa</bepa:user>
        </bepa:login>
      </soapenv:Body>
    </soapenv:Envelope>
    

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:build_arg)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    # body=$(multiWrap "${_NS2}" "$@")
    # bodyWithBodyHeader="${body}${_BODY_HEADER}"
    # out=$(printf '%s' "${_SOAP_PREENVELOPE2}${bodyWithBodyHeader}${_SOAP_POSTENVELOPE2}")
    # printf '%s' "${out}"

    local body
    if body=$(bcu__xml_build "${_NS2}" "${build_arg[@]}"); then
	local preout="${_SOAP_PREENVELOPE2}${body}${_SOAP_POSTENVELOPE2}"
	local sub
	if sub=$(pcregrep -o '<.*?>' <<< "$preout" | tail -n +4 | head -n 1); then
	    local bh
	    if bh=$(bcu__sed_esc_rep "${_BODY_HEADER}"); then
		local out
		if out=$(sed "s/$sub/${sub}${bh}/g" <<< "$preout"); then
		    printf '%s' "${out}"
		else
		    printf '%s' "Failed attaching the body header: ${_STACK}" && bcu__stack "$@" && return 1; fi
	    else
		printf '%s' "Failed escaping the body header for sed: ${_STACK}" && bcu__stack "$@" && return 1; fi
	else
	    printf '%s' "Failed grepping tags from built xml-data: ${_STACK}" && bcu__stack "$@" && return 1; fi
    else
	printf '%s' "Failed building xml-data: ${_STACK}" && bcu__stack "$@" && return 1; fi
}
bcu__xml_grep(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() xml_data text content item
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Grep for xml-tags recursively and return matches with or without the
tags.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__xml_build ns -open=country name=Sweden king=Gustav -open=neighbours name=Norway name=Denmark name=Finland -close=neighbours -close=country | xmllint --format - 2>/dev/null > /tmp/tempfile.txt
    < /tmp/tempfile.txt bcu__xml_grep -c "ns:name" --
    < /tmp/tempfile.txt bcu__xml_grep --text -c "ns:name" --

Results:

    <ns:name>Sweden</ns:name>
    <ns:name>Norway</ns:name>
    <ns:name>Denmark</ns:name>
    <ns:name>Finland</ns:name>
    Sweden
    Norway
    Denmark
    Finland

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Text=(t text "" "Whether to just print the text between the tags or with the tags included" 0 f f)
    local Content=(c content "" "The exact tag name of the tag elements to return (greps recursively)" 1 t f)
    Options+=("(${null[*]@Q})" "(${Text[*]@Q})" "(${Content[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:xml_data)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    # if not using grep -o -P .*? (for non-greedy matching), then this was the old solution.
    # xmlgrep(){ # TESTED=OK (for multiple occurences it works too)
    #     if [[ -n "$1" ]] && [[ -n "$2" ]] && [[ -r "$2" ]]; then
    # 	local arg="$1"
    # 	local data=$(cat "$2")
    #     elif
    # 	[[ -n "$1" ]] && [[ -z "$2" ]]; then
    # 	local arg="$1"
    # 	while read -r stdin; do
    # 	    local data+="$stdin"; done
    #     else
    # 	return 1; fi

    #     if [[ "$arg" =~ ^[a-zA-Z0-9:]+$ ]] ; then 
    # 	local result=$(grep -o -P "<$arg>.*</$arg>" <<<"$data")
    # 	declare -a Items
    # 	declare -il n=0
    # 	while [[ "$result" =~ \<$arg\>(.*)\<$arg\> ]]; do
    # 	    n+=1
    # 	    local dev="${BASH_REMATCH[1]}"
    # 	    local dev2="${dev%%</$arg>*}"
    # 	    local result="${result//<$arg>$dev2<\/$arg>/}"
    # 	    Items+=("$dev2"); done
    # 	if [[ $n -eq 0 ]]; then
    # 	    printf '%s' $(grep -o -P "(?<=(<$arg>)).*(?=(</$arg>))" <<<"$result")
    # 	    return 0; fi
    # 	for item in "${Items[@]}"; do
    # 	    printf '%s\n' "$item"; done
    # 	return 0
    #     else
    # 	return 1; fi
    # }

    # if [[ -n "$1" ]] && [[ -n "$2" ]] && [[ -r "$2" ]]; then
    # 	local arg="$1"
    # 	local data; data=$(cat "$2")
    # elif
    # 	[[ -n "$1" ]] && [[ -z "$2" ]]; then
    # 	local arg="$1"
    # 	while read -r stdin; do
    # 	    local data+="$stdin"; done
    # else
    # 	return 1; fi

    local -a Items
    if [[ "$content" =~ ^[a-zA-Z0-9:]+$ ]] ; then
	while read -r item; do
	    Items+=("$item"); done < <(pcregrep -o "(?<=(<$content)).*?(?=(</$content>))" <<<"${xml_data[@]}" | pcregrep -o ">.*" | sed 's/^>//g')
    else
	return 1; fi

    if [[ ${#Items[@]} -gt 0 ]]; then
	# shellcheck disable=SC2154
	if [[ -n "$text" ]]; then
	    for item in "${Items[@]}"; do
		printf '%s\n' "$item"; done
	else
	    for item in "${Items[@]}"; do
		printf '%s\n' "<$content>$item</$content>"; done
	    return 0; fi
    else
	return 1; fi
}
bcu__xml_xpath(){
    local _STACK z Options=() Input=() xmldoc s selection
    [[ "$1" =~ (-h|--help) ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Query an xml-document using xpath.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    xml="<?xml version=\"1.0\" encoding=\"UTF-8\"?><breakfast_menu><food><name>Belgian Waffles</name><price>\$5.95</price><description>Two of our famous Belgian Waffles with plenty of real maple syrup</description><calories>650</calories></food><food><name>Strawberry Belgian Waffles</name><price>\$7.95</price><description>Light Belgian waffles covered with strawberries and whipped cream</description><calories>900</calories></food></breakfast_menu>"
    echo "$xml" | bcu__xml_xpath -z -s '//breakfast_menu/food/name[re:test(.,"Straw.*")]'

Results:

    <name>Strawberry Belgian Waffles</name>

EOF
		)
    }
    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Selection=(s selection "" "XPath query" 1)
    Options+=("(${Selection[*]@Q})" "(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:xmldoc)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || {
	printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ (-h|--help) ]] && return 0

    # Check args
    #declare -p xmldoc >&2
    #declare -p s >&2
    #declare -p selection >&2
    python3 <(printf '%b' "from lxml.html import tostring\nfrom lxml import etree\nfrom sys import stdin\nregexpNS = \"http://exslt.org/regular-expressions\"\ntree = etree.parse(stdin)\nfor e in tree.xpath('""$selection""', namespaces={'re':regexpNS}):\n  if isinstance(e, str):\n    print(e)\n  else:\n    print(tostring(e).decode('UTF-8'))") <<<"$xmldoc"
    #printf '%s\n' "$xmldoc" | python3 <(printf '%b' "from lxml.html import tostring\nfrom lxml import etree\nfrom sys import stdin\nregexpNS = \"http://exslt.org/regular-expressions\"\ntree = etree.parse(stdin)\nfor e in tree.xpath('""$selection""', namespaces={'re':regexpNS}):\n  print(tostring(e).decode('UTF-8'))")
    #python3 <(printf '%b' "from lxml.html import tostring\nfrom lxml import etree\nfrom sys import stdin\nregexpNS = \"http://exslt.org/regular-expressions\"\ntree = etree.parse(stdin)\nfor e in tree.xpath('$s', namespaces={'re':regexpNS}):\n  print(tostring(e).decode('UTF-8'))") <<<"${xmldoc}"
}
xml_private(){
    :
bcu__wrap(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() tag_equal_text
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Wrap a given string within xml tags. Wrap reads the _NS variable as
namespace prefix for the given tag.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    _NS=x
    bcu__wrap apa=bepa && echo

Results:

    <x:apa>bepa</x:apa>

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:tag_equal_text)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    local tag="${tag_equal_text%%=*}"
    local text="${tag_equal_text##*=}"
    [[ -n "$tag" ]] || { printf '%s' "No tag in input: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ -n "$text" ]] || { printf '%s' "No text in input: ${_STACK}" && bcu__stack "$@" && return 1; }
    
    #local content="${1##*=}"

    local content
    if content="$(bcu__xml_encode "$text")"; then
	printf '%s' "<${_NS:+${_NS}:}${tag}>$content</${_NS:+${_NS}:}${tag}>"
    else
	printf '%s' "Failed to xml_encode text - $text. Result was - $content - : ${_STACK}" && bcu__stack "$@" && return 1; fi
}
bcu__open(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() tag
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Open a tag. Open reads the _NS variable as namespace prefix for the
given tag.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    _NS=x
    bcu__open apa && echo

Results:

    <x:apa>

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:tag)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    #local name; name="${Input[0]%%=*}"
    #printf '%s' "<${_NS}:${name}>"
    printf '%s' "<${_NS}:${tag}>"
}
bcu__close(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() tag
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Close a tag. Close reads the _NS variable as namespace prefix for the
given tag.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    _NS=x
    bcu__close apa && echo

Results:

    </x:apa>

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:tag)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    #local name="${Input[0]%%=*}"
    #printf '%s' "</${_NS}:${name}>"
    printf '%s' "</${_NS}:${tag}>"
}
}
xml_main(){
    :
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	xml_main "${org_args[@]}"
    else
	xml_main "${@}"
    fi
fi
# xml.sh ends here
