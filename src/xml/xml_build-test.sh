#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::xml_build-test-main][xml_build-test-main]]
org_args='()'
. bcu.sh
bcu__xml_build(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() build_arg namespace
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
xml_build can be used to build N-level hierarchical xml-tag
structures. It does not support attributes or CDATA. See the example.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__xml_build NS -open=country 'name=Sweden With A <tag>' king=Gustav -open=neighbours name=Norway name=Denmark name=Finland -close=neighbours -close=country | xmllint --format - 2>/dev/null

Results:

    <?xml version="1.0"?>
    <NS:country>
      <NS:name>Sweden With A &lt;tag&gt;</NS:name>
      <NS:king>Gustav</NS:king>
      <NS:neighbours>
        <NS:name>Norway</NS:name>
        <NS:name>Denmark</NS:name>
        <NS:name>Finland</NS:name>
      </NS:neighbours>
    </NS:country>
    

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:namespace 0:t:build_arg)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    xml_private
    _NS="${namespace}"
    local arg
    for arg in "${build_arg[@]}"; do
	if [[ "$arg" =~ ^-open= ]]; then
	    bcu__open "${arg#*=}"
	elif [[ "$arg" =~ ^-close= ]]; then
	    bcu__close "${arg#*=}"
	else
	    bcu__wrap "${arg}" ; fi; done
}
xml_build_test_f(){
    local a
    a=$(bcu__xml_build NS -open=country name=Sweden king=Gustav -open=neighbours name=Norway name=Denmark name=Finland -close=neighbours -close=country | xmllint --format - 2>/dev/null)
    declare -p a
}
xml_build_test_1()(
    . <(xml_build_test_f)
    b=$(cat <<EOF
<?xml version="1.0"?>
<NS:country>
  <NS:name>Sweden</NS:name>
  <NS:king>Gustav</NS:king>
  <NS:neighbours>
    <NS:name>Norway</NS:name>
    <NS:name>Denmark</NS:name>
    <NS:name>Finland</NS:name>
  </NS:neighbours>
</NS:country>
EOF
     )
    [[ "${a}" == "${b}" ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
xml_build_test_main(){
    printf xml_build_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )xml_build_test_1(\ |$) ]]; then xml_build_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	xml_build_test_main "${org_args[@]}"
    else
	xml_build_test_main "${@}"
    fi
fi
# xml_build-test-main ends here
