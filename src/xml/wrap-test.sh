#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::wrap-test-main][wrap-test-main]]
org_args='()'
. bcu.sh
bcu__wrap(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() tag_equal_text
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Wrap a given string within xml tags. Wrap reads the _NS variable as
namespace prefix for the given tag.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    _NS=x
    bcu__wrap apa=bepa && echo

Results:

    <x:apa>bepa</x:apa>

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:tag_equal_text)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    local tag="${tag_equal_text%%=*}"
    local text="${tag_equal_text##*=}"
    [[ -n "$tag" ]] || { printf '%s' "No tag in input: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ -n "$text" ]] || { printf '%s' "No text in input: ${_STACK}" && bcu__stack "$@" && return 1; }
    
    #local content="${1##*=}"

    local content
    if content="$(bcu__xml_encode "$text")"; then
	printf '%s' "<${_NS:+${_NS}:}${tag}>$content</${_NS:+${_NS}:}${tag}>"
    else
	printf '%s' "Failed to xml_encode text - $text. Result was - $content - : ${_STACK}" && bcu__stack "$@" && return 1; fi
}
wrap_test_f(){
    local a
    a=$(_NS=x && bcu__wrap 'apa=bepa<<')
    declare -p a
}
wrap_test_1()(
    . <(wrap_test_f)
    [[ "${a}" == '<x:apa>bepa&lt;&lt;</x:apa>' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
wrap_test_main(){
    printf wrap_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )wrap_test_1(\ |$) ]]; then wrap_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	wrap_test_main "${org_args[@]}"
    else
	wrap_test_main "${@}"
    fi
fi
# wrap-test-main ends here

# [[file:~/src/bash-coding-utils/bcu.org::open-test-main][open-test-main]]
org_args='()'
. bcu.sh
bcu__open(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() tag
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Open a tag. Open reads the _NS variable as namespace prefix for the
given tag.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    _NS=x
    bcu__open apa && echo

Results:

    <x:apa>

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:tag)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    #local name; name="${Input[0]%%=*}"
    #printf '%s' "<${_NS}:${name}>"
    printf '%s' "<${_NS}:${tag}>"
}
open_test_f(){
    local a
    a=$(_NS=x bcu__open 'apa')
    declare -p a
}
open_test_1()(
    . <(open_test_f)
    [[ "${a}" == '<x:apa>' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
open_test_main(){
    printf open_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )open_test_1(\ |$) ]]; then open_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	open_test_main "${org_args[@]}"
    else
	open_test_main "${@}"
    fi
fi
# open-test-main ends here

# [[file:~/src/bash-coding-utils/bcu.org::close-test-main][close-test-main]]
org_args='()'
. bcu.sh
bcu__close(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() tag
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Close a tag. Close reads the _NS variable as namespace prefix for the
given tag.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    _NS=x
    bcu__close apa && echo

Results:

    </x:apa>

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    Options+=("(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:tag)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0
    
    #local name="${Input[0]%%=*}"
    #printf '%s' "</${_NS}:${name}>"
    printf '%s' "</${_NS}:${tag}>"
}
close_test_f(){
    local a
    a=$(_NS=x bcu__close 'apa')
    declare -p a
}
close_test_1()(
    . <(close_test_f)
    [[ "${a}" == '</x:apa>' ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
close_test_main(){
    printf close_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )close_test_1(\ |$) ]]; then close_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	close_test_main "${org_args[@]}"
    else
	close_test_main "${@}"
    fi
fi
# close-test-main ends here
