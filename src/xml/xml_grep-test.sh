#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::xml_grep-test-main][xml_grep-test-main]]
org_args='()'
. bcu.sh
bcu__xml_grep(){
    # shellcheck disable=SC2034
    local _STACK z Options=() Input=() xml_data text content item
    [[ "$1" =~ ^(-h|--help)$ ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Grep for xml-tags recursively and return matches with or without the
tags.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    bcu__xml_build ns -open=country name=Sweden king=Gustav -open=neighbours name=Norway name=Denmark name=Finland -close=neighbours -close=country | xmllint --format - 2>/dev/null > /tmp/tempfile.txt
    < /tmp/tempfile.txt bcu__xml_grep -c "ns:name" --
    < /tmp/tempfile.txt bcu__xml_grep --text -c "ns:name" --

Results:

    <ns:name>Sweden</ns:name>
    <ns:name>Norway</ns:name>
    <ns:name>Denmark</ns:name>
    <ns:name>Finland</ns:name>
    Sweden
    Norway
    Denmark
    Finland

EOF
		)
    }

    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Text=(t text "" "Whether to just print the text between the tags or with the tags included" 0 f f)
    local Content=(c content "" "The exact tag name of the tag elements to return (greps recursively)" 1 t f)
    Options+=("(${null[*]@Q})" "(${Text[*]@Q})" "(${Content[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(0:t:xml_data)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || { printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ ^(-h|--help)$ ]] && return 0

    # if not using grep -o -P .*? (for non-greedy matching), then this was the old solution.
    # xmlgrep(){ # TESTED=OK (for multiple occurences it works too)
    #     if [[ -n "$1" ]] && [[ -n "$2" ]] && [[ -r "$2" ]]; then
    # 	local arg="$1"
    # 	local data=$(cat "$2")
    #     elif
    # 	[[ -n "$1" ]] && [[ -z "$2" ]]; then
    # 	local arg="$1"
    # 	while read -r stdin; do
    # 	    local data+="$stdin"; done
    #     else
    # 	return 1; fi

    #     if [[ "$arg" =~ ^[a-zA-Z0-9:]+$ ]] ; then 
    # 	local result=$(grep -o -P "<$arg>.*</$arg>" <<<"$data")
    # 	declare -a Items
    # 	declare -il n=0
    # 	while [[ "$result" =~ \<$arg\>(.*)\<$arg\> ]]; do
    # 	    n+=1
    # 	    local dev="${BASH_REMATCH[1]}"
    # 	    local dev2="${dev%%</$arg>*}"
    # 	    local result="${result//<$arg>$dev2<\/$arg>/}"
    # 	    Items+=("$dev2"); done
    # 	if [[ $n -eq 0 ]]; then
    # 	    printf '%s' $(grep -o -P "(?<=(<$arg>)).*(?=(</$arg>))" <<<"$result")
    # 	    return 0; fi
    # 	for item in "${Items[@]}"; do
    # 	    printf '%s\n' "$item"; done
    # 	return 0
    #     else
    # 	return 1; fi
    # }

    # if [[ -n "$1" ]] && [[ -n "$2" ]] && [[ -r "$2" ]]; then
    # 	local arg="$1"
    # 	local data; data=$(cat "$2")
    # elif
    # 	[[ -n "$1" ]] && [[ -z "$2" ]]; then
    # 	local arg="$1"
    # 	while read -r stdin; do
    # 	    local data+="$stdin"; done
    # else
    # 	return 1; fi

    local -a Items
    if [[ "$content" =~ ^[a-zA-Z0-9:]+$ ]] ; then
	while read -r item; do
	    Items+=("$item"); done < <(pcregrep -o "(?<=(<$content)).*?(?=(</$content>))" <<<"${xml_data[@]}" | pcregrep -o ">.*" | sed 's/^>//g')
    else
	return 1; fi

    if [[ ${#Items[@]} -gt 0 ]]; then
	# shellcheck disable=SC2154
	if [[ -n "$text" ]]; then
	    for item in "${Items[@]}"; do
		printf '%s\n' "$item"; done
	else
	    for item in "${Items[@]}"; do
		printf '%s\n' "<$content>$item</$content>"; done
	    return 0; fi
    else
	return 1; fi
}
xml_grep_test_f(){
    local a
    a=$(bcu__xml_grep -c "ns:name" < <(bcu__xml_build ns -open=country name=Sweden king=Gustav -open=neighbours name=Norway name=Denmark name=Finland -close=neighbours -close=country | xmllint --format - 2>/dev/null))
    b=$(bcu__xml_grep --text -c "ns:name" < <(bcu__xml_build ns -open=country name=Sweden king=Gustav -open=neighbours name=Norway name=Denmark name=Finland -close=neighbours -close=country | xmllint --format - 2>/dev/null))
    declare -p a b
}
xml_grep_test_1()(
    . <(xml_grep_test_f)
    local c d
    c=$(
	cat <<EOF
<ns:name>Sweden</ns:name>
<ns:name>Norway</ns:name>
<ns:name>Denmark</ns:name>
<ns:name>Finland</ns:name>
EOF
     )
    d=$(
	cat <<EOF
Sweden
Norway
Denmark
Finland
EOF
     )
    [[ "${a}" == "$c" ]] || { echo FAIL && return 1; }
    [[ "${b}" == "$d" ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
xml_grep_test_main(){
    printf xml_grep_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )xml_grep_test_1(\ |$) ]]; then xml_grep_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	xml_grep_test_main "${org_args[@]}"
    else
	xml_grep_test_main "${@}"
    fi
fi
# xml_grep-test-main ends here
