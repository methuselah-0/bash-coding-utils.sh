#!/usr/bin/env bash
# [[file:~/src/bash-coding-utils/bcu.org::xml_xpath-test-main][xml_xpath-test-main]]
org_args='()'
. bcu.sh
bcu__xml_xpath(){
    local _STACK z Options=() Input=() xmldoc s selection
    [[ "$1" =~ (-h|--help) ]] && {
	local _DESCRIPTION _EXAMPLE
	# shellcheck disable=SC2034
	_DESCRIPTION=$(cat <<'EOF'
Query an xml-document using xpath.


EOF
		    )
	# shellcheck disable=SC2034
	_EXAMPLE=$(
cat <<'EOF'
Example:

    xml="<?xml version=\"1.0\" encoding=\"UTF-8\"?><breakfast_menu><food><name>Belgian Waffles</name><price>\$5.95</price><description>Two of our famous Belgian Waffles with plenty of real maple syrup</description><calories>650</calories></food><food><name>Strawberry Belgian Waffles</name><price>\$7.95</price><description>Light Belgian waffles covered with strawberries and whipped cream</description><calories>900</calories></food></breakfast_menu>"
    echo "$xml" | bcu__xml_xpath -z -s '//breakfast_menu/food/name[re:test(.,"Straw.*")]'

Results:

    <name>Strawberry Belgian Waffles</name>

EOF
		)
    }
    # Options
    local null=(z null "" "Read null-separated operands from stdin" 0)
    local Selection=(s selection "" "XPath query" 1)
    Options+=("(${Selection[*]@Q})" "(${null[*]@Q})")

    # Operands
    # shellcheck disable=SC2034
    local -a Operands=(1:t:xmldoc)

    # Parse args
    # shellcheck disable=SC2034
    [[ ! -t 0 ]] && mapfile -t -d '' Input
    bcu__setopts "$@" || {
	printf '%s' "Parsing input failed: ${_STACK}" && bcu__stack "$@" && return 1; }
    [[ "$1" =~ (-h|--help) ]] && return 0

    # Check args
    #declare -p xmldoc >&2
    #declare -p s >&2
    #declare -p selection >&2
    python3 <(printf '%b' "from lxml.html import tostring\nfrom lxml import etree\nfrom sys import stdin\nregexpNS = \"http://exslt.org/regular-expressions\"\ntree = etree.parse(stdin)\nfor e in tree.xpath('""$selection""', namespaces={'re':regexpNS}):\n  if isinstance(e, str):\n    print(e)\n  else:\n    print(tostring(e).decode('UTF-8'))") <<<"$xmldoc"
    #printf '%s\n' "$xmldoc" | python3 <(printf '%b' "from lxml.html import tostring\nfrom lxml import etree\nfrom sys import stdin\nregexpNS = \"http://exslt.org/regular-expressions\"\ntree = etree.parse(stdin)\nfor e in tree.xpath('""$selection""', namespaces={'re':regexpNS}):\n  print(tostring(e).decode('UTF-8'))")
    #python3 <(printf '%b' "from lxml.html import tostring\nfrom lxml import etree\nfrom sys import stdin\nregexpNS = \"http://exslt.org/regular-expressions\"\ntree = etree.parse(stdin)\nfor e in tree.xpath('$s', namespaces={'re':regexpNS}):\n  print(tostring(e).decode('UTF-8'))") <<<"${xmldoc}"
}
xml_xpath_test_f(){
    local a
    a=$(xml="<?xml version=\"1.0\" encoding=\"UTF-8\"?><breakfast_menu><food><name>Belgian Waffles</name><price>\$5.95</price><description>Two of our famous Belgian Waffles with plenty of real maple syrup</description><calories>650</calories></food><food><name>Strawberry Belgian Waffles</name><price>\$7.95</price><description>Light Belgian waffles covered with strawberries and whipped cream</description><calories>900</calories></food></breakfast_menu>"; echo "$xml" | bcu__xml_xpath -z -s '//breakfast_menu/food/name[re:test(.,"Straw.*")]/text()')
    declare -p a
}
xml_xpath_test_1()(
    . <(xml_xpath_test_f)
    [[ "${a}" == "Strawberry Belgian Waffles" ]] || { echo FAIL && return 1; }
    echo PASS && return 0
)
xml_xpath_test_main(){
    printf xml_xpath_test_1.. ; if [[ ! "${_BCU_TESTS_DISABLED[*]}" =~ (^|\ )xml_xpath_test_1(\ |$) ]]; then xml_xpath_test_1; else echo DISABLED; fi
}
declare -a org_args="${org_args}"
if [ "${1:-${org_args[0]}}" != "--source-only" ]; then
    # $@ evaluates to all of the arguments passed to the function or script as individual strings.
    if [[ -n "$org_args" ]]; then
	xml_xpath_test_main "${org_args[@]}"
    else
	xml_xpath_test_main "${@}"
    fi
fi
# xml_xpath-test-main ends here
