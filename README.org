#+HTML_HEAD: <style>.outline-text-2, .outline-text-3, .outline-text-4, .outline-text-5, .outline-text-6, .outline-text-7, .outline-text-8, .outline-text-9, .outline-text-10, .outline-3 > ul, .outline-4 > ol, #text-footnotes { margin-left: 100px; }</style>
#+HTML_HEAD: <style> li{max-width:800px;}</style>
#+HTML_HEAD: <style> pre.src{overflow-x: auto ;}</style>
#+HTML_HEAD: <script> var HS_STARTUP_FOLDED = true; </script>
#+HTML_HEAD: <style> .p-img{max-width:768px; text-align: left; margin-left:0px; display:block;}</style>
#+HTML_HEAD: <style> p{max-width:1600px;}</style>
#+HTML_HEAD: <style> #postamble{text-align:center ;}</style>
#+HTML_HEAD: <style> img{display:inline-block ; max-width:100% ; height:auto ;}</style>
#+HTML_HEAD: <style> table[class="right-table"]{margin-right:0px;}</style>
#+HTML_HEAD: <style> table[class="left-table"]{margin-left:0px;}</style>
#+HTML_HEAD: <style> img{display:inline-block ;}</style>;
#+OPTIONS: H:15 num:15 toc:15
#+OPTIONS: ^:{}
#+SETUPFILE: org-html-themes/setup/theme-bigblow-local.setup
#+HTML_HEAD: <style> p{max-width:800px;}</style>
#+HTML_HEAD: <style> #content{max-width:1800px;}</style>
#+OPTIONS: H:5 num:5 toc:5
#+OPTIONS: ^:nil
#+SETUPFILE: org-html-themes/setup/theme-readtheorg-local.setup

* Introduction

BCU (bash-coding-utils) or bcu.sh, is a bash library that aims to
assist with prototyping programming projects in Bash. The focus is on
extending the lifespan of the Bash implementation and also providing a
smooth transition to a guile implementation. This is done by including
packages such as guile-bash and guile-daemon when you install the BCU
Guix package. Since guile don't have as large of a selection of
libraries as for example python, the pydaemon utility is also provided
which works similarly to guile-daemon where you can pipe code strings
to a running code evaluation session.

For full documentation, git clone this repo, ~. ./bcu.sh~ and run
~bcu__docs~.

*Hihglights*

All functions have a ~-h~ and ~--help~ option that outputs example and
usage info.

All or almost all functions work as a filter, i.e. you may provide the
function operands via either stdin or via command-line args.

BCU can be installed via a guix package.

 - Bash Parallel
   - /mapfork/ does parallel processing of commands with capturing of
     /both/ stdout and stderr, as well as the exit codes to a bash
     array or as a json list)
   - /bashp/ or (bcu__parallel) supersedes mapfork, and let's you run N
     number of functions /continuously/ in parallel using a semaphore
     instead of N at a time in batches.
 - XML module
   - xmlgrep and a fork of xmllint with an option to print XPath
     search results with either null or newline as output-delimiter.
   - xml_build (conveniently build nested xml tag structures)
   - xml_wrap_1 and xml_wrap_2 to build xml SOAP calls.
   - csv2xml
 - JSON module
   - json document building functions (jq wrappers)
   - json diff (super fast diffing of 2 json lists based on one or
     multiple json keys and return a list of json objects in one input
     file but not the other)
 - IP set calculations module
   - is_ip_subset (check if a list of ip's or CIDR's are contained in
     another list of ip's and CIDR's)
   - has_ip_intersection (same but whether there's an intersection)
   - ip_types_to_cidrs (convert a list of hosts and ranges to their CIDR
     represantion)
 - arrays module
   - arr_diff: return a subset or intersection
   - head, tail etc. for arrays.
 - Guix package, containing lots of common bash-related programs
   - just run guix package -f guix.scm to install it.

BCU is:
  - Testable
    - According to the principles of TDD at least 1 test is included
      for each function.
  - A literate program
    - I.e. documented according to the principles of literate
      programming - LP. E.g. a function's comment(s) or documentation
      is linked to a string variable through ~<<noweb>>~-references
      and is printed when the ~--help~ option is used.
  - Reproducible & verifiable
    - It's packaged using Guix, and can be verified to install and
      pass its test-suite for a specified Guix commit and specified
      bash-coding-utils git commit.

#+begin_tip
bash-wrappers for guile-functions that are shared to Bash via
guile-bash, can be written with setopts and the recommended
yasnippets, which makes theam easier to use and work with.
#+end_tip

#+begin_tip
Using guile-daemon is very simple:
  - guile-daemon &
  - gdpipe '(define (apa) (display "bepa\n"))'
  - gdpipe '(apa)' && echo hej
#+end_tip

* Why, why not?

*Why bcu.sh?*

  - Bash development speed is quite fast.
  - It's convenient for smaller API integration projects.
  - Bash and jq programs are often much shorter than their counterpart
    implementations in larger general purpose programming languages.

*Some reasons why you might not like bcu.sh are:*

  - It's not portable
    - Unless you package it with ~guix pack -m
      <mymanifest_with_bash-bcu.scm>~, and unpack on a destination
      server. On that server, run ~export
      PATH=/path/to/unpacked:$PATH~ and ~bash~ and you have BCU's full
      dependencies deployed in a reproducible and portable
      fashion. Recent versions of Windows has WSL on which you can
      deploy it in the same way.
  - It's slow.
    - The Bash stuff is slow. But ~xmllint~ and ~jq~ are not slow, and
      any part of your program that may require some more intense
      computation can be done either in jq, guile or some other
      language. The trade-off is in development speed where Bash is
      faster for many small to medium size projects, versus run-speed
      where other languages are often faster. Obviously, not all
      projects are suited for BCU.
  - No data-structures in Bash.
    - Just arrays and associative arrays. Though bash 4.4 and onwards
      do have support for nested indexed arrays with fast creation
      speed and associative arrays with fast lookup speed. BCU uses
      Bash 4.4 or later. If you are mainly using json or xml, you may
      write your code in such a way that your datastructures are Bash
      variables holding json or xml strings.

* License stuff

This is a Bash library that uses GPLv3 components, LGPL components
(stackoverflow contributions), and I think some other licensed stuff
too (TODO to cover it here). As a result, the whole library is
GPLv3.

#+begin_quote
...in many cases you can distribute the GPL-covered software
alongside your proprietary system. To do this validly, you must make
sure that the free and nonfree programs communicate at arms length,
that they are not combined in a way that would make them effectively a
single program.
#+end_quote
--[[https://www.gnu.org/licenses/gpl-faq.html#GPLInProprietarySystem][from GPL F.A.Q.]]

[[https://ask.slashdot.org/story/01/01/22/2340214/using-gplbsd-code-in-closed-source-projects][GPL and closed source projects discussion]]

